#include "stdafx.h"
#include "WinDinkeditDoc.h"
#include "LeftView.h"
#include "Globals.h"
#include "Map.h"
#include "Interface.h"
#include "SpriteLibrary.h"
#include "SpriteLibraryEntry.h"
#include "WinDinkeditView.h"
#include "MainFrm.h"
#include "Actions/SpriteCreate.h"


IMPLEMENT_DYNCREATE(CLeftView, CTreeView)

BEGIN_MESSAGE_MAP(CLeftView, CTreeView)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	ON_COMMAND(IDM_REFRESH_LIST, OnRefreshList)
	ON_COMMAND(IDM_DELETE_SPRITE_ENTRY, OnDeleteSpriteEntry)
	ON_COMMAND(IDM_RENAME_SPRITE_ENTRY, OnRenameSpriteEntry)
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_RCLICK, &CLeftView::OnNMRClick)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


CLeftView::CLeftView()
{
}

CLeftView::~CLeftView()
{
}

BOOL CLeftView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	return CTreeView::PreCreateWindow(cs);
}

void CLeftView::OnDraw(CDC* pDC)
{
	CWinDinkeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

void CLeftView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();

	// TODO: You may populate your TreeView with items by directly accessing its tree control through a call to GetTreeCtrl().
}


#ifdef _DEBUG
void CLeftView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CLeftView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

CWinDinkeditDoc* CLeftView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CWinDinkeditDoc)));
	return (CWinDinkeditDoc*)m_pDocument;
}
#endif //_DEBUG


void CLeftView::UpdateVisionTree()
{
	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM vision_num;

	// first empty the subtree out
	while((vision_num = tree.GetChildItem(visions_root)) != nullptr)
	{
		tree.DeleteItem(vision_num);
	}

	TCHAR buffer[5];

	// always add 0
	wsprintf(buffer, _T("%d"), 0);
	tree.InsertItem(buffer, visions_root);

	// add back the rest
	for (int i = 1; i < VISIONS_TRACKED; i++)
	{
		if (vision_used[i] != 0)
		{
			wsprintf(buffer, _T("%d"), i);
			tree.InsertItem(buffer, visions_root);
		}
	}
}

void CLeftView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	keyPressed((BYTE)nChar);
}

void CLeftView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	keyReleased((BYTE)nChar);
}

void CLeftView::UpdateSpriteLibraryTree(SpriteLibrary& sprite_library)
{
	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM sprite_node;

	// first empty the subtree out
	while((sprite_node = tree.GetChildItem(sprite_library_root)) != nullptr)
	{
		tree.DeleteItem(sprite_node);
	}

	for (auto& p : sprite_library)
		tree.InsertItem(p.first.c_str(), sprite_library_root);

	tree.SortChildren(sprite_library_root);
}

#define MAX_C_FILENAME_LENGTH 20

void CLeftView::UpdateScriptTree(CString const& dmod_path)
{
	HANDLE hFind;
	WIN32_FIND_DATA dataFind;
	BOOL bMoreFiles = TRUE;

	CTreeCtrl& tree = GetTreeCtrl();

	TCHAR full_path[250];
	wsprintf(full_path, _T("%sstory\\*.c"), (LPCTSTR)dmod_path);

	TCHAR filename[MAX_C_FILENAME_LENGTH];

	hFind = FindFirstFile(full_path, &dataFind);

	// get all the *.c files in the directory
	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)
	{
		int length = _tcslen(dataFind.cFileName) - 2;

		//TODO: simply clipping seems wonky; also, we should check the ANSI length  @unicode
		if (length >= MAX_C_FILENAME_LENGTH)
			length = MAX_C_FILENAME_LENGTH - 1;

		_tcsncpy_s(filename, dataFind.cFileName, length);
		filename[length] = 0;

		// make lower
		for (int i = 1; i < MAX_C_FILENAME_LENGTH - 1; i++)
		{
			filename[i] = (char) tolower(filename[i]);
		}

		tree.InsertItem(filename, script_root);

		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	FindClose(hFind);

	//RW: now find the .d files. We want to show their content to the user in a read-only text box.
	wsprintf(full_path, _T("%sstory\\*.d"), (LPCTSTR)dmod_path);
	hFind = FindFirstFile(full_path, &dataFind);
	bMoreFiles = TRUE;
	TCHAR dfilename[MAX_C_FILENAME_LENGTH+2];

	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)	//get all the *.d files in the directory	
	{
		int length = _tcslen(dataFind.cFileName) - 2;

		if (length >= MAX_C_FILENAME_LENGTH)
			length = MAX_C_FILENAME_LENGTH - 1;

		_tcsncpy_s(dfilename, dataFind.cFileName, length);
		dfilename[length] = 0;

		// make lower
		for (int i = 1; i < MAX_C_FILENAME_LENGTH - 1; i++)
			dfilename[i] = _totlower(dfilename[i]);
		_tcscat_s(dfilename, _T(".d"));
		tree.InsertItem(dfilename, script_root);

		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	FindClose(hFind);
}

int CLeftView::PopulateTree(Map* map)
{
	CTreeCtrl& tree = GetTreeCtrl();

	tree.DeleteAllItems();

	visions_root = tree.InsertItem(_T("Visions In Use"));
	script_root = tree.InsertItem(_T("Scripts"));
	sprite_library_root = tree.InsertItem(_T("Sprite Library"));

	SetWindowLong(tree, GWL_STYLE, WS_VISIBLE | WS_CHILD | TVS_HASLINES | TVS_SINGLEEXPAND);
	// TVS_HASBUTTONS | TVS_LINESATROOT  //use these to display the little plus/minus sign on the left

	UpdateScriptTree(map->dmod_path);
	UpdateSpriteLibraryTree(map->sprite_library);
	UpdateVisionTree();

	tree.SelectItem(visions_root);
	tree.SortChildren(script_root);

	return true;
}

void CLeftView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if (!current_map)
		return;

	// need to get selected item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM filename = tree.GetSelectedItem();

	if (filename == nullptr)
		return;

	TCHAR buffer[250];
	
	_tcscpy_s(buffer, (LPCTSTR) tree.GetItemText(filename));

	if (script_root == tree.GetParentItem(filename))
	{
		editScript(buffer);		
	}
	else if (visions_root == tree.GetParentItem(filename))
	{
		current_map->setVision(_ttoi(buffer));
	}
	else if (sprite_library_root == tree.GetParentItem(filename))
	{
		if (current_map->sprite_library.doesExist(buffer))
		{
			auto new_mouse_sprite = current_map->sprite_library.getSprite(buffer);

			current_map->undo_buffer->PushAndDo<actions::SpriteCreate>(current_map->getMouseSprite(), new_mouse_sprite);
		}
	}
	
	CTreeView::OnLButtonDblClk(nFlags, point);
}

void CLeftView::OnRefreshList() 
{
	PopulateTree(current_map);
}

void CLeftView::OnDeleteSpriteEntry() 
{
	// need to get selected item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM filename = tree.GetSelectedItem();

	if (filename == nullptr)
		return;
	
	current_map->sprite_library.removeSprite((LPCTSTR) tree.GetItemText(filename));
}

void CLeftView::OnRenameSpriteEntry() 
{
	// need to get selected item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM filename = tree.GetSelectedItem();

	if (filename == nullptr)
		return;

	TCHAR old_name[250];
	
	_tcscpy_s(old_name, (LPCTSTR) tree.GetItemText(filename));


	// create the dialog box
	SpriteLibraryEntry newdlg(this);

	newdlg.m_sprite_name = old_name;

	// set the title of the dialog box
	TCHAR title_buffer[300];
	wsprintf(title_buffer, _T("Rename sprite \'%s\'"), old_name);
	newdlg.DialogTitle = title_buffer;
	
	// now show the dialog box
	mainWnd->GetRightPane()->gainFocus();
	int retcode = newdlg.DoModal();

	// check if ok or cancel was pressed
	if (retcode == IDOK)
	{
		TCHAR new_name[MAX_SPRITE_NAME_LENGTH];
		_tcscpy_s(new_name, (LPCTSTR) newdlg.m_sprite_name);

		current_map->sprite_library.renameSprite(old_name, new_name);
	}
}


void CLeftView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (current_map == nullptr)
		mainWnd->SetActiveView((CView*) mainWnd->GetRightPane());

	CTreeView::OnLButtonDown(nFlags, point);
}


void CLeftView::OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	SendMessage(WM_CONTEXTMENU, (WPARAM) m_hWnd, GetMessagePos());
	
	*pResult = 1; //Mark message as handled and suppress default handling
}


void CLeftView::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	if (!current_map)
		return;

	ScreenToClient(&point);
	
	// need to get selected item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM treeItem = tree.HitTest(point); //tree.GetSelectedItem();

	if (treeItem == nullptr)
		return;

	tree.Select(treeItem, TVGN_CARET);
	
	//TODO: wth is this unused variable? was it for something?
	TCHAR buffer[250];
	_tcscpy_s(buffer, (LPCTSTR) tree.GetItemText(treeItem));

	ClientToScreen(&point);
	if (sprite_library_root == tree.GetParentItem(treeItem))
	{
		CMenu menuPopup;
		menuPopup.LoadMenu(IDR_SPRITE_ENTRY);
		menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, point.x, point.y, this);
	}
	else
	{
		CMenu menuPopup;
		menuPopup.LoadMenu(IDR_SIDEBAR);
		menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, point.x, point.y, this);
	}
}
