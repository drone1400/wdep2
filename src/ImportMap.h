#pragma once
#include "Const.h"

void removeImportMap();

class ImportMap
{
public:
	ImportMap();
	~ImportMap();
	int loadMap(CString pathname);
	int importScreen(CString pathname, int cur_screen, int new_screen);
	bool render();
	void tileClicked();

	int screen_order[NUM_SCREENS];
	int midi_num[NUM_SCREENS];
	int indoor[NUM_SCREENS];

	bool load_failed;
	int source_tile;
private:
	int dest_tile;
	CString src_dmod_path;
};