#include "stdafx.h"
#include "ScreenProperties.h"
#include "Common.h"
#include "Interface.h"
#include "Map.h"
#include "PathUtil.h"


ScreenProperties::ScreenProperties(CWnd* pParent /*=nullptr*/) : CDialog(ScreenProperties::IDD, pParent)
{
	m_inside = FALSE;
	m_midi = 0;
	m_script = _T("");
}

void ScreenProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_Inside, m_inside);
	DDX_Text(pDX, IDC_Midi, m_midi);
	DDX_Text(pDX, IDC_Script, m_script);
	DDV_MaxChars(pDX, m_script, 20);
}

BEGIN_MESSAGE_MAP(ScreenProperties, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
END_MESSAGE_MAP()


void ScreenProperties::OnButton1() 
{
	UpdateData(TRUE);

	if (m_script.GetLength() > 0)
	{
		CString scriptfile = current_map->dmod_path;
		scriptfile += "story\\";
		scriptfile += m_script;
		scriptfile += ".c";

		if (!FileReadable((LPCTSTR) scriptfile)) //RW: if the .c file doesn't exist
		{
			scriptfile.SetAt(scriptfile.GetLength()-1, 'd');
			if (!FileReadable((LPCTSTR) scriptfile)) //RW: if the .d file doesn't exist
			{
				scriptfile.SetAt(scriptfile.GetLength()-1, 'c');
				FILE* f;
				if ((f=_tfopen((LPCTSTR) scriptfile, _T("wt"))) == nullptr)
					MB_ShowError(_T("Could not open or create the script file!"));
				else
				{
					fclose(f);
					editScript(m_script);
				}
			}
			else
				editScript(m_script + ".d");
		}
		else
			editScript(m_script);
	}
}

void ScreenProperties::OnButton2() 
{
	CFileDialog dialog(true, _T(""), nullptr, 0, _T("Midi Files (*.mid)|*.mid|All Files (*.*)|*.*||"));
	CString dir = current_map->dmod_path;
	dir += "sound\\";
	dialog.m_ofn.lpstrInitialDir = (LPCTSTR) dir;

	if (dialog.DoModal() == IDOK)
	{
		m_midi = _ttoi(dialog.GetFileName());
		UpdateData(FALSE);
	}
}