#pragma once
#include "resource.h"

class ViewCompressedScript : public CDialog
{
	DECLARE_DYNAMIC(ViewCompressedScript)

public:
	ViewCompressedScript(const TCHAR* filepath, CWnd* pParent = nullptr);   // standard constructor
	virtual ~ViewCompressedScript();

// Dialog Data
	enum { IDD = IDD_VIEWCOMPRESSEDSCRIPT };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_text;	
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	BOOL OnInitDialog() override;

private:
	CString m_filepath;
	CFont m_textfont;
};