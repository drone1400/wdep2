#include "StdAfx.h"
#include "Colors.h"

#define COLOR_TILE_GRID                  SDL_Color{255, 200,   0, 255}
#define COLOR_MAP_SCREEN_GRID            SDL_Color{  0,   0, 220, 255}
#define COLOR_MOUSE_BOX                  SDL_Color{255, 255, 255, 255}
#define COLOR_BLANK_SCREEN_FILL          SDL_Color{  0,   0,   0, 255}
#define COLOR_TILE_SEL_BORDER            SDL_Color{  0, 255, 255, 255}
#define COLOR_TILE_SEL_ERROR             SDL_Color{255,   0,   0, 255}
#define COLOR_TILE_SEL_ERROR_BACK        SDL_Color{  0,   0,   0, 128}
#define COLOR_TILE_GRID_IN_BOUNDARY      SDL_Color{  0, 255,   0, 255}
#define COLOR_TILE_GRID_OUT_BOUNDARY     SDL_Color{  0,   0, 255, 255}
#define COLOR_MINIMAP_SCREEN_SELECTED    SDL_Color{255,   0,   0, 255}
#define COLOR_SCREEN_SELECTED_BOX        SDL_Color{  0, 255,   0, 255}
#define COLOR_SPRITE_INFO_TEXT           SDL_Color{  0,   0, 255, 255}
#define COLOR_SPRITE_HOVER_BOX           SDL_Color{  0, 255,   0, 255}
#define COLOR_SPRITE_HOVER_BOX_RAW       SDL_Color{255, 255,   0, 255}
#define COLOR_MATCH_DUPLICATE_HOVER_BOX  SDL_Color{220,   0,   0, 255}
#define COLOR_SPRITE_SELECTED_BOX        SDL_Color{  0, 128, 255, 255}
#define COLOR_SPRITE_HOVER_SELECTED_BOX  SDL_Color{  0, 192, 128, 255}
#define COLOR_RECT_SELECT_NORMAL_BOX     SDL_Color{255, 255, 255, 255}
#define COLOR_RECT_SELECT_EXCLUSIVE_BOX  SDL_Color{255, 255,   0, 255}

#define COLOR_HARD_EDIT_CURSOR           SDL_Color{255, 255, 255, 255}
#define COLOR_HARD_EDIT_BORDER           SDL_Color{  0, 255, 255, 255}
#define COLOR_HARD_EDIT_NOTEX            SDL_Color{  0,   0,   0, 255}

#define COLOR_SPRITE_FALLBACK_BACK       SDL_Color{  0,   0,   0, 255}
#define COLOR_SPRITE_FALLBACK_FORE       SDL_Color{255,   0, 255, 255}

#define COLOR_SPRITE_HARDNESS            SDL_Color{170, 170, 170, 255}
#define COLOR_WARP_SPRITE_HARDNESS       SDL_Color{255,   0,   0, 255}

#define COLOR_NORMAL_TILE_HARDNESS       SDL_Color{220, 220, 170, 255}
#define COLOR_LOW_TILE_HARDNESS          SDL_Color{  0, 220, 220, 255}
#define COLOR_OTHER_TILE_HARDNESS        SDL_Color{255,   0, 255, 255}

#define COLOR_POLYGON_FILL_HARDNESS      SDL_Color{255,   0,   0, 255}

SDL_Color color_tile_grid;
SDL_Color color_mouse_box;
SDL_Color color_blank_screen_fill;
SDL_Color color_tile_sel_border;
SDL_Color color_tile_sel_error;
SDL_Color color_tile_sel_error_back;
SDL_Color color_tile_grid_in_boundary;
SDL_Color color_tile_grid_out_boundary;
SDL_Color color_map_screen_grid;
SDL_Color color_minimap_screen_selected;
SDL_Color color_screen_selected_box;
SDL_Color color_sprite_info_text;
SDL_Color color_sprite_hover_box;
SDL_Color color_sprite_hover_box_raw;
SDL_Color color_match_duplicate_hover_box;
SDL_Color color_sprite_selected_box;
SDL_Color color_sprite_hover_selected_box;
SDL_Color color_rect_select_normal_box;
SDL_Color color_rect_select_exclusive_box;

SDL_Color color_hard_edit_cursor;
SDL_Color color_hard_edit_border;
SDL_Color color_hard_edit_notex;

SDL_Color color_sprite_fallback_back;
SDL_Color color_sprite_fallback_fore;

SDL_Color color_sprite_hardness;
SDL_Color color_warp_sprite_hardness;
SDL_Color color_normal_tile_hardness;
SDL_Color color_low_tile_hardness;
SDL_Color color_other_tile_hardness;

SDL_Color color_polygon_fill_hardness;


void setColors()
{
	color_tile_grid                 = COLOR_TILE_GRID;
	color_mouse_box                 = COLOR_MOUSE_BOX;
	color_blank_screen_fill         = COLOR_BLANK_SCREEN_FILL;
	color_tile_sel_border           = COLOR_TILE_SEL_BORDER;
	color_tile_sel_error            = COLOR_TILE_SEL_ERROR;
	color_tile_sel_error_back       = COLOR_TILE_SEL_ERROR_BACK;
	color_tile_grid_in_boundary     = COLOR_TILE_GRID_IN_BOUNDARY;
	color_tile_grid_out_boundary    = COLOR_TILE_GRID_OUT_BOUNDARY;
	color_map_screen_grid           = COLOR_MAP_SCREEN_GRID;
	color_minimap_screen_selected   = COLOR_MINIMAP_SCREEN_SELECTED;
	color_screen_selected_box       = COLOR_SCREEN_SELECTED_BOX;
	color_sprite_info_text          = COLOR_SPRITE_INFO_TEXT;
	color_sprite_hover_box          = COLOR_SPRITE_HOVER_BOX;
	color_sprite_hover_box_raw      = COLOR_SPRITE_HOVER_BOX_RAW;
	color_match_duplicate_hover_box = COLOR_MATCH_DUPLICATE_HOVER_BOX;
	color_sprite_selected_box       = COLOR_SPRITE_SELECTED_BOX;
	color_sprite_hover_selected_box = COLOR_SPRITE_HOVER_SELECTED_BOX;
	color_rect_select_normal_box    = COLOR_RECT_SELECT_NORMAL_BOX;
	color_rect_select_exclusive_box = COLOR_RECT_SELECT_EXCLUSIVE_BOX;

	color_hard_edit_cursor          = COLOR_HARD_EDIT_CURSOR;
	color_hard_edit_border          = COLOR_HARD_EDIT_BORDER;
	color_hard_edit_notex           = COLOR_HARD_EDIT_NOTEX;

    color_sprite_fallback_back      = COLOR_SPRITE_FALLBACK_BACK;
    color_sprite_fallback_fore      = COLOR_SPRITE_FALLBACK_FORE;

	color_sprite_hardness           = COLOR_SPRITE_HARDNESS;
	color_warp_sprite_hardness      = COLOR_WARP_SPRITE_HARDNESS;
	color_normal_tile_hardness      = COLOR_NORMAL_TILE_HARDNESS;
	color_low_tile_hardness         = COLOR_LOW_TILE_HARDNESS;
	color_other_tile_hardness       = COLOR_OTHER_TILE_HARDNESS;

	color_polygon_fill_hardness     = COLOR_POLYGON_FILL_HARDNESS;
}
