#include "StdAfx.h"
#include "Undo.h"
#include "Globals.h"
#include "Map.h"
#include "Engine.h"
#include "MainFrm.h"
#include "WinDinkeditView.h"
#include "Actions/Action.h"
using std::deque;

UndoStack::UndoStack()
	: redo_begin_index(0)
	, redo_begin_index_at_last_save(0)
{
}

void UndoStack::Undo()
{
	if (!CanUndo())
		return;
	
	--redo_begin_index;

	auto& action = stack[redo_begin_index];
	action->Undo();

	TCHAR statusText[1024];
	wsprintf(statusText, _T("Undo Action: %s."), action->Msg());
	mainWnd->setStatusText(statusText);

	mainWnd->GetActiveDocument()->SetModifiedFlag(redo_begin_index_at_last_save != redo_begin_index);
	
	// change the vision to the vision that was used during the action
	current_map->setVision(action->vision);

	// update the map position to the spot the action took place
	current_map->updateMapPosition(action->map_x, action->map_y);
	
	if (action->mouse.x != -1 && action->mouse.y != -1)
	{
		POINT p = action->mouse;
		mainWnd->GetRightPane()->ClientToScreen(&p);
		SetCursorPos(p.x, p.y);
	}

	Game_Main();
}

// redoes the next action
void UndoStack::Redo()
{
	if (!CanRedo())
		return;	

	auto& action = stack[redo_begin_index];
	++redo_begin_index;

	action->Do();

	TCHAR statusText[1024];
	wsprintf(statusText, _T("Action: %s."), action->Msg());
	mainWnd->setStatusText(statusText);

	Game_Main();
		
	current_map->setVision(action->vision);		// change the vision to the vision that was used during the action	
	current_map->updateMapPosition(action->map_x, action->map_y);	// update the map position to the spot the action took place

	if (action->mouse.x != -1 && action->mouse.y != -1)
	{
		POINT p = action->mouse;
		mainWnd->GetRightPane()->ClientToScreen(&p);
		SetCursorPos(p.x, p.y);
	}

	mainWnd->GetActiveDocument()->SetModifiedFlag(redo_begin_index_at_last_save != redo_begin_index);
}

bool UndoStack::CanUndo() const
{
	return redo_begin_index > 0;
}

bool UndoStack::CanRedo() const
{
	return redo_begin_index < stack.size();
}

void UndoStack::MarkSave()
{
	redo_begin_index_at_last_save = redo_begin_index;

	mainWnd->GetActiveDocument()->SetModifiedFlag(false);
}
