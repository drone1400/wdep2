#pragma once

#include <functional>

class Map;
class Screen;
class Sprite;

struct SpriteRef
{
	Map* map;
	int screen_num;
	int sprite_num;

	SpriteRef(Map* map, int screen_num, int sprite_num);
	SpriteRef at(int screen_num, int sprite_num);

	bool operator==(SpriteRef other) const;
	bool operator!=(SpriteRef other) const;

	explicit operator bool() const;

	Screen*& screen();
	Sprite*& sprite();

	Sprite& operator*();
	Sprite* operator->();

	template <typename T>
	auto operator->*(T Sprite::* mp) -> typename std::enable_if<!std::is_function<T>::value, T&>::type
	{
		return **this.*mp;
	}

	template <typename T>
	auto operator->*(T Sprite::* mp) -> typename std::enable_if<std::is_function<T>::value, decltype(std::bind(mp, (*this).operator*()))>::type //RW: (*this).operator*() should really be **this, but MSVC 2022 crashes on that
	{
		return std::bind(mp, **this);
	}
};
