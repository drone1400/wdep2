#pragma once

// placed functions related to WDEP / DMOD file manipulation here... 

void ReadWDEIni();
void WriteWDEIni();

bool DmodDizRead(CString const dmodPath, CString &dmod_title, CString &copyright1, CString &copyright2, CString &description);
bool DmodDizWrite(CString const dmodPath, CString &dmod_title, CString &copyright1, CString &copyright2, CString &description);

bool IsDmodDir(CString const dmodPath);
bool IsCoreDinkDir(CString const dmodPath);

bool DetectDinkInstallation(CString& detectedDinkExe);
bool GuessMissingDinkInstallationPaths(CString& dinkExePath, CString& dinkCorePath, CString& skeletonPath, CString& otherDmodsPath);

bool PopulateDmodClistCtrl(CListCtrl *dmod_list);

// tries to copy all the files from the source location to the destination location...
bool FilesCopyAllRecursive(CString const source, CString const destination);

bool CreateNewDmod(CString& dmod_dir_name, CString& dmod_title, CString& copyright1, CString& copyright2, CString& description);