#include "StdAfx.h"
#include "Globals.h"
#include "Colors.h"
#include "ddutil.h"
#include "SDL_image.h"

//SDL_RWops implementation that will allow us to SDL_LoadBMP_RW using a FILE* pointing into the middle of a dir.ff
struct BMP_Loader_RWops
{
	static SDL_RWops* create(FILE* f)
	{
		SDL_RWops* rw = SDL_AllocRW();
		rw->type = SDL_RWOPS_UNKNOWN;
		rw->size = BMP_Loader_RWops::size;
		rw->seek = BMP_Loader_RWops::seek;
		rw->read = BMP_Loader_RWops::read;
		rw->write = BMP_Loader_RWops::write;
		rw->close = BMP_Loader_RWops::close;
		rw->hidden.unknown.data1 = f;
		rw->hidden.unknown.data2 = (void*)_ftelli64(f);
		return rw;
	}

	static Sint64 SDLCALL size(SDL_RWops* rw)
	{
		return -1; //no known size
	}

	static Sint64 SDLCALL seek(SDL_RWops* rw, Sint64 offset, int whence)
	{
		assert(whence != RW_SEEK_END);

		FILE* f = (FILE*)rw->hidden.unknown.data1;
		Sint64 base_offset = (Sint64)rw->hidden.unknown.data2;

		switch (whence)
		{
			case RW_SEEK_SET:
				if (_fseeki64(f, base_offset + offset, SEEK_SET))
					return -1;
				break;
			case RW_SEEK_CUR:
				if (_fseeki64(f, offset, SEEK_CUR))
					return -1;
				break;
			default:
				return -1;
		}

		Sint64 new_offset = _ftelli64(f);
		if (new_offset < 0)
			return -1;

		return new_offset - base_offset;
	}

	static size_t SDLCALL read(SDL_RWops* rw, void* buf, size_t size, size_t count)
	{
		FILE* f = (FILE*)rw->hidden.unknown.data1;
		return fread(buf, size, count, f);
	}

	static size_t SDLCALL write(SDL_RWops* rw, const void* buf, size_t size, size_t count)
	{
		return 0; //we don't support writing
	}

	static int SDLCALL close(SDL_RWops* rw)
	{
		FILE* f = (FILE*)rw->hidden.unknown.data1;
		fclose(f);
		SDL_FreeRW(rw);
		return 0;
	}
};

SDL_Texture* loadBitmap(LPCTSTR filename, int& width, int& height, ColorKey color_key)
{
	// open the bitmap file
	FILE* stream;
	if ((stream = _tfopen(filename, _T("rb"))) == nullptr)
		return nullptr;

	// load the bitmap file
	SDL_Texture* pdds = loadBitmapStream(stream, width, height, color_key);

	// close the bitmap file
	fclose(stream);

	// return a pointer to the bitmap surface
	return pdds;
}

SDL_Texture* loadBitmapStream(FILE *input_file, int& new_width, int& new_height, ColorKey color_key)
{
	SDL_RWops* bmp_loader_rw = BMP_Loader_RWops::create(input_file);
	SDL_Surface* surface = SDL_LoadBMP_RW(bmp_loader_rw, SDL_TRUE);

	if (!surface) {
		new_width = 0;
		new_height = 0;
		return nullptr;
	}

	if (color_key == COLORKEY_WHITE)
		SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 255, 255, 255));
	else if (color_key == COLORKEY_BLACK)
		SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0, 0));

	new_width = surface->w;
	new_height = surface->h;

	SDL_Texture* texture = SDL_CreateTextureFromSurface(sdl_renderer, surface);

	SDL_FreeSurface(surface);
	return texture;
}


SDL_Texture* loadBitmapImage(LPCTSTR filename, int& new_width, int& new_height, ColorKey color_key)
{
	CStringA sB(filename);
	const char* pszC = sB;
	char* pszD = const_cast<char*>(pszC);
	
	
	SDL_Surface* surface = IMG_Load(pszD);
	
	if (!surface) {
		new_width = 0;
		new_height = 0;
		return nullptr;
	}

	if (color_key == COLORKEY_WHITE)
		SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 255, 255, 255));
	else if (color_key == COLORKEY_BLACK)
		SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0, 0));

	new_width = surface->w;
	new_height = surface->h;

	SDL_Texture* texture = SDL_CreateTextureFromSurface(sdl_renderer, surface);

	SDL_FreeSurface(surface);
	return texture;
}