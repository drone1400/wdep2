#include "StdAfx.h"
#include "TileSelector.h"
#include "Tile.h"
#include "Map.h"
#include "Globals.h"
#include "Screen.h"
#include "Colors.h"
#include "Common.h"
#include "CommonMath.h"
#include "CommonSdl.h"
#include "MainFrm.h"
#include "Undo.h"
#include "Actions/HardTileSetDefault.h"
#include "Actions/TilesChange.h"

TileSelector::TileSelector()
{
	x_tiles_selected = -1;
	y_tiles_selected = -1;
	screen_first_x_tile = -1;
	screen_last_x_tile = -1;
	screen_first_y_tile = -1;
	screen_last_y_tile = -1;

	rendSheetWidth = -1;
	rendSheetHeight = -1;
	rendTileWidth = -1;
	rendTileHeight = -1;
	rendOffsetX = 0;
	rendOffsetY = 0;
	// NOTE: for now it is hardcoded that 4 tile sheets displayed at a time in 2x2 grid
	// but perhaps will change that soon...
	rendSheetRows = 2;
	rendSheetCols = 3;
	// if true, will render tile sheets centered
	rendCentered = TRUE;

	hoverXTile = 0;
	hoverYTile = 0;
	hoverTile = {0,0,0,0};
}

TileSelector::~TileSelector()
{

}

int TileSelector::selectScreen(int new_screen)
{
	int pages = (int)ceil(double(MAX_TILE_BMPS) / (rendSheetRows * rendSheetCols));
	currentTileSheet = rendSheetRows * rendSheetCols * new_screen;
	if (currentTileSheet >= MAX_TILE_BMPS)
	{
		currentTileSheet = (pages -1) * rendSheetRows * rendSheetCols;
	}
	
	current_map->editor_state = EditorState::TileSelector;
	if (current_map->screenMode != EditorSubmode::Tile &&
		current_map->screenMode != EditorSubmode::Hardbox)
	{
		current_map->screenMode = EditorSubmode::Tile;
	}

//RW: I'm curious why these 4 lines were commented out. They seem to be quite essential.
	first_x_tile = 0;
	last_x_tile = 0;
	first_y_tile = 0;
	last_y_tile = 0;

	return true;
}

void TileSelector::nextPage()
{
	int count = rendSheetRows * rendSheetCols;
	if (currentTileSheet + count <= MAX_TILE_BMPS)
		currentTileSheet += count;
}

void TileSelector::prevPage()
{
	int count = rendSheetRows * rendSheetCols;
	if (currentTileSheet >= count)
		currentTileSheet -= count;
	else
		currentTileSheet = 0;
}

void TileSelector::refreshTileSheetRenderParameters()
{
	// get tilesheet width and height
	int width = (current_map->window_width - TILE_SELECTOR_GAP) / rendSheetCols;
	int height = (current_map->window_height - TILE_SELECTOR_GAP) / rendSheetRows;

	// correct width or height based on aspect ratio
	int width2 = height * TILESHEET_WIDTH / TILESHEET_HEIGHT;
	int height2 = width * TILESHEET_HEIGHT / TILESHEET_WIDTH;
	if (width2 > width)
	{
		height = height2;
	} else
	{
		width = width2;
	}

	// get tile width and height in pixels
	rendTileWidth = width / TILESHEET_TILE_WIDTH;
	rendTileHeight = height / TILESHEET_TILE_HEIGHT;

	// recalculate width and height based on tile width and tile height
	rendSheetWidth = rendTileWidth * TILESHEET_TILE_WIDTH;
	rendSheetHeight = rendTileHeight * TILESHEET_TILE_HEIGHT;

	if (rendCentered)
	{
		rendOffsetX = (current_map->window_width - rendSheetCols * rendSheetWidth - TILE_SELECTOR_GAP) / 2;
		rendOffsetY = (current_map->window_height - rendSheetRows * rendSheetHeight - TILE_SELECTOR_GAP) / 2;
	} else
	{
		rendOffsetX = 0;
		rendOffsetY = 0;
	}
}


// Gets the tile X,Y position in the tilesheet at the given cursor position
// WARNING: does not refresh render sizes, make sure to do that first if needed
void TileSelector::getTileSheetTileXY(int inCursorX, int inCursorY, int& outTileX, int& outTileY)
{
	inCursorX -= rendOffsetX;
	inCursorY -= rendOffsetY;

	if (inCursorX < 0 || inCursorX >= (rendSheetCols - 1) * (rendSheetWidth + TILE_SELECTOR_GAP) + rendSheetWidth ||
		inCursorY < 0 || inCursorY >= (rendSheetRows - 1) * (rendSheetHeight + TILE_SELECTOR_GAP) + rendSheetHeight )
	{
		// we are out of bounds
		outTileX = -1;
		outTileY = -1;
		return;
	}

	const int sheetX = inCursorX / (rendSheetWidth + TILE_SELECTOR_GAP);
	const int sheetY = inCursorY / (rendSheetHeight + TILE_SELECTOR_GAP);
	inCursorX -= sheetX * (rendSheetWidth + TILE_SELECTOR_GAP);
	inCursorY -= sheetY * (rendSheetHeight + TILE_SELECTOR_GAP);

	outTileX = sheetX * TILESHEET_TILE_WIDTH + inCursorX / rendTileWidth;
	outTileY = sheetY * TILESHEET_TILE_HEIGHT + inCursorY / rendTileWidth;
}

void TileSelector::selectTileSheetTileXY(int x, int y)
{
	// make sure render sizes are correct
	refreshTileSheetRenderParameters();
	if (rendSheetWidth <= 0 || rendSheetHeight <= 0 ||
		rendTileWidth <= 0 || rendTileHeight <= 0)
	{
		first_x_tile = last_x_tile = -1;
		first_y_tile = last_y_tile = -1;
		return;
	}

	int tileX, tileY;
	getTileSheetTileXY(x,y, tileX, tileY);
	first_x_tile = last_x_tile = tileX;
	first_y_tile = last_y_tile = tileY;
}

// calculates a grid from a rect that contains the currently selected tiles
//RW: C6244: renamed local mouse_x_origin to param_mouse_x_origin
void TileSelector::selectTileSheetTileXY(int param_mouse_x_origin, int param_mouse_x_position, int param_mouse_y_origin, int param_mouse_y_position)
{
	refreshTileSheetRenderParameters();
	getTileSheetTileXY(param_mouse_x_origin,param_mouse_y_origin, first_x_tile, first_y_tile);
	getTileSheetTileXY(param_mouse_x_position,param_mouse_y_position, last_x_tile, last_y_tile);
	// make sure tiles are in right order
	EnsureAsc(first_x_tile, last_x_tile);
	EnsureAsc(first_y_tile,last_y_tile);
}


// stores tiles on tile bmp screens
void TileSelector::storeTileSheetTiles()
{
	if (first_x_tile < 0 || first_y_tile < 0 || current_map->screenMode == EditorSubmode::Hardbox)
	{
		x_tiles_selected = 0;
		y_tiles_selected = 0;
		return;
	}
	
	x_tiles_selected = 0;
	for (int x = first_x_tile; x <= last_x_tile; x++, x_tiles_selected++)
	{
		y_tiles_selected = 0;
		for (int y = first_y_tile; y <= last_y_tile; y++, y_tiles_selected++)
		{
			int xBmp = x / TILESHEET_TILE_WIDTH;
			int yBmp = y / TILESHEET_TILE_HEIGHT;

			short muhBmp = short(yBmp * rendSheetCols + xBmp + currentTileSheet);
			short muhX = short(x - xBmp * TILESHEET_TILE_WIDTH);
			short muhY = short(y - yBmp * TILESHEET_TILE_HEIGHT);

			if (muhY == 10 && muhX >= 8)
			{
				// NOTE: tilesheets can only have 128 tiles due to the way the tile-id gets encoded
				// however, the 12x11 grid has 132 slots
				// this means that the last 4 tiles in the tile sheet, actually overflow into the next tile sheet!
				// copied data should reflect this reality...
				muhX -= 8;
				muhY = 0;
				muhBmp++;
			}

		    if (muhBmp >= MAX_TILE_BMPS)
		    {
		        muhBmp = 0;
		        muhX = 0;
		        muhY = 0;
		    }
			
			tile_set[x_tiles_selected][y_tiles_selected].bmp = muhBmp;
			tile_set[x_tiles_selected][y_tiles_selected].x = muhX;
			tile_set[x_tiles_selected][y_tiles_selected].y = muhY;
			tile_set[x_tiles_selected][y_tiles_selected].alt_hardness = 0;
		}
	}

	x_tiles_selected = last_x_tile - first_x_tile + 1;
	y_tiles_selected = last_y_tile - first_y_tile + 1;

	mainWnd->setStatusText(_T("Copied tile sheet tiles!"));
}


void TileSelector::renderTileSheetHardnessAt(int tileSheet, int sheetCol, int sheetRow)
{
	int posX = sheetCol * (rendSheetWidth + TILE_SELECTOR_GAP) + rendOffsetX;
	int posY = sheetRow * (rendSheetHeight + TILE_SELECTOR_GAP) + rendOffsetY;
	
	for (int y = 0; y < TILESHEET_TILE_HEIGHT; y++)
	{
		for (int x = 0; x < TILESHEET_TILE_WIDTH; x++)
		{
			// do not evaluate last 4 tiles of the tilesheet... (from #129 to #132)
			if (y == TILESHEET_TILE_HEIGHT -1 && x >= TILESHEET_TILE_WIDTH - 4)
				break;
			
			int hard_tile_num = tileBmp[tileSheet]->tile_hardness[y][x];

			// render the tile
			current_map->hard_tile_selector.renderHardness(x * rendTileWidth + posX, y * rendTileHeight + posY,
			rendTileWidth, rendTileHeight,
				hard_tile_num, true);
		}
	}
}

void TileSelector::renderTileSheetAt(int tileSheet, int sheetCol, int sheetRow)
{
	int posX = sheetCol * (rendSheetWidth + TILE_SELECTOR_GAP) + rendOffsetX;
	int posY = sheetRow * (rendSheetHeight + TILE_SELECTOR_GAP) + rendOffsetY;
	
	tileBmp[tileSheet]->renderAll(posX, posY, rendSheetWidth, rendSheetHeight);
	const RECT border0 = { posX-1, posY-1, posX + rendSheetWidth+1, posY + rendSheetHeight+1};
	drawBox(border0, color_tile_sel_border);

	// save old blend mode so we can restore later...
	SDL_BlendMode blendModeOld;
	SDL_GetRenderDrawBlendMode(sdl_renderer, &blendModeOld);

	// switch to SDL_BLENDMODE_BLEND for transparency
	SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);
	
	// cross out the last 4 tiles!
	for (int x = 8, y=10; x < 12; x++)
	{
		int ix1 = posX + int(floor(rendTileWidth * x));
		int iy1 = posY + int(floor(rendTileHeight * y));
		int ix2 = posX + int(floor(rendTileWidth * (x+1)));
		int iy2 = posY + int(floor(rendTileHeight * (y+1)));
		RECT errBoxBack = {ix1, iy1, ix2, iy2};
		drawFilledBox( errBoxBack, color_tile_sel_error_back);
		//RECT errBox = {ix1+2, iy1+2, ix2-2, iy2-2};
		//drawBox( errBox, color_tile_sel_error);
		drawX(ix1+4, iy1+4, ix2-4, iy2-4, color_tile_sel_error);
	}

	SDL_SetRenderDrawBlendMode(sdl_renderer, blendModeOld);
}

void TileSelector::renderTileSheetSelection()
{
	if (first_x_tile < 0 || first_y_tile < 0 || current_map->screenMode == EditorSubmode::Hardbox)
	{
		// nothing to render
		return;
	}
	
	int x1sheet = first_x_tile / TILESHEET_TILE_WIDTH;
	int x1tile = first_x_tile - x1sheet * TILESHEET_TILE_WIDTH;
	int y1sheet = first_y_tile / TILESHEET_TILE_HEIGHT;
	int y1tile = first_y_tile - y1sheet * TILESHEET_TILE_HEIGHT;
	
	int x2sheet = last_x_tile / TILESHEET_TILE_WIDTH;
	int x2tile = last_x_tile - x2sheet * TILESHEET_TILE_WIDTH;
	int y2sheet = last_y_tile / TILESHEET_TILE_HEIGHT;
	int y2tile = last_y_tile - y2sheet * TILESHEET_TILE_HEIGHT;

	int x,y,xs,xt,ys,yt;

	x = rendOffsetX + x1sheet * (rendSheetWidth + TILE_SELECTOR_GAP) + x1tile * rendTileWidth; 
	y = rendOffsetY + y1sheet * (rendSheetHeight + TILE_SELECTOR_GAP) + y1tile * rendTileHeight;

	xs = x1sheet;
	xt = x1tile;
	ys = y1sheet;
	yt = y1tile;
	
	while(true)
	{
		drawBox(x,y, rendTileWidth, rendTileHeight, color_tile_grid_in_boundary, 1);

		// increment x
		xt++;
		x += rendTileWidth;
		
		if (xt == TILESHEET_TILE_WIDTH)
		{
			// next sheet, add gap
			xs++;
			xt = 0;
			x+= TILE_SELECTOR_GAP;
		}

		// check end of row
		if (xs == x2sheet && xt > x2tile || xs > x2sheet)
		{
			// reset x
			xs = x1sheet;
			xt = x1tile;
			x = rendOffsetX + x1sheet * (rendSheetWidth + TILE_SELECTOR_GAP) + x1tile * rendTileWidth;

			// increment y
			yt++;
			y += rendTileHeight;
			
			if (yt == TILESHEET_TILE_HEIGHT)
			{
				// next sheet, add gap
				ys++;
				yt = 0;
				y+= TILE_SELECTOR_GAP;
			}
			if (ys == y2sheet && yt > y2tile || ys > y2sheet)
			{
				// all done
				return;
			}
		}
	}
}


void TileSelector::renderTileSheet()
{
	refreshTileSheetRenderParameters();

	for (int row = 0, ctOffset = 0; row < rendSheetRows && currentTileSheet + ctOffset < MAX_TILE_BMPS; row++)
	{
		for (int col = 0; col < rendSheetCols && currentTileSheet + ctOffset < MAX_TILE_BMPS; col++, ctOffset++)
		{
			renderTileSheetAt(currentTileSheet + ctOffset, col, row);
			if (displayhardness) renderTileSheetHardnessAt(currentTileSheet + ctOffset, col, row);
		}
	}

	renderTileSheetSelection();
}

void TileSelector::renderTileSheetTileHover(int x, int y)
{
	refreshTileSheetRenderParameters();

	getTileSheetTileXY(x,y, hoverXTile, hoverYTile);

	if (hoverXTile < 0 || hoverYTile <0)
	{
		
		return;
	}
	
	int x1sheet = hoverXTile / TILESHEET_TILE_WIDTH;
	int x1tile = hoverXTile - x1sheet * TILESHEET_TILE_WIDTH;
	int y1sheet = hoverYTile / TILESHEET_TILE_HEIGHT;
	int y1tile = hoverYTile - y1sheet * TILESHEET_TILE_HEIGHT;

	int posX = rendOffsetX + x1sheet * (rendSheetWidth + TILE_SELECTOR_GAP) + x1tile * rendTileWidth;
	int posY = rendOffsetY + y1sheet * (rendSheetHeight + TILE_SELECTOR_GAP) + y1tile * rendTileHeight;
	drawBox(posX-1, posY-1, rendTileWidth+2, rendTileHeight+2, color_tile_grid, 2);

	// update hover tile info
	int sheetId = currentTileSheet + y1sheet * rendSheetCols + x1sheet;

    // make sure sheet id is within bounds...
    if (sheetId < 0 || sheetId >= MAX_TILE_BMPS)
    {
        hoverTile = { short(0), short(-1), short(-1), short(-1)};
        return;
    }
    
	hoverTile = { short(0), short(sheetId), short(x1tile), short(y1tile)};
	hoverHardIndex = tileBmp[sheetId]->tile_hardness[y1tile][x1tile];

	//
	// if we're not in hardbox edit mode, we're done here...
	//
	if (current_map->screenMode != EditorSubmode::Hardbox)
		return;

	// check if hard selection is active in HardTileSelector
	int selX, selY;
	current_map->hard_tile_selector.getSelectionSize(selX, selY);
	if (selX <= 0 || selY <=0)
		return; // no selection data

	// check if valid hardindex in HardTileSelector
	int hardIndex;
	current_map->hard_tile_selector.getSelectedTileAt(0,0,hardIndex);
	if (hardIndex < 0)
		return; // no selection data
	
	// draw hardness preview on mouse hover
	current_map->hard_tile_selector.renderHardness(posX, posY,
		rendTileWidth, rendTileHeight,
			hardIndex, true);
}

bool TileSelector::tryDoHardTileSetDefaultOnHover()
{
	// if we're not in hardbox edit mode, we're done here...
	if (current_map->screenMode != EditorSubmode::Hardbox)
		return false;

	// check if hard selection is active in HardTileSelector
	int selX, selY;
	current_map->hard_tile_selector.getSelectionSize(selX, selY);
	if (selX <= 0 || selY <=0)
		return false; // no selection data

	// check if valid hardindex in HardTileSelector
	int hardIndex;
	current_map->hard_tile_selector.getSelectedTileAt(0,0,hardIndex);
	if (hardIndex < 0)
		return false; // no selection data

	int x1sheet = hoverXTile / TILESHEET_TILE_WIDTH;
	int x1tile = hoverXTile - x1sheet * TILESHEET_TILE_WIDTH;
	int y1sheet = hoverYTile / TILESHEET_TILE_HEIGHT;
	int y1tile = hoverYTile - y1sheet * TILESHEET_TILE_HEIGHT;

	int sheetId = currentTileSheet + y1sheet * rendSheetCols + x1sheet;

	current_map->undo_buffer->PushAndDo<actions::HardTileSetDefault>(tileBmp[sheetId]->tile_hardness[y1tile][x1tile], hardIndex, sheetId, x1tile, y1tile);
	return true;
}

bool TileSelector::tryDoHardTileSetDefaultClearOnHover()
{
	// if we're not in hardbox edit mode, we're done here...
	if (current_map->screenMode != EditorSubmode::Hardbox)
		return false;

	int x1sheet = hoverXTile / TILESHEET_TILE_WIDTH;
	int x1tile = hoverXTile - x1sheet * TILESHEET_TILE_WIDTH;
	int y1sheet = hoverYTile / TILESHEET_TILE_HEIGHT;
	int y1tile = hoverYTile - y1sheet * TILESHEET_TILE_HEIGHT;

	int sheetId = currentTileSheet + y1sheet * rendSheetCols + x1sheet;

	current_map->undo_buffer->PushAndDo<actions::HardTileSetDefault>(tileBmp[sheetId]->tile_hardness[y1tile][x1tile], 0, sheetId, x1tile, y1tile);
	return true;
}

void TileSelector::switchHardTileEditModeOnHover()
{
	// if we're not in hardbox edit mode, we're done here...
	if (current_map->screenMode != EditorSubmode::Hardbox)
		return;
	
	int x1sheet = hoverXTile / TILESHEET_TILE_WIDTH;
	int x1tile = hoverXTile - x1sheet * TILESHEET_TILE_WIDTH;
	int y1sheet = hoverYTile / TILESHEET_TILE_HEIGHT;
	int y1tile = hoverYTile - y1sheet * TILESHEET_TILE_HEIGHT;

	int sheetId = currentTileSheet + y1sheet * rendSheetCols + x1sheet;

	int hardIndex = tileBmp[sheetId]->tile_hardness[y1tile][x1tile];
	TILEDATA data { 0, short(sheetId), short(x1tile),short(y1tile)};

	if (hardIndex < 0 || hardIndex >= NUM_HARD_TILES)
	{
		hardIndex = 0;
	}
	
	current_map->hard_tile_selector.hardTileEditorPrepareTile(hardIndex,data);

	current_map->editor_state = EditorState::TileHardnessEditor;
	current_map->hardTileSelectorReturnState = EditorState::TileSelector;
}

void TileSelector::getCurrentTileData(TILEDATA& data)
{
	data = hoverTile;
}

void TileSelector::getCurrentTileHardIndex(int& hardIndex)
{
	hardIndex = hoverHardIndex;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Screen related functions below
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void TileSelector::updateScreenGrid(int x, int y)
{
    POINT point{x, y};
    if (current_map->bindViewPoint(point) == false)
    {
        screen_first_x_tile = -1;
        screen_first_y_tile = -1;
        screen_last_x_tile = -1;
        screen_last_y_tile = -1;
        return;
    }
    
	screen_first_x_tile = screen_last_x_tile = point.x / SCREEN_WIDTH_GAP * SCREEN_TILE_WIDTH
		+ (point.x % SCREEN_WIDTH_GAP) / TILEWIDTH;

	screen_first_y_tile = screen_last_y_tile = point.y / SCREEN_HEIGHT_GAP * SCREEN_TILE_HEIGHT
		+ (point.y % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	anchorRight = false;
	anchorBottom = false;
}

void TileSelector::updateScreenGrid(const RECT &mouse_box)
{
    RECT sel_rect = mouse_box;
    if (current_map->bindViewRect(sel_rect) == false)
    {
        screen_first_x_tile = -1;
        screen_first_y_tile = -1;
        screen_last_x_tile = -1;
        screen_last_y_tile = -1;
        return;
    }
    
	screen_first_x_tile = (sel_rect.left / SCREEN_WIDTH_GAP) * SCREEN_TILE_WIDTH +
		(sel_rect.left % SCREEN_WIDTH_GAP) / TILEWIDTH;

	screen_last_x_tile = (sel_rect.right / SCREEN_WIDTH_GAP) * SCREEN_TILE_WIDTH +
		(sel_rect.right % SCREEN_WIDTH_GAP) / TILEWIDTH;

	screen_first_y_tile = (sel_rect.top / SCREEN_HEIGHT_GAP) * SCREEN_TILE_HEIGHT +
		(sel_rect.top % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	screen_last_y_tile = (sel_rect.bottom / SCREEN_HEIGHT_GAP) * SCREEN_TILE_HEIGHT +
		(sel_rect.bottom % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

    // NOTE: these should not be needed anymore since current_map->bindViewRect ensures that
    // the selection rectangle coordinats are in ascending order
	//EnsureAsc(screen_first_x_tile, screen_last_x_tile);
	//EnsureAsc(screen_first_y_tile, screen_last_y_tile);

    anchorRight = (mouse_box.left > mouse_box.right);
    anchorBottom = (mouse_box.top > mouse_box.bottom);

	int x_tiles = (screen_last_x_tile - screen_first_x_tile + 1) - SCREEN_TILE_WIDTH * 2;
	int y_tiles = (screen_last_y_tile - screen_first_y_tile + 1) - SCREEN_TILE_HEIGHT * 2;
	if (x_tiles > 0)
	{
		if (anchorRight)
			screen_first_x_tile += x_tiles;
		else
			screen_last_x_tile -= x_tiles;
	}

	if (y_tiles > 0)
	{
		if (anchorBottom)
			screen_first_y_tile += y_tiles;
		else
			screen_last_y_tile -= y_tiles;
	}
}

void TileSelector::renderScreenTileHover(int x, int y)
{
    POINT point{x, y};
    if (current_map->bindViewPoint(point) == false)
        return;
    
	int x_screen = point.x / SCREEN_WIDTH_GAP;
	int y_screen = point.y / SCREEN_HEIGHT_GAP;

	int cur_screen = y_screen * MAP_COLUMNS + x_screen;

	if (current_map->screen[cur_screen] == nullptr)
		return;

	int x_tile = (point.x % SCREEN_WIDTH_GAP) / TILEWIDTH;
	int y_tile = (point.y % SCREEN_HEIGHT_GAP) / TILEHEIGHT;

	int x_offset = x_screen * SCREEN_WIDTH_GAP + x_tile * TILEWIDTH - current_map->window.left;
	int y_offset = y_screen * SCREEN_HEIGHT_GAP + y_tile * TILEHEIGHT - current_map->window.top;

	RECT dest = {x_offset, y_offset, x_offset + TILEWIDTH, y_offset + TILEHEIGHT};
	drawBox(dest, color_tile_grid, 1);
}



// draws tile grid for selecting tiles, does not draw the actual tiles
void TileSelector::renderScreenTileGrid()
{
	if (current_map->screenMode != EditorSubmode::Tile)
		return;

    RECT window_rect = current_map->window;
    if (current_map->bindMapRect(window_rect) == false)
        return;

	// calculate possible screens to draw
	int first_x_screen = window_rect.left / SCREEN_WIDTH_GAP;
	int last_x_screen = window_rect.right / SCREEN_WIDTH_GAP;
	int first_y_screen = window_rect.top / SCREEN_HEIGHT_GAP;
	int last_y_screen = window_rect.bottom / SCREEN_HEIGHT_GAP;

    int first_x_screen_raw = current_map->window.left / SCREEN_WIDTH_GAP;
    int first_y_screen_raw = current_map->window.top / SCREEN_HEIGHT_GAP;

    int screen;
    int x_offset = -(current_map->window.left % SCREEN_WIDTH_GAP) + (first_x_screen - first_x_screen_raw) * SCREEN_WIDTH_GAP;
    int y_offset = -(current_map->window.top % SCREEN_HEIGHT_GAP) + (first_y_screen - first_y_screen_raw) * SCREEN_HEIGHT_GAP;

	int x_screen_offset = 0;
	int y_screen_offset;

	int screen_first_x_tile_in = screen_first_x_tile;
	int screen_last_x_tile_in = screen_last_x_tile;
	int screen_first_y_tile_in = screen_first_y_tile;
	int screen_last_y_tile_in = screen_last_y_tile;

	int x_diff = screen_last_x_tile - screen_first_x_tile + 1 - x_tiles_selected;
	if (x_diff > 0)
	{
		if (anchorRight)
			screen_first_x_tile_in += x_diff;
		else
			screen_last_x_tile_in -= x_diff;
	}
	
	int y_diff = screen_last_y_tile_in - screen_first_y_tile_in + 1 - y_tiles_selected;
	if (y_diff > 0)
	{
		if (anchorBottom)
			screen_first_y_tile_in += y_diff;
		else
			screen_last_y_tile_in -= y_diff;
	}

	for (int x_screen = first_x_screen; x_screen <= last_x_screen; x_screen++)
	{
		y_screen_offset = 0;
		for (int y_screen = first_y_screen; y_screen <= last_y_screen; y_screen++)
		{
			screen = y_screen * MAP_COLUMNS + x_screen;
			if (screen > 768)
				return;
			
			if (current_map->screen[screen] != nullptr)
			{
				int x_offset2 = x_offset + x_screen_offset;
				int y_offset2 = y_offset + y_screen_offset;
				
				// now print grid
				int cur_x_tile, cur_y_tile;
				for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
				{
					for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
					{
						cur_x_tile = x_screen * SCREEN_TILE_WIDTH + x;
						cur_y_tile = y_screen * SCREEN_TILE_HEIGHT + y;

						RECT dest = {x * TILEWIDTH + x_offset2, y * TILEHEIGHT + y_offset2, 
							(x + 1) * TILEWIDTH + x_offset2, (y + 1) * TILEHEIGHT + y_offset2};

						// check if tile is in boundaries
						if (screen_first_x_tile_in <= cur_x_tile && screen_last_x_tile_in >= cur_x_tile && 
							screen_first_y_tile_in <= cur_y_tile && screen_last_y_tile_in >= cur_y_tile)
						{
							// draw the grid box around the tile
							drawBox(dest, color_tile_grid_in_boundary, 1);
						}
						// tile must be out of boundary
						else if (screen_first_x_tile <= cur_x_tile && screen_last_x_tile >= cur_x_tile && 
							screen_first_y_tile <= cur_y_tile && screen_last_y_tile >= cur_y_tile)
						{
							// draw the grid box around the tile
							drawBox(dest, color_tile_grid_out_boundary, 1);
						}
					}
				}

			}
			y_screen_offset += SCREEN_HEIGHT_GAP;
		}
		x_screen_offset += SCREEN_WIDTH_GAP;
	}

	return;
}



// stores tiles from the map screen
void TileSelector::storeScreenTiles() 
{  
	if (screen_first_x_tile == -1) 
	{ 
		MB_ShowError(_T("You must click on one or more tile destinations before tile(s) can be copied."));
		return;  
	} 
	x_tiles_selected = 0; 
//	TILEDATA* current_tile; 
	int cur_screen, x_tile, y_tile;  

	for (int x = screen_first_x_tile; x <= screen_last_x_tile; x++, x_tiles_selected++) 
	{
		y_tiles_selected = 0;
		for (int y = screen_first_y_tile; y <= screen_last_y_tile; y++, y_tiles_selected++)  
		{ 
			cur_screen = MAP_COLUMNS * (y / SCREEN_TILE_HEIGHT) + x / SCREEN_TILE_WIDTH; 
			if (current_map->screen[cur_screen] == nullptr) 
			{ 
				memset(&tile_set[x_tiles_selected][y_tiles_selected], 0, sizeof(TILEDATA)); 
			} 
			else 
			{ 
				x_tile = x % SCREEN_TILE_WIDTH;  
				y_tile = y % SCREEN_TILE_HEIGHT; 
				memcpy(&tile_set[x_tiles_selected][y_tiles_selected], &current_map->screen[cur_screen]->tiles[y_tile][x_tile], sizeof(TILEDATA)); 
				//tile_set[x_tiles_selected][y_tiles_selected].alt_hardness = 0; 
			} 
		} 
	} 
 
	x_tiles_selected = screen_last_x_tile - screen_first_x_tile + 1; 
	y_tiles_selected = screen_last_y_tile - screen_first_y_tile + 1;

	mainWnd->setStatusText(_T("Copied screen tiles!"));
}

bool TileSelector::tryDoPlaceTiles()
{
	//is there any tiles selected?
	if (screen_first_x_tile == -1)
	{
		MB_ShowError(_T("You must click on one or more tile destinations before tile(s) can be placed."));
		return false;
	}
	
	//used if not all the tiles are used from 0,0
	int x_diff = 0, y_diff = 0;

	//is there tiles on the right not being used?
	if (anchorRight)
	{
		//how many?
		x_diff = x_tiles_selected - (screen_last_x_tile - screen_first_x_tile + 1);

		//over flowed, too many used, lets bring it down to max (0)
		if (x_diff < 0)
		{
			screen_first_x_tile -= x_diff;
			x_diff = 0;
		}
	}
	//is there tiles on the top not being used?
	if (anchorBottom)
	{
		//how many?
		y_diff = y_tiles_selected - (screen_last_y_tile - screen_first_y_tile + 1);

		//over flowed, too many used, lets bring it down to max (0)
		if (y_diff < 0)
		{
			screen_first_y_tile -= y_diff;
			y_diff = 0;
		}	
	}

	//lets get the dimensions of what the user wants to paste
	//make sure we don't go out of bounds
	int width = screen_last_x_tile - screen_first_x_tile + 1;

	if (width > x_tiles_selected - x_diff)
		width = x_tiles_selected - x_diff;

	int height = screen_last_y_tile - screen_first_y_tile + 1;
	if (height > y_tiles_selected - y_diff)
		height = y_tiles_selected - y_diff;

	//add to undo buffer
	actions::TilesChange action(screen_first_x_tile, screen_first_y_tile, width, height);
	
	int cur_screen, x_tile, y_tile, x, y;

	// save the new tiles
	for (x = 0; x < width; x++)
		for(y = 0; y < height; y++)
			action.new_tiles[x][y] = tile_set[x+x_diff][y+y_diff];

	//save old tiles
	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++)
		{
			//check screen we could have moved onto a new screen, or we could still be on it.
			cur_screen = MAP_COLUMNS * ((y + screen_first_y_tile) / SCREEN_TILE_HEIGHT) + (x + screen_first_x_tile) / SCREEN_TILE_WIDTH;

			//make sure the screen is valid
			if (current_map->screen[cur_screen] == nullptr)
				continue;
			
			//zoom into the tile in question
			x_tile = (x + screen_first_x_tile) % SCREEN_TILE_WIDTH;
			y_tile = (y + screen_first_y_tile) % SCREEN_TILE_HEIGHT;

			//copy it over
			action.old_tiles[x][y] = current_map->screen[cur_screen]->tiles[y_tile][x_tile];
		}
	}

	current_map->undo_buffer->PushAndDo<actions::TilesChange>(std::move(action));
	return true;
}