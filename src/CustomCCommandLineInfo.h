#pragma once
#include "stdafx.h"
class CustomCCommandLineInfo : public CCommandLineInfo
{
public:
    CustomCCommandLineInfo(void);
    void ParseParam(const TCHAR* pszParam, BOOL bFlag, BOOL bLast) override;
    CString m_strGameLocation;
    CString m_strRefDirLocation;
    bool m_hasGameLocation;
    bool m_hasRefDirLocation;

	bool GetNormalizedOpenFileName(CString & outStr);
	bool GetNormalizedGameArg(CString & outStr);
	bool GetNormalizedValidRefDirArg(CString & outStr);
private:
    bool _expectGameLocation;
    bool _expectRefDirLocation;
    bool _canExpectGameLocation;
    bool _canExpectRefDirLocation;
	bool GetNormalizedFileName_Internal(CString & outStr);
	

};