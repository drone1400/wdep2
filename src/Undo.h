#pragma once
#include "Actions/Action.h"

extern UINT max_undo_level;

class UndoStack
{
public:
	UndoStack();

	void Undo();
	void Redo();
	bool CanUndo() const;
	bool CanRedo() const;

	void MarkSave();

	template<typename T, typename... Args>
	typename std::enable_if<std::is_base_of<actions::Action, T>::value>::type
	PushAndDo(Args&&... args)
	{
		while (redo_begin_index != stack.size())
			stack.pop_back();

		if (redo_begin_index_at_last_save > redo_begin_index) //if we undid some stuff since the last save, then we can't redo there anymore, so the modified flag must be true
			redo_begin_index_at_last_save = -1;

		while (redo_begin_index > max_undo_level)
		{
			stack.pop_front();
			redo_begin_index_at_last_save--; //note: underflow is ok, it just means it'll never be equal to redo_begin_index in practice (which is exactly what we want when that happens)
			redo_begin_index--;
		}

		stack.push_back(std::make_unique<T>(std::forward<Args>(args)...));

		Redo();
	}

private:
	std::deque<std::unique_ptr<actions::Action>> stack;
	std::size_t redo_begin_index;
	std::size_t redo_begin_index_at_last_save;
};
