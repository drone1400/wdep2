#include "StdAfx.h"
#include "SpriteRef.h"
#include "Map.h"
#include "Screen.h"

SpriteRef::SpriteRef(Map* map, int screen_num, int sprite_num)
	: map(map)
	, screen_num(screen_num)
	, sprite_num(sprite_num)
{
}

SpriteRef SpriteRef::at(int screen_num, int sprite_num)
{
	return SpriteRef(map, screen_num, sprite_num);
}

bool SpriteRef::operator==(SpriteRef other) const
{
	return screen_num == other.screen_num && sprite_num == other.sprite_num;
}

bool SpriteRef::operator!=(SpriteRef other) const
{
	return !(*this == other);
}

SpriteRef::operator bool() const
{
	return map->screen[screen_num] != nullptr && map->screen[screen_num]->sprite[sprite_num] != nullptr;
}

Screen*& SpriteRef::screen()
{
	return map->screen[screen_num];
}

Sprite*& SpriteRef::sprite()
{
	return map->screen[screen_num]->sprite[sprite_num];
}

Sprite& SpriteRef::operator*()
{
	return *sprite();
}

Sprite* SpriteRef::operator->()
{
	return sprite();
}
