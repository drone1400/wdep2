#include "stdafx.h"
#include "ViewCompressedScript.h"
#include <exception>

IMPLEMENT_DYNAMIC(ViewCompressedScript, CDialog)

ViewCompressedScript::ViewCompressedScript(const TCHAR* filepath, CWnd* pParent /*=nullptr*/) : CDialog(ViewCompressedScript::IDD, pParent)
	, m_text(_T(""))
	, m_filepath(filepath)
{
	//RW: read the text from the compressed file.
	FILE* fin;
	if ((fin = _tfopen(m_filepath, _T("rb"))) == nullptr)
		throw std::exception();

	BYTE s[16], pairs[128][2];
	int i=0, c;

	if ((c = getc(fin)) > 127)
		fread(pairs,2,c-128,fin);
	else
		m_text += (char) c;

	while (1)
	{
		if (i!=0)
			c = s[--i];
		else if ((c = getc(fin)) == EOF)
			break;

		if (c > 127)
		{
			s[i++] = pairs[c-128][1];
			s[i++] = pairs[c-128][0];
		}
		else
			m_text += (char) c;
	}
	fclose(fin);
	
}

ViewCompressedScript::~ViewCompressedScript()
{
}

void ViewCompressedScript::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_VCS, m_text);
}


BEGIN_MESSAGE_MAP(ViewCompressedScript, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// ViewCompressedScript message handlers


HBRUSH ViewCompressedScript::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_VCS)
	{
		pDC->SetBkColor(RGB(255,255,255));
		HBRUSH whitebr = CreateSolidBrush(RGB(255,255,255));
		return whitebr;
	}

	return hbr;
}


void ViewCompressedScript::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CWnd* editbox = GetDlgItem(IDC_EDIT_VCS);
	if (editbox != nullptr)
		editbox->MoveWindow(11, 11, cx-22, cy-22);
}


BOOL ViewCompressedScript::OnInitDialog()
{
	CDialog::OnInitDialog();

	//RW: set the title (caption) of the dialog to be the file name
	SetWindowText(m_filepath);
		
	m_textfont.CreatePointFont(100, _T("Courier New"));
	GetDlgItem(IDC_EDIT_VCS)->SetFont(&m_textfont);

	GetDlgItem(IDC_EDIT_VCS)->SetFocus();

	return FALSE;	//RW: we return FALSE because we set focus to the read-only editor control
}
