#include "StdAfx.h"
#include "DinkInstallationDialog.h"
#include "CommonFile.h"
#include "Globals.h"
#include "PathUtil.h"

IMPLEMENT_DYNAMIC(DinkInstallationDialog, CDialog)

DinkInstallationDialog::DinkInstallationDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DINK_INSTALLATION, pParent)
	, m_dinkExePath(dink_exe_path)
	, m_dinkCorePath(dink_core_path)
	, m_skeletonPath(dink_skeleton_path)
	, m_otherDmodsPath(dink_dmods_path)
{
}

DinkInstallationDialog::~DinkInstallationDialog()
{
}

void DinkInstallationDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_CORE_DMOD_PATH, m_dinkCorePath);
	DDV_MaxChars(pDX, m_dinkCorePath, 259);
	DDX_Text(pDX, IDC_DINK_EXE_PATH, m_dinkExePath);
	DDV_MaxChars(pDX, m_dinkExePath, 259);
	DDX_Text(pDX, IDC_SKELETON_DMOD_PATH, m_skeletonPath);
	DDV_MaxChars(pDX, m_skeletonPath, 259);
	DDX_Text(pDX, IDC_OTHER_DMOD_PATH, m_otherDmodsPath);
	DDV_MaxChars(pDX, m_otherDmodsPath, 259);
}


BEGIN_MESSAGE_MAP(DinkInstallationDialog, CDialog)
	ON_BN_CLICKED(IDC_BROWSE_DINK_EXE, &DinkInstallationDialog::OnClickedBrowseDinkExe)
	ON_BN_CLICKED(IDC_BROWSE_CORE_DMOD, &DinkInstallationDialog::OnClickedBrowseDinkCore)
	ON_BN_CLICKED(IDC_BROWSE_SKELETON_DMOD, &DinkInstallationDialog::OnClickedBrowseSkeletonDmod)
	ON_BN_CLICKED(IDC_BROWSE_OTHER_DMOD, &DinkInstallationDialog::OnClickedBrowseOtherDmod)
END_MESSAGE_MAP()


void DinkInstallationDialog::OnClickedBrowseDinkExe()
{
	UpdateData(TRUE);

	CFileDialog dialog(true, _T(".exe"), nullptr, OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, _T("Executable Files (*.exe)|*.exe|All Files (*.*)|*.*||"));
	
	dialog.m_ofn.lpstrTitle = _T("Locate the Dink executable...");

	CString initial_dir;
	if (Path_GetDirectoryOf(m_dinkExePath, initial_dir) && !initial_dir.IsEmpty())
		dialog.m_ofn.lpstrInitialDir = initial_dir.GetBuffer(0);

	if (dialog.DoModal() == IDOK)
	{
		m_dinkExePath = dialog.GetPathName();
		GuessMissingDinkInstallationPaths(m_dinkExePath, m_dinkCorePath, m_skeletonPath, m_otherDmodsPath);
		UpdateData(FALSE);
	}
}


void DinkInstallationDialog::OnClickedBrowseDinkCore()
{
	UpdateData(TRUE);

	CFolderPickerDialog dialog;
	dialog.m_ofn.lpstrTitle = _T("Locate the Dink core module (\"dink\" folder)...");

	CString initial_dir;
	if (Path_GetDirectoryOf(m_dinkCorePath, initial_dir) && !initial_dir.IsEmpty())
		dialog.m_ofn.lpstrInitialDir = initial_dir.GetBuffer(0);

	if (dialog.DoModal() == IDOK)
	{
		m_dinkCorePath = dialog.GetPathName();
		GuessMissingDinkInstallationPaths(m_dinkExePath, m_dinkCorePath, m_skeletonPath, m_otherDmodsPath);
		UpdateData(FALSE);
	}
}


void DinkInstallationDialog::OnClickedBrowseSkeletonDmod()
{
	UpdateData(TRUE);

	CFolderPickerDialog dialog;
	dialog.m_ofn.lpstrTitle = _T("Locate the skeleton dmod...");

	CString initial_dir;
	if (Path_GetDirectoryOf(m_skeletonPath, initial_dir) && !initial_dir.IsEmpty())
		dialog.m_ofn.lpstrInitialDir = initial_dir.GetBuffer(0);

	if (dialog.DoModal() == IDOK)
	{
		m_skeletonPath = dialog.GetPathName();
		GuessMissingDinkInstallationPaths(m_dinkExePath, m_dinkCorePath, m_skeletonPath, m_otherDmodsPath);
		UpdateData(FALSE);
	}
}


void DinkInstallationDialog::OnClickedBrowseOtherDmod()
{
	UpdateData(TRUE);

	CFolderPickerDialog dialog;
	dialog.m_ofn.lpstrTitle = _T("Locate the folder where dmods live...");

	CString initial_dir;
	if (Path_GetDirectoryOf(m_otherDmodsPath, initial_dir) && !initial_dir.IsEmpty())
		dialog.m_ofn.lpstrInitialDir = initial_dir.GetBuffer(0);

	if (dialog.DoModal() == IDOK)
	{
		m_otherDmodsPath = dialog.GetPathName();
		GuessMissingDinkInstallationPaths(m_dinkExePath, m_dinkCorePath, m_skeletonPath, m_otherDmodsPath);
		UpdateData(FALSE);
	}
}
