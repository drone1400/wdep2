#pragma once
#include "resource.h"

class SpriteLibraryEntry : public CDialog
{
public:
	SpriteLibraryEntry(CWnd* pParent = nullptr);   // standard constructor

	enum { IDD = IDD_SPRITE_LIBRARY };
	CString	m_sprite_name;
	TCHAR* DialogTitle;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	BOOL OnInitDialog() override;
};