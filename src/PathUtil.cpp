#include "stdafx.h"
#include "PathUtil.h"

// known path prefixes that need to be preserved during normalization..
static CString PathKnownPrefixes[6] {
	_T("\\\\?\\UNC\\"),
	_T("\\\\.\\UNC\\"),
	_T("\\\\?\\"),
	_T("\\\\.\\"),
	_T("\\\\"),
	_T("\\"),
};

static bool PathHasPrefix(CString const inStr, CString const prefix)
{
	int prefixLen = prefix.GetLength();
	int inLen = inStr.GetLength();
	
	if (inLen < prefixLen) return false;

	for (int i = 0; i < prefixLen; i++)
	{
		if (inStr[i] != prefix[i]) return false;
	}

	return true;
}

int PathGetTokens(CString inStr, CString * tokensOut)
{
	// sanity check input length
	if (inStr.GetLength() >= MAX_PATH) return 0;

	// store prefix in first token out position
	// if no prefix, will be empty string
	tokensOut[0].SetString(_T(""));
	for (int i = 0; i < 6; i++)
	{
		if (PathHasPrefix(inStr,PathKnownPrefixes[i]))
		{
			tokensOut[0].SetString(PathKnownPrefixes[i]);
			int inLen = inStr.GetLength();
			inStr = inStr.Right(inLen-tokensOut[0].GetLength());
			break;
		}
	}

	int tidx = 1;
	TCHAR * context;
	TCHAR * result;
	result = _tcstok_s(inStr.GetBuffer(), DIRSEPS,&context);
	while (result != nullptr)
	{
		// we have a token!
		if (_tcscmp(result,_T("..")) == 0)
		{
			// token is go back a directory... try to go back?...
			if (tidx == 1)
			{
				// can't go back further, just add the ".." token
				tokensOut[tidx++].SetString(result);
			} else if (_tcscmp(tokensOut[tidx-1],_T("..")) != 0)
			{
				// skip previous token by overwriting it with the next one
				tidx--;
			} else
			{
				// previous token is another ".." token, just add the new one
				tokensOut[tidx++].SetString(result);
			}
		}
		else if (_tcscmp(result,_T(".")) != 0)
		{
			// token is different from current directory, add it!
			tokensOut[tidx++].SetString(result);
		} 
		result = _tcstok_s(nullptr, DIRSEPS, &context);
	}

	// return token count minus the prefix which takes one slot...
	return tidx - 1;
}

bool Path_Normalize(CString inStr, CString & outStr)
{
	// initialize out string to empty
	outStr.SetString(_T(""));
	
	// sanity check input length
	if (inStr.GetLength() >= MAX_PATH) return false;
	
	// NOTE: drone1400 - tokens will have a pointer to each distinct non empty path token from the original path
	// since to have a token you must have character and a directory separator character, the max number of tokens
	// we can have is at most half of max path length...
	CString tokens[MAX_PATH / 2];
	int tokenCount = PathGetTokens(inStr,  tokens);
	
	if (tokenCount <= 0) return false;

	// initialize out str with prefix
	outStr.SetString(tokens[0]);
	// append first token
	outStr.Append(tokens[1]);

	for (int i = 2; i <= tokenCount; i++)
	{
		// append a directory separator followed by the token
		outStr.AppendChar(DIRSEPC);
		outStr.Append(tokens[i]);
	}

	return true;
}

bool Path_MakeRelative(CString baseStr, CString absoluteStr, CString & relativeStr)
{
	relativeStr.SetString(absoluteStr);
	
	// sanity check input string lengths
	if (baseStr.GetLength() >= MAX_PATH) return false;
	if (absoluteStr.GetLength() >= MAX_PATH) return false;

	CString tokensBase[MAX_PATH / 2];
	CString tokensAbs[MAX_PATH / 2];

	int tokCntBase = PathGetTokens(baseStr, tokensBase);
	int tokCntAbs = PathGetTokens(absoluteStr, tokensAbs);

	// return false if couldn't tokenize paths...
	if (tokCntBase <= 0 || tokCntAbs <= 0) return false;

	// zero position will have path prefix, ignore that...

	// find until what token index the paths match
	int matchIdx;
	for (matchIdx = 1; matchIdx <= tokCntBase && matchIdx <= tokCntAbs; matchIdx++)
	{
		if (_tcscmp(tokensBase[matchIdx], tokensAbs[matchIdx]) != 0)
		{
			break;
		}
	}

	matchIdx--;

	if (matchIdx <= 1) return false;
	
	relativeStr.SetString(_T(""));

	// travel back from base path to match index...
	for (int i = matchIdx + 1; i <= tokCntBase; i++)
	{
		relativeStr.Append(_T(".."));
		relativeStr.AppendChar(DIRSEPC);
	}

	// travel forward in absolute path from match index
	for (int i = matchIdx +1; i <= tokCntAbs; i++)
	{
		relativeStr.Append(tokensAbs[i]);
		if (i < tokCntAbs)
		{
			relativeStr.AppendChar(DIRSEPC);
		}
	}

	return true;
}

bool Path_MakeAbsolute(CString relativeStr, CString absoluteStr, CString & outStr)
{
	// initialize out string to empty
	outStr.SetString(_T(""));

	// sanity check input length
	if (relativeStr.GetLength() >= MAX_PATH) return false;
	
	// if in string is rooted, it can not be made absolute as it already is
	if (Path_IsRooted(relativeStr)) return false;
	if (!Path_IsRooted(absoluteStr)) return false;

	CString temp;
	temp.SetString(absoluteStr);
	temp.AppendChar(DIRSEPC);
	temp.Append(relativeStr);

	// normalize path just in case
	return Path_Normalize(temp,outStr);
}

bool Path_IsRooted(CString str)
{
	if (str.IsEmpty()) return false;
	PathStripToRoot(str.GetBuffer());
	return PathIsRoot(str.GetBuffer());
}

bool Path_ConcatIntoAndNormalize(
	CString & destination,
	CString const& name1,
	CString const& name2 /*= _T("")*/,
	CString const& name3 /*= _T("")*/,
	CString const& name4 /*= _T("")*/)
{
	destination.SetString(name1);
	if (!name2.IsEmpty())
	{
		destination.AppendChar(DIRSEPC);
		destination.Append(name2);
	}
	if (!name3.IsEmpty())
	{
		destination.AppendChar(DIRSEPC);
		destination.Append(name3);
	}
	if (!name4.IsEmpty())
	{
		destination.AppendChar(DIRSEPC);
		destination.Append(name4);
	}
	return Path_Normalize(destination, destination);
}

bool Path_GetDirectoryOf(CString const inPath, CString & outPath)
{
	CString tempPath;

	outPath.SetString(_T(""));
	
	if (!Path_Normalize(inPath, tempPath)) return false;
	
	int lastDirSep = tempPath.ReverseFind(DIRSEPC);
	if (lastDirSep > 0)
	{
		outPath = tempPath.Left(lastDirSep);
		return true;
	}
	
	return false;
}

bool Path_GetLastToken(CString const inPath, CString & outName)
{
	CString tempPath;

	outName.SetString(_T(""));
	
	if (!Path_Normalize(inPath, tempPath)) return false;
	
	int lastDirSep = tempPath.ReverseFind(DIRSEPC);
	if (lastDirSep > 0)
	{
		outName = tempPath.Right(tempPath.GetLength() - lastDirSep - 1);
		return true;
	}
	
	outName.SetString(tempPath);
	return true;
}

int Path_IsDestinationChildOfSourceOrSame(CString source, CString destination)
{
	if (!Path_Normalize(source,source)) return -1;
	if (!Path_Normalize(destination,destination)) return -1;

	// note: paths must be rooted to determine result properly...
	if (!Path_IsRooted(source)) return -1;
	if (!Path_IsRooted(destination)) return -1;

	int lenSrc = source.GetLength();
	int lenDst = destination.GetLength();

	if (lenSrc > lenDst) return 0; // destination can not be child or same if source has greater length...

	for (int i = 0; i < lenSrc; i++)
	{
		if (destination[i] != source[i]) return 0;
	}

	if (lenSrc == lenDst) return 1;                // if we got here, the paths match...

	if (destination[lenSrc] == DIRSEPC) return 1;  // if we got here, the destination is a child of source...

	// if we got here, the two paths seem similar, but the last token is actually different...
	return 0;
}



bool FileReadable(const TCHAR* path)
{
	FILE* f = _tfopen(path, _T("rt"));
	if (f == nullptr)
		return false;

	fclose(f);
	return true;
}

bool FileWritable(const TCHAR* path)
{
	FILE* f = _tfopen(path, _T("a"));
	if (f == nullptr)
		return false;

	fclose(f);
	return true;
}

bool FolderExists(const TCHAR* path)
{
	if (_taccess(path, 4 /*read mode*/) == 0)
	{
#ifdef WDEP_WIN_XP
		// NOTE: for some reason, under the windows XP version i used to test this,
		// the status.st_mode doesn't get the S_IFDIR bit set, so just return true here...
		// TODO: maybe find some other way to check that the given path was actually a directory and not a file?...
		return true;
#endif
		struct _stat status;
		_tstat(path, &status);

		if (status.st_mode & S_IFDIR)
			return true;
	}

	return false;
}