#pragma once
#include "resource.h"

class CScriptDialog : public CDialog
{
	DECLARE_DYNAMIC(CScriptDialog)

public:
	CScriptDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CScriptDialog();

// Dialog Data
	enum { IDD = IDD_SCRIPT_DIALOG };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClickedBrowseScript();
	CString m_script;
};