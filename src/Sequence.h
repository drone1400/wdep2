#pragma once
#include "Const.h"
class Sprite;
struct GRAPHICQUE;

#define UNKNOWN_GRAPHIC		0
#define DMOD_GRAPHIC		1
#define DINK_GRAPHIC		2
#define GRAPHIC_NOT_FOUND	3

#define SET_FRAME_FRAME_MAX_RECURSE_DEPTH 256

struct FRAME_INFO
{
	int frame_delay;
	int center_x;
	int center_y;
	int left_boundary;
	int top_boundary;
	int right_boundary;
	int bottom_boundary;
};

struct SET_FRAME_FRAME
{
	int source_seq;
	int source_frame;
	int dest_seq;
	int dest_frame;
};

enum ClipRenderReturn
{
    ClipRender_Ok,
    ClipRender_BadFrameData,
    ClipRender_OutOfBounds,
};


class Sequence
{
public:
	Sequence(int sequence_num, char* graphics_path, int frame_delay, int type, int center_x, int center_y, 
				   int left_boundary, int top_boundary, int right_boundary, int bottom_boundary, bool now);
	~Sequence();

	bool addFrameInfo(int frame_num, int center_x, int center_y, int left, int top, int right, int bottom);
	FILE* extractBitmap(const TCHAR* bitmap_file);
	bool loadFrame(int cur_frame, int search_depth = 0);
	bool loadFrame(int cur_frame, const TCHAR* bmp_file);

	bool getBounds(int frame, int size, RECT &rect);
	static bool getBoundsFallback(int frame, int size, RECT &rect);
	bool getCenter(int frame, int& center_x, int& center_y);

	bool renderFrameForSpriteSelector(int x, int y, int cur_frame);
	ClipRenderReturn clipRender(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect, int sprite_num, bool draw_info);
	static bool clipRenderFallback(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect, int sprite_num, bool draw_info);
	static bool clipRenderCommon(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect, int sprite_num, bool draw_info,
	    int frame_width, int frame_height, SDL_Texture * texture, RECT & bounds, bool ignore_trim);
	int drawSpriteInfo(int x_offset, int y_offset, Sprite* cur_sprite, int sprite_num);
	static int drawSpriteInfoFallback(int x_offset, int y_offset, Sprite* cur_sprite, int sprite_num);
	static int drawSpriteInfoCommon(Sprite* cur_sprite, int sprite_num, RECT & bounds);
	int clipRenderHardness(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect);
	
	// for cases where the frame info is different
	FRAME_INFO* frame_info[MAX_FRAMES];

	int set_frame_delay[MAX_FRAMES];
	int set_frame_special[MAX_FRAMES];
	int set_frame_frame_seq[MAX_FRAMES];
	int set_frame_frame_frame[MAX_FRAMES];
	
	TCHAR graphics_path[MAX_PATH];

	// stuff that can be different for every frame
	short frame_delay;
	int center_x;
	int center_y;
	int left_boundary;
	int top_boundary;
	int right_boundary;
	int bottom_boundary;
	int size;
	int type;
	int frame_width[MAX_FRAMES];
	int frame_height[MAX_FRAMES];
	bool now;

	// same for every frame
	SDL_Texture* frame_image[MAX_FRAMES];

private:
	int sequence_num;
	ColorKey color_key;

	int graphic_location;
};