#include "stdafx.h"
#include "FastfileExtract.h"
#include "Common.h"
#include "Globals.h"
#include "MainFrm.h"
#include "Map.h"
using std::vector;


IMPLEMENT_DYNAMIC(FastfileExtract, CDialog)

FastfileExtract::FastfileExtract(CWnd* pParent /*=nullptr*/)
	: CDialog(FastfileExtract::IDD, pParent)
{
	m_delete_dirff = FALSE;
}

FastfileExtract::~FastfileExtract()
{
}

void FastfileExtract::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_DELETE_DIRFF, m_delete_dirff);
}


BEGIN_MESSAGE_MAP(FastfileExtract, CDialog)
	ON_BN_CLICKED(IDOK, &FastfileExtract::OnBnClickedOk)
END_MESSAGE_MAP()

void FastfileExtract::OnBnClickedOk()
{
	UpdateData();

	TCHAR path[MAX_PATH];
	wsprintf(path, _T("%sGraphics\\"), (LPCTSTR)current_map->dmod_path);

	mainWnd->setStatusText(_T("Extracting..."));
	BeginWaitCursor();
	int err = FFExtract(path);
	EndWaitCursor();
	mainWnd->setStatusText(_T("Done extracting graphics!"));

	if (err > 0)	//if some error happened while extracting
	{
		CString fastFileExtractError;
		fastFileExtractError.Format(_T("%d error(s) happened while extracting graphics."), err);
		if (m_delete_dirff)
			fastFileExtractError.Append(_T("\nNot every dir.ff has been deleted."));
		MB_ShowError(fastFileExtractError);
	}

	CDialog::OnOK();
}

//returns number of errors
int FastfileExtract::FFExtract(const TCHAR* path)
{
	HANDLE hFind;
	WIN32_FIND_DATA dataFind;
	BOOL moreFiles = true;
	int err = 0;
	TCHAR buffer[MAX_PATH];

	//Get all the directories...
	_tcscpy_s(buffer, path);
	_tcscat_s(buffer, _T("*.*"));
	hFind = FindFirstFile(buffer, &dataFind);	
	while (hFind != INVALID_HANDLE_VALUE && moreFiles)
	{
		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 && dataFind.cFileName[0] != '.')
		{
			_tcscpy_s(buffer, path);
			_tcscat_s(buffer, dataFind.cFileName);
			_tcscat_s(buffer, _T("\\"));

			err += FFExtract(buffer);
		}

		moreFiles = FindNextFile(hFind, &dataFind);
	}
	FindClose(hFind);

	vector<Img> files;		
	FILE* fin;
	FILE* fout;
	int n, i;

	_tcscpy_s(buffer, path);
	_tcscat_s(buffer, _T("dir.ff"));

	//Show the user that something is actually happening
	mainWnd->setStatusText(buffer);

	if (_tfopen_s(&fin, buffer, _T("rb")) == EINVAL) return 0;
	if (fin == nullptr) return 0;

	fread(&n, sizeof(int), 1, fin);

	int maxlen = 0;
	Img file;
	for (i=0; i<n; i++)
	{		
		fread (&file, 17, 1, fin);
		files.push_back(file);

		if (i != 0 && file.offset - files[i-1].offset > maxlen) 
			maxlen = file.offset - files[i-1].offset;
	}

	BYTE* readbuffer = new BYTE[maxlen];

	for (i=0; i<n-1; i++)
	{
		//if dir.ff was accidentally included in itself, then make sure it is preserved
		if (_stricmp(files[i].name.s, "dir.ff") == 0) continue;

		int len = files[i+1].offset - files[i].offset;
		fseek(fin, files[i].offset, SEEK_SET);

		_tcscpy_s(buffer, path);
		_tcscat_s(buffer, files[i].name.as_tchar());

		if ((fout = _tfopen(buffer, _T("wb"))) == nullptr)
		{
			err++;
			continue;
		}

		fread (readbuffer, len, 1, fin);
		fwrite (readbuffer, len, 1, fout);
		fclose (fout);
	}

	delete [] readbuffer;
	fclose(fin);

	if (err > 0) return err;

	if (m_delete_dirff)
	{
		_tcscpy_s(buffer, path);
		_tcscat_s(buffer, _T("dir.ff"));
		_tremove(buffer);
	}

	return 0;
}