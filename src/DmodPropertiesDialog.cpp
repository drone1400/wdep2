#include "stdafx.h"
#include "DmodPropertiesDialog.h"
#include "CommonFile.h"
#include "Map.h"
#include "Globals.h"

DmodPropertiesDialog::DmodPropertiesDialog(CWnd* pParent /*=nullptr*/) : CDialog(DmodPropertiesDialog::IDD, pParent)
{
	m_author = _T("");
	m_description = _T("");
	m_dmod_title = _T("");
	m_email_website = _T("");
	
	DmodDizRead(current_map->dmod_path.GetBuffer(0), m_dmod_title, m_author, m_email_website, m_description);
}

void DmodPropertiesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
	DDX_Text(pDX, IDC_AUTHOR, m_author);
	DDX_Text(pDX, IDC_DMOD_DESCRIPTION, m_description);
	DDX_Text(pDX, IDC_DMOD_TITLE, m_dmod_title);
	DDX_Text(pDX, IDC_EMAIL_WEBSITE, m_email_website);
}

BEGIN_MESSAGE_MAP(DmodPropertiesDialog, CDialog)
END_MESSAGE_MAP()

void DmodPropertiesDialog::OnOK() 
{
	UpdateData();
	
	DmodDizWrite(current_map->dmod_path.GetBuffer(0), m_dmod_title, m_author, m_email_website, m_description);
	
	CDialog::OnOK();
}

BOOL DmodPropertiesDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_DMOD_TITLE)->SetFocus();

	return FALSE;
}