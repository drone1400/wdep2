#include "StdAfx.h"
#include "resource.h"
#include "Globals.h"
#include "Map.h"
#include "Colors.h"
#include "CommonSdl.h"

using std::vector;
using std::ifstream;
using std::ofstream;
using std::endl;

// -------------------------------------------------------------------------------------------------------------------------------------
// SDL Helper functions
// -------------------------------------------------------------------------------------------------------------------------------------

// prints the destination screen to the specified .bmp file in 24 bit color
bool PrintDmodScreen(const TCHAR *filename)
{
	const int format = SDL_PIXELFORMAT_RGB24;

	int w, h;
	if (SDL_GetRendererOutputSize(sdl_renderer, &w, &h) != 0)
		return false;

	vector<BYTE> pixels;
	pixels.resize(w * h * SDL_BYTESPERPIXEL(format));
	
	if (SDL_RenderReadPixels(sdl_renderer, nullptr, format, pixels.data(), w * SDL_BYTESPERPIXEL(format)) != 0)
		return false;

	SDL_Surface* surface = SDL_CreateRGBSurfaceWithFormatFrom(pixels.data(), w, h, SDL_BITSPERPIXEL(format), w * SDL_BYTESPERPIXEL(format), format);
	if (!surface)
		return false;

	//We manually convert the string to utf-8 so we can use SDL_RWfromFile. We prefer this over enabling stdio support in SDL and using SDL_RWfromFP, even tho that would be nicer.
	char utf8_filename[MAX_PATH];
#ifdef UNICODE
	WideCharToMultiByte(CP_UTF8, 0, filename, -1, utf8_filename, MAX_PATH, nullptr, nullptr);
#else
	strcpy_s(utf8_filename, filename);
#endif

	if (SDL_SaveBMP_RW(surface, SDL_RWFromFile(utf8_filename, "wb"), SDL_TRUE) != 0)
	{
		SDL_FreeSurface(surface);
		return false;
	}

	SDL_FreeSurface(surface);
	return true;
}

static SDL_Texture* font_texture; //the texture we copy characters out of when drawing text

bool loadBitmapFont()
{
	HMODULE module = GetModuleHandleW(nullptr);
	if (!module)
		return false;

	HRSRC font_resource_info = FindResource(module, MAKEINTRESOURCE(IDR_BITMAP_FONT), _T("BINARY"));
	if (!font_resource_info)
		return false;

	HGLOBAL font_resource = LoadResource(module, font_resource_info);
	if (!font_resource)
		return false;

	DWORD font_resource_size = SizeofResource(module, font_resource_info);
	if (font_resource_size != 128 * 128)
	{
		assert(!"Bitmap font resource found, but has wrong size!");
		return false;
	}

	BYTE* font_pixels = (BYTE*)LockResource(font_resource);
	if (!font_pixels)
		return false;

	SDL_Surface* font_surface = SDL_CreateRGBSurfaceWithFormatFrom(font_pixels, 128, 128, 8, 128, SDL_PIXELFORMAT_INDEX8);
	if (!font_surface)
		return false;

	SDL_SetColorKey(font_surface, SDL_TRUE, 0);

	SDL_Color font_colors[2] = { SDL_Color{ 0, 0, 0, 0 }, SDL_Color{ 255, 255, 255, 255 } };
	SDL_Palette* font_palette = SDL_AllocPalette(2);
	SDL_SetPaletteColors(font_palette, font_colors, 0, 2);
	SDL_SetSurfacePalette(font_surface, font_palette);

	font_texture = SDL_CreateTextureFromSurface(sdl_renderer, font_surface);
	if (!font_texture)
		return false;

	SDL_FreePalette(font_palette);
	SDL_FreeSurface(font_surface);

	return true;
}

void unloadBitmapFont()
{
	SDL_DestroyTexture(font_texture);
}

void drawText(const TCHAR *text, int x, int y, SDL_Color colorFore, SDL_Color colorBack)
{
	// save old blend mode so we can restore later...
	SDL_BlendMode blendModeOld;
	SDL_GetRenderDrawBlendMode(sdl_renderer, &blendModeOld);

	// switch to SDL_BLENDMODE_BLEND for transparency
	SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);
	

	// measure font width
	int width = 0;
	for (const TCHAR* p0 = text; *p0; p0++)
	{
		width += 8;// 8 pixel wide font
	}
	if (width > 0)
	{
		// rectangle for background color
		SDL_Rect dst0 = { x-8, y-2, width+16, 20 };
		
		SDL_SetRenderDrawColor(sdl_renderer, colorBack.r, colorBack.g, colorBack.b, colorBack.a);

		SDL_RenderFillRect(sdl_renderer, &dst0);
	}

	SDL_SetTextureColorMod(font_texture, colorFore.r, colorFore.g, colorFore.b);

	SDL_Rect src = { 0, 0, 8, 16 };
	SDL_Rect dst = { x, y, 8, 16 };

	for (const TCHAR* p = text; *p; p++)
	{
		TCHAR c = *p;
		if (c >= 128)
			c = 0;

		src.x = (c % 16) * 8;
		src.y = (c / 16) * 16;
		SDL_RenderCopy(sdl_renderer, font_texture, &src, &dst);
		dst.x += 8; // 8 pixel wide font
	}

	SDL_SetRenderDrawBlendMode(sdl_renderer, blendModeOld);
}

bool fixBounds(RECT &dest_box, RECT &src_box, RECT &clip_box)
{
	if (dest_box.left < clip_box.left)
	{
		src_box.left += clip_box.left - dest_box.left;
		dest_box.left = clip_box.left;
	}

	if (dest_box.right > clip_box.right)
	{
		src_box.right -= dest_box.right - clip_box.right;
		dest_box.right = clip_box.right;
	}

	if (dest_box.left >= dest_box.right)
		return false;

	if (dest_box.top < clip_box.top)
	{
		src_box.top += clip_box.top - dest_box.top;
		dest_box.top = clip_box.top;
	}

	if (dest_box.bottom > clip_box.bottom)
	{
		src_box.bottom -= dest_box.bottom - clip_box.bottom;
		dest_box.bottom = clip_box.bottom;
	}

	if (dest_box.top >= dest_box.bottom)
		return false;

	return true;
}

bool fixBounds(RECT &box, RECT &clip_box)
{
	int temp; //used to hold for fixing boxes.

	//check if we are inverted, if so, correct.
	if (box.left > box.right)
	{
		temp = box.right;
		box.right = box.left;
		box.left = temp;
	}
	if (box.top > box.bottom)
	{
		temp = box.top;
		box.top = box.bottom;
		box.bottom = temp;
	}
	if (clip_box.left > clip_box.right)
	{
		temp = clip_box.right;
		clip_box.right = clip_box.left;
		clip_box.left = temp;
	}
	if (clip_box.top > clip_box.bottom)
	{
		temp = clip_box.top;
		clip_box.top = clip_box.bottom;
		clip_box.bottom = temp;
	}

	//actually clip the bounds.
	if (box.left < clip_box.left)
	{
		box.left = clip_box.left;
	}

	if (box.right > clip_box.right)
	{
		box.right = clip_box.right;
	}

	if (box.left >= box.right)
		return false;

	if (box.top < clip_box.top)
	{
		box.top = clip_box.top;
	}

	if (box.bottom > clip_box.bottom)
	{
		box.bottom = clip_box.bottom;
	}

	if (box.top >= box.bottom)
		return false;

	return true;
}

void drawBox(RECT box, SDL_Color color, int line_width/*=1*/)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));

	SDL_Rect sdl_box = RECT_to_SDL(box);

	//TOOD: maybe draw as 4 rects instead of this loop
	for (int i = 0; i < line_width; i++)
	{
		SDL_RenderDrawRect(sdl_renderer, &sdl_box);
		sdl_box.x -= 1;
		sdl_box.y -= 1;
		sdl_box.w += 2;
		sdl_box.h += 2;
	}
}

void drawBox(int x, int y, int width, int height, SDL_Color color, int line_width/*=1*/)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));

	SDL_Rect sdl_box = { x, y, width, height};

	//TOOD: maybe draw as 4 rects instead of this loop
	for (int i = 0; i < line_width; i++)
	{
		SDL_RenderDrawRect(sdl_renderer, &sdl_box);
		sdl_box.x -= 1;
		sdl_box.y -= 1;
		sdl_box.w += 2;
		sdl_box.h += 2;
	}
}

void drawFilledBox(RECT box, SDL_Color color)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));

	SDL_Rect sdl_box = RECT_to_SDL(box);

	SDL_RenderFillRect(sdl_renderer, &sdl_box);
}

void drawClearAll(SDL_Color color)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));
	SDL_RenderClear(sdl_renderer);
}

void drawLine(int x1, int y1, int x2, int y2, SDL_Color color)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));
	SDL_RenderDrawLine(sdl_renderer, x1, y1, x2, y2);
}

void drawX(int x1, int y1, int x2, int y2, SDL_Color color)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));
	SDL_RenderDrawLine(sdl_renderer, x1, y1, x2, y2);
	SDL_RenderDrawLine(sdl_renderer, x2, y1, x1, y2);
}

void drawX(RECT rect, SDL_Color color)
{
	SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));
	SDL_RenderDrawLine(sdl_renderer, rect.left, rect.top, rect.right, rect.bottom);
	SDL_RenderDrawLine(sdl_renderer, rect.right, rect.top, rect.left, rect.bottom);
}

void ScreenText(const TCHAR output[MAX_PATH], int y_offset)
{
	if (help_text)
		drawText(output, 10, current_map->window_height - y_offset, help_text_color, help_text_color_back);
}

SDL_Rect RECT_to_SDL(RECT rect)
{
	if (rect.left > rect.right)
		std::swap(rect.left, rect.right);

	if (rect.top > rect.bottom)
		std::swap(rect.top, rect.bottom);

	return SDL_Rect{ rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top };
}

bool drawTexture(SDL_Texture* texture, RECT* src, RECT* dest)
{
	SDL_Rect src_rect;
	SDL_Rect dest_rect;

	SDL_Rect* p_src = nullptr;
	SDL_Rect* p_dest = nullptr;

	if (src)
	{
		src_rect = RECT_to_SDL(*src);
		p_src = &src_rect;
	}

	if (dest)
	{
		dest_rect = RECT_to_SDL(*dest);
		p_dest = &dest_rect;
	}

	return SDL_RenderCopy(sdl_renderer, texture, p_src, p_dest) == 0;
}

bool drawTextureClipped(SDL_Texture* texture, RECT* src, RECT* dest, RECT* clip)
{
	SDL_Rect clip_rect;
	if (clip)
	{
		clip_rect = RECT_to_SDL(*clip);
		SDL_RenderSetClipRect(sdl_renderer, &clip_rect);
	}

	bool ret = drawTexture(texture, src, dest);

	SDL_RenderSetClipRect(sdl_renderer, NULL);

	return ret;
}