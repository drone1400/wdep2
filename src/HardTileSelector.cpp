#include "StdAfx.h"
#include "HardTileSelector.h"
#include "Map.h"
#include "Globals.h"
#include "Engine.h"
#include "Screen.h"
#include "Tile.h"
#include "MainFrm.h"
#include "Colors.h"
#include "Common.h"
#include "CommonSdl.h"
#include "SimpleLog.h"
#include "Undo.h"
#include "Actions/HardTileSetDefault.h"
#include "Actions/HardTileTransform.h"
#include "Actions/HardTileChange.h"
#include "Actions/HardTileEdit.h"
#include "Actions/HardTileMultipleChange.h"

HardTileSelector::HardTileSelector()
{
	selectorScreen = 0;
	tile_box_points = 0;

	screen_first_x_tile = -1;
	y_tiles_selected = 0;
	x_tiles_selected = 0;

	memset(hardTileSurface, 0, NUM_HARD_TILES * sizeof(hardTileSurface[0]));
	memset(hard_tile_set, 0, sizeof(hard_tile_set));
}

HardTileSelector::~HardTileSelector()
{

}

void HardTileSelector::getSelectionSize(int& xTiles, int& yTiles)
{
	xTiles = x_tiles_selected;
	yTiles = y_tiles_selected;
}


void HardTileSelector::getSelectedTileAt(int x, int y, int& hardIndex)
{
	hardIndex = -1;
	if (x < 0 || x >= x_tiles_selected || x >= SCREEN_TILE_WIDTH * 2 ||
		y < 0 || y >= y_tiles_selected || y >= SCREEN_TILE_HEIGHT * 2)
			// out of bounds, no index
			return;
	
	hardIndex=hard_tile_set[x][y];
}

void HardTileSelector::getCurrentTileHardIndex(int& hardIndex)
{
	hardIndex = cur_hard_num;
}

void HardTileSelector::getCurrentTileData(TILEDATA& data)
{
	data = cur_tile;
}



//called from MFC class
void HardTileSelector::SetDefaultHardTile()
{
	if (x_tiles_selected < 1 || y_tiles_selected < 1)
	{
		MB_ShowError(_T("Please copy a tile onto the clipboard before attempting to set a default hard tile."));
		return;
	}

	//get screen
	int cur_screen = MAP_COLUMNS * (screen_first_y_tile / SCREEN_TILE_HEIGHT) + screen_first_x_tile / SCREEN_TILE_WIDTH;

    // sanity check to prevent crash on empty screens and such...
    if (cur_screen < 0 || cur_screen >= NUM_SCREENS)
        return;
    if (current_map->screen[cur_screen] == nullptr)
        return;

	//get current tile
	int x_tile = screen_first_x_tile % SCREEN_TILE_WIDTH;
	int y_tile = screen_first_y_tile % SCREEN_TILE_HEIGHT;

	//get tile data
	TILEDATA tile = current_map->screen[cur_screen]->tiles[y_tile][x_tile];

    if (tile.alt_hardness == 0)
    {
        current_map->undo_buffer->PushAndDo<actions::HardTileSetDefault>(tileBmp[tile.bmp]->tile_hardness[tile.y][tile.x], hard_tile_set[0][0], tile.bmp, tile.x, tile.y);
    }
}

//called from MFC class
void HardTileSelector::TransformHardTile(BYTE hardness)
{
	//get screen
	int cur_screen = MAP_COLUMNS * (screen_first_y_tile / SCREEN_TILE_HEIGHT) + screen_first_x_tile / SCREEN_TILE_WIDTH;

    // sanity check to prevent crash on empty screens and such...
    if (cur_screen < 0 || cur_screen >= NUM_SCREENS)
        return;
    if (current_map->screen[cur_screen] == nullptr)
        return;

	//get current tile
	int x_tile = screen_first_x_tile % SCREEN_TILE_WIDTH;
	int y_tile = screen_first_y_tile % SCREEN_TILE_HEIGHT;

	int tile_num;

	if (current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness)
	{
		tile_num = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
	}
	else
	{
		TILEDATA tile = current_map->screen[cur_screen]->tiles[y_tile][x_tile];
		tile_num = tileBmp[tile.bmp]->tile_hardness[tile.y][tile.x];
	}
	
	current_map->undo_buffer->PushAndDo<actions::HardTileTransform>(tile_num, hardTile[tile_num], hardness);
}

// deletes the current hardness on a tile and reverts it back to it's natural hard tile
int HardTileSelector::revertTile()
{
	//is there any tiles selected?
	if (screen_first_x_tile == -1)
	{
		MB_ShowError(_T("You must click on one or more tile destinations before tile(s) can be placed."));
		return false;
	}

	//lets get the dimensions of what the user wants to paste
	//make sure we don't go out of bounds
	int width = screen_last_x_tile - screen_first_x_tile + 1;

	int height = screen_last_y_tile - screen_first_y_tile + 1;

	//add to undo buffer
	actions::HardTileChange action(screen_first_x_tile, screen_first_y_tile, width, height);

	int cur_screen, x_tile, y_tile, x, y;

	//save old tiles
	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++)
		{
			//check screen we could have moved onto a new screen, or we could still be on it.
			cur_screen = MAP_COLUMNS * ((y + screen_first_y_tile) / SCREEN_TILE_HEIGHT) + (x + screen_first_x_tile) / SCREEN_TILE_WIDTH;

			if (cur_screen > 768 || cur_screen < 1)
				continue;

			//make sure the screen is valid
			if (current_map->screen[cur_screen] == nullptr)
				continue;
			
			//zoom into the tile in question
			x_tile = (x + screen_first_x_tile) % SCREEN_TILE_WIDTH;
			y_tile = (y + screen_first_y_tile) % SCREEN_TILE_HEIGHT;

			//copy it over if it is there
			if (current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness)
			{
				action.old_hard_tiles[x][y] = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
			}
		}
	}

	//TODO
	current_map->undo_buffer->PushAndDo<actions::HardTileChange>(std::move(action));

	return true;
}

int HardTileSelector::renderScreenTileHover(int x, int y)
{
    POINT point{x, y};
    if (current_map->bindViewPoint(point) == false)
        return false;
    
	int x_screen = point.x / SCREEN_WIDTH_GAP;
	int y_screen = point.y / SCREEN_HEIGHT_GAP;

	int cur_screen = y_screen * MAP_COLUMNS + x_screen;

	if (current_map->screen[cur_screen] == nullptr)
		return false;

	int x_tile = (point.x % SCREEN_WIDTH_GAP) / TILEWIDTH;
	int y_tile = (point.y % SCREEN_HEIGHT_GAP) / TILEHEIGHT;

	int x_offset = x_screen * SCREEN_WIDTH_GAP + x_tile * TILEWIDTH - current_map->window.left;
	int y_offset = y_screen * SCREEN_HEIGHT_GAP + y_tile * TILEHEIGHT - current_map->window.top;

	RECT dest = {x_offset, y_offset, x_offset + TILEWIDTH, y_offset + TILEHEIGHT};
	drawBox(dest, color_tile_grid, 1);

	// possibly draw box
	for (int i = 1; i < tile_box_points; i++)
	{
		drawLine(tile_box_x[i - 1], tile_box_y[i - 1], tile_box_x[i], tile_box_y[i], color_polygon_fill_hardness);
	}

	if (tile_box_points > 0 && tile_box_points < MAX_TILE_BOX_POINTS)
	{
		drawLine(mouse_x_position, mouse_y_position, tile_box_x[0], tile_box_y[0], color_polygon_fill_hardness);
	}

	if (tile_box_points == MAX_TILE_BOX_POINTS)
	{
		drawLine(tile_box_x[0], tile_box_y[0], 
			tile_box_x[MAX_TILE_BOX_POINTS - 1], tile_box_y[MAX_TILE_BOX_POINTS - 1], color_polygon_fill_hardness);

		placeTileBox();
	}

	if (tile_box_points < MAX_TILE_BOX_POINTS && tile_box_points > 1)
	{
		drawLine(mouse_x_position, mouse_y_position, 
			tile_box_x[tile_box_points - 1], tile_box_y[tile_box_points - 1], color_polygon_fill_hardness);
	}

	return true;
}

int HardTileSelector::nextPage()
{
	if ((selectorScreen + 1) * max_pics < NUM_HARD_TILES)
		selectorScreen++;

	return true;
}

int HardTileSelector::prevPage()
{
	if (selectorScreen > 0)
		selectorScreen--;

	return true;
}

int HardTileSelector::resizeScreen()
{
	tiles_per_row = current_map->window_width / (TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP);
	tiles_per_column = current_map->window_height / (TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP);

	max_pics = tiles_per_row * tiles_per_column;

	selectorScreen = 0;

	return true;
}

int HardTileSelector::render()
{
	//+1 so it wont display 0. 0 is bad...
	pics_displayed = (selectorScreen * max_pics) + 1;
	
	for (int y = 0; (y < tiles_per_column) && (pics_displayed < NUM_HARD_TILES); y++)
	{
		for (int x = 0; (x < tiles_per_row) && (pics_displayed < NUM_HARD_TILES); x++)
		{
			renderHardness(x * (TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP), 
				y * (TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP),
				TILEWIDTH, TILEHEIGHT,
				pics_displayed, false);
			pics_displayed++;
		}
	}

	return true;
}

void HardTileSelector::drawHardTileSelectorGrid(int x, int y)
{
	int x_tile = x / (TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP);
	int y_tile = y / (TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP);

	int pic_selected = y_tile * tiles_per_row + x_tile + selectorScreen * max_pics;

	if (pic_selected >= NUM_HARD_TILES)
		return;

	RECT dest = {(TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP) * x_tile - HARD_TILE_SELECTOR_BMP_GAP,
			(TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP) * y_tile - HARD_TILE_SELECTOR_BMP_GAP,
			(TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP) * x_tile + TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP,
			(TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP) * y_tile + TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP};
	drawBox(dest, color_tile_grid, HARD_TILE_SELECTOR_BMP_GAP);

	cur_hard_num = pic_selected + 1;
}

void HardTileSelector::hardTileEditorPrepareTile(int hardIndex, TILEDATA tileData)
{
	if (hardIndex < 0 || hardIndex > NUM_HARD_TILES)
		hardIndex = 0;
	
	cur_hard_num = hardIndex;
	cur_hardness = hardTile[hardIndex];
	cur_tile = tileData;
}


void HardTileSelector::hardTileEditorLoadFromScreen()
{
	current_map->editor_state = EditorState::TileHardnessEditor;
	current_map->hardTileSelectorReturnState = EditorState::Screen;
	
	if (screen_first_x_tile < 1 || screen_first_y_tile < 1)
	{
		MB_ShowError(_T("You must select a tile before trying to edit it."));
		current_map->editor_state = EditorState::Screen;
		return;
	}

	//get screen
	int cur_screen = MAP_COLUMNS * (screen_first_y_tile / SCREEN_TILE_HEIGHT) + screen_first_x_tile / SCREEN_TILE_WIDTH;

    // sanity check to prevent crash on empty screens and such...
    if (cur_screen < 0 || cur_screen >= NUM_SCREENS)
        return;
    if (current_map->screen[cur_screen] == nullptr)
        return;

	//get current tile
	int x_tile = screen_first_x_tile % SCREEN_TILE_WIDTH;
	int y_tile = screen_first_y_tile % SCREEN_TILE_HEIGHT;

	//get tile data
	if (current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness == 0)
	{
		TILEDATA temp = current_map->screen[cur_screen]->tiles[y_tile][x_tile];
		cur_hard_num = tileBmp[temp.bmp]->tile_hardness[temp.y][temp.x];
	}
	else
	{
		cur_hard_num = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
	}

	//
	hardTileEditorPrepareTile(cur_hard_num, current_map->screen[cur_screen]->tiles[y_tile][x_tile]);

	//This is a bit crazy: we draw the editor here to temp_texture, and then later Game_Main() will call displayEditor(),
	//which won't call DrawEditor(), but it'll copy temp_texture to the screen.
	// drone: we don't actually need to do this here, calling displayEditor() will do this anyway!
	//SDL_SetRenderTarget(sdl_renderer, temp_texture);
	//drawHardTile(hardedit_show_texture);
}

void HardTileSelector::drawHardTileToTempTexture(BOOL enableTileTexture)
{
	// save old render target
	SDL_Texture * oldTarget = SDL_GetRenderTarget(sdl_renderer);

	// save old blend mode so we can restore later...
	SDL_BlendMode blendModeOld;
	SDL_GetRenderDrawBlendMode(sdl_renderer, &blendModeOld);

	// switch to SDL_BLENDMODE_BLEND for transparency
	SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);
	try
	{
		// render to temp texture
		SDL_SetRenderTarget(sdl_renderer, temp_texture);

		//we draw the editor at a 1:1 scale, and then whoever calls us will do the upscaling to the screen size
		RECT dest_rect = {0, 0, TILEWIDTH, TILEHEIGHT};

		// draw black background first, if texture is missing, then this will also serve as a fallback
		SDL_Rect sdl_dest_rect = RECT_to_SDL(dest_rect);
		SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color_hard_edit_notex));
		SDL_RenderFillRect(sdl_renderer, &sdl_dest_rect);
		
		if (cur_tile.bmp >= 0 && cur_tile.bmp < 41 && enableTileTexture)
		{
			tileBmp[cur_tile.bmp]->render(dest_rect, cur_tile.x, cur_tile.y);
		}
	
		for (int x = 0; x < TILEWIDTH; x++)
		{
			for (int y = 0; y < TILEHEIGHT; y++)
			{
				bool draw = true;
				SDL_Color color;

				switch (cur_hardness[y][x])
				{
				case 1:
					color = color_normal_tile_hardness;
					break;
				case 2:
					color = color_low_tile_hardness;
					break;
				case 3:
					color = color_other_tile_hardness;
					break;
				default:
					draw = false;
					break;
				}
				color.a = hardness_alpha_tile;
				if (draw)
				{
					SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color));
					SDL_RenderDrawPoint(sdl_renderer, x, y);
				}
			}
		}
	}
	catch (...)
	{
		// note: this would probably spam the log, but at the same time it should never really happen during any release build...
		Log_Error(_T("RENDERING - Some weird shit happened during drawHardTileToTempTexture..."));
	}

	// restore render target
	SDL_SetRenderTarget(sdl_renderer, oldTarget);

	SDL_SetRenderDrawBlendMode(sdl_renderer, blendModeOld);
}

void HardTileSelector::hardTileEditorRender(int x, int y, BOOL enableTileTexture)
{
	int box_width = (current_map->window_width / TILEWIDTH);
	int box_height = (current_map->window_height / TILEHEIGHT);
	// truncate box size to square aspect ratio
	int box_min_size = box_width < box_height ? box_width : box_height;	
	box_width = box_min_size;
	box_height = box_min_size;

	// src rectangle for tile texture
	RECT src_rect = {0, 0, TILEWIDTH, TILEHEIGHT};

	// top left aligned dest rectangle
	RECT dest_rect;
	dest_rect.top = 0;
	dest_rect.left = 0;
	dest_rect.bottom = box_height * TILEHEIGHT;
	dest_rect.right = box_width * TILEWIDTH;

	// offset for centering dest
	int offsetX = (current_map->window_width - dest_rect.right)/2;
	int offsetY = (current_map->window_height - dest_rect.bottom)/2;

	// correct dest with offset
	dest_rect.left += offsetX;
	dest_rect.right += offsetX;
	dest_rect.top += offsetY;
	dest_rect.bottom += offsetY;
	
	// x/y tile pixel coordinates
	int x_tile = (x-offsetX) / box_width;
	int y_tile = (y-offsetY) / box_height;

	if (cur_hard_num > 0 && ((KEY_DOWN(VK_LBUTTON) || KEY_DOWN(VK_MBUTTON)) && draw_box))
	{
		for (x = 0; x < tile_brush_size; x++)
		{
			for (y = 0; y < tile_brush_size; y++)
			{
				if ((y_tile + y) >= TILEHEIGHT || (x_tile + x) >= TILEWIDTH ||
					(y_tile + y) < 0 || (x_tile + x) < 0)
					continue;

				if (drag_hardness == -1)
				{
					int cur_hard = int (cur_hardness[y_tile+y][x_tile+x]);

					switch(cur_hard)
					{
						default:
						case 0:
							drag_hardness = KEY_DOWN(VK_CONTROL) ? 2 : KEY_DOWN(VK_SHIFT) ? 3 : 1;
							break;
						case 1:
							drag_hardness = KEY_DOWN(VK_CONTROL) ? 2 : KEY_DOWN(VK_SHIFT) ? 3 : 0;
							break;
						case 2:
							drag_hardness = KEY_DOWN(VK_CONTROL) ? 0 : KEY_DOWN(VK_SHIFT) ? 3 : 1;
							break;
						case 3:
							drag_hardness = KEY_DOWN(VK_CONTROL) ? 2 : KEY_DOWN(VK_SHIFT) ? 0 : 1;
					}
				}
				
				cur_hardness[y_tile+y][x_tile+x] = BYTE(drag_hardness);
			}
		}
	}
	else
	{
		drag_hardness = -1;
	}

	// draw hard tile on temporary texture
	if (cur_hard_num > 0)
	{
		drawHardTileToTempTexture(enableTileTexture);
		// draw temporary texture on screen
		drawTexture(temp_texture, &src_rect, &dest_rect);
		// draw border
		drawBox(dest_rect, color_hard_edit_border);
	} else
	{
		// hard tile #0 is reserved!
		drawX(dest_rect, color_tile_sel_error);
		drawBox(dest_rect, color_tile_sel_error);
	}

	// draw cursor
	RECT dest_hover = {
		offsetX + box_width * x_tile,
		offsetY + box_height * y_tile,
		offsetX + (box_width * x_tile) + (box_width * tile_brush_size),
		offsetY + (box_height * y_tile) + (box_height * tile_brush_size)
	};
	drawBox(dest_hover, color_hard_edit_cursor);
}

void HardTileSelector::hardTileEditorClearAllHardness()
{
	if (cur_hard_num > 0)
	{
		for (int x = 0; x < TILEWIDTH; x++)
		{
			for (int y = 0; y < TILEHEIGHT; y++)
			{
				cur_hardness[y][x] = 0x00;
			}
		}
	}
}


void HardTileSelector::hardTileEditorSaveAndReturn()
{
    current_map->editor_state = current_map->hardTileSelectorReturnState;
    
    // sanity check...
    if (cur_hard_num <= 0 || cur_hard_num > NUM_HARD_TILES)
        return;

	for (int x = 0; x < 50; x++)
	{
		for (int y = 0; y < 50; y++)
		{
			if (hardTile[cur_hard_num][x][y] != cur_hardness[x][y])
			{
				current_map->undo_buffer->PushAndDo<actions::HardTileEdit>(cur_hard_num, hardTile[cur_hard_num], cur_hardness);
			}
		}
	}
}

void HardTileSelector::hardTileEditorDiscardAndReturn()
{
	// NOTE: don't actually need to reset current hardness data, it will be reset when entering hard edit mode again
	current_map->editor_state = current_map->hardTileSelectorReturnState;
}


int HardTileSelector::getTile(int x, int y)
{
	tile.bmp = -1;
	// calculate the x and y location of the frame or sequence
	int x_tile = x / (TILEWIDTH + HARD_TILE_SELECTOR_BMP_GAP);
	int y_tile = y / (TILEHEIGHT + HARD_TILE_SELECTOR_BMP_GAP);

	// calculate the sequence number using number of sequences displayed
	int pic_selected = y_tile * tiles_per_row + x_tile + selectorScreen * max_pics + 1; //fix the hardness bug

	// make sure it is a valid sequence
	if (pic_selected >= NUM_HARD_TILES || pic_selected <= 0)
		return false;

	//copy into hard tile set.
	hard_tile_set[0][0] = pic_selected;
	x_tiles_selected = 1;
	y_tiles_selected = 1;

	mainWnd->setStatusText(_T("Got hard tile data!"));
	current_map->editor_state = current_map->editor_state = current_map->hardTileSelectorReturnState;
	Game_Main();

	return true;
}

// draws tile hardness
int HardTileSelector::renderHardness(int x, int y, int tileWidth, int tileHeight, int &hard_tile_num, bool transparent)
{
	if (hard_tile_num < 0 || hard_tile_num >= NUM_HARD_TILES)
	{
		return true;
	}

	// fill in the destination rect
	RECT dest_rect;
	dest_rect.left   = x;
	dest_rect.top    = y;
	dest_rect.right  = x + tileWidth; 
	dest_rect.bottom = y + tileHeight;

	RECT src_rect = {0, 0, TILEWIDTH, TILEHEIGHT};
	RECT clip_rect = {0, 0, current_map->window_width, current_map->window_height};

	if (fixBounds(dest_rect, src_rect, clip_rect) == false)
		return false;

	if (hardTileSurface[hard_tile_num] == nullptr)
		loadSurface(hard_tile_num);

	if (hardTileSurface[hard_tile_num])
	{
		if (!transparent)
		{
			drawFilledBox(dest_rect, SDL_Color{ 0, 0, 0, 255 });
			SDL_SetTextureAlphaMod(hardTileSurface[hard_tile_num], 255);
		}
		else
		{
			SDL_SetTextureAlphaMod(hardTileSurface[hard_tile_num], hardness_alpha_tile);
		}
		drawTexture(hardTileSurface[hard_tile_num], &src_rect, &dest_rect);
	}
	
	return true;
}

void HardTileSelector::unloadAllBitmaps()
{
	for (int i = 0; i < NUM_HARD_TILES; i++)
	{
		if (hardTileSurface[i])
		{
			SDL_DestroyTexture(hardTileSurface[i]);
			hardTileSurface[i] = nullptr;
		}
	}
}

void HardTileSelector::unloadSurface(int hard_tile_num)
{
	if (hardTileSurface[hard_tile_num])
	{
		SDL_DestroyTexture(hardTileSurface[hard_tile_num]);
		hardTileSurface[hard_tile_num] = nullptr;
	}
}

int HardTileSelector::loadSurface(int hard_tile_num)
{
	if (hardTileSurface[hard_tile_num])
		return true;

	const int format = SDL_PIXELFORMAT_RGBA32;
	SDL_PixelFormat* pixelFormat = SDL_AllocFormat(format);

	Uint32 colors[4] = {
		SDL_MapRGBA(pixelFormat, 0, 0, 0, 0), //transparent
		SDL_MapRGBA(pixelFormat, EXPAND_SDL_COLOR(color_normal_tile_hardness)),
		SDL_MapRGBA(pixelFormat, EXPAND_SDL_COLOR(color_low_tile_hardness)),
		SDL_MapRGBA(pixelFormat, EXPAND_SDL_COLOR(color_other_tile_hardness)),
	};

	SDL_FreeFormat(pixelFormat);

	SDL_Surface* surface = SDL_CreateRGBSurfaceWithFormat(0, TILEWIDTH, TILEHEIGHT, SDL_BITSPERPIXEL(format), format);
	if (!surface)
		return false;

	if (SDL_MUSTLOCK(surface))
		SDL_LockSurface(surface);

	Uint32* pixels = (Uint32*) surface->pixels;
	for (int row = 0; row < TILEHEIGHT; row++)
		for (int col = 0; col < TILEWIDTH; col++)
			pixels[row * TILEHEIGHT + col] = colors[hardTile[hard_tile_num][row][col] % 4];

	if (SDL_MUSTLOCK(surface))
		SDL_UnlockSurface(surface);

	hardTileSurface[hard_tile_num] = SDL_CreateTextureFromSurface(sdl_renderer, surface);

	SDL_FreeSurface(surface);

	if (hardTileSurface[hard_tile_num] == nullptr)
		return false;

	return true;
}
void HardTileSelector::placeTileBoxPoint(int x, int y)
{
	if (tile_box_points < MAX_TILE_BOX_POINTS)
	{
		tile_box_x[tile_box_points] = x;
		tile_box_y[tile_box_points] = y;
		tile_box_points++;

		// redraw the screen
		Game_Main();
	}
}

void fillTriangle(BYTE* matrix, int x1, int y1, int x2, int y2, int x3, int y3, int tile_width)
{
	int i;
	int y;
	int new_x[3] = {x1, x2, x3};
	int new_y[3] = {y1, y2, y3};

	int temp_x, temp_y;
	int in;

	for (i = 1; i < 3; i++)
	{
		temp_x = new_x[i];
		temp_y = new_y[i];
		for (in = i; in > 0 && new_y[in-1] > temp_y; in--)
		{
			new_y[in] = new_y[in-1];
			new_x[in] = new_x[in-1];
		}
		new_x[in] = temp_x;
		new_y[in] = temp_y;
	}

	double left_ratio = double(new_x[1] - new_x[0]) / double(new_y[1] - new_y[0]);
	double right_ratio = double(new_x[2] - new_x[0]) / double(new_y[2] - new_y[0]);

	double temp;

	if (left_ratio > right_ratio)
	{
		temp = left_ratio;
		left_ratio = right_ratio;
		right_ratio = temp;
	}

	double start_x;
	double end_x = start_x = double(new_x[0]);

	BYTE* dest_ptr;// = tile_width * new_y[0];

	for (y = new_y[0]; y < new_y[1]; y++)
	{
		dest_ptr = matrix + y * tile_width * TILEWIDTH + int(start_x);

		for (i = int(start_x); i < int(end_x); i++)
		{
			*dest_ptr = 255^(*dest_ptr);
			dest_ptr++;
		}

		start_x += left_ratio;
		end_x += right_ratio;
	}

	if (new_x[2] > new_x[1])
	{
		left_ratio = double(new_x[2] - new_x[1]) / double(new_y[2] - new_y[1]);
	}
	else
	{
		right_ratio = double(new_x[2] - new_x[1]) / double(new_y[2] - new_y[1]);
	}

	for (y = new_y[1]; y < new_y[2]; y++)
	{
		dest_ptr = matrix + y * tile_width * TILEWIDTH + int(start_x);

		for (i = int(start_x); i < int(end_x); i++)
		{
			*dest_ptr = 255^(*dest_ptr);

			dest_ptr++;
		}

		start_x += left_ratio;
		end_x += right_ratio;
	}
}
// stores the general structure of the hard tiles used for filling in tile boxes
BYTE tile_hard_data[25][2] = 
{
// 775
{4, 4},
{4, 4},
{4, 4},
{4, 4},

// 779
{3, 3},

// 780
{1, 3},
{3, 2},
{3, 1},
{2, 3},

// 784
{0, 1},
{2, 0},
{1, 0},
{0, 2},

// 788
{4, 4},
{4, 4},
{4, 4},
{4, 4},

// 792
{4, 4},
{4, 4},
{4, 4},
{4, 4},

// 796
{1, 1},
{2, 2},
{3, 0},
{0, 3}};

void HardTileSelector::placeTileBox()
{
	//int x, y, x2, y2;
	if (tile_box_points <= 2)
		return;

	RECT bounds = {tile_box_x[0], tile_box_y[0], tile_box_x[0], tile_box_y[0]};

	// calculate bounds of polygon
	for (int i = 0; i < MAX_TILE_BOX_POINTS; i++)
	{
		if (tile_box_x[i] < bounds.left)
			bounds.left = tile_box_x[i];
		if (tile_box_x[i] > bounds.right)
			bounds.right = tile_box_x[i];
		if (tile_box_y[i] < bounds.top)
			bounds.top = tile_box_y[i];
		if (tile_box_y[i] > bounds.bottom)
			bounds.bottom = tile_box_y[i];
	}

    if (current_map->bindViewRect(bounds) == false)
        return;

	// calculate location of possible hard tiles
	RECT tiles;

	tiles.left = (bounds.left / SCREEN_WIDTH_GAP) * SCREEN_TILE_WIDTH +
		(bounds.left % SCREEN_WIDTH_GAP) / TILEWIDTH;

	tiles.right = (bounds.right / SCREEN_WIDTH_GAP) * SCREEN_TILE_WIDTH +
		(bounds.right % SCREEN_WIDTH_GAP) / TILEWIDTH;

	tiles.top = (bounds.top / SCREEN_HEIGHT_GAP) * SCREEN_TILE_HEIGHT +
		(bounds.top % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	tiles.bottom = (bounds.bottom / SCREEN_HEIGHT_GAP) * SCREEN_TILE_HEIGHT +
		(bounds.bottom % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	int x_tiles = tiles.right - tiles.left + 1;
	int y_tiles = tiles.bottom - tiles.top + 1;

	// reset the location of the triangles coordinates to match the matrix
	int cut_x = (tiles.left / SCREEN_TILE_WIDTH) * SCREEN_WIDTH_GAP + 
		(tiles.left % SCREEN_TILE_WIDTH) * TILEWIDTH -  current_map->window.left;

	int cut_y = (tiles.top / SCREEN_TILE_HEIGHT) * SCREEN_HEIGHT_GAP + 
		(tiles.top % SCREEN_TILE_HEIGHT) * TILEHEIGHT - current_map->window.top;

	for (int i = 0; i < MAX_TILE_BOX_POINTS; i++)
	{
		tile_box_x[i] -= cut_x;
		tile_box_y[i] -= cut_y;
	}

	struct HARD_TILE
	{
		BYTE hard[TILEHEIGHT][TILEWIDTH];
	};

	struct MINI_HARD_TILE
	{
		short hard[2][2];
	};

	BYTE* matrix = new BYTE[y_tiles * TILEHEIGHT * x_tiles * TILEWIDTH];
	memset(matrix, 0, y_tiles * TILEHEIGHT * x_tiles * TILEWIDTH);

	for (int i = 2; i < MAX_TILE_BOX_POINTS; i++)
	{
		fillTriangle(matrix, tile_box_x[0], tile_box_y[0],
			tile_box_x[i - 1], tile_box_y[i - 1], tile_box_x[i], tile_box_y[i], x_tiles);
	}

	MINI_HARD_TILE* mini_matrix = new MINI_HARD_TILE[y_tiles * x_tiles];

	memset(mini_matrix, 0, y_tiles * x_tiles * sizeof(MINI_HARD_TILE));

	HARD_TILE temp_tile;

	BYTE *tmp_tile_ptr;

	for (int y = 0; y < y_tiles; y++)
	{
		for (int x = 0; x < x_tiles; x++)
		{
			tmp_tile_ptr = temp_tile.hard[0];

			memset(tmp_tile_ptr, 0, TILEWIDTH * TILEHEIGHT);

			for (int i = 0; i < TILEHEIGHT; i++)
			{
				memcpy(tmp_tile_ptr, matrix + (y * TILEHEIGHT + i) * x_tiles * TILEWIDTH + x * TILEWIDTH, TILEWIDTH);
				tmp_tile_ptr += TILEWIDTH;
			}

			tmp_tile_ptr = temp_tile.hard[0];

			for (int y2 = 0; y2 < TILEHEIGHT; y2++)
			{
				for (int x2 = 0; x2 < TILEWIDTH; x2++)
				{
					if (*tmp_tile_ptr)
						mini_matrix[y * x_tiles + x].hard[y2 / (TILEHEIGHT / 2)][x2 / (TILEWIDTH / 2)]++;

					tmp_tile_ptr++;
				}
			}
		}
	}

	delete [] matrix;

	int current_screen;

	BYTE new_tile[2];

	int width = tiles.right - tiles.left + 1;
	int height = tiles.bottom - tiles.top + 1;

	// create an action for this command
	actions::HardTileMultipleChange action(width, height, tiles.left, tiles.top);
	
	// save the old hard tile information
	for (int m_y = tiles.top, y = 0; y < height; m_y++, y++)
	{
		for (int m_x = tiles.left, x = 0; x < width; m_x++, x++)
		{
			int cur_screen = MAP_COLUMNS * (m_y / SCREEN_TILE_HEIGHT) + m_x / SCREEN_TILE_WIDTH;
			if (current_map->screen[cur_screen] == nullptr)
				continue;
			int x_tile = m_x % SCREEN_TILE_WIDTH;
			int y_tile = m_y % SCREEN_TILE_HEIGHT;

			action.old_hard_tiles[y * width + x] = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
			action.new_hard_tiles[y * width + x] = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
		}
	}

	// get the new hard tiles to use
	for (int m_y = tiles.top, y = 0; y < height; m_y++, y++)
	{
		for (int m_x = tiles.left, x = 0; x < width; m_x++, x++)
		{
			current_screen = MAP_COLUMNS * (m_y / SCREEN_TILE_HEIGHT) + m_x / SCREEN_TILE_WIDTH;
			if (current_map->screen[current_screen] == nullptr)
				continue;

			// drone: these two lines did nothing as the private fields x_tile and y_tile were not used anywhere else.. weird..
			//x_tile = m_x % SCREEN_TILE_WIDTH;
			//y_tile = m_y % SCREEN_TILE_HEIGHT;

			memset(new_tile, 0, 2);

			for (int y3 = 0; y3 < 2; y3++)
			{
				for (int x3 = 0; x3 < 2; x3++)
				{
					if (mini_matrix[y * x_tiles + x].hard[y3][x3] > 150)
						new_tile[y3] += 2 >> x3; 
				}
			}

			for (int i = 0; i < 25; i++)
			{
				if (memcmp(tile_hard_data[i], new_tile, 2) == 0)
				{
					action.new_hard_tiles[y * width + x] = short(775 + i);
					break;
				}
			}
		}
	}

	tile_box_points = 0;

	//TODO
	current_map->undo_buffer->PushAndDo<actions::HardTileMultipleChange>(std::move(action));

	delete [] mini_matrix;	
}

int HardTileSelector::updateScreenGrid(int x, int y)
{
    POINT point{x, y};
    if (current_map->bindViewPoint(point) == false)
    {
        screen_first_x_tile = -1;
        screen_first_y_tile = -1;
        return false;
    }
    
	screen_first_x_tile = screen_last_x_tile = point.x / SCREEN_WIDTH_GAP * SCREEN_TILE_WIDTH
		+ (point.x % SCREEN_WIDTH_GAP) / TILEWIDTH;

	screen_first_y_tile = screen_last_y_tile = point.y / SCREEN_HEIGHT_GAP * SCREEN_TILE_HEIGHT
		+ (point.y % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	anchorRight = false;
	anchorBottom = false;

	return true;
}

int HardTileSelector::updateScreenGrid(const RECT &mouse_box)
{
    RECT sel_rect = mouse_box;
    if (current_map->bindViewRect(sel_rect) == false)
    {
        screen_first_x_tile = -1;
        screen_first_y_tile = -1;
        return false;
    }
    
	screen_first_x_tile = (sel_rect.left / SCREEN_WIDTH_GAP) * SCREEN_TILE_WIDTH +
		(sel_rect.left % SCREEN_WIDTH_GAP) / TILEWIDTH;

	screen_last_x_tile = (sel_rect.right / SCREEN_WIDTH_GAP) * SCREEN_TILE_WIDTH +
		(sel_rect.right % SCREEN_WIDTH_GAP) / TILEWIDTH;

	screen_first_y_tile = (sel_rect.top / SCREEN_HEIGHT_GAP) * SCREEN_TILE_HEIGHT +
		(sel_rect.top % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	screen_last_y_tile = (sel_rect.bottom / SCREEN_HEIGHT_GAP) * SCREEN_TILE_HEIGHT +
		(sel_rect.bottom % SCREEN_HEIGHT_GAP) / TILEHEIGHT; 

	anchorRight = (mouse_box.left > mouse_box.right);
	anchorBottom = (mouse_box.top > mouse_box.bottom);

	int x_tiles = (screen_last_x_tile - screen_first_x_tile + 1) - SCREEN_TILE_WIDTH * 2;
	int y_tiles = (screen_last_y_tile - screen_first_y_tile + 1) - SCREEN_TILE_HEIGHT * 2;
	if (x_tiles > 0)
	{
		if (anchorRight)
			screen_first_x_tile += x_tiles;
		else
			screen_last_x_tile -= x_tiles;
	}

	if (y_tiles > 0)
	{
		if (anchorBottom)
			screen_first_y_tile += y_tiles;
		else
			screen_last_y_tile -= y_tiles;
	}

	return true;
}

int HardTileSelector::screenRender()
{
	if (current_map->screenMode != EditorSubmode::Hardbox)
		return false;

    RECT window_rect = current_map->window;
    if (current_map->bindMapRect(window_rect) == false)
        return false;

	// calculate possible screens to draw
	int first_x_screen = window_rect.left / SCREEN_WIDTH_GAP;
	int last_x_screen = window_rect.right / SCREEN_WIDTH_GAP;
	int first_y_screen = window_rect.top / SCREEN_HEIGHT_GAP;
	int last_y_screen = window_rect.bottom / SCREEN_HEIGHT_GAP;

    int first_x_screen_raw = current_map->window.left / SCREEN_WIDTH_GAP;
    int first_y_screen_raw = current_map->window.top / SCREEN_HEIGHT_GAP;

    int screen;
    int x_offset = -(current_map->window.left % SCREEN_WIDTH_GAP) + (first_x_screen - first_x_screen_raw) * SCREEN_WIDTH_GAP;
    int y_offset = -(current_map->window.top % SCREEN_HEIGHT_GAP) + (first_y_screen - first_y_screen_raw) * SCREEN_HEIGHT_GAP;

	int x_screen_offset = 0;
	int y_screen_offset;

	int screen_first_x_tile_in = screen_first_x_tile;
	int screen_last_x_tile_in = screen_last_x_tile;
	int screen_first_y_tile_in = screen_first_y_tile;
	int screen_last_y_tile_in = screen_last_y_tile;

	int x_diff = screen_last_x_tile - screen_first_x_tile + 1 - x_tiles_selected;
	if (x_diff > 0)
	{
		if (anchorRight)
			screen_first_x_tile_in += x_diff;
		else
			screen_last_x_tile_in -= x_diff;
	}
	
	int y_diff = screen_last_y_tile_in - screen_first_y_tile_in + 1 - y_tiles_selected;
	if (y_diff > 0)
	{
		if (anchorBottom)
			screen_first_y_tile_in += y_diff;
		else
			screen_last_y_tile_in -= y_diff;
	}

	for (int x_screen = first_x_screen; x_screen <= last_x_screen; x_screen++)
	{
		y_screen_offset = 0;
		for (int y_screen = first_y_screen; y_screen <= last_y_screen; y_screen++)
		{
			screen = y_screen * MAP_COLUMNS + x_screen;
			if (current_map->screen[screen] != nullptr)
			{
				int x_offset2 = x_offset + x_screen_offset;
				int y_offset2 = y_offset + y_screen_offset;
				
				// now print grid
				int cur_x_tile, cur_y_tile;
				for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
				{
					for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
					{
						cur_x_tile = x_screen * SCREEN_TILE_WIDTH + x;
						cur_y_tile = y_screen * SCREEN_TILE_HEIGHT + y;

						RECT dest = {x * TILEWIDTH + x_offset2, y * TILEHEIGHT + y_offset2, 
							(x + 1) * TILEWIDTH + x_offset2, (y + 1) * TILEHEIGHT + y_offset2};

						// check if tile is in boundaries
						if (screen_first_x_tile_in <= cur_x_tile && screen_last_x_tile_in >= cur_x_tile && 
							screen_first_y_tile_in <= cur_y_tile && screen_last_y_tile_in >= cur_y_tile)
						{
							// draw the grid box around the tile
							drawBox(dest, color_tile_grid_in_boundary, 1);
						}
						// tile must be out of boundary
						else if (screen_first_x_tile <= cur_x_tile && screen_last_x_tile >= cur_x_tile && 
							screen_first_y_tile <= cur_y_tile && screen_last_y_tile >= cur_y_tile)
						{
							// draw the grid box around the tile
							drawBox(dest, color_tile_grid_out_boundary, 1);
						}
					}
				}

			}
			y_screen_offset += SCREEN_HEIGHT_GAP;
		}
		x_screen_offset += SCREEN_WIDTH_GAP;
	}

	return true;
}

int HardTileSelector::tryDoPlaceTiles()
{
	//is there any tiles selected?
	if (screen_first_x_tile == -1)
	{
		MB_ShowError(_T("You must click on one or more tile destinations before tile(s) can be placed."));
		return false;
	}
	
	//used if not all the tiles are used from 0,0
	int x_diff = 0, y_diff = 0;

	//is there tiles on the right not being used?
	if (anchorRight)
	{
		//how many?
		x_diff = x_tiles_selected - (screen_last_x_tile - screen_first_x_tile + 1);

		//over flowed, too many used, lets bring it down to max (0)
		if (x_diff < 0)
		{
			screen_first_x_tile -= x_diff;
			x_diff = 0;
		}
	}
	//is there tiles on the top not being used?
	if (anchorBottom)
	{
		//how many?
		y_diff = y_tiles_selected - (screen_last_y_tile - screen_first_y_tile + 1);

		//over flowed, too many used, lets bring it down to max (0)
		if (y_diff < 0)
		{
			screen_first_y_tile -= y_diff;
			y_diff = 0;
		}	
	}

	//lets get the dimensions of what the user wants to paste
	//make sure we don't go out of bounds
	int width = screen_last_x_tile - screen_first_x_tile + 1;

	if (width > x_tiles_selected - x_diff)
		width = x_tiles_selected - x_diff;

	int height = screen_last_y_tile - screen_first_y_tile + 1;
	if (height > y_tiles_selected - y_diff)
		height = y_tiles_selected - y_diff;

	//add to undo buffer
	actions::HardTileChange action(screen_first_x_tile, screen_first_y_tile, width, height);

	int cur_screen, x_tile, y_tile, x, y;

	// save the new tiles
	for (x = 0; x < width; x++)
		for(y = 0; y < height; y++)
			action.new_hard_tiles[x][y] = hard_tile_set[x+x_diff][y+y_diff];

	//save old tiles
	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++)
		{
			//check screen we could have moved onto a new screen, or we could still be on it.
			cur_screen = MAP_COLUMNS * ((y + screen_first_y_tile) / SCREEN_TILE_HEIGHT) + (x + screen_first_x_tile) / SCREEN_TILE_WIDTH;

			//make sure the screen is valid
			if (current_map->screen[cur_screen] == nullptr)
				continue;
			
			//zoom into the tile in question
			x_tile = (x + screen_first_x_tile) % SCREEN_TILE_WIDTH;
			y_tile = (y + screen_first_y_tile) % SCREEN_TILE_HEIGHT;

			//copy it over
			if (current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness)
			{
				action.old_hard_tiles[x][y] = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
			}
			else
			{
				TILEDATA tempdata = current_map->screen[cur_screen]->tiles[y_tile][x_tile];

				action.old_hard_tiles[x][y] = tileBmp[tempdata.bmp]->tile_hardness[tempdata.y][tempdata.x];
			}
		}
	}
	
	//TODO
	current_map->undo_buffer->PushAndDo<actions::HardTileChange>(std::move(action));

	return true;
}

int HardTileSelector::storeScreenTiles() 
{  
	if (screen_first_x_tile == -1) 
	{ 
		MB_ShowError(_T("You must click on one or more tile destinations before tile(s) can be copied."));
		return false;  
	}
	
	int cur_screen, x_tile, y_tile;  

	x_tiles_selected = 0; 
	for (int x = screen_first_x_tile; x <= screen_last_x_tile; x++, x_tiles_selected++) 
	{
		y_tiles_selected = 0;
		for (int y = screen_first_y_tile; y <= screen_last_y_tile; y++, y_tiles_selected++)  
		{ 
			cur_screen = MAP_COLUMNS * (y / SCREEN_TILE_HEIGHT) + x / SCREEN_TILE_WIDTH; 

			//if the screen is null (not used), we can skip it and map the hardness nullptr
			if (current_map->screen[cur_screen] == nullptr) 
			{ 
				hard_tile_set[x_tiles_selected][y_tiles_selected] = 0;
			} 
			else 
			{ 
				x_tile = x % SCREEN_TILE_WIDTH;  
				y_tile = y % SCREEN_TILE_HEIGHT; 
				if (current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness)
				{
					//use the alternate hardness
					hard_tile_set[x_tiles_selected][y_tiles_selected] = current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness;
				}
				else
				{
					//no alternate hardness, use default
					//get the tile information (bmp and tile x,y for the TS#)
					TILEDATA tempdata = current_map->screen[cur_screen]->tiles[y_tile][x_tile];

					hard_tile_set[x_tiles_selected][y_tiles_selected] = tileBmp[tempdata.bmp]->tile_hardness[tempdata.y][tempdata.x];
				}
			} 
		} 
	} 
 
	x_tiles_selected = screen_last_x_tile - screen_first_x_tile + 1; 
	y_tiles_selected = screen_last_y_tile - screen_first_y_tile + 1; 

	mainWnd->setStatusText(_T("Copied screen hard tiles!"));
	
	return true; 
}