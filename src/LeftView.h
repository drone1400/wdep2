#pragma once
class CWinDinkeditDoc;
class SpriteLibrary;
class Map;

class CLeftView : public CTreeView
{
protected: // create from serialization only
	CLeftView();
	DECLARE_DYNCREATE(CLeftView)

// Attributes
public:
	CWinDinkeditDoc* GetDocument();
	int PopulateTree(Map* map);
	void UpdateVisionTree();
	void UpdateSpriteLibraryTree(SpriteLibrary& sprite_library);
	void UpdateScriptTree(CString const& dmod_path);

private:
	HTREEITEM visions_root;
	HTREEITEM script_root;
	HTREEITEM sprite_library_root;

public:
	void OnDraw(CDC* pDC) override;  // overridden to draw this view
	BOOL PreCreateWindow(CREATESTRUCT& cs) override;

protected:
	void OnInitialUpdate() override; // called first time after construct


public:
	virtual ~CLeftView();
#ifdef _DEBUG
	void AssertValid() const override;
	void Dump(CDumpContext& dc) const override;
#endif

protected:
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRefreshList();
	afx_msg void OnDeleteSpriteEntry();
	afx_msg void OnRenameSpriteEntry();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};

#ifndef _DEBUG  // debug version in LeftView.cpp
inline CWinDinkeditDoc* CLeftView::GetDocument()
   { return (CWinDinkeditDoc*)m_pDocument; }
#endif