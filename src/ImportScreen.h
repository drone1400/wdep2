#pragma once
#include "resource.h"

class ImportScreen : public CDialog
{
public:
	ImportScreen(CWnd* pParent = nullptr);   // standard constructor

	enum { IDD = IDD_IMPORT_SCREEN };
	CListCtrl	m_dmod_list;
	
protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support
	BOOL OnInitDialog() override;
	void OnOK() override;
	
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMDblclkDmodImportList(NMHDR *pNMHDR, LRESULT *pResult);
};
