#pragma once

// makes sure x1 and x2 are in ascending order
// returns true if the values were swapped
inline BOOL EnsureAsc(int& x1, int& x2)
{
	if (x1 > x2)
	{
		int temp = x1;
		x1 = x2;
		x2 = temp;
		return TRUE;
	}
	return FALSE;
}

// makes sure x1 and x2 are in ascending order
// returns true if the values were swapped
inline BOOL EnsureAsc(double& x1, double& x2)
{
	if (x1 > x2)
	{
		double temp = x1;
		x1 = x2;
		x2 = temp;
		return TRUE;
	}
	return FALSE;
}