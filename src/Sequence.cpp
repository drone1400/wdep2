#include "StdAfx.h"
#include <string.h>
#include "Sequence.h"
#include "ddutil.h"
#include "Globals.h"
#include "Map.h"
#include "Colors.h"
#include "Structs.h"
#include "CommonSdl.h"
#include "Engine.h"
#include "PathUtil.h"


Sequence::Sequence(int sequence_num, char* graphics_path, int frame_delay, int type, int center_x, int center_y, 
				   int left_boundary, int top_boundary, int right_boundary, int bottom_boundary, bool now)
{
	#ifdef UNICODE
	MultiByteToWideChar(CP_UTF8, 0, graphics_path, -1, this->graphics_path, MAX_PATH);
	#else
	strcpy_s(this->graphics_path, graphics_path);
	#endif
	this->frame_delay = frame_delay;
	this->center_x = center_x;
	this->center_y = center_y;
	this->left_boundary = left_boundary;
	this->top_boundary = top_boundary;
	this->right_boundary = right_boundary;
	this->bottom_boundary = bottom_boundary;
	this->sequence_num = sequence_num - 1;
	this->type = type;
	this->now = now;

	// set color key
	if (type == BLACK)
		color_key = COLORKEY_BLACK;
	else
		color_key = COLORKEY_WHITE;

	memset(set_frame_delay, 0, sizeof(set_frame_delay));
	memset(set_frame_special, 0, sizeof(set_frame_special));
	memset(set_frame_frame_seq, 0, sizeof(set_frame_frame_seq));
	memset(set_frame_frame_frame, 0, sizeof(set_frame_frame_frame));

	graphic_location = UNKNOWN_GRAPHIC;

	memset(frame_image, 0, MAX_FRAMES * sizeof(frame_image[0]));
	memset(frame_info, 0, MAX_FRAMES * sizeof(frame_info[0]));
}

Sequence::~Sequence()
{
	// free the memory for all of the sequence's frames
	for (int i = 0; i < MAX_FRAMES; i++)
	{
		if (frame_image[i])
		{
			SDL_DestroyTexture(frame_image[i]);
			frame_image[i] = nullptr;
		}

		if (frame_info[i])
		{
			delete frame_info[i];
			frame_info[i] = nullptr;
		}
	}
}

bool Sequence::addFrameInfo(int frame_num, int center_x, int center_y, int left, int top, int right, int bottom)
{
	if (frame_num < 0 || frame_num >= MAX_FRAMES)
		return false;

	if (frame_info[frame_num] == nullptr)
	{
		// create a new frame info
		frame_info[frame_num] = new FRAME_INFO;
	}
	FRAME_INFO* new_frame_info = frame_info[frame_num];
	
	// fill out the info for a special frame
	
	new_frame_info->center_x = center_x;
	new_frame_info->center_y = center_y;
	new_frame_info->left_boundary = left;
	new_frame_info->top_boundary = top;
	new_frame_info->right_boundary = right;
	new_frame_info->bottom_boundary = bottom;

	return true;
}

// returns the location in the .ff file of the bitmap
FILE* Sequence::extractBitmap(const TCHAR* bitmap_file)
{
	FILE* fastfile;
	EightDotThreeFilename filename;
	int num_files;
	int file_begin_location;
	int i;

	const TCHAR* outFile = bitmap_file;
	TCHAR ffFile[MAX_PATH] = _T("dir.ff");

	// clip the filename off then end and append dir.ff to the path
	int string_length = _tcslen(bitmap_file);
	for (i = string_length - 1; i >= 0; i--)
	{ 
		if (bitmap_file[i] == '\\')
		{
			outFile = bitmap_file + i + 1;
			_tcsncpy_s(ffFile, bitmap_file, i);
			ffFile[i] = 0;
			_tcscat_s(ffFile, _T("\\dir.ff"));
			break;
		}
	}

	// open the fastfile
	if ((fastfile = _tfopen(ffFile, _T("rb"))) == nullptr)
		return nullptr;

	// get the number of .bmp files
	if (fread(&num_files, sizeof(int), 1, fastfile) <= 0)
	{
		fclose(fastfile);
		return nullptr;
	}

	for (i = 0; i < num_files; i++)
	{
		// get the file location in the fastfile
		if (fread(&file_begin_location, sizeof(int), 1, fastfile) <= 0)
		{
			fclose(fastfile);
			return nullptr;
		}

		// get the filename
		if (fread(filename.s, sizeof(filename), 1, fastfile) <= 0)
		{
			fclose(fastfile);
			return nullptr;
		}

		TCHAR tFilename[EightDotThreeFilename::capacity()];
		#ifdef UNICODE
		MultiByteToWideChar(CP_ACP, 0, filename.s, -1, tFilename, EightDotThreeFilename::capacity());
		#else
		strcpy(tFilename, filename.s);
		#endif

		// check if filename matches (case insensitive)
		if (_tcsicmp(tFilename, outFile) == 0) //TODO: convert  @unicode
			break;
	}

	// return if nothing was found
	if (i == num_files)
	{
		fclose(fastfile);
		return nullptr;
	}

	// seek to the location of the .bmp file
	fseek(fastfile, file_begin_location, SEEK_SET);

	return fastfile;
}

bool Sequence::getBounds(int frame, int size, RECT &rect)
{
	if (frame < 0 || frame >= MAX_FRAMES)
		return false;

	// make sure the frame is valid and has been loaded
	if (frame_image[frame] == nullptr)
	{
		if (loadFrame(frame) == false)
			return false;
	}

	int x, y;
	getCenter(frame, x, y);

	int xoffset = 0;
	int yoffset = 0;

	if (size != 100) {
		xoffset = ((frame_width[frame] * size / 100) - frame_width[frame]) / 2;
		yoffset = ((frame_height[frame] * size / 100) - frame_height[frame]) / 2;
	}

	rect.left = -x - xoffset;
	rect.top = -y - yoffset;
	rect.right = frame_width[frame] - x + xoffset;
	rect.bottom = frame_height[frame] - y + yoffset;

	return true;
}

bool Sequence::getBoundsFallback(int frame, int size, RECT &rect)
{
    int x = SPRITE_FALLBACK_WIDTH / 2;
    int y = SPRITE_FALLBACK_HEIGHT / 2;

    int xoffset = 0;
    int yoffset = 0;

    if (size != 100) {
        xoffset = ((SPRITE_FALLBACK_WIDTH * size / 100) - SPRITE_FALLBACK_WIDTH) / 2;
        yoffset = ((SPRITE_FALLBACK_HEIGHT * size / 100) - SPRITE_FALLBACK_HEIGHT) / 2;
    }

    rect.left = -x - xoffset;
    rect.top = -y - yoffset;
    rect.right = SPRITE_FALLBACK_WIDTH - x + xoffset;
    rect.bottom = SPRITE_FALLBACK_HEIGHT - y + yoffset;

    return true;
}

bool Sequence::getCenter(int frame, int& x, int& y)
{
	if (frame < 0 || frame >= MAX_FRAMES)
		return false;

	// make sure the frame is valid and has been loaded  
	if (frame_image[frame] == nullptr)  
		return false;  

	if (frame_info[frame] == nullptr)  
	{
		if (center_x > 0 && center_y > 0)
		{
			x = center_x;
			y = center_y;
		} 



		// check if hardbox default should be used  
		else if (type == NOTANIM)  
		{ 
			x = (center_x > 0) ? center_x : (frame_width[frame] - frame_width[frame] / 2) + frame_width[frame] / 6;
			y = (center_y > 0) ? center_y : (frame_height[frame] - frame_height[frame] / 4) - frame_height[frame] / 30;
		} 
		else 
		{ 
			// use the first frame's information for computing the center  
			if (frame_image[0] == nullptr)  
			{ 
				if (loadFrame(0) == false) 
					return false;  
			} 
 
			// permanently change the centers for the sequence 
			x = center_x = (center_x > 0) ? center_x : (frame_width[0] - frame_width[0] / 2) + frame_width[0] / 6; 
			y = center_y = (center_y > 0) ? center_y : (frame_height[0] - frame_height[0] / 4) - frame_height[0] / 30;  
		}
	} 
	else 
	{ 
		x = frame_info[frame]->center_x; 
		y = frame_info[frame]->center_y;  
	} 
	return true; 
}

bool Sequence::loadFrame(int cur_frame, const TCHAR* bmp_file)
{
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return false;

	if (frame_image[cur_frame] != nullptr)
		return true;

	FILE* stream;

    CString temp;
    temp.SetString(bmp_file);
    CString png_file = temp.Left(temp.GetLength()-4);
    png_file.Append(_T(".png"));

    // first try reading from PNG file
    if (FileReadable(png_file))
    {
        frame_image[cur_frame] = loadBitmapImage(png_file, frame_width[cur_frame], frame_height[cur_frame], color_key);
    }
    // if png file does not exist, try reading from BMP file
    else if (FileReadable(bmp_file))
	{
		frame_image[cur_frame] = loadBitmapImage(bmp_file, frame_width[cur_frame], frame_height[cur_frame], color_key);
	}
	// bitmap doesn't exist, try loading it from the "dir.ff"
	else if ((stream = extractBitmap(bmp_file)) != nullptr)
	{
		frame_image[cur_frame] = loadBitmapStream(stream, frame_width[cur_frame], frame_height[cur_frame], color_key);
		fclose(stream);
	}

	if (frame_image[cur_frame] == nullptr)
		return false;

	return true;
}

bool Sequence::loadFrame(int cur_frame, int search_depth)
{
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return false;

	TCHAR bmp_file[MAX_PATH];

	if (graphic_location == GRAPHIC_NOT_FOUND)
	{
		// graphic cannot be loaded
		return false;
	}

	if (search_depth >= SET_FRAME_FRAME_MAX_RECURSE_DEPTH)
	{
		return false;
	}

	int sff_seq = set_frame_frame_seq[cur_frame];
	int sff_frame = set_frame_frame_frame[cur_frame];
	if (sff_seq > 0 && sff_frame > 0)
	{
		Sequence* reference_seq = current_map->sequence[sff_seq];
		if (reference_seq == nullptr)
		{
			//set_frame_frame refers to nonexistent sequence
			return false;
		}
		if (reference_seq->frame_image[sff_frame] == nullptr)
		{
			if (!reference_seq->loadFrame(sff_frame, search_depth + 1))
			{
				return false;
			}
		}
		frame_image[cur_frame] = reference_seq->frame_image[sff_frame];
		frame_width[cur_frame] = reference_seq->frame_width[sff_frame];
		frame_height[cur_frame] = reference_seq->frame_height[sff_frame];
		return (frame_image[cur_frame] != nullptr);
	}

	if (graphic_location == UNKNOWN_GRAPHIC)
	{
		// try the dmod directory first
		wsprintf(bmp_file, _T("%s%s%.2d.bmp"), (LPCTSTR)current_map->dmod_path, graphics_path, 1);
		if (loadFrame(0, bmp_file) == true)
		{
			graphic_location = DMOD_GRAPHIC;
		}
		else
		{
			if (is_dink_core_path_override_enabled) {
				wsprintf(bmp_file, _T("%s\\%s%.2d.bmp"), (LPCTSTR)dink_core_path_override, graphics_path, 1);
			} else {
				wsprintf(bmp_file, _T("%s\\%s%.2d.bmp"), (LPCTSTR)dink_core_path, graphics_path, 1);
			}
			if (loadFrame(0, bmp_file) == true)
			{
				graphic_location = DINK_GRAPHIC;
			}
			else
			{
				graphic_location = GRAPHIC_NOT_FOUND;
				return false;
			}
		}
	}

	if (graphic_location == DMOD_GRAPHIC)
	{
		wsprintf(bmp_file, _T("%s%s%.2d.bmp"), (LPCTSTR)current_map->dmod_path, graphics_path, cur_frame + 1);
		return loadFrame(cur_frame, bmp_file);
	}
	else if (graphic_location == DINK_GRAPHIC)
	{
		if (is_dink_core_path_override_enabled) {
			wsprintf(bmp_file, _T("%s\\%s%.2d.bmp"), (LPCTSTR)dink_core_path_override, graphics_path, cur_frame + 1);
		} else {
			wsprintf(bmp_file, _T("%s\\%s%.2d.bmp"), (LPCTSTR)dink_core_path, graphics_path, cur_frame + 1);
		}
		return loadFrame(cur_frame, bmp_file);
	}

	return true;
}

// used for drawing sprites in the sprite selector
bool Sequence::renderFrameForSpriteSelector(int x, int y, int cur_frame)
{
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return false;

	if (frame_image[cur_frame] == nullptr)
		if (loadFrame(cur_frame) == false)
			return false;

	RECT dest_rect;
	dest_rect.left   = x;
	dest_rect.top    = y;
	dest_rect.right  = x + sprite_selector_bmp_width;
	dest_rect.bottom = y + sprite_selector_bmp_height;

	drawFilledBox(dest_rect, color_key == COLORKEY_BLACK ? COLOR_BLACK : COLOR_WHITE); //draw a backgound for the sprite, as if its color key were disabled, emulating pre-SDL WDE
	drawTexture(frame_image[cur_frame], nullptr, &dest_rect);

	return true;
}

// clips sprites to screens, offsets are screen locations
ClipRenderReturn Sequence::clipRender(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect, int sprite_num, bool draw_info)
{
	int cur_frame = cur_sprite->frame - 1;
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return ClipRender_BadFrameData;

	size = cur_sprite->size;
	int x = cur_sprite->x + x_offset;
	int y = cur_sprite->y + y_offset; 

	if (frame_image[cur_frame] == nullptr)
	{
		if (loadFrame(cur_frame) == false)
			return ClipRender_BadFrameData;
	}

	RECT dest_rect;

	if (getBounds(cur_frame, cur_sprite->size, dest_rect) == false)
		return ClipRender_BadFrameData;

	dest_rect.left		+= x - SIDEBAR_WIDTH;
	dest_rect.top		+= y;
	dest_rect.right		+= x - SIDEBAR_WIDTH;
	dest_rect.bottom	+= y;

    return clipRenderCommon(x_offset, y_offset, cur_sprite, clip_rect, sprite_num, draw_info,
        frame_width[cur_frame], frame_height[cur_frame], frame_image[cur_frame], dest_rect, false)
        ? ClipRender_Ok : ClipRender_OutOfBounds;
}

bool Sequence::clipRenderFallback(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect, int sprite_num, bool draw_info)
{
    int framewidth = SPRITE_FALLBACK_WIDTH;
    int frameheight = SPRITE_FALLBACK_HEIGHT;

    
    int size = cur_sprite->size;
    int x = cur_sprite->x + x_offset;
    int y = cur_sprite->y + y_offset;


    int xcenter = framewidth/2;
    int ycenter = frameheight/2;
    int xboundsoffset = 0;
    int yboundsoffset = 0;

    if (size != 100) {
        xboundsoffset = ((framewidth * size / 100) - framewidth) / 2;
        yboundsoffset = ((frameheight * size / 100) - frameheight) / 2;
    }

    RECT dest_rect {
        -xcenter -xboundsoffset,
        -ycenter - yboundsoffset,
        framewidth -xcenter + xboundsoffset,
        frameheight -ycenter + yboundsoffset};

    dest_rect.left		+= x - SIDEBAR_WIDTH;
    dest_rect.top		+= y;
    dest_rect.right		+= x - SIDEBAR_WIDTH;
    dest_rect.bottom	+= y;

    return clipRenderCommon(x_offset, y_offset, cur_sprite, clip_rect, sprite_num, draw_info,
        framewidth, frameheight, temp_sprite_fallback, dest_rect, true);
}

bool Sequence::clipRenderCommon(const int x_offset, const int y_offset, Sprite* cur_sprite, RECT &clip_rect, int sprite_num, bool draw_info,
    int frame_width, int frame_height, SDL_Texture * texture, RECT & dest_rect, bool ignore_trim)
{
	
	RECT source_rect;

	// to trim or not to trim
	if (ignore_trim || cur_sprite->trim_right == 0)
	{
		// fill in the source rect
		source_rect.left	= 0;
		source_rect.top		= 0;
		source_rect.right	= frame_width;
		source_rect.bottom	= frame_height;
	}
	else
	{
		// trim the sprite
		source_rect.left = max(cur_sprite->trim_left, 0);
		source_rect.top = max(cur_sprite->trim_top, 0);
		source_rect.right = min(cur_sprite->trim_right, frame_width);
		source_rect.bottom = min(cur_sprite->trim_bottom, frame_height);

		dest_rect.left += source_rect.left;
		dest_rect.top += source_rect.top;
		dest_rect.right -= frame_width - source_rect.right;
		dest_rect.bottom -= frame_height - source_rect.bottom;

	}


	// blt to destination surface
	if (cur_sprite->type == SpriteType::Invisible)
	{
		// draw sprite interlaced
		int src_height = source_rect.bottom - source_rect.top;
		int dest_height = dest_rect.bottom - dest_rect.top;
		double ratio = 2.0 * double(src_height) / double(dest_height);
		double src_row = 0.0;

		RECT source_rect2, dest_rect2;
		dest_rect2.left = dest_rect.left;
		dest_rect2.right = dest_rect.right;
		source_rect2.left = source_rect.left;
		source_rect2.right = source_rect.right;

		for (int dest_row = 0; dest_row < dest_height; dest_row += 2)
		{
			dest_rect2.top = dest_rect.top + dest_row;
			dest_rect2.bottom = dest_rect2.top + 1;
			source_rect2.top = int(src_row) + source_rect.top;
			source_rect2.bottom = source_rect2.top + 1;
			
			drawTextureClipped(texture, &source_rect2, &dest_rect2, &clip_rect);
			src_row += ratio;
		}
	}
	else
	{
		// draw solid sprite
		drawTextureClipped(texture, &source_rect, &dest_rect, &clip_rect);
	}

	//should I draw it now?
	if (draw_info)
		drawSpriteInfoCommon(cur_sprite, sprite_num+1, dest_rect);

	return true;
}

int Sequence::drawSpriteInfo(const int x_offset, const int y_offset, Sprite* cur_sprite, int sprite_num)
{
	int cur_frame = cur_sprite->frame - 1;
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return false;

	size = cur_sprite->size;

	if (frame_image[cur_frame] == nullptr)
		return false;

	RECT bounds;
	
	// fill in the destination rect
	if (cur_sprite->getImageBounds(bounds) != TRUE)
		return false;

    bounds.left += x_offset;
    bounds.right += x_offset;
    bounds.top += y_offset;
    bounds.bottom += y_offset;
    return drawSpriteInfoCommon(cur_sprite, sprite_num, bounds);
}

int Sequence::drawSpriteInfoFallback(const int x_offset, const int y_offset, Sprite* cur_sprite, int sprite_num)
{
    RECT bounds;
    cur_sprite->getImageBoundsFallback(bounds);
    
    bounds.left += x_offset;
    bounds.right += x_offset;
    bounds.top += y_offset;
    bounds.bottom += y_offset;
    return drawSpriteInfoCommon(cur_sprite, sprite_num, bounds);
}

int Sequence::drawSpriteInfoCommon(Sprite* cur_sprite, int sprite_num, RECT & bounds)
{
    int height = bounds.bottom-bounds.top;
    int width = bounds.right-bounds.left;
    
    int textPosX = (bounds.left + bounds.right) / 2;
    int textPosY = (bounds.top + bounds.bottom) / 2;
    
    BOOL drawBottom = false;

    if (height < SCREEN_HEIGHT && width < SCREEN_WIDTH)
    {
        textPosX = bounds.left + 8;
        textPosY = bounds.top - 60;
        
        // check if offscreen top
        if (textPosY < 0)
        {
            textPosY += height + 60;
            drawBottom = true;
        }

        // check if offscreen left
        if (textPosX < 0)
        {
            textPosX += width;
        }
    }

    TCHAR buffer[1024];
	
    // draw script name
    wsprintf(buffer, _T("%s"), cur_sprite->script.as_tchar());
    if (buffer[0] == '\0')
    {
        textPosY += drawBottom ? 0 : 20;
    } else
    {
        drawText(buffer, textPosX, textPosY + 40, help_text_color, help_text_color_back);
    }

    // draw script brain
    wsprintf(buffer, _T("Brain: %d"), cur_sprite->brain);
    drawText(buffer, textPosX, textPosY + 20, help_text_color, help_text_color_back);

    //draw sprite number
    wsprintf(buffer, _T("Sprite: %d"), sprite_num);
    drawText(buffer, textPosX, textPosY, help_text_color, help_text_color_back);

    return true;
}


// draws the sprite hardness'
int Sequence::clipRenderHardness(int x_offset, int y_offset, Sprite* cur_sprite, RECT &clip_rect)
{
	size = cur_sprite->size;

	int cur_frame = cur_sprite->frame - 1;
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return false;


	//double mysize = (double(size) / 100.00);
	// NOTE drone1400: The Dink engines don't actually resize the hardbox with the sprite!
	// The editor should reflect that
	int mysize = 1;

	if (frame_image[cur_frame] == nullptr)
		return false;

	int x = cur_sprite->x + x_offset;
	int y = cur_sprite->y + y_offset;

	RECT dest_rect;

	// fill in the destination rect
	if (frame_info[cur_frame] != nullptr)
	{
		dest_rect.left		= (long) (x + (frame_info[cur_frame]->left_boundary * mysize) - SIDEBAR_WIDTH);
		dest_rect.top		= (long) (y + (frame_info[cur_frame]->top_boundary * mysize));
		dest_rect.right		= (long) (x + (frame_info[cur_frame]->right_boundary * mysize) - SIDEBAR_WIDTH);
		dest_rect.bottom	= (long) (y + (frame_info[cur_frame]->bottom_boundary * mysize));
	}
	else
	{
		// check if hardbox default should be used
		if (right_boundary <= 0)
		{
			dest_rect.left = (long)(x - (frame_width[cur_frame] * mysize) / 4 - SIDEBAR_WIDTH);
			dest_rect.right = (long)(x + (frame_width[cur_frame] * mysize) / 4 - SIDEBAR_WIDTH);
		}
		else
		{
			dest_rect.left = (long)(x + (left_boundary * mysize) - SIDEBAR_WIDTH);
			dest_rect.right = (long)(x + (right_boundary * mysize) - SIDEBAR_WIDTH);
		}
		if (bottom_boundary <= 0)
		{
			dest_rect.top = (long)(y - ((frame_height[cur_frame] * mysize)) / 10);
			dest_rect.bottom = (long)(y + ((frame_height[cur_frame] * mysize)) / 10);
		}
		else
		{
			dest_rect.top = (long)(y + (top_boundary * mysize));
			dest_rect.bottom = (long)(y + (bottom_boundary * mysize));
		}
	}

	// now clip sprites according to what screen they are on
	if (fixBounds(dest_rect, clip_rect) == false)
		return false;

    // save old blend mode so we can restore later...
    SDL_BlendMode blendModeOld;
    SDL_GetRenderDrawBlendMode(sdl_renderer, &blendModeOld);

    // switch to SDL_BLENDMODE_BLEND for transparency
    SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);
    
	// set the hardbox color based on whether it is a warp or not
	SDL_Color hard_fill;

	if (cur_sprite->warp_enabled)
	{	
		hard_fill = color_warp_sprite_hardness;
	}
	else
	{
		// normal hardness color
		hard_fill = color_sprite_hardness;
	}

    hard_fill.a = hardness_alpha_sprite;

	// blt to destination surface
	if (cur_sprite->hardness == 1)
	{
		drawBox(dest_rect, hard_fill);
	}
	else
	{
		drawFilledBox(dest_rect, hard_fill);
	}

    SDL_SetRenderDrawBlendMode(sdl_renderer, blendModeOld);
    
	return true;
}