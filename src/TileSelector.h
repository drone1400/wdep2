#pragma once
#include "Const.h"
#include "Structs.h"

class TileSelector
{
public:
	TileSelector();
	~TileSelector();

	int selectScreen(int new_screen);
	void nextPage();
	void prevPage();

	// tile sheet related functions
	void selectTileSheetTileXY(int x, int y);
	void selectTileSheetTileXY(int mouse_x_origin, int mouse_x_position, int mouse_y_origin, int mouse_y_position);
	void storeTileSheetTiles();
	void renderTileSheet();
	void renderTileSheetTileHover(int x, int y);
	void switchHardTileEditModeOnHover();
	bool tryDoHardTileSetDefaultOnHover();
	bool tryDoHardTileSetDefaultClearOnHover();	

	// screen related functions
	void updateScreenGrid(int x, int y);
	void updateScreenGrid(const RECT &mouse_box);
	void renderScreenTileGrid();
	void renderScreenTileHover(int x, int y);
	void storeScreenTiles();
	bool tryDoPlaceTiles();

	// selection stuff
	void getCurrentTileData(TILEDATA& data);
	void getCurrentTileHardIndex(int& hardIndex);
	
private:
	// helper functions for tile sheets
	void refreshTileSheetRenderParameters();
	void getTileSheetTileXY(int inCursorX, int inCursorY, int& outTileX, int& outTileY);
	void renderTileSheetSelection();
	void renderTileSheetAt(int tileSheet, int sheetCol, int sheetRow);
	void renderTileSheetHardnessAt(int tileSheet, int sheetCol, int sheetRow);

	// rendering parameters
	int rendSheetWidth, rendSheetHeight, rendOffsetX, rendOffsetY, rendTileWidth, rendTileHeight, rendSheetRows, rendSheetCols;
	BOOL rendCentered;
	
	// grid for tilemode
	int first_x_tile, last_x_tile, first_y_tile, last_y_tile;

	// mouse hover position
	int hoverXTile, hoverYTile;
	TILEDATA hoverTile;
	int hoverHardIndex;
	

	// current tile bitmap
	int currentTileSheet;

	// stores tiles in a buffer for copying and pasting
	TILEDATA tile_set[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2];
	int x_tiles_selected;
	int y_tiles_selected;

	// grid for screenmode
	int screen_first_x_tile, screen_last_x_tile, screen_first_y_tile, screen_last_y_tile;

	bool anchorRight, anchorBottom;
};