#pragma once

#define DIRSEPC		_T('\\')
#define DIRSEPS		_T("\\")

// tries to make a relative path absolute
// returns false if the relative path is rooted or the absolute path is not rooted or the length would exceed MAX_PATH
bool Path_MakeAbsolute(CString relativeStr, CString absoluteStr, CString & outStr);

// tries to make an absolute path relative to a base path that must also be absolute
// returns false if the path can not be made relative (relativeStr will be a copy of absoluteStr in this case..);
bool Path_MakeRelative(CString baseStr, CString absoluteStr, CString & relativeStr);

// this is kind of like PathCanonicalize but not quite,
// it will return false if the CString exceeds MAX_PATH length
// it will preserve leading .. that don't resolve internally in the path
// ex: for a path like "..\dir1\\\dir2\\..\dir3\.\dir4\..\dir5\"
// PathCanonicalize will return "dir1\\\dir2\dir3\dir5"
// while this will return "..\dir1\dir3\dir5"
// this helps return a nice and correct user input relative path
bool Path_Normalize(CString inStr, CString & outStr);

inline bool Path_IsSafeLength(CString & str)
{
	return str.GetLength() < MAX_PATH;
}

// use PathIsRoot to check if path is rooted, but also return false if path is empty!
// note: does not check input length!
bool Path_IsRooted(CString str);

// appends multiple CStrings with directory separators into a destination CString
// then normalizes the path
// returns false if new string exceeds MAX_PATH
bool Path_ConcatIntoAndNormalize(CString & destination, CString const& name1, CString const& name2 = _T(""), CString const& name3 = _T(""), CString const& name4 = _T(""));

// appends a path and normalizes it just to be safe
// returns false if new string exceeds MAX_PATH
inline bool Path_AppendNormalize(CString & str, CString const& name)
{
	str.AppendChar(DIRSEPC);
	str.Append(name);
	return Path_Normalize(str, str);
}


// gets the parent directory of a given path...
// NOTE, does not actually check that the directory eixsts or anything
// NOTE, will return true but have invalid data if called for a file in a root directory like "C:\something" or "\\something"
// will normalize inPath and remove the last token in the path
// returns false if input path can not be normalized or if there is no parent directory
bool Path_GetDirectoryOf(CString const inPath, CString & outPath);

// gets the directory or file name at the end of the input path
// basically the opposite of PathGetDirectoryOf
// returns false if input path can not be normalized
bool Path_GetLastToken(CString const inPath, CString & outName);

// checks if the given destination path is contained in the given source path or is the same
// returns:
//    -1 - can not determine
//     0 - result is false
//     1 - result is true
int Path_IsDestinationChildOfSourceOrSame(CString source, CString destination);


// formerly in tools.h, moved here since they are general path/file operation related...
bool FileReadable(const TCHAR* path);
bool FileWritable(const TCHAR* path);
bool FolderExists(const TCHAR* path);