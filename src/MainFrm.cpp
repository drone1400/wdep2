#include "stdafx.h"
#include "MainFrm.h"
#include "Engine.h"
#include "LeftView.h"
#include "WinDinkeditView.h"
#include "Common.h"
#include "interface.h"
#include "PlayDmodDialog.h"
#include "DinkInstallationDialog.h"
#include "DmodPropertiesDialog.h"
#include "Fastfile.h"
#include "FastfileExtract.h"
#include "Globals.h"
#include "ImportScreen.h"
#include "Map.h"
#include "Minimap.h"
#include "Options.h"
#include "ScreenshotMapDialog.h"
#include "ScriptCompress.h"
#include "ScriptCompressor.h"
#include "ScriptDecompressor.h"
#include "Tile.h"
#include "VisionChange.h"

CMainFrame* mainWnd;

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_MOVE()
	ON_WM_TIMER()
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_MOUSE, &CMainFrame::OnUpdateIndicatorMousePos)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_VISION, &CMainFrame::OnUpdateIndicatorVision)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SPRID, &CMainFrame::OnUpdateIndicatorSpriteId)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SPRSEQUENCE, &CMainFrame::OnUpdateIndicatorSpriteSequence)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SPRFRAME, &CMainFrame::OnUpdateIndicatorSpriteFrame)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_TILEID, &CMainFrame::OnUpdateIndicatorTileId)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_TILEHARD, &CMainFrame::OnUpdateIndicatorTileHard)
	ON_COMMAND_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnViewStyle)
	ON_UPDATE_COMMAND_UI_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnUpdateViewStyles)
	ON_COMMAND(ID_DMOD_PROPERTIES, &CMainFrame::OnDmodProperties)
	ON_UPDATE_COMMAND_UI(ID_DMOD_PROPERTIES, &CMainFrame::OnUpdateDmodProperties)
	ON_COMMAND(ID_EDIT_UNDO, &CMainFrame::OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, &CMainFrame::OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_REDO, &CMainFrame::OnEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, &CMainFrame::OnUpdateEditRedo)
	ON_COMMAND(ID_EDIT_COPY, &CMainFrame::OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, &CMainFrame::OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_CUT, &CMainFrame::OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, &CMainFrame::OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_PASTE, &CMainFrame::OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &CMainFrame::OnUpdateEditPaste)

	ON_COMMAND(ID_SAVE_FIX, OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_SAVE_FIX, OnUpdateFileSave)
	ON_COMMAND(ID_FILE_PLAYGAME, OnFilePlaygame)
	ON_UPDATE_COMMAND_UI(ID_FILE_PLAYGAME, OnUpdateFilePlaygame)
	ON_COMMAND(IDM_AUTO_UPDATE_MINIMAP, &CMainFrame::OnAutoUpdateMinimap)
	ON_UPDATE_COMMAND_UI(IDM_AUTO_UPDATE_MINIMAP, &CMainFrame::OnUpdateAutoUpdateMinimap)
	ON_COMMAND(IDM_SETTINGS_SNAPTOGRID, &CMainFrame::OnSettingsSnaptogrid)
	ON_UPDATE_COMMAND_UI(IDM_SETTINGS_SNAPTOGRID, &CMainFrame::OnUpdateSettingsSnaptogrid)
	ON_COMMAND(ID_FILE_OPENDMODFOLDER, &CMainFrame::OnFileOpendmodfolder)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPENDMODFOLDER, &CMainFrame::OnUpdateFileOpendmodfolder)
	ON_COMMAND(ID_FILE_DINKINSTALLATIONFOLDER, &CMainFrame::OnFileDinkInstallation)
	ON_UPDATE_COMMAND_UI(ID_FILE_DINKINSTALLATIONFOLDER, &CMainFrame::OnUpdateFileDinkInstallation)
	ON_COMMAND(ID_FILE_CLOSEDMOD, &CMainFrame::OnFileCloseDMod)
	ON_UPDATE_COMMAND_UI(ID_FILE_CLOSEDMOD, &CMainFrame::OnUpdateFileCloseDMod)
	ON_COMMAND(IDM_DUPLICATE_DETECT, &CMainFrame::OnDuplicateDetect)
	ON_UPDATE_COMMAND_UI(IDM_DUPLICATE_DETECT, &CMainFrame::OnUpdateDuplicateDetect)
	ON_COMMAND(IDM_OPTIONS, &CMainFrame::OnOptions)
	ON_UPDATE_COMMAND_UI(IDM_OPTIONS, &CMainFrame::OnUpdateOptions)
	ON_COMMAND(IDM_SCREEN_SHOT, &CMainFrame::OnScreenShot)
	ON_UPDATE_COMMAND_UI(IDM_SCREEN_SHOT, &CMainFrame::OnUpdateScreenShot)
	ON_COMMAND(IDM_COMPRESS_SCRIPTS, &CMainFrame::OnCompressScripts)
	ON_UPDATE_COMMAND_UI(IDM_COMPRESS_SCRIPTS, &CMainFrame::OnUpdateCompressScripts)
	ON_COMMAND(IDM_DECOMPRESS_SCRIPTS, &CMainFrame::OnDecompressScripts)
	ON_UPDATE_COMMAND_UI(IDM_DECOMPRESS_SCRIPTS, &CMainFrame::OnUpdateDecompressScripts)
	ON_COMMAND(IDM_SCREEN_MATCH, &CMainFrame::OnScreenMatch)
	ON_UPDATE_COMMAND_UI(IDM_SCREEN_MATCH, &CMainFrame::OnUpdateScreenMatch)
	ON_COMMAND(IDM_FFCREATE, &CMainFrame::OnFfcreate)
	ON_UPDATE_COMMAND_UI(IDM_FFCREATE, &CMainFrame::OnUpdateFfcreate)
	ON_COMMAND(ID_TOOLS_FFEXTRACT, &CMainFrame::OnToolsFfextract)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_FFEXTRACT, &CMainFrame::OnUpdateToolsFfextract)
	ON_COMMAND(IDM_VISION, &CMainFrame::OnVision)
	ON_UPDATE_COMMAND_UI(IDM_VISION, &CMainFrame::OnUpdateVision)
	ON_COMMAND(IDM_SPRITE_HARDNESS, &CMainFrame::OnSpriteHardness)
	ON_UPDATE_COMMAND_UI(IDM_SPRITE_HARDNESS, &CMainFrame::OnUpdateSpriteHardness)
	ON_COMMAND(IDM_IMPORT_SCREEN, &CMainFrame::OnImportScreen)
	ON_COMMAND(IDM_SPRITE_REPORT, &CMainFrame::OnSpriteReport)
	ON_UPDATE_COMMAND_UI(IDM_IMPORT_SCREEN, &CMainFrame::OnUpdateImportScreen)
	ON_UPDATE_COMMAND_UI(IDM_SPRITE_REPORT, &CMainFrame::OnUpdateSpriteReport)
	ON_COMMAND(IDM_VIEW_MINIMAP, &CMainFrame::OnViewMinimap)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_MINIMAP, &CMainFrame::OnUpdateViewMinimap)
	ON_COMMAND(IDM_VIEW_SPRITE_MODE, &CMainFrame::OnViewSpriteMode)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_SPRITE_MODE, &CMainFrame::OnUpdateViewSpriteMode)
	ON_COMMAND(ID_TOOLS_SPRITENOHITBYDEFAULT, &CMainFrame::OnToolsSpritenohitbydefault)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SPRITENOHITBYDEFAULT, &CMainFrame::OnUpdateToolsSpritenohitbydefault)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_MOUSE,
	ID_INDICATOR_VISION,
	ID_INDICATOR_SPRID,
	ID_INDICATOR_SPRSEQUENCE,
	ID_INDICATOR_SPRFRAME,
	ID_INDICATOR_TILEID,
	ID_INDICATOR_TILEHARD,
};


CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	mainWnd = this;
	timerIndicatorScreenPosDelayRunning = FALSE;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::setStatusText(const TCHAR *text)
{
	m_wndStatusBar.OnProgress(0);
	m_wndStatusBar.SetPaneText(0, text);

	return true;
}


void CMainFrame::OnUpdateIndicatorMousePos(CCmdUI* pCmdUI)
{
	// using a delay when updating this because otherwise it updates too fast and flickers...
	if (timerIndicatorScreenPosDelayRunning)
		return;	
	SetTimer(TIMER_INDICATOR_SCREEN_POS_DELAY, 10, nullptr);
	timerIndicatorScreenPosDelayRunning = TRUE;
	
	pCmdUI->Enable();
	
	if (current_map != nullptr && current_map->editor_state == EditorState::Minimap)
	{
		CString s;
		s.Format(_T("Screen:  %3d, x: %5d, y: %5d"), current_map->minimap->getHoverScreen() + 1, 0, 0);
		pCmdUI->SetText((LPCTSTR) s);
	} else if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
	{
	    int32_t x = current_map->hoverRelX;
	    if (use_statusbar_sidebar_offset) x += SIDEBAR_WIDTH;
		CString s;
		s.Format(_T("Screen:  %3d, x: %5d, y: %5d"), current_map->hoverScreen + 1, x, current_map->hoverRelY);
		pCmdUI->SetText((LPCTSTR) s);
	} else
	{
		pCmdUI->SetText(_T("Screen: ---, x:  ----, y:  ----"));
	}
}

void CMainFrame::OnUpdateIndicatorVision(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();

	if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
	{
		CString s;
		s.Format(_T("Vision: %5d"), current_map->cur_vision);
		pCmdUI->SetText((LPCTSTR) s);
	} else
	{
		pCmdUI->SetText(_T("Vision:  --- "));
	}
}

void CMainFrame::OnUpdateIndicatorSpriteId(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();

    Sprite * hover_sprite = current_map != nullptr ? current_map->getHoverSprite() : nullptr;
	if (hover_sprite != nullptr && current_map->editor_state == EditorState::Screen)
	{
		CString s;
		// sprite ID is indexed at 0 internally in WDE, but as far as the game is concerned, sprites are indexed at 1
		s.Format(_T("Sprite Id: %03d"), current_map->hoverSpriteNum + 1);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else
	{
		pCmdUI->SetText(_T("Sprite Id: ---"));
	}
}

void CMainFrame::OnUpdateIndicatorSpriteSequence(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();
    Sprite * hover_sprite = current_map != nullptr ? current_map->getHoverSprite() : nullptr;
    if (hover_sprite != nullptr && current_map->editor_state == EditorState::Screen)
	{
	    CString s;
		s.Format(_T("Sprite Seq: %03d"), hover_sprite->sequence);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else if (current_map != nullptr && current_map->editor_state == EditorState::SpriteSelector)
	{
	    CString s;
		s.Format(_T("Sprite Seq: %03d"), current_map->sprite_selector.currentSequence);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else
		pCmdUI->SetText(_T("Sprite Seq: ---"));
}


void CMainFrame::OnUpdateIndicatorSpriteFrame(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();
    Sprite * hover_sprite = current_map != nullptr ? current_map->getHoverSprite() : nullptr;
    if (hover_sprite != nullptr && current_map->editor_state == EditorState::Screen)
	{
		CString s;
		s.Format(_T("Sprite Frame: %02d"), hover_sprite->frame);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else if (current_map != nullptr && current_map->editor_state == EditorState::SpriteSelector)
	{
		CString s;
		s.Format(_T("Sprite Frame: %02d"), current_map->sprite_selector.currentFrame+1);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else
	{
		pCmdUI->SetText(_T("Sprite Frame: --"));
	}
}


void CMainFrame::OnUpdateIndicatorTileId(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
	TILEDATA data = {0,0,0,0};
	BOOL hasValidData = FALSE;
	
	if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
	{
		data = current_map->hoverTileData;
	    int tile_idx = data.x + data.y * TILESHEET_TILE_WIDTH;

	    // validate tile data coordinates...
	    if (data.bmp >= 0 && data.bmp < MAX_TILE_BMPS &&
            data.x >= 0 && data.x < TILESHEET_TILE_WIDTH &&
            data.y >= 0 && data.y < TILESHEET_TILE_HEIGHT &&
	        tile_idx < TILESHEET_ID_MULTIPLIER)
	    {
	        hasValidData = TRUE;
	    }
	} else if (current_map != nullptr && current_map->editor_state == EditorState::TileSelector)
	{
		current_map->tile_selector.getCurrentTileData(data);
		hasValidData = TRUE;
	}
	else if (current_map != nullptr && current_map->editor_state == EditorState::TileHardnessEditor)
	{
		current_map->hard_tile_selector.getCurrentTileData(data);
		hasValidData = TRUE;
	}

	if (hasValidData)
	{
		int bmp = data.bmp;
		int x = data.x;
		int y = data.y;
		int id = bmp * TILESHEET_ID_MULTIPLIER + y * TILESHEET_TILE_WIDTH + x;
		CString s;
		// tilesheet bmp id is indexed at 0 internally in WDE, but as far as the game is concerned, it is indexed at 1
		s.Format(_T("TS: %02d, X: %02d, Y: %02d, ID: %05d"), bmp + 1, x, y, id);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else
	{
		pCmdUI->SetText(_T("TS: --, X: --, Y: --, ID: -----"));
	}
}

void CMainFrame::OnUpdateIndicatorTileHard(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
	if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
	{
		CString s = _T("DEF. Hard ID: ---");
		if (current_map->hoverTileData.alt_hardness != 0)
		{
			s.Format(_T("ALT. Hard ID: %03d"), current_map->hoverTileData.alt_hardness);
		} else
		{
			int bmp = current_map->hoverTileData.bmp;
			int bmpx = current_map->hoverTileData.x;
			int bmpy = current_map->hoverTileData.y;

		    int tile_idx = bmpx + bmpy * TILESHEET_TILE_WIDTH;
		    
		    // validate tile data coordinates...
		    if (bmp >= 0 && bmp < MAX_TILE_BMPS &&
                bmpx >= 0 && bmpx < TILESHEET_TILE_WIDTH &&
                bmpy >= 0 && bmpy < TILESHEET_TILE_HEIGHT &&
                tile_idx < TILESHEET_ID_MULTIPLIER && tileBmp[bmp] != nullptr)
		    {
		        int hard = tileBmp[bmp]->tile_hardness[bmpy][bmpx];
		        s.Format(_T("DEF. Hard ID: %03d"), hard);
		    }
		}
		pCmdUI->SetText((LPCTSTR) s);
	}
	else if (current_map != nullptr &&
		(current_map->editor_state == EditorState::TileHardnessEditor ||
			current_map->editor_state == EditorState::HardTileSelector))
	{
		int hardIdx = 0;
		current_map->hard_tile_selector.getCurrentTileHardIndex(hardIdx);
		CString s;
		s.Format(_T("Hard ID: %03d"), hardIdx);
		pCmdUI->SetText((LPCTSTR) s);
	} else if (current_map != nullptr && current_map->editor_state == EditorState::TileSelector)
	{
		int hardIdx = 0;
		current_map->tile_selector.getCurrentTileHardIndex(hardIdx);
		CString s;
		s.Format(_T("Hard ID: %03d"), hardIdx);
		pCmdUI->SetText((LPCTSTR) s);
	}
	else
	{
		pCmdUI->SetText(_T("Hard ID: ---"));
	}
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	//kill timer, we will reset it later if needed
	KillTimer(nIDEvent);

	//what timer got called?
	switch (nIDEvent)
	{
		case TIMER_AUTO_SAVE:
			if (auto_save_time > 0)
			{
				//autosave!
				try 
				{
					current_map->save_dmod();
					mainWnd->setStatusText(_T("Dmod Auto-saved."));
				}
				catch(...)
				{
					mainWnd->setStatusText(_T("Dmod auto-save failed!"));
				}

				SetTimer(TIMER_AUTO_SAVE, auto_save_time * 60000, nullptr);
			}
			break;
		
		case TIMER_MOUSE_CHECK:
			//is the mouse still down?
			if (KEY_DOWN(VK_LBUTTON) || KEY_DOWN(VK_MBUTTON))
			{
				SetTimer(TIMER_MOUSE_CHECK, 100, nullptr);
			} 
			else
			{
				mouseLeftReleased(current_map->hoverRelX, current_map->hoverRelY);
			}
			break;

	case TIMER_INDICATOR_SCREEN_POS_DELAY:
		timerIndicatorScreenPosDelayRunning = FALSE;
		break;
		
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_TRANSPARENT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)	//RW:flat->transparent.
		|| !m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||	!m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	// TODO: actually commenting these out seems to mess up the look of the toolbar... need to investigate what's up...

	Game_Init();

	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext)
{
	// create splitter window
	if (!m_wndSplitter.CreateStatic(this, 1, 2))
		return FALSE;

	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CLeftView), CSize(120, 100), pContext) ||
		!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CWinDinkeditView), CSize(100, 100), pContext))
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}

	SetActiveView((CView*)m_wndSplitter.GetPane(0,1));	//RW: this activates the right pane so that menus get that context, otherwise they don't work properly until user clicks.
														//RW: (I spent 2 days finding out about this! Oh, I thought my brain would definitely get charged to 100GV if it took any longer...)
	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs

	return TRUE;
}


void CMainFrame::OnDestroy()
{
	Game_Shutdown();

	CFrameWnd::OnDestroy();
}


#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


CWinDinkeditView* CMainFrame::GetRightPane()
{
	CWnd* pWnd = m_wndSplitter.GetPane(0, 1);
	CWinDinkeditView* pView = DYNAMIC_DOWNCAST(CWinDinkeditView, pWnd);
	return pView;
}

CLeftView* CMainFrame::GetLeftPane()
{
	CWnd* pWnd = m_wndSplitter.GetPane(0, 0);
	CLeftView* pView = DYNAMIC_DOWNCAST(CLeftView, pWnd);
	return pView;
}

void CMainFrame::OnUpdateViewStyles(CCmdUI* pCmdUI)
{
	// TODO: customize or extend this code to handle choices on the View menu.

	CWinDinkeditView* pView = GetRightPane(); 

	// if the right-hand pane hasn't been created or isn't a view, disable commands in our range

	if (pView == nullptr)
		pCmdUI->Enable(FALSE);
}

void CMainFrame::OnViewStyle(UINT nCommandID)
{
	// TODO: customize or extend this code to handle choices on the View menu.
	CWinDinkeditView* pView = GetRightPane();
}

void CMainFrame::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);

	// TODO: Add your message handler code here
	if (current_map)
		GetRightPane()->windowMoved(x, y);
}


void CMainFrame::OnUpdateDmodProperties(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(current_map != nullptr);
}

void CMainFrame::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(current_map != nullptr);
}

void CMainFrame::OnFileSave()
{
	if (current_map != nullptr)
	{
		BeginWaitCursor();
		try 
		{
			current_map->save_dmod();
			current_map->undo_buffer->MarkSave();
		}
		catch(...)
		{
			MB_ShowError(_T("Failed to save dmod."));
		}
		EndWaitCursor();
	}
}


void CMainFrame::OnFilePlaygame()
{
	CPlayDmodDialog dialog(nullptr);
	dialog.SetCheckBoxStatus(play_truecolor, play_windowed, play_sound, play_joystick, play_debug, play_pathquotes);	//RW: the checkboxes are set to the values read from the prefs file.
	dialog.DoModal();
	dialog.GetCheckBoxStatus(play_truecolor, play_windowed, play_sound, play_joystick, play_debug, play_pathquotes);	//RW: we get the new values so we can later write them to the prefs file.
}

void CMainFrame::OnUpdateFilePlaygame(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


// undo an action if possible
void CMainFrame::OnEditUndo()
{
	current_map->cmdEditUndo();
}
void CMainFrame::OnUpdateEditUndo(CCmdUI* pCmdUI)
{
	//RW: if there are no actions to undo then disable undo button
	pCmdUI->Enable(current_map != nullptr && current_map->canCmdEditUndo());
}

// redo an action if possible
void CMainFrame::OnEditRedo()
{
	current_map->cmdEditRedo();
}
void CMainFrame::OnUpdateEditRedo(CCmdUI *pCmdUI)
{
	//RW: if there are no actions to redo then disable redo button
	pCmdUI->Enable(current_map != nullptr && current_map->canCmdEditRedo());
}

void CMainFrame::OnEditCopy()
{
	current_map->cmdEditCopy();
}
void CMainFrame::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr && current_map->canCmdEditCopy());
}

void CMainFrame::OnEditCut()
{
	current_map->cmdEditCut();
}
void CMainFrame::OnUpdateEditCut(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr && current_map->canCmdEditCut());
}

void CMainFrame::OnEditPaste()
{
	current_map->cmdEditPaste();
}
void CMainFrame::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr && current_map->canCmdEditPaste());
}


void CMainFrame::OnUpdateAutoUpdateMinimap(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(update_minimap);
}


void CMainFrame::OnAutoUpdateMinimap()
{
	update_minimap = !update_minimap;
}


void CMainFrame::OnUpdateSettingsSnaptogrid(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(snapto_grid);
}


void CMainFrame::OnSettingsSnaptogrid()
{
	snapto_grid = !snapto_grid;
}


void CMainFrame::OnUpdateFileOpendmodfolder(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnFileOpendmodfolder()
{
	ShellExecute(nullptr, _T("open"), current_map->dmod_path, _T(""), _T(""), SW_SHOWNORMAL);
}

void CMainFrame::OnFileDinkInstallation()
{
	DinkInstallationDialog dialog;
	if (dialog.DoModal() == IDOK)
	{
		dink_exe_path = dialog.m_dinkExePath;
		dink_core_path = dialog.m_dinkCorePath;
		dink_skeleton_path = dialog.m_skeletonPath;
		dink_dmods_path = dialog.m_otherDmodsPath;
	}
}


void CMainFrame::OnUpdateFileDinkInstallation(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map == nullptr);	//only enable when there is no map open!
}


void CMainFrame::OnFileCloseDMod()
{
	if (GetActiveDocument()->SaveModified())
	{
		delete current_map;  //destroy map
		current_map = nullptr;
		GetRightPane()->RedrawWindow();  //clear right pane
		GetLeftPane()->GetTreeCtrl().DeleteAllItems();  //clear left pane

		//reset status bar to defaults
		m_wndStatusBar.SetPaneText(m_wndStatusBar.CommandToIndex(ID_INDICATOR_MOUSE), (LPCTSTR) CString(MAKEINTRESOURCE(ID_INDICATOR_MOUSE)), TRUE);
		m_wndStatusBar.SetPaneText(m_wndStatusBar.CommandToIndex(ID_INDICATOR_VISION), (LPCTSTR) CString(MAKEINTRESOURCE(ID_INDICATOR_VISION)), TRUE);
		m_wndStatusBar.SetPaneText(m_wndStatusBar.CommandToIndex(ID_INDICATOR_SPRSEQUENCE), (LPCTSTR) CString(MAKEINTRESOURCE(ID_INDICATOR_SPRSEQUENCE)), TRUE);
		m_wndStatusBar.SetPaneText(m_wndStatusBar.CommandToIndex(ID_INDICATOR_SPRFRAME), (LPCTSTR) CString(MAKEINTRESOURCE(ID_INDICATOR_SPRFRAME)), TRUE);

		// NOTE: drone1400 - must call new document otherwise the current document remains open
		// and prevents reopening the same dmod you just closed
		GetActiveDocument()->OnNewDocument();
	}
}


void CMainFrame::OnUpdateFileCloseDMod(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnDuplicateDetect()
{
	detect_screenmatch_duplicates = !detect_screenmatch_duplicates;
}


void CMainFrame::OnUpdateDuplicateDetect(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
	pCmdUI->SetCheck(detect_screenmatch_duplicates);
}


void CMainFrame::OnUpdateOptions(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateScreenShot(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateCompressScripts(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateScreenMatch(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
	if (current_map != nullptr)
		pCmdUI->SetCheck(current_map->screenmatch);
}


void CMainFrame::OnUpdateFfcreate(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateVision(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateSpriteHardness(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr && current_map->screenMode != EditorSubmode::Hardbox);
	pCmdUI->SetCheck(sprite_hard);
}


void CMainFrame::OnUpdateImportScreen(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}

void CMainFrame::OnUpdateSpriteReport(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateViewMinimap(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnUpdateViewSpriteMode(CCmdUI *pCmdUI)
{
	if (pCmdUI->m_pSubMenu != nullptr)	//pCmdUI refers to the parent of the screen mode selection submenu
		pCmdUI->m_pMenu->EnableMenuItem(pCmdUI->m_nIndex, MF_BYPOSITION | ((current_map!=nullptr) ? MF_ENABLED : (MF_DISABLED|MF_GRAYED)));
	else
		pCmdUI->Enable(current_map!=nullptr);
}


void CMainFrame::OnDecompressScripts()
{
	ScriptDecompressor dlg(this);
	if (dlg.DoModal() == IDOK)
	{
		if (dlg.m_delete_d_files)
			if (MB_ShowGeneric(_T("You have chosen to delete the .d files.\nAre you sure you want to do this?"),
				_T("Question"), MB_YESNO|MB_ICONQUESTION) == IDNO)
				return;

		ScriptDecompressAll(dlg.m_delete_d_files != FALSE);
	}
}


void CMainFrame::OnUpdateDecompressScripts(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}


void CMainFrame::OnToolsSpritenohitbydefault()
{
	sprite_nohit = !sprite_nohit;
}


void CMainFrame::OnUpdateToolsSpritenohitbydefault(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
	if (current_map != nullptr)
		pCmdUI->SetCheck(sprite_nohit);
}


void CMainFrame::OnToolsFfextract()
{
	FastfileExtract dlg;
	dlg.DoModal();
}


void CMainFrame::OnUpdateToolsFfextract(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(current_map != nullptr);
}

void CMainFrame::OnVision()
{
	// create the dialog box
	VisionChange newdlg(this);
	newdlg.m_vision = current_map->cur_vision;

	// now show the dialog box
	int retcode = newdlg.DoModal();

	// check if ok or cancel was pressed
	if (retcode == IDOK)
	{
		current_map->setVision(newdlg.m_vision);
	}
}

void CMainFrame::OnImportScreen()
{
	if (current_map->import_map == nullptr)
	{
		ImportScreen newdlg(this);
		newdlg.DoModal();
	}
}

void CMainFrame::OnSpriteReport()
{
    if (current_map != nullptr)
    {
        current_map->generateSpriteReport();
    }
}


void CMainFrame::OnViewMinimap()
{
    if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
    {
        current_map->editor_state = EditorState::Minimap;
        if (current_map->isScreenFocusMode)
        {
            current_map->isScreenFocusMode = false;
            current_map->updateMapPosition(current_map->window.left, current_map->window.top);
        }
    }
}

void CMainFrame::OnViewSpriteMode()
{
    if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
    {
        current_map->screenMode = EditorSubmode::Sprite;
        hide_all_sprites = false; // make sure sprites are visible
    }
}

void CMainFrame::OnScreenMatch()
{
    if (current_map != nullptr)
    {
        current_map->screenmatch = !current_map->screenmatch;
    }
}

void CMainFrame::OnSpriteHardness()
{
    if (current_map != nullptr)
    {
        sprite_hard = !sprite_hard;
    }
}

void CMainFrame::OnScreenShot()
{
    CScreenshotMapDialog dialog(nullptr);
    dialog.SetData(screenshot_render_options);
    if (dialog.DoModal() != IDOK)
        return;
    dialog.GetData(screenshot_render_options);
    
	TCHAR filename[MAX_PATH];

	//our file name will be Screen1.bmp to Screen999.bmp
	//this for loop will make it so we can have all 999 .bmp's if necessary
	for (int i = 1; i < 1000; i++)
	{
		//copy it all over to the file name
		wsprintf(filename, _T("%sScreen%.3d.bmp"), (LPCTSTR)current_map->dmod_path, i);

		//Try to open the file but don't create a file. If it fails, then there is no image, and we will use this file! If not, continue.
		std::ifstream fout_test(filename);
		if (!fout_test)	//the file doesn't exist
		{
			//send the file name over so we can get a screen shot!
			current_map->screenshotV2(filename, screenshot_render_options);

			return; //return when done so we don't create any more screen shots.
		}
	}
	//uh oh, the user used all the files. Lets tell them.
	MB_ShowError(_T("Too many screenshots. Please delete some and try again."));
}

void CMainFrame::OnFfcreate()
{
	Fastfile dlg(this);
	dlg.DoModal();
}

void CMainFrame::OnCompressScripts()
{
	// create the dialog box
	ScriptCompressor newdlg(this);

	// now show the dialog box
	int retcode = newdlg.DoModal();

	// check if ok or cancel was pressed
	if (retcode == IDOK)
	{
		if (newdlg.m_delete_c_files)
			if (MB_ShowGeneric(_T("You have chosen to delete the original .c files. Make sure that you have backed them up, just in case!\n\nAre you sure you wish to continue?"),
					   _T("Important!"), MB_YESNO|MB_ICONWARNING) == IDNO)
				return;

		ScriptCompressAll(newdlg.m_delete_c_files, newdlg.m_remove_comments);
	}
}

void CMainFrame::OnOptions()
{
	Options newdlg(this);

	// now show the dialog box
	int retcode = newdlg.DoModal();

	// check if ok or cancel was pressed
	if (retcode == IDOK)
	{
		screen_gap = newdlg.m_screen_gap;
		max_undo_level = newdlg.m_max_undos;
		auto_save_time = newdlg.m_autosave_time;
		show_minimap_progress = newdlg.m_show_progress != FALSE;
		tile_brush_size = newdlg.m_brush_size;
		hover_sprite_info = newdlg.m_hover_sprite_info != FALSE;
		hover_sprite_hardness = newdlg.m_hover_sprite_hardness != FALSE;
		help_text = newdlg.m_help_text != FALSE;
		snapto_offset = newdlg.m_offset;
		autoscript = newdlg.m_autoscript != FALSE;
		mouse_wheel_updown_invert = newdlg.m_invert_vertical_wheel != FALSE;
		mouse_wheel_updown_shift_invert = newdlg.m_invert_shiftvertical_wheel != FALSE;
		mouse_wheel_leftright_invert = newdlg.m_invert_horizontal_wheel != FALSE;
		use_hard_nohit_default_stamp = newdlg.m_spritestamp_hard_nohit != FALSE;
		use_hard_nohit_default_place = newdlg.m_spriteplace_hard_nohit != FALSE;
	    use_statusbar_sidebar_offset = newdlg.m_statusbar_sidebar_offset != FALSE;

	    hardness_alpha_tile = static_cast<uint8_t>(newdlg.m_hardtile_transparency);
	    hardness_alpha_sprite = static_cast<uint8_t>(newdlg.m_hardsprite_transparency);

		if (newdlg.m_sprite_preview_size*20 != sprite_selector_bmp_width)
		{
			if (current_map->editor_state == EditorState::SpriteSelector)	//RW: I do this because I want no surprises.
				current_map->editor_state = EditorState::Screen;
			sprite_selector_bmp_height = sprite_selector_bmp_width = newdlg.m_sprite_preview_size * 20;
			current_map->sprite_selector.resizeScreen();
		}

		int i=newdlg.m_minimap_detail;
		if (minimap_detail_levels[i] != square_width)		//RW: we only want to create a new surface if detail level changed
		{
			square_width = minimap_detail_levels[i];
			square_height = square_width;

			if (current_map->minimap->image)
				SDL_DestroyTexture(current_map->minimap->image);

			//RW: recreate the image surface: its size changed.
			do
			{
				current_map->minimap->image = SDL_CreateTexture(sdl_renderer, SDL_GetWindowPixelFormat(sdl_window), SDL_TEXTUREACCESS_TARGET, MINI_MAP_WIDTH, MINI_MAP_HEIGHT);
				if (current_map->minimap->image == nullptr)
				{
					square_width = minimap_detail_levels[--i];
					square_height = square_width;
				}
			} while (current_map->minimap->image == nullptr && i > 0);

			if (square_width < minimap_detail_levels[newdlg.m_minimap_detail])	//RW: we had to decrease the detail
			{
				CString minimapDetailsError;
				minimapDetailsError.Format(_T("This system is not capable of handling a minimap detail that large!\nIt had to be decreased (from %d to %d)."), newdlg.m_minimap_detail, i);
				MB_ShowWarning(minimapDetailsError);
			}

			if (newdlg.m_fast_minimap)
				for (i=0; i<NUM_SCREENS; i++)	//RW: fast minimap update needs this, or it wouldn't draw.
					current_map->miniupdated[i] = FALSE;
		}

		if(update_minimap && !newdlg.m_fast_minimap && fast_minimap_update)
		{
			int retcode2 = MB_ShowGeneric(_T("Disabling Fast Minimap Update can slow down the program if Auto Update Minimap is on!\n\nDo you wish to continue?"),
				_T("WDEP2 - Warning"), MB_YESNO | MB_ICONWARNING);	//RW: my tests suggest otherwise.

			if (retcode2 == IDYES)
			{
				fast_minimap_update = newdlg.m_fast_minimap != FALSE;
			}
		}
		else
		{
			fast_minimap_update = newdlg.m_fast_minimap != FALSE;
		}

		if ((!optimize_dink_ini) && newdlg.m_dink_ini)
		{
			int retcode2 = MB_ShowGeneric(_T("Optimize Dink Ini feature will remove all comments, redundant commands, and sort you Dink.ini file for fast loading.\n\nDo you wish to continue?"),
				_T("WDEP2 - Warning"), MB_YESNO | MB_ICONWARNING);

			if (retcode2 == IDYES)
			{
				optimize_dink_ini = newdlg.m_dink_ini != FALSE;
			}
		}
		else
		{
			optimize_dink_ini = newdlg.m_dink_ini != FALSE;
		}
		KillTimer(TIMER_AUTO_SAVE);

		if (auto_save_time > 0)
		{
			SetTimer(TIMER_AUTO_SAVE, auto_save_time * 60000, nullptr);
		}

		//RW: if minimap needs to be updated and Auto Update Minimap is on then draw it!
		if (!current_map->miniupdated[0] && update_minimap && current_map->editor_state == EditorState::Minimap)
			current_map->minimap->drawMap();
		else
			current_map->minimap->drawSquares();
	}
	Game_Main();
}

void CMainFrame::OnDmodProperties()
{
	DmodPropertiesDialog newdlg(nullptr);

	newdlg.DoModal();
}