#pragma once
#include "Const.h"
#include <array>
#include <map>

struct DUMMY_TILE
{
	int tile;
	int garbage;
	int alt_hardness;
	int garbage2[17];
};

struct TILEDATA
{
	short alt_hardness;
	short bmp;
	short x;
	short y;
};

using TileHardness = std::array<std::array<BYTE, TILEHEIGHT>, TILEWIDTH>;

template<std::size_t SZ>
struct OldFilename
{
	char s[SZ];

	OldFilename(const char* filename = "")
	{
		if (strlen(filename) + 1 > SZ)
			throw "OldFilename: Name too long.";

		strcpy(s, filename);
	}

	OldFilename(const OldFilename& other)
	{
		strcpy(s, other.s);
	}

	constexpr static std::size_t capacity() { return SZ; }

	static bool from_tchar(const TCHAR* filename, OldFilename<SZ>& result)
	{
		#ifdef UNICODE
		if (WideCharToMultiByte(CP_UTF8, 0, filename, -1, nullptr, 0, nullptr, nullptr) > SZ)
			return false;
		WideCharToMultiByte(CP_UTF8, 0, filename, -1, result.s, SZ, nullptr, nullptr);
		#else
		if (strlen(filename) + 1 > SZ)
			return false;
		strcpy(s, filename);
		#endif
		return true;
	}

	const TCHAR* as_tchar()
	{
		#ifdef UNICODE
		static WCHAR wide_buf[SZ];
		MultiByteToWideChar(CP_UTF8, 0, s, -1, wide_buf, SZ);
		return wide_buf;
		#else
		return s;
		#endif
	}
};

using EightDotThreeFilename = OldFilename<13>;
static_assert(sizeof(EightDotThreeFilename) == 13, "EightDotThreeFilename must be 13 bytes long for binary compatibility with char[13].");

using BaseScriptFilename = OldFilename<BASE_SCRIPT_MAX_LENGTH>;
static_assert(sizeof(BaseScriptFilename) == BASE_SCRIPT_MAX_LENGTH, "BaseScriptFilename must be 21 bytes long for binary compatibility with char[21].");

//Used for packing/unpacking graphics
struct Img
{
	int offset;
	EightDotThreeFilename name;
};

struct op_deref
{
	template<typename T>
	constexpr decltype(auto) operator()(T&& t) const { return *std::forward<T>(t); }
};

struct is_truelike
{
	template<typename T>
	constexpr bool operator()(T& t) const { return static_cast<bool>(t); }
};

struct is_falselike
{
	template<typename T>
	constexpr bool operator()(T& t) const { return !static_cast<bool>(t); }
};

struct SpriteRenderOptions
{
    bool renderInvisible;
    bool renderIndoor;
    std::map<int32_t, std::map<int32_t, bool>> sequenceFilter;

    SpriteRenderOptions()
        : renderInvisible(false)
        , renderIndoor(true)
    {
    }
};