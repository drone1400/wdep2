#pragma once
#include "Sprite.h"

#define MAX_SPRITE_NAME_LENGTH 20

#ifdef UNICODE
typedef std::wstring TStdString;
#else
typedef std::string TStdString;
#endif

class SpriteLibrary
{
public:
	auto begin() const { return library.cbegin(); }
	auto end() const { return library.cend(); }

	Sprite getSprite(TStdString const& name);
	bool doesExist(TStdString const& name);
	void addSprite(Sprite* cur_sprite, TStdString const& name);
	void renameSprite(TStdString const& old_name, TStdString const& new_name);
	void removeSprite(TStdString const& name);

	void storeList();
	void readList(CString const& dmod_path);
	
private:
	std::unordered_map<TStdString, Sprite> library;
};
