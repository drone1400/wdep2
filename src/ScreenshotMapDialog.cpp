#include "stdafx.h"
#include "ScreenshotMapDialog.h"

#include <sstream>

#include "StringUtil.h"
#include "Structs.h"


CScreenshotMapDialog::CScreenshotMapDialog(CWnd* pParent /*=nullptr*/) : CDialog(CScreenshotMapDialog::IDD, pParent)	
{
    m_renderInvisible = false;
    m_renderIndoor = true;
    m_sequenceFilters = _T("");
}

void CScreenshotMapDialog::GetData(SpriteRenderOptions& options)
{
    std::string filters = cs2s(m_sequenceFilters);

    options.sequenceFilter.clear();

    std::istringstream iss1(filters);
    std::string pair;

    int32_t errors = 0;
    
    while (std::getline(iss1,pair,';'))
    {
        std::istringstream iss2(pair);
        std::string str_seq;
        std::string str_frame;
        
        if (std::getline(iss2, str_seq, ',') &&
            std::getline(iss2, str_frame, ','))
        {
            try
            {
                int32_t seq = std::stoi(str_seq);
                int32_t frame = std::stoi(str_frame);
                options.sequenceFilter[seq][frame] = false;
            } catch (...)
            {
                // ignore pair?...
                errors++;
            }
        }
    }
    
    options.renderInvisible = m_renderInvisible;
    options.renderIndoor = m_renderIndoor;
}

void CScreenshotMapDialog::SetData(const SpriteRenderOptions& options)
{
    m_renderInvisible = options.renderInvisible;
    m_renderIndoor = options.renderIndoor;

    m_sequenceFilters = _T("");
    for (const auto& it1 : options.sequenceFilter)
    {
        for (const auto& it2 : it1.second)
        {
            if (it2.second == false)
            {
                m_sequenceFilters.AppendFormat(_T("%d, %d; "), it1.first, it2.first);
            }
        }
    }
}



void CScreenshotMapDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_SCREENSHOTMAP_RENDERINVISIBLE, m_renderInvisible);
	DDX_Check(pDX, IDC_SCREENSHOTMAP_RENDERINDOOR, m_renderIndoor);
    DDX_Text(pDX, IDC_SCREENSHOTMAP_SEQFILTER, m_sequenceFilters);
}

BEGIN_MESSAGE_MAP(CScreenshotMapDialog, CDialog)
END_MESSAGE_MAP()


void CScreenshotMapDialog::OnOK()
{
	//close dialog
	CDialog::OnOK();
}
