#pragma once

// ------------------------------------------------------------------------------------------------------------------------------
// SDL helpers
//
bool PrintDmodScreen(const TCHAR *filename);

bool loadBitmapFont();
void unloadBitmapFont();
void drawText(const TCHAR *text, int x, int y, SDL_Color colorFore, SDL_Color colorBack);

bool fixBounds(RECT &dest_box, RECT &src_box, RECT &clip_box);
bool fixBounds(RECT &box, RECT &clip_box);
void drawBox(int x, int y, int width, int height, SDL_Color color, int line_width=1);
void drawBox(RECT box, SDL_Color color, int line_width=1);
void drawFilledBox(RECT box, SDL_Color color);
void drawClearAll(SDL_Color color);
void drawLine(int x1, int y1, int x2, int y2, SDL_Color color);
void drawX(int x1, int y1, int x2, int y2, SDL_Color color);
void drawX(RECT rect, SDL_Color color);
void ScreenText(const TCHAR output[MAX_PATH], int y_offset);

SDL_Rect RECT_to_SDL(RECT rect);
bool drawTexture(SDL_Texture* texture, RECT* src, RECT* dest);
bool drawTextureClipped(SDL_Texture* texture, RECT* src, RECT* dest, RECT* clip);

//
// ------------------------------------------------------------------------------------------------------------------------------