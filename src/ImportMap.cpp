#include "StdAfx.h"
#include "ImportMap.h"
#include "Globals.h"
#include "Screen.h"
#include "Engine.h"
#include "MainFrm.h"
#include "Map.h"
#include "Minimap.h"
#include "Sprite.h"
#include "undo.h"
#include "Actions/ScreenImport.h"


void removeImportMap()
{
	if (current_map->import_map != nullptr)
	{
		delete current_map->import_map;
		current_map->import_map = nullptr;
	}
	// show screen
	current_map->minimap->drawSquares();
	current_map->editor_state = EditorState::Minimap;
	Game_Main();
}

ImportMap::ImportMap()
{
	src_dmod_path = current_map->import_dmod_path;

	load_failed = !loadMap(src_dmod_path);

	source_tile = -1;
	dest_tile = -1;
}

ImportMap::~ImportMap()
{
}

int ImportMap::loadMap(CString pathname)
{
	FILE *stream;

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)pathname, _T("dink.dat"));

	if ((stream = _tfopen(filename, _T("rb"))) == nullptr)
		return false;

	fseek(stream, 24, SEEK_SET);

	// read in the order the screens were created
	// this lets us know where in the map.dat each screen is located
	fread(screen_order, sizeof(int) * NUM_SCREENS, 1, stream);

	fseek(stream, 4, SEEK_CUR);

	// read in midi numbers for each screen
	fread(midi_num, sizeof(int) * NUM_SCREENS, 1, stream);

	fseek(stream, 4, SEEK_CUR);

	// read in whether the screen is indoor or outdoor
	fread(indoor, sizeof(int) * NUM_SCREENS, 1, stream);

	fclose(stream);

	return true;
}

// a screen was selected
void ImportMap::tileClicked()
{
	if (source_tile == -1)
	{
		source_tile = current_map->minimap->getHoverScreen();	//RW: moved this from below to above the next 2 lines - would make no sense that way, I think it's useful now, though.

		if (screen_order[source_tile] == 0)		//RW: if inexistent screen selected
		{
			MB_ShowError(_T("You must select a non-empty screen to import!"));
			source_tile = -1;
			return;	
		}

		current_map->minimap->drawSquares();
		Game_Main();
	}
	else
	{
		// load the new screen
		dest_tile = current_map->minimap->getHoverScreen();

		if (current_map->screen[dest_tile] != nullptr)
		{
			MB_ShowError(_T("You cannot import a screen over an existing one!\n\nEither select an empty screen to import to, or cancel the import, delete the target screen, and try again!"));
			return;
		}
	
		importScreen(src_dmod_path, source_tile, dest_tile);

		current_map->minimap->drawSquares();
		
		removeImportMap();
	}
}

// draw the import map
bool ImportMap::render()
{
	if (source_tile == -1)
		current_map->minimap->renderImportMap(screen_order, midi_num, indoor);

	current_map->minimap->render();

	return true;
}

// import the selected screen to the destination screen
int ImportMap::importScreen(CString pathname, int src_screen, int dest_screen)
{
	FILE *stream;

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)pathname, _T("map.dat"));

	// find the next screen to load
	int map_location = screen_order[src_screen] - 1;
	if (map_location == -1)
	{
		return false;
	}

	if ((stream = _tfopen(filename, _T("rb"))) == nullptr)
		return false;

	// go to that spot in the map file to load
	fseek(stream, map_location * SCREEN_DATA_SIZE, SEEK_SET);

	// advance to first tile
	fseek(stream, 20, SEEK_CUR);

	// if old screen doesn't exist, create a new one
	if (current_map->screen[dest_screen] != nullptr)
	{
		delete current_map->screen[dest_screen];
		current_map->screen[dest_screen] = nullptr;
	}

	current_map->screen[dest_screen] = new Screen;

	int temp;

	DUMMY_TILE tile_data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];

	// read in the tile data
	fread(tile_data, sizeof(DUMMY_TILE) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH, 1, stream);
	
	for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
		{
			temp = tile_data[y][x].tile;
			// make the map format easier to read
			current_map->screen[dest_screen]->tiles[y][x].bmp = short((temp >> 7) & ((1 << 9) - 1));
			if (current_map->screen[dest_screen]->tiles[y][x].bmp < 0 || current_map->screen[dest_screen]->tiles[y][x].bmp > 40)
			{
				current_map->screen[dest_screen]->tiles[y][x].bmp = 30;
			}
			current_map->screen[dest_screen]->tiles[y][x].x = short((temp & ((1 << 7) - 1)) % 12);
			current_map->screen[dest_screen]->tiles[y][x].y = short((temp & ((1 << 7) - 1)) / 12);

			current_map->screen[dest_screen]->tiles[y][x].alt_hardness = tile_data[y][x].alt_hardness;
		}
	}
	
	// advance to sprite data
	fseek(stream, 540, SEEK_CUR);

	Sprite* new_sprite;

	memset(current_map->screen[dest_screen]->sprite, 0 , MAX_SPRITES * sizeof(Sprite*));
		
	DUMMY_SPRITE* sprite_data = (DUMMY_SPRITE*) malloc (MAX_SPRITES * sizeof(DUMMY_SPRITE));
	if (sprite_data == nullptr)
		return false;

	// read in a sprite block
	if (fread(sprite_data, sizeof(DUMMY_SPRITE) * MAX_SPRITES, 1, stream) == 0)
	{
		free (sprite_data);
		return false;
	}
	int next_sprite = 0;

	// read in the sprite data
	for (int cur_sprite = 0; cur_sprite < MAX_SPRITES; cur_sprite++)
	{
		// check if there's anything in the sprite block
		if (sprite_data[cur_sprite].sprite_exist == true)
		{
			// found a sprite, now allocate the structure for it
			new_sprite = new Sprite(sprite_data[cur_sprite]);

			current_map->screen[dest_screen]->sprite[next_sprite] = new_sprite;
			next_sprite++;
		}
	}

	fclose(stream);
	free (sprite_data);

	// copy over the midi number and indoor status
	current_map->midi_num[dest_screen] = midi_num[src_screen];
	current_map->indoor[dest_screen] = indoor[src_screen];

	
	current_map->undo_buffer->PushAndDo<actions::ScreenImport>(dest_screen, current_map->screen[dest_screen], midi_num[src_screen], indoor[src_screen]);

	return true;
}


