#include "stdafx.h"
#include "SpriteProperties.h"
#include "Common.h"
#include "Interface.h"
#include "Sprite.h"
#include "Map.h"
#include "PathUtil.h"


SpriteProperties::SpriteProperties(Sprite* backingSprite, CWnd* pParent /*=nullptr*/) : CDialog(IDD, pParent)
{
	sprite = backingSprite;

	m_base_attack = sprite->base_attack;
	m_base_death = sprite->base_death;
	m_base_idle = sprite->base_idle;
	m_base_walk = sprite->base_walk;
	m_brain = sprite->brain;
	m_defense = sprite->defense;
	m_depth_que = sprite->depth;
	m_enable_warp = sprite->warp_enabled;
	m_frame = sprite->frame;
	m_hard = !sprite->hardness;
	m_hitpoints = sprite->hitpoints;
	m_nohit = sprite->nohit;
	m_sequence = sprite->sequence;
	m_sound = sprite->sound;
	m_speed = sprite->speed;
	m_timing = sprite->timing;
	m_touch_damage = sprite->touch_damage;
	m_touch_sequence = sprite->touch_sequence;
	m_vision = sprite->vision;
	m_warp_screen = sprite->warp_screen;
	m_warp_x = sprite->warp_x;
	m_warp_y = sprite->warp_y;
	m_x = sprite->x;
	m_y = sprite->y;
	m_size = sprite->size;
	m_type = static_cast<short>(sprite->type);
	m_experience = sprite->experience;
	m_script = sprite->script.s;
	// recently added properties
	m_base_hit = sprite->base_hit;
	m_strength = sprite->strength;
	m_gold = sprite->gold_dropped;
	m_trim_left = sprite->trim_left;
	m_trim_right = sprite->trim_right;
	m_trim_top = sprite->trim_top;
	m_trim_bottom = sprite->trim_bottom;
}

BOOL SpriteProperties::OnInitDialog()
{
	CDialog::OnInitDialog();

	setBrain(m_brain);

	return TRUE;
}

void SpriteProperties::saveSpriteData()
{
	sprite->base_attack = m_base_attack;
	sprite->base_death = m_base_death;
	sprite->base_idle = m_base_idle;
	sprite->base_walk = m_base_walk;
	sprite->brain = m_brain;
	sprite->defense = m_defense;
	sprite->depth = m_depth_que;
	sprite->warp_enabled = m_enable_warp;
	sprite->frame = m_frame;
	sprite->hardness = !m_hard;
	sprite->hitpoints = m_hitpoints;
	sprite->nohit = m_nohit;
	sprite->sequence = m_sequence;
	sprite->sound = m_sound;
	sprite->speed = m_speed;
	sprite->timing = m_timing;
	sprite->touch_damage = m_touch_damage;
	sprite->touch_sequence = m_touch_sequence;
	sprite->vision = m_vision;
	sprite->warp_screen = m_warp_screen;
	sprite->warp_x = m_warp_x;
	sprite->warp_y = m_warp_y;
	sprite->x = m_x;
	sprite->y = m_y;
	sprite->size = m_size;
	sprite->type = static_cast<SpriteType>(m_type);
	sprite->experience = m_experience;
	EightDotThreeFilename::from_tchar(m_script.GetBuffer(0), sprite->script);
	// recently added properties
	sprite->base_hit = m_base_hit;
	sprite->strength = m_strength;
	sprite->gold_dropped = m_gold;
	sprite->trim_left = m_trim_left;
	sprite->trim_right = m_trim_right;
	sprite->trim_top = m_trim_top;
	sprite->trim_bottom = m_trim_bottom;

	// sets the world vision to the sprite's new vision
	//setVision(m_vision);
}

void SpriteProperties::setBrain(int brain)
{
	m_brain = brain;

	if (brain >= 0 && brain < 18)
	{
		m_brainDropdown.SetCurSel(brain);
		m_brainTextbox.EnableWindow(FALSE);
	}
	else
	{
		m_brainDropdown.SetCurSel(18); //"custom"
		m_brainTextbox.EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}

void SpriteProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_X, m_x);
	DDX_Text(pDX, IDC_Y, m_y);
	DDX_Text(pDX, IDC_Vision, m_vision);
	DDX_Text(pDX, IDC_Touch_damage, m_touch_damage);
	DDX_Text(pDX, IDC_Script, m_script);
	DDV_MaxChars(pDX, m_script, (int)EightDotThreeFilename::capacity()-1);
	DDX_Check(pDX, IDC_Nohit, m_nohit);
	DDX_Check(pDX, IDC_Hard, m_hard);
	DDX_Check(pDX, IDC_Enable_warp, m_enable_warp);
	DDX_Text(pDX, IDC_Base_walk, m_base_walk);
	DDV_MinMaxInt(pDX, m_base_walk, -1, MAX_SEQUENCES);
	DDX_Text(pDX, IDC_Base_death, m_base_death);
	DDV_MinMaxInt(pDX, m_base_death, -1, MAX_SEQUENCES);
	DDX_Text(pDX, IDC_Base_attack, m_base_attack);
	DDV_MinMaxInt(pDX, m_base_attack, -1, MAX_SEQUENCES);
	DDX_Text(pDX, IDC_Depth_que, m_depth_que);
	DDX_Text(pDX, IDC_Base_idle, m_base_idle);
    DDV_MinMaxInt(pDX, m_base_idle, -1, MAX_SEQUENCES);
    DDX_Text(pDX, IDC_Base_hit, m_base_hit);
    //DDV_MinMaxInt(pDX, m_base_hit, -1, MAX_SEQUENCES);
	DDX_Radio(pDX, IDC_ORNAMENTAL, m_type);
	DDX_Text(pDX, IDC_Trim_left, m_trim_left);
	DDX_Text(pDX, IDC_Trim_right, m_trim_right);
	DDX_Text(pDX, IDC_Trim_top, m_trim_top);
	DDX_Text(pDX, IDC_Trim_bottom, m_trim_bottom);
	DDX_Text(pDX, IDC_Experience_Given, m_experience);
	DDX_Text(pDX, IDC_Gold, m_gold);
	DDX_Text(pDX, IDC_Strength, m_strength);
	DDX_Text(pDX, IDC_Defense, m_defense);
	DDX_Text(pDX, IDC_Hitpoints, m_hitpoints);
	DDX_Text(pDX, IDC_Sequence, m_sequence);
	DDV_MinMaxInt(pDX, m_sequence, 1, MAX_SEQUENCES);
	DDX_Text(pDX, IDC_Size, m_size);
	DDX_Text(pDX, IDC_Sound, m_sound);
	DDX_Text(pDX, IDC_Speed, m_speed);
	DDX_Text(pDX, IDC_Frame, m_frame);
	DDV_MinMaxInt(pDX, m_frame, 1, MAX_FRAMES);
	DDX_Text(pDX, IDC_Timing, m_timing);
	DDX_Text(pDX, IDC_Touch_sequence, m_touch_sequence);
	DDX_Text(pDX, IDC_Warp_screen, m_warp_screen);
	DDV_MinMaxInt(pDX, m_warp_screen, 0, 768);
	DDX_Text(pDX, IDC_Warp_x, m_warp_x);
    DDV_MinMaxInt(pDX, m_warp_x, 0, SCREEN_WIDTH+SIDEBAR_WIDTH-1);
	DDX_Text(pDX, IDC_Warp_y, m_warp_y);
    DDV_MinMaxInt(pDX, m_warp_y, 0, SCREEN_HEIGHT-1);
	DDX_Text(pDX, IDC_Custom_Brain, m_brain);
	DDV_MinMaxInt(pDX, m_brain, 0, MAXINT);
	DDX_Control(pDX, IDC_BRAIN, m_brainDropdown);
	DDX_Control(pDX, IDC_Custom_Brain, m_brainTextbox);

	saveSpriteData();
}


BEGIN_MESSAGE_MAP(SpriteProperties, CDialog)
	ON_BN_CLICKED(IDC_EDIT_SPRITE_SCRIPT, OnEditSpriteScript)
	ON_BN_CLICKED(IDC_BROWSE_SCRIPT, OnBrowseScript)
	ON_BN_CLICKED(IDC_AUTO1, OnAuto1)
	ON_CBN_SELCHANGE(IDC_BRAIN, &SpriteProperties::OnCbnSelchangeBrain)
END_MESSAGE_MAP()


// open up the script in the default text editor
void SpriteProperties::OnEditSpriteScript() 
{
	UpdateData();

	if (m_script.GetLength() > 0)
	{
		CString scriptfile = current_map->dmod_path;
		scriptfile += "story\\";
		scriptfile += m_script;
		scriptfile += ".c";

		if (!FileReadable((LPCTSTR) scriptfile)) //RW: if the .c file doesn't exist
		{
			scriptfile.SetAt(scriptfile.GetLength()-1, 'd');
			if (!FileReadable((LPCTSTR) scriptfile)) //RW: if the .d file doesn't exist
			{
				scriptfile.SetAt(scriptfile.GetLength()-1, 'c');
				FILE* f;
				if ((f=_tfopen((LPCTSTR) scriptfile, _T("wt"))) == nullptr)
					MB_ShowError(_T("Could not open or create the script file!"));
				else
				{
					fclose(f);
					editScript(m_script);
				}
			}
			else
				editScript(m_script + ".d");
		}
		else
			editScript(m_script);
	}
}

void SpriteProperties::OnBrowseScript() 
{
	CFileDialog dialog(true, _T(".c"), nullptr, 0, _T("C Files (*.c)|*.c|All Files (*.*)|*.*||"));

	CString scriptdir = current_map->dmod_path;
	scriptdir += "story";
	dialog.m_ofn.lpstrInitialDir = (LPCTSTR)scriptdir;
	
	if (dialog.DoModal() == IDOK)
	{
		m_script = dialog.GetFileName();
		m_script = m_script.Left(m_script.GetLength()-2); //remove ext
		UpdateData(FALSE);
	}
}

void SpriteProperties::OnAuto1() 
{
	CString tmp;
	tmp.Format(_T("%u"), m_sequence);
	CString tmp2 = tmp.Left(tmp.GetLength() - 1);
	tmp2+='0';

	m_base_walk = _ttoi(tmp2);

	//UpdateData(FALSE);
	CDataExchange dx(this, FALSE);
	DDX_Text(&dx, IDC_Base_walk, m_base_walk);
	DDV_MinMaxInt(&dx, m_base_walk, -1, 1000);
}


void SpriteProperties::OnCbnSelchangeBrain()
{
	int sel = m_brainDropdown.GetCurSel();
	if (sel == CB_ERR)
		return;

    UpdateData(TRUE);
	setBrain(sel);
}
