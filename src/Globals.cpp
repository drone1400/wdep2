#include "StdAfx.h"
#include "Globals.h"

SDL_Window* sdl_window = nullptr;
SDL_Renderer* sdl_renderer = nullptr;

SDL_Texture* temp_texture = nullptr;
SDL_Texture* temp_sprite_fallback = nullptr;

RECT mapRect;	// stores the location of the drawing window

Map* current_map = nullptr;

Tile* tileBmp[MAX_TILE_BMPS];

SpriteEditor* spriteeditor;

BYTE vision_used[VISIONS_TRACKED];
BYTE new_vision_used[VISIONS_TRACKED];

bool need_resizing = true;
bool displayhardness = false;
bool sprite_hard = false;
bool sprite_nohit = false;
bool hide_all_sprites = false;
bool show_sprite_info = false;
bool show_minimap_progress = true;
bool optimize_dink_ini = false;
bool fast_minimap_update = true;
bool hover_sprite_info = false;
bool hover_sprite_hardness = false;
bool help_text = true;
bool update_minimap = false;
bool snapto_grid = false;
bool autoscript = true;
bool hardedit_show_texture = true;

int snapto_offset = 16;

int help_text_color_index = 1;
SDL_Color help_text_color = helpcolors[help_text_color_index];
SDL_Color help_text_color_back = COLOR_BCK_BLACK1;

int screen_gap = DEFAULT_SCREEN_GAP;
UINT max_undo_level = DEFAULT_MAX_UNDO_LEVEL;
int auto_save_time = 0;
int tile_brush_size = 0;

CString dink_exe_path;
CString dink_core_path;
CString dink_dmods_path;
CString dink_skeleton_path;
bool is_dink_core_path_override_enabled = false;
CString dink_core_path_override;

CString loaded_dmod_path = "";
CString WDE_path;

int square_width = 20;
int square_height = 20;

//RW: these are for saving the state of the Play DMOD dialog.
BOOL play_truecolor=TRUE;
BOOL play_windowed=FALSE;
BOOL play_sound=TRUE;
BOOL play_joystick=FALSE;
BOOL play_debug=FALSE;
BOOL play_pathquotes=TRUE;

int sprite_selector_bmp_width = 40;
int sprite_selector_bmp_height = 40;

bool mouse_wheel_updown_invert = false;
bool mouse_wheel_updown_shift_invert = false;
bool mouse_wheel_leftright_invert = false;

const int minimap_detail_levels[] = {1, 10, 20, 25, 50, 75, 100};

bool use_hard_nohit_default_stamp = true;
bool use_hard_nohit_default_place = false;

bool detect_screenmatch_duplicates = true;

SpriteRenderOptions screenshot_render_options = SpriteRenderOptions();

uint8_t hardness_alpha_tile = 255;
uint8_t hardness_alpha_sprite = 255;

bool use_statusbar_sidebar_offset = true;