#include "StdAfx.h"
#include "Screen.h"
#include "Globals.h"
#include "Tile.h"
#include "Map.h"
#include "Engine.h"
#include "Sequence.h"
#include "HardTileSelector.h"
#include "Sprite.h"
#include "Common.h"
using std::vector;

// creates a blank screen with just grass tiles
Screen::Screen()
{
	// initialize the sprites
	memset(sprite, 0, MAX_SPRITES * sizeof(sprite[0]));

	// initialize tiles (bitmaps and hardness)
	memset(tiles, 0, SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH * sizeof(TILEDATA));

	// set base script to nothing.
	memset(script.s, 0, BASE_SCRIPT_MAX_LENGTH);
}

Screen::Screen(Screen* copy_screen)
{
	// copy tiles
	memcpy(tiles, copy_screen->tiles, sizeof(TILEDATA) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH);

	// initialize the sprites
	memset(sprite, 0, MAX_SPRITES * sizeof(sprite[0]));

	// now copy over the ones necessary
	int cur_sprite = 0;
	for (int i = 0; i < MAX_SPRITES; i++)
	{
		if (copy_screen->sprite[i])
		{
			sprite[cur_sprite] = new Sprite(*copy_screen->sprite[i]);
			cur_sprite++;
		}
	}

	// copy over the base script for the screen
	script = copy_screen->script;
}

Screen::~Screen()
{
	// free all the sprites on the screen
	for (int i = 0; i < MAX_SPRITES; i++)
	{
		if (sprite[i])
		{
			delete sprite[i];
			sprite[i] = nullptr;
		}
	}
}

// draw all the hardness for the tiles on the screen
int Screen::drawHardTiles(int x_offset, int y_offset)
{
	int hard_tile_num;

	for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
		{
			if (tiles[y][x].alt_hardness != 0)
			{
				// use the user specified hardness
				hard_tile_num = tiles[y][x].alt_hardness;
			}
			else
			{
				// use the default hardness for this tile
				hard_tile_num = tileBmp[tiles[y][x].bmp]->tile_hardness[tiles[y][x].y][tiles[y][x].x];
			}

			// render the tile
			current_map->hard_tile_selector.renderHardness(x * TILEWIDTH + x_offset, y * TILEHEIGHT + y_offset,
				TILEWIDTH, TILEHEIGHT,
				hard_tile_num, true);
		}
	}
	return true;
}

// draw all the sprite hardboxes for the screen
int Screen::drawHardSprites(int x_offset, int y_offset, RECT &clip_rect)
{
	Sequence *cur_seq;
	// read in the sprites to a sprite list so we can sort for depth dot
	for (int i = 0; i < MAX_SPRITES; i++)
	{
		// render the sprite
		if (sprite[i] != nullptr)
		{
			// if vision doesn't match, go on to the next possible sprite
			int sprite_vision = sprite[i]->vision;
			if ((sprite_vision != current_map->cur_vision) && (sprite_vision != 0))
				continue;

			cur_seq = current_map->sequence[sprite[i]->sequence];
			if (cur_seq != nullptr)
			{
				cur_seq->clipRenderHardness(x_offset, y_offset, sprite[i], clip_rect);
			}
		}
	}

	return true;
}

// draw all the tiles for the screen
int Screen::drawTiles(int x_offset, int y_offset, RECT* cliprect /*= nullptr*/)
{
	RECT dest_rect;

	dest_rect.top    = y_offset;
	dest_rect.bottom = y_offset + TILEHEIGHT;

	for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
	{
		dest_rect.left   = x_offset;
		dest_rect.right  = x_offset + TILEWIDTH;

		for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
		{
			if (tiles[y][x].bmp < 0)
				tiles[y][x].bmp = 30;
			
			// render the tile
			tileBmp[tiles[y][x].bmp]->render(dest_rect, tiles[y][x].x, tiles[y][x].y, cliprect);

			dest_rect.left   += TILEWIDTH;
			dest_rect.right  += TILEWIDTH;
		}
		dest_rect.top    += TILEHEIGHT;
		dest_rect.bottom += TILEHEIGHT;
	}
	return true;
}


// draw all the sprites to the screen
int Screen::drawSprites(int x_offset, int y_offset, RECT &clip_rect, int drawn_screen /*= -1*/, SpriteRenderOptions * render_options /*= nullptr*/)
{
	if (drawn_screen == -1)
		drawn_screen = current_map->hoverScreen;

	struct SPRITEQUE
	{
		Sprite* sprite;
		int depth;
		int sprite_num;
	};

	const auto drawDepth = [](Sprite const& sp) { return sp.depth == 0 ? sp.y : sp.depth; };

	vector<SPRITEQUE> background_sprites;
	vector<SPRITEQUE> foreground_sprites;
    
	
	for (int i = 0; i < MAX_SPRITES; i++)
	{
		if (sprite[i] == nullptr)
			continue;

		// used for tracking visions on the sidebar
		if (sprite[i]->vision < VISIONS_TRACKED)
			new_vision_used[sprite[i]->vision] = 1;

		if (sprite[i]->vision != 0 && sprite[i]->vision != current_map->cur_vision)
			continue;

		if (sprite[i]->type == SpriteType::Background)
			background_sprites.push_back(SPRITEQUE{ sprite[i], drawDepth(*sprite[i]), i });
		else if (render_options == nullptr)
		{
		    foreground_sprites.push_back(SPRITEQUE{ sprite[i], drawDepth(*sprite[i]), i });
		} else {
		    // check render filter options...
		    if (sprite[i]->type != SpriteType::Invisible || render_options->renderInvisible)
		    {
		        bool render_this = true;
		        auto it1 = render_options->sequenceFilter.find(sprite[i]->sequence);
		        if (it1 != render_options->sequenceFilter.end())
		        {
		            auto it2 = it1->second.find(sprite[i]->frame);
		            if (it2 != it1->second.end())
		            {
		                render_this = it2->second;
		            }
		        }
		        if (render_this)
		        {
		            foreground_sprites.push_back(SPRITEQUE{ sprite[i], drawDepth(*sprite[i]), i });
		        }
		    }
		}
	}

	vector<int> old_xs, old_ys;

	Sprite* mouseSprite = current_map->getMouseSprite();
	int mouseSprite_x_old{}, mouseSprite_y_old{};
	if (mouseSprite != nullptr)
	{
		mouseSprite_x_old = mouseSprite->x;
		mouseSprite_y_old = mouseSprite->y;

		mouseSprite->x -= x_offset - SIDEBAR_WIDTH;
		mouseSprite->y -= y_offset;

		compensateScreenGap(mouseSprite->x, mouseSprite->y);
		
		if (mouseSprite->type == SpriteType::Background)
			background_sprites.push_back(SPRITEQUE{ mouseSprite, drawDepth(*mouseSprite), -1 });
		else
			foreground_sprites.push_back(SPRITEQUE{ mouseSprite, drawDepth(*mouseSprite), -1 });
	}
	else if (current_map->mouseMultiSprites.size() > 0)
	{
		for (auto it = current_map->mouseMultiSprites.begin(); it != current_map->mouseMultiSprites.end(); ++it)
		{
			old_xs.push_back(it->sprite.x);
			old_ys.push_back(it->sprite.y);

			it->sprite.x = mouse_x_position + it->x_offset - (x_offset - SIDEBAR_WIDTH);
			it->sprite.y = mouse_y_position + it->y_offset - y_offset;

			if (current_map->hoverRelX > SCREEN_WIDTH) it->sprite.x -= current_map->hoverRelX - SCREEN_WIDTH;
			if (current_map->hoverRelY > SCREEN_HEIGHT) it->sprite.y -= current_map->hoverRelY - SCREEN_HEIGHT;

			it->sprite.x += screen_gap * (drawn_screen % MAP_COLUMNS - current_map->hoverScreenX);
			it->sprite.y += screen_gap * (drawn_screen / MAP_COLUMNS - current_map->hoverScreenY);

			RECT r;
		    if (current_map->sequence[it->sprite.sequence] != nullptr)
		    {
		        current_map->sequence[it->sprite.sequence]->getBounds(it->sprite.frame - 1, it->sprite.size, r);
		    } else
		    {
		        Sequence::getBoundsFallback(it->sprite.frame - 1, it->sprite.size, r);
		    }

		    int frame_width = SPRITE_FALLBACK_WIDTH;
		    int frame_height = SPRITE_FALLBACK_HEIGHT;
		    if (current_map->sequence[it->sprite.sequence] != nullptr)
		    {
		        frame_width = current_map->sequence[it->sprite.sequence]->frame_width[it->sprite.frame - 1];
		        frame_height = current_map->sequence[it->sprite.sequence]->frame_height[it->sprite.frame - 1];
		    }

			//if the sprite would actually be drawn
			if (it->sprite.x + r.left < SCREEN_WIDTH + SIDEBAR_WIDTH &&
				it->sprite.x + r.right + frame_width > 0 &&
				it->sprite.y + r.top < SCREEN_HEIGHT &&
				it->sprite.y + r.bottom + frame_height > 0)
			{
				if (it->sprite.type == SpriteType::Background)
					background_sprites.push_back(SPRITEQUE{ &it->sprite, drawDepth(it->sprite), -1 });
				else
					foreground_sprites.push_back(SPRITEQUE{ &it->sprite, drawDepth(it->sprite), -1 });
			}
		}
	}
	
	std::sort(background_sprites.begin(), background_sprites.end(), [](auto& sq1, auto& sq2) {return sq1.depth == sq2.depth ? sq1.sprite_num < sq2.sprite_num : sq1.depth < sq2.depth;});
	std::sort(foreground_sprites.begin(), foreground_sprites.end(), [](auto& sq1, auto& sq2) {return sq1.depth == sq2.depth ? sq1.sprite_num < sq2.sprite_num : sq1.depth < sq2.depth;});

	background_sprites.insert(background_sprites.end(), foreground_sprites.begin(), foreground_sprites.end());

    vector<SPRITEQUE> fallback_sprites;
    
	// now print out the sprites
	for (const auto& spque : background_sprites)
	{
		
		Sequence* cur_seq = current_map->sequence[spque.sprite->sequence];
        ClipRenderReturn ret_code = ClipRender_BadFrameData;
	    
		if (cur_seq != nullptr)
			ret_code = cur_seq->clipRender(x_offset, y_offset, spque.sprite, clip_rect, spque.sprite_num, show_sprite_info);

	    if (ret_code == ClipRender_BadFrameData)
	    {
	        // render fallback sprite after done with all good sprites...
	        // this makes sure the sprite doesn't get drawn over by some other sprite
	        // thus making sure that the user notices it
	        fallback_sprites.emplace_back(spque);
	    }
	}

    for (const auto& spque : fallback_sprites)
    {
        Sequence::clipRenderFallback(x_offset, y_offset, spque.sprite, clip_rect, spque.sprite_num, show_sprite_info);
    }
    

	// restore old mouseSprite position
	if (mouseSprite != nullptr)
	{
		mouseSprite->x = mouseSprite_x_old;
		mouseSprite->y = mouseSprite_y_old;
	}
	else if (current_map->mouseMultiSprites.size() > 0)
	{
		auto itx = old_xs.begin();
		auto ity = old_ys.begin();
		auto itms = current_map->mouseMultiSprites.begin();

		for (; itx != old_xs.end(); ++itx, ++ity, ++itms)
		{
			itms->sprite.x = *itx;
			itms->sprite.y = *ity;
		}
	}

	return true;
}

// returns the sprite located at the x and y locations and detaches it from the screen if erase=true
Sprite* Screen::getSpriteAtXy(int x, int y, int &sprite_num)
{
	RECT bounds; 
  
	int old_area = 9999999; //silly rig
	sprite_num = -1; 
  
	for (int i = 0; i < MAX_SPRITES; i++) 
	{ 
		 // check if sprite exists 
        if (sprite[i] != nullptr && (sprite[i]->vision == 0 || sprite[i]->vision == current_map->cur_vision)) 
        { 
            if (sprite[i]->getImageBounds(bounds) == TRUE ||
                sprite[i]->getImageBoundsFallback(bounds) == TRUE) 
            { 
                // check bounds 
                if ((y > bounds.top) && (y < bounds.bottom) && (x > bounds.left) && (x < bounds.right)) 
                { 
                    int area = (bounds.bottom - bounds.top)*(bounds.right - bounds.left); 

                    if (area < old_area) 
                    { 
                         old_area = area; 
                         sprite_num = i; 
                    } 
                } 
            }
        } 
	} 
 
	
	if (sprite_num == -1)
		return nullptr;

	return sprite[sprite_num]; 
}

// places a sprite on the screen
bool Screen::addSprite(int sprite_num, Sprite const& new_sprite)
{
	int available_slot = sprite_num;

	for (; available_slot < MAX_SPRITES-1; available_slot++)
		if (sprite[available_slot] == nullptr)	// check if sprite exists
			break;	

	if (available_slot >= MAX_SPRITES-1)	//can't add sprites, there are already 100 on this screen
		return false;

	// make another mouseSprite to add to the next screen
	sprite[available_slot] = new Sprite(new_sprite);

	return true;
}

// deletes a sprite from the screen
void Screen::removeSprite(int sprite_num)
{
	if (sprite[sprite_num])
	{
		delete sprite[sprite_num];
		sprite[sprite_num] = nullptr;
	}
}

// returns the number of the first available sprite slot
int Screen::findFirstAvailableSpriteSlot()
{
	for (int i = 0; i < MAX_SPRITES; i++)
		if (sprite[i] == nullptr)
			return i;

	return -1;
}
