#include "StdAfx.h"
#include "Tile.h"
#include "Globals.h"
#include "ddutil.h"
#include "Map.h"
#include "CommonSdl.h"

Tile::Tile(int file_source)
{
	file_location = file_source;
	image = nullptr;
}

Tile::~Tile()
{
	if (image != nullptr)
	{
		SDL_DestroyTexture(image);
		image = nullptr;
	}
}

int Tile::loadSurface()
{
	assert(image == nullptr);

	TCHAR bmp_file[MAX_PATH];
	TCHAR png_file[MAX_PATH];
	
	wsprintf(bmp_file, _T("%sTiles\\Ts%.2d.bmp"), (LPCTSTR)current_map->dmod_path, file_location);
    wsprintf(png_file, _T("%sTiles\\Ts%.2d.png"), (LPCTSTR)current_map->dmod_path, file_location);

    if ((image = loadBitmapImage(png_file, width, height)) == nullptr &&
        (image = loadBitmapImage(bmp_file, width, height)) == nullptr)
	{
		// try loading from the dink dmod directory
		if (is_dink_core_path_override_enabled) {
			wsprintf(bmp_file, _T("%s\\Tiles\\Ts%.2d.bmp"), (LPCTSTR)dink_core_path_override, file_location);
		    wsprintf(png_file, _T("%s\\Tiles\\Ts%.2d.png"), (LPCTSTR)dink_core_path_override, file_location);
		} else {
			wsprintf(bmp_file, _T("%s\\Tiles\\Ts%.2d.bmp"), (LPCTSTR)dink_core_path, file_location);
			wsprintf(png_file, _T("%s\\Tiles\\Ts%.2d.png"), (LPCTSTR)dink_core_path, file_location);
		}
		if ((image = loadBitmapImage(png_file, width, height)) == nullptr &&
		    (image = loadBitmapImage(bmp_file, width, height)) == nullptr)
			return false;
	}

	return true;
}

// render the entire tile bitmap
bool Tile::renderAll(int x, int y, int dest_width, int dest_height)
{
	if (image == nullptr)
		loadSurface();

	if (image == nullptr)
		return false;

	RECT dest_rect;                        

	// fill in the destination rect
	dest_rect.left   = x;
	dest_rect.top    = y;
	dest_rect.right  = x + int(double(dest_width) * (double(width) / double(TILESHEET_WIDTH)));
	dest_rect.bottom = y + int(double(dest_height) * (double(height) / double(TILESHEET_HEIGHT)));
	return drawTexture(image, nullptr, &dest_rect) == 0;
}

bool Tile::render(RECT dest_rect, int tileX, int tileY, RECT* cliprect /*= nullptr*/)
{
	// draw a bob at the x,y defined in the BOB
	// on the destination surface defined in dest
	if (image == nullptr)
		loadSurface();

	RECT source_rect;

	// fill in the source rect
	source_rect.left    = tileX * TILEWIDTH;
	source_rect.top     = tileY * TILEHEIGHT;
	source_rect.right   = source_rect.left + TILEWIDTH;
	source_rect.bottom  = source_rect.top + TILEHEIGHT;

	// trim tile
	RECT clip_rect = {0, 0, current_map->window_width, current_map->window_height};

	if (cliprect != nullptr)
		clip_rect = *cliprect;

	if (fixBounds(dest_rect, source_rect, clip_rect) == false)
		return false;

	// blt to destination surface
	if (!drawTexture(image, &source_rect, &dest_rect))
		return false;
	
	return true;
}