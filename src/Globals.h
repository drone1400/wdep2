#pragma once
#include "Const.h"
#include "Structs.h"

// STRUCTS ////////////////////////////////////////////////

struct SDL_Window;
struct SDL_Renderer;

class Map;
class Tile;
class SpriteEditor;
class Sprite;

// GLOBALS ////////////////////////////////////////////////

extern HWND main_window_handle; // save the window handle

extern SDL_Window* sdl_window;
extern SDL_Renderer* sdl_renderer;

extern SDL_Texture* temp_texture; //offscreen render target, having the size of one Dink screen (600x400)
extern SDL_Texture* temp_sprite_fallback; 


extern RECT mapRect;	// location of the map on the screen

extern Map* current_map;

extern Tile* tileBmp[MAX_TILE_BMPS];

extern SpriteEditor* spriteeditor;

extern BYTE vision_used[VISIONS_TRACKED];
extern BYTE new_vision_used[VISIONS_TRACKED];

extern bool need_resizing;
extern bool sprite_hard;
extern bool sprite_nohit;
extern bool displayhardness;
extern bool hide_all_sprites;
extern bool show_sprite_info;
extern bool show_minimap_progress;
extern bool optimize_dink_ini;
extern bool fast_minimap_update;
extern bool hover_sprite_info;
extern bool hover_sprite_hardness;
extern bool help_text;

extern int screen_gap;
extern UINT max_undo_level;
extern int auto_save_time;
extern int tile_brush_size;
extern SDL_Color help_text_color;
extern SDL_Color help_text_color_back;
extern int help_text_color_index;

extern bool update_minimap;
extern bool snapto_grid;
extern int snapto_offset;
extern bool autoscript;
extern bool hardedit_show_texture;

extern CString dink_exe_path;
extern CString dink_dmods_path;
extern CString dink_core_path; //path of the "dink" dmod itself; needed because it acts as a fallback for files not found in the dmod being edited/played
extern CString dink_skeleton_path;

// NOTE: drone1400 - dink_core_path_override is used if a valid --refdir argument is passed when launching the application
// this effectively overrides the default path, but only when loading dmod assets
// otherwise, the configured dink_core_path value remains unaffected!
// TODO: maybe add an indicator in the config window that the override is enabled?...
extern bool is_dink_core_path_override_enabled;
extern CString dink_core_path_override;

extern CString loaded_dmod_path; // drone1400: used when launching the dmod to playtest
extern CString WDE_path;	// path to the windinkedit directory

extern int square_width;
extern int square_height;

//RW: these are for saving the state of the Play DMOD dialog.
extern BOOL play_truecolor;
extern BOOL play_windowed;
extern BOOL play_sound;
extern BOOL play_joystick;
extern BOOL play_debug;
extern BOOL play_pathquotes;

extern int sprite_selector_bmp_width;
extern int sprite_selector_bmp_height;

//RW: colors used by the help text
const SDL_Color helpcolors[] = 
{ 
	COLOR_WHITE, 
	COLOR_YELLOW, 
	COLOR_ORANGE,
	COLOR_RED, 
	COLOR_MAGENTA, 
	COLOR_BLUE, 
	COLOR_CYAN, 
	COLOR_LIME, 
	COLOR_GREEN,
	COLOR_BLACK 
};

extern bool mouse_wheel_updown_invert;
extern bool mouse_wheel_updown_shift_invert;
extern bool mouse_wheel_leftright_invert;

extern const int minimap_detail_levels[];

extern bool use_hard_nohit_default_stamp;
extern bool use_hard_nohit_default_place;

extern bool detect_screenmatch_duplicates;

extern SpriteRenderOptions screenshot_render_options;

extern uint8_t hardness_alpha_tile;
extern uint8_t hardness_alpha_sprite;

extern bool use_statusbar_sidebar_offset;