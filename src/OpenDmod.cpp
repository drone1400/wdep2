#include "stdafx.h"
#include "OpenDmod.h"
#include "Common.h"
#include "CommonFile.h"


OpenDmod::OpenDmod(CWnd* pParent /*=nullptr*/) : CDialog(OpenDmod::IDD, nullptr)
{
}

void OpenDmod::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_LIST2, m_dmod_list);	
}

BEGIN_MESSAGE_MAP(OpenDmod, CDialog)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST2, &OpenDmod::OnNMDblclkList2)
END_MESSAGE_MAP()


void OpenDmod::setDmodNamePtr(TCHAR *buffer)
{
	dmod_selected = buffer;
}

// open the dmod
void OpenDmod::OnOK()
{	
	POSITION pos;

	// return if an invalid dmod name is selected
	pos = m_dmod_list.GetFirstSelectedItemPosition();
	if (pos == nullptr)
	{
		MB_ShowError(_T("You must select a dmod from the list!"));
		return;
	}
		
	CString dmod_name_entered;	
	dmod_name_entered = m_dmod_list.GetItemText(m_dmod_list.GetNextSelectedItem(pos), 0);
	
	if (dmod_selected != nullptr)	//RW: do not copy to nullptr, now.
		_tcscpy(dmod_selected, dmod_name_entered);

	CDialog::OnOK();
}

//RW: focus to the list control so that the user can use the mouse wheel without having to click
BOOL OpenDmod::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_dmod_list.SetFocus();	
	m_dmod_list.SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT);

	CRect listrect;
	m_dmod_list.GetWindowRect(&listrect);
	m_dmod_list.InsertColumn(0, _T("DMod folder"), LVCFMT_LEFT, 100, 0);
	m_dmod_list.InsertColumn(1, _T("DMod title"), LVCFMT_CENTER, 100, 1);

	PopulateDmodClistCtrl(&m_dmod_list);

	if ((m_dmod_list.GetStyle() & WS_VSCROLL) != 0)
		m_dmod_list.SetColumnWidth(1, listrect.Width()-105-GetSystemMetrics(SM_CXVSCROLL));
	else
		m_dmod_list.SetColumnWidth(1, listrect.Width()-105);

	return FALSE;
}


void OpenDmod::OnNMDblclkList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	OnOK();

	*pResult = 0;
}