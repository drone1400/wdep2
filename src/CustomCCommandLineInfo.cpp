#include "stdafx.h"
#include "CustomCCommandLineInfo.h"
#include "CommonFile.h"
#include "Globals.h"
#include "SimpleLog.h"
#include "PathUtil.h"


CustomCCommandLineInfo::CustomCCommandLineInfo()
{
    m_strGameLocation.SetString(_T(""));
    m_strRefDirLocation.SetString(_T(""));
    m_hasGameLocation = false;
    m_hasRefDirLocation = false;
    // privates..
    _expectGameLocation = false;
    _expectRefDirLocation = false;
    _canExpectGameLocation = true;
    _canExpectRefDirLocation = true;

}

bool CustomCCommandLineInfo::GetNormalizedFileName_Internal(CString & outStr)
{
	Log_Info(_T("CMD LINE - Normalizing file name..."));
	bool result = Path_Normalize(outStr, outStr);
	Log_Info(outStr,LOG_FORMAT_CONTINUE);
	if (result)
	{
		if (Path_IsRooted(outStr))
		{
			Log_Info(_T("CMD LINE - File name was normalized successfully!"));
			return true;
		}
		
		Log_Info(_T("CMD LINE - Path is relative. Attempting to make absolute in regards to WDE location."));
		result = Path_MakeAbsolute(outStr,WDE_path,outStr);
		Log_Info(outStr,LOG_FORMAT_CONTINUE);
		if (result)
		{
			Log_Info(_T("CMD LINE - File name was normalized successfully!"));
			return true;
		}
	}
	

	Log_Error(_T("CMD LINE - File name was not normalized successfully..."));	
	return false;
}

bool CustomCCommandLineInfo::GetNormalizedOpenFileName(CString & outStr)
{
	outStr.SetString(_T(""));
	if (m_nShellCommand != FileOpen)
	{
		Log_Info(_T("CMD LINE - ShellCommand is not FileOpen, can not get normalized file name..."));
		return false;
	}
	
	outStr.SetString(m_strFileName);
	Log_Info(_T("CMD LINE - Open file command found!"));
	Log_Info(outStr,LOG_FORMAT_CONTINUE);
	
	return GetNormalizedFileName_Internal(outStr);
}

bool CustomCCommandLineInfo::GetNormalizedGameArg(CString & outStr)
{
	if (!m_hasGameLocation)
	{
		Log_Info(_T("CMD LINE - '-game' argument was not found..."));
		return false;
	}

	outStr.SetString(m_strGameLocation);	
	Log_Info(_T("CMD LINE - '-game' argument found!"));
	Log_Info(outStr,LOG_FORMAT_CONTINUE);
	
	return GetNormalizedFileName_Internal(outStr);
}

bool CustomCCommandLineInfo::GetNormalizedValidRefDirArg(CString & outStr)
{
	if (!m_hasRefDirLocation)
	{
		Log_Info(_T("CMD LINE - '--refdir' argument was not found..."));
		return false;
	}
	

	outStr.SetString(m_strRefDirLocation);	
	Log_Info(_T("CMD LINE - '--refdir' argument found!"));
	Log_Info(outStr, LOG_FORMAT_CONTINUE);

	bool resultNormalize = GetNormalizedFileName_Internal(outStr);
	if (!resultNormalize)
	{
		// no need to log result again, already handled by GetNormalizedFileName_Internal
		return false;
	}

	// Time to validate base game DMOD Directory...
	if (IsDmodDir(outStr)) {
		// only enabling override if parameter defined and it is a valid dmod dir...
		Log_Info(_T("CMD LINE - '--refdir' is valid DMOD path!"));
		return true;
	}
	
	Log_Error(_T("CMD LINE - '--refdir' argument is NOT valid DMOD path! Trying to look in dink subfolder..."));
	Path_AppendNormalize(outStr,_T("dink"));
	Log_Error(outStr, LOG_FORMAT_CONTINUE);

	// Time to validate base game DMOD Directory AGAIN!...
	if (IsDmodDir(outStr)) {
		// only enabling override if parameter defined and it is a valid dmod dir...
		Log_Info(_T("CMD LINE - '--refdir' argument is valid DMOD path!"));
		return true;
	}
	
	Log_Error(_T("CMD LINE - '--refdir' argument is NOT a valid DMOD path..."));
	return false;
}

void CustomCCommandLineInfo::ParseParam(const TCHAR* pszParam, BOOL bFlag, BOOL bLast)
{
    bool bHandled = false;
    if (bFlag)
    {
        _expectGameLocation = false;
        _expectRefDirLocation = false;
        if (_canExpectRefDirLocation &&
        	// NOTE: accepting both --refdir and -refdir arguments
        	(_tcscmp(pszParam, _T("-refdir")) == 0 || _tcscmp(pszParam, _T("refdir")) == 0)
        	) _expectRefDirLocation = true;
        else if (_canExpectGameLocation && _tcscmp(pszParam, _T("game")) == 0) _expectGameLocation = true;
        
    } else {
        if (_expectRefDirLocation) {
            _expectRefDirLocation = false;
            _canExpectRefDirLocation = false;
            m_strRefDirLocation.SetString(pszParam);
            m_hasRefDirLocation = true;
        }

        if (_expectGameLocation) {
            _expectGameLocation = false;
            _canExpectGameLocation = false;
            m_strGameLocation.SetString(pszParam);
            m_hasGameLocation = true;
        }
    }
    // If the last parameter has no flag, it is treated as the file name to be
    //  opened and the string is stored in the m_strFileName member.
    if (!bHandled)
        CCommandLineInfo::ParseParam(pszParam, bFlag, bLast);
}