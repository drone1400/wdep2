#pragma once

class SpriteEditor
{
public:
	SpriteEditor();
	void DrawSprite();
	void DrawHardness();
	void Save();
	void SetHardness(int left, int top, int right, int bottom);
	void SetDepth(int x, int y);
	void DrawEditor(bool withHardness);		//RW: draws a background, then the sprite, then the hardness and depth dot if needed.
	void Reset();

	int zoom;
	RECT hardness_bounds;	//real sprite bounds
	int y_offset, x_offset;	//for recalculating the hardness
	int seq, frame;			//our trusty sequence and frame in the ini
	int width, height;
	int x1, x2, y1, y2;
	int depth_x, depth_y;
	bool edited;
};