#include "StdAfx.h"
#include "Common.h"
#include "CommonFile.h"
#include "Globals.h"
#include "Map.h"
#include "SimpleLog.h"
#include "PathUtil.h"
#include "atlstr.h"

using std::ifstream;
using std::ofstream;
using std::endl;


// TODO... 
// drone1400: figure out a way to make this configurable at runtime?...
// maybe make it so that by default the application saves locally but you can toggle an option to save to appdata?
// once toggled will save there until toggled off...
// but this may be annoying to some users?...
// maybe just add a different build config for a portable release?

//#define WDEINI_PORTABLE_MODE

// NOTE: drone1400 - changed to prioritize reading from local file, with fallback values to appdata
//Load program configuration from disk
void ReadWDEIni()
{
	TCHAR appdata_path[MAX_PATH];
	CString file_path;
	bool file_found = false;
	bool app_data_ok = false;

	appdata_path[0] = 0;
	if (SUCCEEDED(SHGetFolderPath(nullptr, CSIDL_APPDATA, nullptr, SHGFP_TYPE_CURRENT, appdata_path)))
	{
		app_data_ok = true;
	}

	// local file takes priority in portable mode
	if (Path_ConcatIntoAndNormalize(file_path,WDE_path, CONFIG_FILE_NAME))
	{
		Log_Info(_T("READ WDE INI - Attempting to read config from local portable file..."));
		Log_Info(file_path,LOG_FORMAT_CONTINUE);
		file_found = FileReadable(file_path);
	}
	
	

	// try to read from %appdata%\dink\ folder
	// this is the default config for DinkHD, so why not use it to keep things organized?
	if (!file_found && app_data_ok) 
	{
		if (Path_ConcatIntoAndNormalize(file_path,appdata_path, _T("dink"),CONFIG_FILE_NAME))
		{
			Log_Info(_T("READ WDE INI - Previous file not found! Attempting to read WDE config from standard WDEP2.6 config file..."));
			Log_Info(file_path, LOG_FORMAT_CONTINUE);
			file_found = FileReadable(file_path);
		}
	}

	// try to read from %appdata%\configfile.ini
	// fallback to RW's default appdata location
	if (!file_found && app_data_ok)
	{
		if (Path_ConcatIntoAndNormalize(file_path,appdata_path, CONFIG_FILE_NAME))
		{
			Log_Info(_T("READ WDE INI - Previous file not found! Attempting to read WDE config from WDEP2.5 config file..."));
			Log_Info(file_path, LOG_FORMAT_CONTINUE);
			file_found = FileReadable(file_path);
		}
	}

	if (!file_found)
	{
		Log_Info(_T("READ WDE INI - No config file found! WDE config will be using default values..."));
	}

	screen_gap                      = GetPrivateProfileInt(_T("Display_Settings"), _T("screen_gap"), DEFAULT_SCREEN_GAP, file_path);
	update_minimap                  = GetPrivateProfileInt(_T("Display_Settings"), _T("update_minimap"), 0, file_path) != 0;  //auto update minimap
	max_undo_level                  = GetPrivateProfileInt(_T("General_Settings"), _T("Max_Undo_Level"), DEFAULT_MAX_UNDO_LEVEL, file_path);
	auto_save_time                  = GetPrivateProfileInt(_T("General_Settings"), _T("Auto_Save_Time"), DEFAULT_AUTOSAVE_TIME, file_path);
	show_minimap_progress           = GetPrivateProfileInt(_T("Display_Settings"), _T("show_minimap_progress"), true, file_path) != 0;
	optimize_dink_ini               = GetPrivateProfileInt(_T("General_Settings"), _T("Optimize_Dink_Ini"), false, file_path) != 0;
	tile_brush_size                 = GetPrivateProfileInt(_T("General_Settings"), _T("Tile_Brush_Size"), 1, file_path);
	fast_minimap_update             = GetPrivateProfileInt(_T("Display_Settings"), _T("Fast_Minimap_Update"), fast_minimap_update, file_path) != 0;
	hover_sprite_info               = GetPrivateProfileInt(_T("General_Settings"), _T("Hover_Sprite_Info"), hover_sprite_info, file_path) != 0;
	hover_sprite_hardness           = GetPrivateProfileInt(_T("General_Settings"), _T("Hover_Sprite_Hardness"), hover_sprite_hardness, file_path) != 0;
	help_text                       = GetPrivateProfileInt(_T("General_Settings"), _T("Show_Help_Text"), help_text, file_path) != 0;
	use_hard_nohit_default_stamp    = GetPrivateProfileInt(_T("General_Settings"), _T("Sprite_Stamp_UseDefaults"), use_hard_nohit_default_stamp, file_path) != 0;
	use_hard_nohit_default_place    = GetPrivateProfileInt(_T("General_Settings"), _T("Sprite_Place_UseDefaults"), use_hard_nohit_default_place, file_path) != 0;
	autoscript                      = GetPrivateProfileInt(_T("General_Settings"), _T("AutoScript"), autoscript, file_path) != 0;
	square_width                    = GetPrivateProfileInt(_T("General_Settings"), _T("Minimap_Detail"), square_width, file_path); //minimap detail level
	sprite_selector_bmp_width       = GetPrivateProfileInt(_T("General_Settings"), _T("Sprite_Preview_Size"), sprite_selector_bmp_width, file_path); //sprite selector preview size
	use_statusbar_sidebar_offset    = GetPrivateProfileInt(_T("General_Settings"), _T("Statusbar_Sidebar_Offset"), use_statusbar_sidebar_offset, file_path);

    int hard_alpha_temp = hardness_alpha_tile;
    hard_alpha_temp                  = GetPrivateProfileInt(_T("General_Settings"), _T("Hard_Tile_Transparency"), hard_alpha_temp, file_path);
    if (hard_alpha_temp < HARDTILE_TRANSPARENCY_MIN) hard_alpha_temp = HARDTILE_TRANSPARENCY_MIN;
    if (hard_alpha_temp > HARDTILE_TRANSPARENCY_MAX) hard_alpha_temp = HARDTILE_TRANSPARENCY_MAX;
    hardness_alpha_tile = static_cast<uint8_t>(hard_alpha_temp);

    hard_alpha_temp = hardness_alpha_sprite;
    hard_alpha_temp                  = GetPrivateProfileInt(_T("General_Settings"), _T("Hard_Sprite_Transparency"), hard_alpha_temp, file_path);
    if (hard_alpha_temp < HARDSPRITE_TRANSPARENCY_MIN) hard_alpha_temp = HARDSPRITE_TRANSPARENCY_MIN;
    if (hard_alpha_temp > HARDSPRITE_TRANSPARENCY_MAX) hard_alpha_temp = HARDSPRITE_TRANSPARENCY_MAX;
    hardness_alpha_sprite = static_cast<uint8_t>(hard_alpha_temp);

	square_height = square_width;
	sprite_selector_bmp_height = sprite_selector_bmp_width;

	//play DMOD dialog
	play_truecolor                  = GetPrivateProfileInt(_T("Play_Dialog_Settings"), _T("Play_Truecolor"), play_truecolor, file_path);
	play_windowed                   = GetPrivateProfileInt(_T("Play_Dialog_Settings"), _T("Play_Windowed"), play_windowed, file_path);
	play_sound                      = GetPrivateProfileInt(_T("Play_Dialog_Settings"), _T("Play_Sound"), play_sound, file_path);
	play_joystick                   = GetPrivateProfileInt(_T("Play_Dialog_Settings"), _T("Play_Joystick"), play_joystick, file_path);
	play_debug                      = GetPrivateProfileInt(_T("Play_Dialog_Settings"), _T("Play_Debug"), play_debug, file_path);
	play_pathquotes                 = GetPrivateProfileInt(_T("Play_Dialog_Settings"), _T("Play_PathQuotes"), play_pathquotes, file_path);

	//mouse wheel inversion
	mouse_wheel_updown_invert       = GetPrivateProfileInt(_T("Mouse_Settings"), _T("Invert_Updown"), mouse_wheel_updown_invert, file_path) != 0;
	mouse_wheel_updown_shift_invert = GetPrivateProfileInt(_T("Mouse_Settings"), _T("Invert_Updown_Shift"), mouse_wheel_updown_shift_invert, file_path) != 0;
	mouse_wheel_leftright_invert    = GetPrivateProfileInt(_T("Mouse_Settings"), _T("Invert_Leftright"), mouse_wheel_leftright_invert, file_path) != 0;



	//dink installation

	// drone1400: NOTE: appdata_path TCHAR buffer is no longer necessary here, so we can use it to read strings!
	GetPrivateProfileString(_T("Dink_Installation"), _T("Dink_Exe"), _T(""), appdata_path, MAX_PATH, file_path);
	dink_exe_path.SetString(appdata_path);
	GetPrivateProfileString(_T("Dink_Installation"), _T("Core_Dmod"), _T(""), appdata_path, MAX_PATH, file_path);
	dink_core_path.SetString(appdata_path);
	GetPrivateProfileString(_T("Dink_Installation"), _T("Skeleton_Dmod"), _T(""), appdata_path, MAX_PATH, file_path);
	dink_skeleton_path.SetString(appdata_path);
	GetPrivateProfileString(_T("Dink_Installation"), _T("Other_Dmods"), _T(""), appdata_path, MAX_PATH, file_path);
	dink_dmods_path.SetString(appdata_path);
}

//Save program configuration to disk
void WriteWDEIni()
{
	TCHAR default_string[256];
	TCHAR appdata_path[MAX_PATH];
	CString file_path;
	bool path_ready = false;
	bool app_data_ok = false;

	appdata_path[0] = 0;
	if (SUCCEEDED(SHGetFolderPath(nullptr, CSIDL_APPDATA, nullptr, SHGFP_TYPE_CURRENT, appdata_path)))
	{
		app_data_ok = true;
	}

#ifdef WDEINI_PORTABLE_MODE
	// Save config file locally in application root folder, for portable builds
	Log_Info(_T("WRITE WDE INI - Attempting to use local folder..."));
	if (Path_ConcatIntoAndNormalize(file_path, WDE_path, CONFIG_FILE_NAME))
	{
		path_ready = true;
	}
#else
	
	// Save config file in appdata, for normal builds
	if (!path_ready && app_data_ok)
	{
		Log_Info(_T("WRITE WDE INI - Attempting to use dink subfolder in %appdata%..."));
		if (Path_ConcatIntoAndNormalize(file_path, appdata_path,_T("dink")))
		{
			if (!FolderExists(file_path)) {
				Log_Info(_T("WRITE WDE INI - Attempting to create directory:"));
				Log_Info(file_path,LOG_FORMAT_CONTINUE);
				// create directory if it does not exist (woah, not using dinkhd? what is this user doing?)
				CreateDirectory(file_path, nullptr);
			}
			if (!FolderExists(file_path))
			{
				Log_Error(_T("WRITE WDE INI - Failed to create directory..."));
				Log_Error(file_path,LOG_FORMAT_CONTINUE);
			} else 	if (Path_AppendNormalize(file_path,CONFIG_FILE_NAME))
			{
				path_ready = true;
			}
		}
	}
#endif
	
	if (!path_ready)
	{
		Log_Error(_T("WRITE WDE INI - Failed to initialize config file path..."));
		Log_Error(file_path,LOG_FORMAT_CONTINUE);
		goto WriteWDEIniError;
	}

	Log_Info(_T("WRITE WDE INI - Writing configuration to file path..."));
	Log_Info(file_path,LOG_FORMAT_CONTINUE);

	wsprintf(default_string, _T("%d"), screen_gap); // screen gap
	if (!WritePrivateProfileString(_T("Display_Settings"), _T("screen_gap"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%d"), (int)update_minimap); //auto update minimap
	if (!WritePrivateProfileString(_T("Display_Settings"), _T("update_minimap"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%d"), (int)show_minimap_progress); //display minimap update progress
	if (!WritePrivateProfileString(_T("Display_Settings"), _T("show_minimap_progress"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%u"), max_undo_level); //max undo level
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Max_Undo_Level"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%d"), auto_save_time); //autosave time interval
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Auto_Save_Time"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%d"), (int)optimize_dink_ini); //optimize dink ini
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Optimize_Dink_Ini"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), tile_brush_size); //brush size for hard tiles
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Tile_Brush_Size"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)fast_minimap_update); //fast minimap updater
	if (!WritePrivateProfileString(_T("Display_Settings"), _T("Fast_Minimap_Update"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)hover_sprite_info);
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Hover_Sprite_Info"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)hover_sprite_hardness);
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Hover_Sprite_Hardness"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)help_text);
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Show_Help_Text"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)use_hard_nohit_default_stamp);
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Sprite_Stamp_UseDefaults"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)use_hard_nohit_default_place);
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Sprite_Place_UseDefaults"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)autoscript);
	if (!WritePrivateProfileString(_T("General_Settings"), _T("AutoScript"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%d"), square_width); //minimap detail level
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Minimap_Detail"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%d"), sprite_selector_bmp_width); //sprite selector preview size
	if (!WritePrivateProfileString(_T("General_Settings"), _T("Sprite_Preview_Size"), default_string, file_path)) goto WriteWDEIniError;

    wsprintf(default_string, _T("%u"), hardness_alpha_tile);
    if (!WritePrivateProfileString(_T("General_Settings"), _T("Hard_Tile_Transparency"), default_string, file_path)) goto WriteWDEIniError;

    wsprintf(default_string, _T("%u"), hardness_alpha_sprite);
    if (!WritePrivateProfileString(_T("General_Settings"), _T("Hard_Sprite_Transparency"), default_string, file_path)) goto WriteWDEIniError;

    wsprintf(default_string, _T("%i"), (int)use_statusbar_sidebar_offset);
    if (!WritePrivateProfileString(_T("General_Settings"), _T("Statusbar_Sidebar_Offset"), default_string, file_path)) goto WriteWDEIniError;

	//play DMOD dialog checkbox status
	wsprintf(default_string, _T("%i"), play_truecolor);
	if (!WritePrivateProfileString(_T("Play_Dialog_Settings"), _T("Play_Truecolor"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), play_windowed);
	if (!WritePrivateProfileString(_T("Play_Dialog_Settings"), _T("Play_Windowed"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), play_sound);
	if (!WritePrivateProfileString(_T("Play_Dialog_Settings"), _T("Play_Sound"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), play_joystick);
	if (!WritePrivateProfileString(_T("Play_Dialog_Settings"), _T("Play_Joystick"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), play_debug);
	if (!WritePrivateProfileString(_T("Play_Dialog_Settings"), _T("Play_Debug"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), play_pathquotes);
	if (!WritePrivateProfileString(_T("Play_Dialog_Settings"), _T("Play_PathQuotes"), default_string, file_path)) goto WriteWDEIniError;

	//mouse wheel inversion
	wsprintf(default_string, _T("%i"), (int)mouse_wheel_updown_invert);
	if (!WritePrivateProfileString(_T("Mouse_Settings"), _T("Invert_Updown"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)mouse_wheel_updown_shift_invert);
	if (!WritePrivateProfileString(_T("Mouse_Settings"), _T("Invert_Updown_Shift"), default_string, file_path)) goto WriteWDEIniError;

	wsprintf(default_string, _T("%i"), (int)mouse_wheel_leftright_invert);
	if (!WritePrivateProfileString(_T("Mouse_Settings"), _T("Invert_Leftright"), default_string, file_path)) goto WriteWDEIniError;

	//dink installation
	if (!WritePrivateProfileString(_T("Dink_Installation"), _T("Dink_Exe"), dink_exe_path, file_path)) goto WriteWDEIniError;
	if (!WritePrivateProfileString(_T("Dink_Installation"), _T("Core_Dmod"), dink_core_path, file_path)) goto WriteWDEIniError;
	if (!WritePrivateProfileString(_T("Dink_Installation"), _T("Skeleton_Dmod"), dink_skeleton_path, file_path)) goto WriteWDEIniError;
	if (!WritePrivateProfileString(_T("Dink_Installation"), _T("Other_Dmods"), dink_dmods_path, file_path)) goto WriteWDEIniError;

	return;
	
WriteWDEIniError:
	Log_Error(_T("WRITE WDE INI - Failed to save the configuration!"));
	MB_ShowWarning(_T("Failed to save the WDE+2 configuration."));
}


//Read only the title from dmod.diz
static bool DmodDizReadTitle(CString dmodPath, CString& dmodTitle)
{
	CString dmodDizPath;
	Path_ConcatIntoAndNormalize(dmodDizPath, dmodPath,_T("dmod.diz"));

	Log_Info(_T("DMOD DIZ - Attempting to read dmod description file..."));
	Log_Info(dmodDizPath,LOG_FORMAT_CONTINUE);

	// open the dink.diz file
	ifstream dmod_diz(dmodDizPath);

	if (!dmod_diz)
	{
		Log_Info(_T("DMOD DIZ - Failed to read dmod description file!"));
		return false;
	}

	//TODO: should we support UTF-8 in dmod.diz? what does freedink do?  @unicode
	char buffer[65536];
	int bufferSize = 65536;
	
	dmod_diz.getline(buffer, bufferSize);
	dmodTitle = buffer;

	Log_Info(_T("DMOD DIZ - Done reading dmod description file!"));
	return true;
}

bool DmodDizRead(CString const dmodPath, CString &dmodTitle, CString &copyright1, CString &copyright2, CString &description)
{
	CString dmodDizPath;
	Path_ConcatIntoAndNormalize(dmodDizPath, dmodPath,_T("dmod.diz"));

	Log_Info(_T("DMOD DIZ - Attempting to read dmod description file..."));
	Log_Info(dmodDizPath,LOG_FORMAT_CONTINUE);

	// open the dink.diz file
	ifstream dmod_diz(dmodDizPath);

	if (!dmod_diz)
	{
		Log_Info(_T("DMOD DIZ - Failed to read dmod description file!"));
		return false;
	}

	//TODO: should we support UTF-8 in dmod.diz? what does freedink do?  @unicode
	char buffer[65536];
	int bufferSize = 65536;
	
	dmod_diz.getline(buffer, bufferSize);
	dmodTitle = buffer;

	dmod_diz.getline(buffer, bufferSize);
	copyright1 = buffer;

	dmod_diz.getline(buffer, bufferSize);
	copyright2 = buffer;

	description = "";
	while (dmod_diz.eof() == false)
	{
		dmod_diz.getline(buffer, bufferSize);
		description += buffer;
		description += _T("\n");
	}

	Log_Info(_T("DMOD DIZ - Done reading dmod description file!"));
	return true;
}

// writes out the dmod.diz file for the dmod
bool DmodDizWrite(CString const dmodPath, CString &dmod_title, CString &copyright1, CString &copyright2, CString &description)
{
	CString dmodDizPath;
	Path_ConcatIntoAndNormalize(dmodDizPath, dmodPath,_T("dmod.diz"));

	Log_Info(_T("DMOD DIZ - Attempting to write dmod description file..."));
	Log_Info(dmodDizPath,LOG_FORMAT_CONTINUE);

	// open the dink.diz file
	ofstream dmod_diz(dmodDizPath);

	if (!dmod_diz)
	{
		Log_Info(_T("DMOD DIZ - Failed to write dmod description file!"));
		return false;
	}

	std::vector<char> buf;

	ConvertToUtf8(dmod_title, buf);
	dmod_diz << buf.data() << endl;

	ConvertToUtf8(copyright1, buf);
	dmod_diz << buf.data() << endl;

	ConvertToUtf8(copyright2, buf);
	dmod_diz << buf.data() << endl;

	ConvertToUtf8(description, buf);
	dmod_diz << buf.data() << endl;

	Log_Info(_T("DMOD DIZ - Done writing dmod description file!"));
	return true;
}

bool DmodCheckFileExists(CString const dmodPath, CString const file)
{
	CString tmpFile;

	// check 
	Path_ConcatIntoAndNormalize(tmpFile,dmodPath,file);
	if (!FileReadable(tmpFile))
	{
		Log_Info(_T("DMOD - Dmod file not found!..."));
		Log_Info(tmpFile,LOG_FORMAT_CONTINUE);
		return false;
	}
	return true;
}

bool IsDmodDir(CString const dmodPath)
{
	Log_Info(_T("DMOD - Checking if dmod directory..."));
	Log_Info(dmodPath,LOG_FORMAT_CONTINUE);
	
	if (!FolderExists(dmodPath))
	{
		Log_Info(_T("DMOD - Root directory does not exist!..."));
		return false;
	}

	// check essential dmod files are there...
	if (!DmodCheckFileExists(dmodPath,_T("dink.dat")))return false;
	if (!DmodCheckFileExists(dmodPath,_T("map.dat")))return false;
	if (!DmodCheckFileExists(dmodPath,_T("hard.dat")))return false;
	if (!DmodCheckFileExists(dmodPath,_T("dink.ini")))return false;


	Log_Info(_T("DMOD - Seems to be valid dmod directory!..."));
	return true;
}

bool IsCoreDinkDir(CString const dmodPath)
{
	Log_Info(_T("DMOD - Checking if core dmod directory..."));
	Log_Info(dmodPath,LOG_FORMAT_CONTINUE);
	
	if (!FolderExists(dmodPath))
	{
		Log_Info(_T("DMOD - Root directory does not exist!..."));
		return false;
	}

	// check essential dmod files are there...
	if (!DmodCheckFileExists(dmodPath,_T("dink.dat")))return false;
	if (!DmodCheckFileExists(dmodPath,_T("map.dat")))return false;
	if (!DmodCheckFileExists(dmodPath,_T("hard.dat")))return false;
	if (!DmodCheckFileExists(dmodPath,_T("dink.ini")))return false;

	CString tileBase;
	if (!Path_ConcatIntoAndNormalize(tileBase,dmodPath,"tiles") ||
		!FolderExists(tileBase))
	{
		Log_Info(_T("DMOD - Tiles Directory does not exist!..."));
		return false;
	}
	
	// check that all the tile sheets exist...
	for (int i = 1; i <= 41; i++)
	{
		CString tsBmp;
		tsBmp.Format(_T("ts%02d.bmp"),i);
	    CString tsPng;
	    tsPng.Format(_T("ts%02d.png"),i);
		if (DmodCheckFileExists(tileBase,tsBmp) == false &&
		    DmodCheckFileExists(tileBase,tsPng) == false) return false;
	}


	Log_Info(_T("DMOD - Seems to be valid core dmod directory!..."));
	return true;
}

static bool DetectRegistryFreeDink(CString& detectedExe)
{
#ifndef WDEP_WIN_XP
	TCHAR buf[MAX_PATH];
	DWORD len;

	//look for FreeDink
	len = sizeof(buf); //docs say "bytes", even for unicode... hopefully that's correct.
	if (RegGetValue(HKEY_CURRENT_USER, _T("Software\\FreeDink"), nullptr, RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("freedink.exe")) &&
			FileReadable(detectedExe))
				return true;

	len = sizeof(buf);
	if (RegGetValue(HKEY_LOCAL_MACHINE, _T("Software\\FreeDink"), nullptr, RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("freedink.exe")) &&
			FileReadable(detectedExe))
				return true;

	len = sizeof(buf);
	if (RegGetValue(HKEY_USERS, _T("Software\\FreeDink"), nullptr, RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("freedink.exe")) &&
			FileReadable(detectedExe))
				return true;
#endif
	detectedExe.SetString(_T(""));
	return false;
}

static bool DetectRegistryDinkHD(CString& detectedExe)
{
#ifndef WDEP_WIN_XP
	TCHAR buf[MAX_PATH];
	DWORD len;

	//look for FreeDink
	len = sizeof(buf); //docs say "bytes", even for unicode... hopefully that's correct.
	if (RegGetValue(HKEY_CURRENT_USER, _T("Software\\RTSOFT\\DINK"), _T("path"), RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("dink.exe")) &&
			FileReadable(detectedExe))
				return true;

	len = sizeof(buf);
	if (RegGetValue(HKEY_LOCAL_MACHINE, _T("Software\\RTSOFT\\DINK"), _T("path"), RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("dink.exe")) &&
			FileReadable(detectedExe))
				return true;

	len = sizeof(buf);
	if (RegGetValue(HKEY_USERS, _T("Software\\RTSOFT\\DINK"), _T("path"), RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("dink.exe")) &&
			FileReadable(detectedExe))
				return true;
#endif
	detectedExe.SetString(_T(""));
	return false;
}

static bool DetectRegistryDinkClassic(CString& detectedExe)
{
#ifndef WDEP_WIN_XP
	TCHAR buf[MAX_PATH];
	DWORD len;

	//look for FreeDink
	len = sizeof(buf); //docs say "bytes", even for unicode... hopefully that's correct.
	if (RegGetValue(HKEY_CURRENT_USER, _T("Software\\RTSOFT\\Dink Smallwood"), _T("path"),RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("dink.exe")) &&
			FileReadable(detectedExe))
				return true;

	len = sizeof(buf);
	if (RegGetValue(HKEY_LOCAL_MACHINE, _T("Software\\RTSOFT\\Dink Smallwood"), _T("path"), RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("dink.exe")) &&
			FileReadable(detectedExe))
				return true;

	len = sizeof(buf);
	if (RegGetValue(HKEY_USERS, _T("Software\\RTSOFT\\Dink Smallwood"), _T("path"), RRF_RT_REG_SZ, nullptr, buf, &len) == ERROR_SUCCESS)
		if (Path_ConcatIntoAndNormalize(detectedExe,buf, _T("dink.exe")) &&
			FileReadable(detectedExe))
				return true;
#endif
	detectedExe.SetString(_T(""));
	return false;
}

static bool DetectIniDinkInstallation(CString& detectedExe)
{
	detectedExe.SetString(_T(""));
	
	TCHAR buf[MAX_PATH];
	
	CString iniPath;
	iniPath.SetString(_T(""));

	FILE* dink_ini = nullptr;
	
	int getWinResult = GetWindowsDirectory(buf, MAX_PATH);

	if (getWinResult > 0 && getWinResult < MAX_PATH &&
		Path_ConcatIntoAndNormalize(iniPath, buf, _T("dinksmallwood.ini")))
	{
		dink_ini = _tfopen(iniPath, _T("r"));
	}
	
	//if we didn't find the file in C:\Windows, then it could be in the post-Vista virtualized path
	if (!dink_ini &&
		SHGetFolderPath(nullptr, CSIDL_LOCAL_APPDATA, nullptr, SHGFP_TYPE_CURRENT, buf) == S_OK &&
		Path_ConcatIntoAndNormalize(iniPath, buf, _T("VirtualStore"), _T("Windows"), _T("dinksmallwood.ini")))
	{
		dink_ini = _tfopen(iniPath, _T("r"));
	}

	if (!dink_ini)
		return false;

	
	char ansiBuf[MAX_PATH];
	CString header;
	CString installPath;
	USES_CONVERSION;

	fgets(ansiBuf, MAX_PATH , dink_ini); //get the first line (header)
	header.SetString(A2T(ansiBuf));
	header.Trim();

	fgets(ansiBuf, MAX_PATH, dink_ini); //get the second line (dink install path)
	installPath.SetString(A2T(ansiBuf));
	installPath.Trim();

	fclose(dink_ini);

	if (header.IsEmpty() || installPath.IsEmpty())
		return false;

	if (Path_ConcatIntoAndNormalize(detectedExe, installPath, _T("dink.exe")) &&
		FileReadable(detectedExe))
	{
		return true;
	}

	detectedExe.SetString(_T(""));
	return false;
}

bool DetectDinkInstallation(CString& detectedDinkExe)
{
	// Dink HD takes priority since it is the current best implementation for windows...
	if (DetectRegistryDinkHD(detectedDinkExe))
	{
		Log_Info("DINK - Detected DinkHD at path...");
		Log_Info(detectedDinkExe, LOG_FORMAT_CONTINUE);
		return true;
	}

	// FreeDink is the next best option...
	if (DetectRegistryFreeDink(detectedDinkExe))
	{
		Log_Info("DINK - Detected FreeDink at path...");
		Log_Info(detectedDinkExe, LOG_FORMAT_CONTINUE);
		return true;
	}

	// Last but not least.. Dink Classic
	if (DetectRegistryDinkClassic(detectedDinkExe))
	{
		Log_Info("DINK - Detected Dink Classic at path...");
		Log_Info(detectedDinkExe, LOG_FORMAT_CONTINUE);
		return true;
	}

	// try to get Dink Classic from ini file...
	if (DetectIniDinkInstallation(detectedDinkExe))
	{
		Log_Info("DINK - Detected Dink Classic at path...");
		Log_Info(detectedDinkExe, LOG_FORMAT_CONTINUE);
		return true;
	}

	return false;
}

bool GuessMissingDinkInstallationPaths(CString& dinkExePath, CString& dinkCorePath, CString& skeletonPath, CString& otherDmodsPath)
{
	CString guess_base, tmp;

	if (!dinkExePath.IsEmpty() &&
		!dinkCorePath.IsEmpty() &&
		!skeletonPath.IsEmpty() &&
		!otherDmodsPath.IsEmpty())
	{
		// nothing to guess, all paths are defined...
		return false;
	}

	// try to set guessed base from known dink exe path
	if (Path_GetDirectoryOf(dinkExePath, guess_base)) 
	{
		Log_Info(_T("GUESS - Guessed base path from known Dink Exe location..."));
		Log_Info(guess_base, LOG_FORMAT_CONTINUE);
	}
	
	// try to set guessed base from known dink core path
	if (guess_base.IsEmpty() && Path_GetDirectoryOf(dinkCorePath, guess_base))
	{
		Log_Info(_T("GUESS - Guessed base path from known Dink Core location..."));
		Log_Info(guess_base, LOG_FORMAT_CONTINUE);
	}

	// NOTE: drone1400 - this is rather weird, what version of free dink came with an installer that made registry entries?...
	// TODO need to investigate this further... for now don't touch it
	
	// try to set guessed base from FreeDink registry entry
	if (guess_base.IsEmpty() &&
		DetectDinkInstallation(guess_base) &&
		Path_GetDirectoryOf(guess_base, guess_base))
	{
		Log_Info(_T("GUESS - Guessed base path from detecting Dink Installation..."));
		Log_Info(guess_base, LOG_FORMAT_CONTINUE);
	}
	
	if (guess_base.IsEmpty())
	{
		Log_Info(_T("GUESS - Can not guess missing Dink paths..."));
		return false; //nothing to base guesses on
	}

	// try to guess dink exe path using normal dink.exe...
	if (dinkExePath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess game exe using 'dink.exe' "));
		
		if (Path_ConcatIntoAndNormalize(tmp, guess_base, _T("dink.exe")) &&
			FileReadable(tmp))
		{
			Log_Info(_T("GUESS - Guessed dink.exe file in..."));
			Log_Info(tmp, LOG_FORMAT_CONTINUE);
			dinkExePath.SetString(tmp);
		}
	}

	// try to guess dink exe path using freedink.exe...
	if (dinkExePath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess game exe using 'freedink.exe' "));
		
		if (Path_ConcatIntoAndNormalize(tmp, guess_base, _T("freedink.exe")) &&
			FileReadable(tmp))
		{
			Log_Info(_T("GUESS - Guessed freedink.exe file in..."));
			Log_Info(tmp, LOG_FORMAT_CONTINUE);
			dinkExePath.SetString(tmp);
		}
	}

	// try to guess dink core path
	if (dinkCorePath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess Dink Core Path..."));
		
		if (Path_ConcatIntoAndNormalize(tmp, guess_base, _T("dink")) &&
			IsCoreDinkDir(tmp))
		{
			Log_Info(_T("GUESS - Guessed Dink Core path in..."));
			Log_Info(tmp, LOG_FORMAT_CONTINUE);
			dinkCorePath.SetString(tmp);
		}
	}

	// try to guess dink dmods path
	if (otherDmodsPath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess Dink DMODS directory using known freedink DMOD location..."));		
		if (Path_ConcatIntoAndNormalize(tmp, guess_base, _T("freedink.exe")) &&
			FileReadable(tmp))
		{
			TCHAR buf[MAX_PATH];
			if (SUCCEEDED(SHGetFolderPath(nullptr, CSIDL_PROFILE, nullptr, SHGFP_TYPE_CURRENT, buf)))
			{
				if (Path_ConcatIntoAndNormalize(tmp, buf, _T("dmods")) &&
					FolderExists(tmp))
				{
					Log_Info(_T("GUESS - Guessed FreeDink dmods path in..."));
					Log_Info(tmp, LOG_FORMAT_CONTINUE);
					otherDmodsPath.SetString(tmp);
				}
			}
		}
	}
	if (otherDmodsPath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess Dink DMODS directory in base guess's dmods subdirectory..."));
		if (Path_ConcatIntoAndNormalize(tmp, guess_base, _T("dmods")) &&
			FolderExists(tmp))
		{
			Log_Info(_T("GUESS - Guessed DMODS path in..."));
			Log_Info(tmp, LOG_FORMAT_CONTINUE);
			otherDmodsPath.SetString(tmp);
		}
	}
	if (otherDmodsPath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess Dink DMODS directory in base guess location..."));
		if (Path_ConcatIntoAndNormalize(tmp, guess_base, _T("island")) &&
			IsDmodDir(tmp))
		{
			Log_Info(_T("GUESS - Guessed DMODS path in..."));
			Log_Info(guess_base, LOG_FORMAT_CONTINUE);
			otherDmodsPath.SetString(guess_base);
		}
	}

	// try to guess skeleton path...
	if (skeletonPath.IsEmpty() && !otherDmodsPath.IsEmpty())
	{
		Log_Info(_T("GUESS - Trying to guess Skeleton directory based on current known DMODS directory..."));
		if (Path_ConcatIntoAndNormalize(tmp, otherDmodsPath, _T("skeleton")) &&
			IsDmodDir(tmp))
		{
			Log_Info(_T("GUESS - Guessed Skeleton path in..."));
			Log_Info(tmp, LOG_FORMAT_CONTINUE);
			skeletonPath.SetString(tmp);
		}
		if (skeletonPath.IsEmpty() &&
			Path_ConcatIntoAndNormalize(tmp, otherDmodsPath, _T("zeleton")) &&
			IsDmodDir(tmp))
		{
			Log_Info(_T("GUESS - Guessed Skeleton path in..."));
			Log_Info(tmp, LOG_FORMAT_CONTINUE);
			skeletonPath.SetString(tmp);
		}
	}

	return true; //some stuff detected
}


bool PopulateDmodClistCtrl(CListCtrl *dmod_list)
{
	Log_Info("DMOD LIST - Starting to populate DMOD list, scanning path:");
	Log_Warning(dink_dmods_path, LOG_FORMAT_CONTINUE);
	
	// make sure dmod list is empty before populating it...
	dmod_list->DeleteAllItems();
	
	if (FolderExists(dink_dmods_path) == false)
	{
		Log_Warning(_T("DMOD LIST - Can not create dmod list because DMODs path directory does not exist!..."));
		Log_Warning(dink_dmods_path, LOG_FORMAT_CONTINUE);
		return false;
	}

	CString dmodPathSearch;
	if (!Path_ConcatIntoAndNormalize(dmodPathSearch,dink_dmods_path, _T("*.*")))
	{
		Log_Warning(_T("DMOD LIST - Can not search DMODs path... Weird, this should be impossible."));
		Log_Warning(dink_dmods_path, LOG_FORMAT_CONTINUE);
		return false;
	}

	int dmodCount = 0;
	BOOL bMoreFiles = TRUE;
	WIN32_FIND_DATA dataFind;
	HANDLE hFind = FindFirstFile(dmodPathSearch, &dataFind);
	
	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)
	{
		// a file/folder has been found!
		
		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) > 0 && dataFind.cFileName[0] != '.')
		{
			CString entry;
			entry.SetString(dataFind.cFileName);         // dmod folder name...
			entry = entry.Left(MAX_DMOD_NAME_LENGTH);    // truncate to max dmod name length...

			CString dmodPath;
			Path_ConcatIntoAndNormalize(dmodPath, dink_dmods_path, entry);
			if (IsDmodDir(dmodPath))
			{
				CString dmodTitle;
				if (!DmodDizReadTitle(dmodPath, dmodTitle))
				{
					dmodTitle.SetString(entry);
				}
				
				dmod_list->InsertItem(dmodCount, entry);
				dmod_list->SetItemText(dmodCount, 1, dmodTitle);
				dmodCount++;

				Log_Info("DMOD LIST - Found dmod at path...");
				Log_Info(dmodPath, LOG_FORMAT_CONTINUE);
			}
		}

		// find the next file / folder
		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	FindClose(hFind);

	Log_Info("DMOD LIST - Done populating list!");

	return true;
}

static int FilesCopyAllRecursive_Internal(const CString pathInBaseNorm, const CString pathOutBaseNorm, const CString pathSub, int recLvl)
{
	int errors = 0;

	// debug recursion level...
	CString debug;
	debug.SetString(_T("FILE COPY - Recusion level: "));
	debug.AppendFormat(_T("%d"),recLvl);
	Log_Verbose(debug);

	//tell it to find *.*
	CString searchpath;
	Path_ConcatIntoAndNormalize(searchpath, pathInBaseNorm, pathSub, _T("*.*"));
	Log_Verbose(searchpath, LOG_FORMAT_CONTINUE);
	if (searchpath.GetLength() >= MAX_PATH)
	{
		Log_Error(_T("FILE COPY - MAX_PATH exceeded!"));
		Log_Error(searchpath, LOG_FORMAT_CONTINUE);
		return errors + 1;
	}

	WIN32_FIND_DATA dataFind;
	BOOL bMoreFiles = true;
	HANDLE hFind = FindFirstFile(searchpath, &dataFind);

	while (hFind != INVALID_HANDLE_VALUE && bMoreFiles)
	{
		// note: do not attempt to copy directories or symbolic links...
		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0 &&
			(dataFind.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == 0)
		{
			CString tmpFin;
			CString tmpFout;
			Path_ConcatIntoAndNormalize(tmpFin, pathInBaseNorm, pathSub,dataFind.cFileName);
			Path_ConcatIntoAndNormalize(tmpFout, pathOutBaseNorm, pathSub,dataFind.cFileName);
			Log_Verbose(tmpFin, LOG_FORMAT_CONTINUE);

			if (tmpFin.GetLength() >= MAX_PATH )
			{
				Log_Error(_T("FILE COPY - MAX_PATH exceeded!"));
				Log_Error(tmpFin, LOG_FORMAT_CONTINUE);
				errors++;
			} else if (tmpFout.GetLength() >= MAX_PATH)
			{
				Log_Error(_T("FILE COPY - MAX_PATH exceeded!"));
				Log_Error(tmpFout, LOG_FORMAT_CONTINUE);
				errors++;
			} else
			{
				if (CopyFile(tmpFin, tmpFout, TRUE) == 0)
				{
					Log_Verbose(_T("ERROR COPYING LAST FILE!..."), LOG_FORMAT_CONTINUE);
					// the copy operation failed...
					errors++;
				}
			}
		}
		
		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	// find new directory
	hFind = FindFirstFile(searchpath, &dataFind);
	bMoreFiles = true;

	while (hFind != INVALID_HANDLE_VALUE && bMoreFiles)
	{
		//make sure it's directory
		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 &&                                     // is a directory
			(dataFind.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) == 0 &&                                 // is not a symbolic link
			!(dataFind.cFileName[0] == '.' && dataFind.cFileName[1] == '\0') &&                                // is not "."
			!(dataFind.cFileName[0] == '.' && dataFind.cFileName[1] == '.' && dataFind.cFileName[2] == '\0'))  // is not ".."
		{
			CString tmpSubPath;
			CString tmpDirIn;
			CString tmpDirOut;
			Path_ConcatIntoAndNormalize(tmpSubPath, pathSub,dataFind.cFileName);
			Path_ConcatIntoAndNormalize(tmpDirIn, pathInBaseNorm, pathSub,dataFind.cFileName);
			Path_ConcatIntoAndNormalize(tmpDirOut, pathOutBaseNorm, pathSub,dataFind.cFileName);

			if (tmpDirIn.GetLength() >= MAX_PATH )
			{
				Log_Error(_T("FILE COPY - MAX_PATH exceeded!"));
				Log_Error(tmpDirIn, LOG_FORMAT_CONTINUE);
				errors++;
			} else if (tmpDirOut.GetLength() >= MAX_PATH)
			{
				Log_Error(_T("FILE COPY - MAX_PATH exceeded!"));
				Log_Error(tmpDirOut, LOG_FORMAT_CONTINUE);
				errors++;
			} else
			{
				CreateDirectory(tmpDirOut, nullptr);
				errors += FilesCopyAllRecursive_Internal(pathInBaseNorm, pathOutBaseNorm, tmpSubPath, recLvl+1);
			}
		}
		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	return errors;
}

bool FilesCopyAllRecursive(CString const source, CString const destination)
{
	CString srcNorm;
	CString dstNorm;

	Log_Info(_T("FILE COPY - Attempting to copy all files recursively..."));
	Log_Info(_T("RAW SRC: "), LOG_FORMAT_SMALL_PADDING_ONLY);
	Log_Info(source,LOG_FORMAT_CONTINUE);
	Log_Info(_T("RAW DST: "), LOG_FORMAT_SMALL_PADDING_ONLY);
	Log_Info(destination,LOG_FORMAT_CONTINUE);

	// normalize source path
	if (!Path_Normalize(source, srcNorm))
	{
		Log_Error(_T("FILE COPY - Could not normalize file copy source!"));
		return false;
	}

	Log_Info(_T("Normalized SRC: "), LOG_FORMAT_SMALL_PADDING_ONLY);
	Log_Info(srcNorm,LOG_FORMAT_CONTINUE);

	// normalize destination path
	if (!Path_Normalize(destination, dstNorm))
	{
		Log_Error(_T("FILE COPY - Could not normalize file copy destination!"));
		return false;
	}

	Log_Info(_T("Normalized DST: "), LOG_FORMAT_SMALL_PADDING_ONLY);
	Log_Info(dstNorm,LOG_FORMAT_CONTINUE);
	
	// check if the src/dst are parent/child
	// NOTE: This also makes sure paths are rooted!
	if (Path_IsDestinationChildOfSourceOrSame(srcNorm, dstNorm) != 0)
	{
		Log_Error(_T("FILE COPY - Could not validate file copy source/destination... Destination is child of source or same?..."));
		return false;
	}

	// if we got here, the paths must be rooted and not parent/child, so it should be safe to copy files from one to the other...
	int err = FilesCopyAllRecursive_Internal(srcNorm, dstNorm, _T(""), 0);

	TCHAR debug[MAX_PATH];
	_stprintf_s(debug, _T("FILE COPY - Done copying with %d errors!"),err);
	Log_Info(debug);

	return true;
}

bool CreateNewDmod(CString& dmod_dir_name, CString& dmod_title, CString& copyright1, CString& copyright2, CString& description)
{	
	CString newdmod_path;
	bool found = false;

	newdmod_path.SetString(dink_dmods_path);
	newdmod_path.AppendChar(_T('\\'));
	newdmod_path.Append(dmod_dir_name);

	// only allow new dmods in new folders, show specific error message if folder exists
	if (FolderExists(newdmod_path)) {
		MB_ShowError(_T("DMOD folder already exists at path:\n"),
			newdmod_path, _T("\n\nCan not create a new DMOD there!"));
		return false;
	}

	// create the main dmod directory
	if (CreateDirectory(newdmod_path, nullptr) == FALSE)
	{
		MB_ShowError( _T("Unknown error creating new DMOD folder at path:\n"),
			newdmod_path, _T("\n\nBad DMOD name?..."));
		return false;
	}

	// check skeleton dmod exists
	if (!FolderExists(dink_skeleton_path))
	{
		MB_ShowError(_T("The configured skeleton folder does not exist at path:\n"),
			dink_skeleton_path,	_T("\n\nPlease double-check File -> Dink Installation."));
		return false;
	}

	if(!FilesCopyAllRecursive(dink_skeleton_path, newdmod_path))
	{
		MB_ShowError(_T("The Dink Skeleton files could not be copied over to the new dmod..."));
		Log_Error(_T("The Dink Skeleton files could not be copied over to the new dmod..."));
		Log_Error(dink_skeleton_path, LOG_FORMAT_CONTINUE);
		Log_Error(newdmod_path, LOG_FORMAT_CONTINUE);
		// continue anyway...
	}

	//TODO: Maybe we should check if there is a hard.dat, and if not, copy the one from dink.

	DmodDizWrite(newdmod_path, dmod_title, copyright1, copyright2, description);

	return true;
}