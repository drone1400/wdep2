#pragma once
#include "resource.h"

class DinkInstallationDialog : public CDialog
{
	DECLARE_DYNAMIC(DinkInstallationDialog)

public:
	DinkInstallationDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DinkInstallationDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DINK_INSTALLATION };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_dinkExePath;
	CString m_dinkCorePath;
	CString m_skeletonPath;
	CString m_otherDmodsPath;
	afx_msg void OnClickedBrowseDinkExe();
	afx_msg void OnClickedBrowseDinkCore();
	afx_msg void OnClickedBrowseSkeletonDmod();
	afx_msg void OnClickedBrowseOtherDmod();
};
