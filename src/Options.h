#pragma once
#include "resource.h"

class Options : public CDialog
{
public:
	Options(CWnd* pParent = nullptr);   // standard constructor

	enum { IDD = IDD_OPTIONS };
	UINT	m_screen_gap;
	UINT	m_max_undos;
	UINT    m_autosave_time;
	UINT	m_offset;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support
	BOOL OnInitDialog() override;

	DECLARE_MESSAGE_MAP()

public:
	BOOL m_show_progress;
	BOOL m_dink_ini;
	int m_brush_size;
	BOOL m_fast_minimap;
	BOOL m_hover_sprite_info;
	BOOL m_hover_sprite_hardness;
	BOOL m_help_text;
	BOOL m_autoscript;
	int m_minimap_detail;
	int m_sprite_preview_size;
    int m_hardtile_transparency;
    int m_hardsprite_transparency;
	BOOL m_invert_vertical_wheel;
	BOOL m_invert_shiftvertical_wheel;
	BOOL m_invert_horizontal_wheel;
	afx_msg void OnBnClickedButtonDefaults();
	BOOL m_spriteplace_hard_nohit;
	BOOL m_spritestamp_hard_nohit;
	BOOL m_statusbar_sidebar_offset;
};
