#include "StdAfx.h"
#include "ScreenCreate.h"
#include "../Screen.h"
#include "../Map.h"
#include "../Minimap.h"

namespace actions
{

ScreenCreate::ScreenCreate(int screen_num)
	: screen_num(screen_num)
{
}

void ScreenCreate::Do()
{
	current_map->screen[screen_num] = new Screen();
	current_map->indoor[screen_num] = 0;
	current_map->midi_num[screen_num] = 0;

	current_map->miniupdated[screen_num] = false;
	current_map->minimap->renderMapSquare(screen_num);
}

void ScreenCreate::Undo()
{
	delete current_map->screen[screen_num];
	current_map->screen[screen_num] = nullptr;

	current_map->minimap->renderMapSquare(screen_num);
	current_map->miniupdated[screen_num] = false;
}

}
