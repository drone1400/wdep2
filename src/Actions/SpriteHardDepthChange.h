#pragma once
#include "Action.h"

namespace actions
{

class SpriteHardDepthChange : public Action
{
public:
	SpriteHardDepthChange(int newSeq, int newFrame, int newDepthX, int newDepthY, int newX1, int newY1, int newX2, int newY2);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Changed hardness/depth dot of a sprite"); }

private:
	int seq, frame;
	int newDepthX, newDepthY, newX1, newY1, newX2, newY2;
	int oldDepthX, oldDepthY, oldX1, oldY1, oldX2, oldY2;
	bool done_with_optimize_off;
	bool old_used_seq_info;
};

}
