#include "StdAfx.h"
#include "MultiSpriteClearSelected.h"
#include "../Map.h"

namespace actions
{

void MultiSpriteClearSelected::Do()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	swap(selection, current_map->selectedSprites);
}

void MultiSpriteClearSelected::Undo()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	swap(selection, current_map->selectedSprites);
}

}
