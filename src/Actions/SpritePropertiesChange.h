#pragma once
#include "Action.h"
#include "../SpriteRef.h"
#include "../Sprite.h"

namespace actions
{

class SpritePropertiesChange : public Action
{
public:
	SpritePropertiesChange(Sprite old_sprite, Sprite new_sprite, SpriteRef spriteRef);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Changed sprite properties"); }

	Sprite old_sprite;
	Sprite new_sprite;
	SpriteRef spriteRef;
};

}
