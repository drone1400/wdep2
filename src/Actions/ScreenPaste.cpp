#include "StdAfx.h"
#include "ScreenPaste.h"
#include "../Screen.h"
#include "../Map.h"
#include "../Minimap.h"

namespace actions
{

ScreenPaste::ScreenPaste(int screen_num, Screen* new_screen, int midi, bool indoor)
{
	this->screen_num = screen_num;
	this->new_screen = new_screen;
	this->midi_num = midi;
	this->indoor = indoor;
}

ScreenPaste::~ScreenPaste()
{
	delete new_screen;
}

void ScreenPaste::Do()
{
	current_map->screen[screen_num] = new Screen(new_screen);
	current_map->indoor[screen_num] = indoor;
	current_map->midi_num[screen_num] = midi_num;

	current_map->miniupdated[screen_num] = false;
	current_map->minimap->renderMapSquare(screen_num);
}

void ScreenPaste::Undo()
{
	delete current_map->screen[screen_num];
	current_map->screen[screen_num] = nullptr;

	current_map->minimap->renderMapSquare(screen_num);
	current_map->miniupdated[screen_num] = false;
}

}
