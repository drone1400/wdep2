#include "StdAfx.h"
#include "ScreenPropertiesChange.h"
#include "../Screen.h"
#include "../Map.h"
#include "../Minimap.h"

namespace actions
{

ScreenPropertiesChange::ScreenPropertiesChange(int screen_num, int old_midi, int new_midi, bool old_indoor, bool new_indoor, BaseScriptFilename old_script, BaseScriptFilename new_script)
	: screen_num(screen_num)
	, old_script(old_script)
	, new_script(new_script)
	, old_midi(old_midi)
	, new_midi(new_midi)
	, old_indoor(old_indoor)
	, new_indoor(new_indoor)
{
}

void ScreenPropertiesChange::Do()
{
	current_map->midi_num[screen_num] = new_midi;
	current_map->indoor[screen_num] = new_indoor;
	current_map->screen[screen_num]->script = new_script;

	current_map->minimap->renderMapSquare(screen_num);

	current_map->miniupdated[screen_num] = false;
}

void ScreenPropertiesChange::Undo()
{
	current_map->midi_num[screen_num] = old_midi;
	current_map->indoor[screen_num] = old_indoor;
	current_map->screen[screen_num]->script = old_script;

	current_map->minimap->renderMapSquare(screen_num);

	current_map->miniupdated[screen_num] = false;
}

}
