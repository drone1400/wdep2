#pragma once
#include "Action.h"
#include "../Sprite.h"
#include "../SpriteRef.h"

namespace actions
{

class MultiSpritePlace : public Action
{
public:
	MultiSpritePlace();
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Placed multiple sprites"); }

	std::vector<MultiMouseSprite> mouseSprites;
	std::vector<SpriteRef> placed_sprites;
	int mouseX, mouseY;
	int hoverX, hoverY;
	int hoverScreenX, hoverScreenY;
	int hoverScreen;
};

}
