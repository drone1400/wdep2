#include "StdAfx.h"
#include "SpritePropertiesChange.h"
#include "../Screen.h"
#include "../Map.h"

namespace actions
{

SpritePropertiesChange::SpritePropertiesChange(Sprite old_sprite, Sprite new_sprite, SpriteRef spriteRef)
	: old_sprite(old_sprite)
	, new_sprite(new_sprite)
	, spriteRef(spriteRef)
{
}

void SpritePropertiesChange::Do()
{
	if (current_map->screenMode != EditorSubmode::MultiSprite)	//keep multi-sprite mode, but kill any other mode to sprite mode
		current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	if (spriteRef.sprite_num == -1)
	{
		// modified sprite was the mouse sprite
		current_map->setMouseSprite(&new_sprite);
	}
	else
	{
	    // instead of deleting and re-adding sprite, just update existing one's values
	    spriteRef.screen()->sprite[spriteRef.sprite_num]->SetFrom(new_sprite);
		current_map->miniupdated[spriteRef.screen_num] = false;
	}
}

void SpritePropertiesChange::Undo()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	if (spriteRef.sprite_num == -1)
	{
		// modified sprite was the mouse sprite
		current_map->setMouseSprite(&old_sprite);
	}
	else
	{
        // instead of deleting and re-adding sprite, just update existing one's values
        spriteRef.screen()->sprite[spriteRef.sprite_num]->SetFrom(old_sprite);
		current_map->miniupdated[spriteRef.screen_num] = false;
	}
}

}
