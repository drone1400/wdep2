#pragma once
#include "Action.h"

class Screen;

namespace actions
{

class ScreenImport : public Action
{
public:
	ScreenImport(int screen_num, Screen* new_screen, int midi, bool indoor);
	~ScreenImport();
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Screen imported"); }

	int screen_num;
	Screen* new_screen;
	int midi_num;
	bool indoor;
};

}
