#include "StdAfx.h"
#include "HardTileSetDefault.h"
#include "../Globals.h"
#include "../Tile.h"

namespace actions
{

HardTileSetDefault::HardTileSetDefault(int old_index, int new_index, int bmp, int x, int y)
	: old_index(old_index)
	, new_index(new_index)
	, bmp(bmp)
	, x(x)
	, y(y)
{
}

void HardTileSetDefault::Do()
{
	tileBmp[bmp]->tile_hardness[y][x] = new_index;
}

void HardTileSetDefault::Undo()
{
	tileBmp[bmp]->tile_hardness[y][x] = old_index;
}

}
