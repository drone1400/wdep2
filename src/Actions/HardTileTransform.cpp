#include "StdAfx.h"
#include "HardTileTransform.h"
#include "../Map.h"

namespace actions
{

HardTileTransform::HardTileTransform(int tile_num, TileHardness old_tile_hard, BYTE hardness)
	: tile_num(tile_num)
	, hardness(hardness)
	, old_tile_hard(old_tile_hard)
{
}

void HardTileTransform::Do()
{
	//unload surface so that the hardness is actually updated on the screen not just in memory.
	current_map->hard_tile_selector.unloadSurface(tile_num);
		
	for (int x = 0; x < TILEWIDTH; x++)
		for (int y = 0; y < TILEHEIGHT; y++)
			if (current_map->hard_tile_selector.hardTile[tile_num][x][y]) //transform only if the current pixel is hard.
				current_map->hard_tile_selector.hardTile[tile_num][x][y] = hardness;
}

void HardTileTransform::Undo()
{
	//unload surface so that the hardness is actually updated on the screen not just in memory.
	current_map->hard_tile_selector.unloadSurface(tile_num);

	current_map->hard_tile_selector.hardTile[tile_num] = old_tile_hard;
}

}
