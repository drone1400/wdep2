#pragma once
#include "Action.h"
#include "../SpriteRef.h"

namespace actions
{

class MultiSpriteRectSelect : public Action
{
public:
	explicit MultiSpriteRectSelect(bool exclusive);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Selected multiple sprites using the rectangle"); }

	std::vector<SpriteRef> old_selection;
	std::vector<SpriteRef> newly_selected;
};

}
