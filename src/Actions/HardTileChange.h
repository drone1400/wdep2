#pragma once
#include "Action.h"
#include "../Const.h"

namespace actions
{

class HardTileChange : public Action
{
public:
	HardTileChange(int first_tile_x, int first_tile_y, int width, int height);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Placed hard tiles"); }

	int first_tile_x, first_tile_y;
	int width, height;
	int new_hard_tiles[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2];
	int old_hard_tiles[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2];
};

}
