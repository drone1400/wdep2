#include "StdAfx.h"
#include "MultiSpritePickup.h"
#include "../Screen.h"
#include "../Map.h"
#include "../MainFrm.h"
#include "../WinDinkeditView.h"

namespace actions
{

MultiSpritePickup::MultiSpritePickup(bool pickup_copy /*= false*/)
	: selection(current_map->selectedSprites)
	, copy(pickup_copy)
{	
	for (auto& sel : selection)
		sprites.push_back(*sel);
}

void MultiSpritePickup::Do()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	//clear any selections because they are invalid now
	current_map->selectedSprites.clear();

	int minx=INT_MAX, maxx=INT_MIN, miny=INT_MAX, maxy=INT_MIN;

	for (auto& sel : selection)
	{
		int x = sel.screen_num%MAP_COLUMNS * SCREEN_WIDTH  + sel->x - SIDEBAR_WIDTH;

		if (x < minx) minx = x;
		if (x > maxx) maxx = x;

		int y = sel.screen_num/MAP_COLUMNS * SCREEN_HEIGHT + sel->y;

		if (y < miny) miny = y;
		if (y > maxy) maxy = y;
	}
	
	//center
	int cx = (minx + maxx) / 2;
	int cy = (miny + maxy) / 2;

	CRect w = current_map->window;
	int l = cx - (w.left   - cx/SCREEN_WIDTH  * screen_gap);
	int r = cx - (w.right  - cx/SCREEN_WIDTH  * screen_gap);
	int t = cy - (w.top    - cy/SCREEN_HEIGHT * screen_gap);
	int b = cy - (w.bottom - cy/SCREEN_HEIGHT * screen_gap);
	int mmovex=0, mmovey=0;
	int smoveoffx=0, smoveoffy=0;

	if (l < 0)
	{
		smoveoffx = l - 50;
		mmovex = 50;
	}
	else if (r > 0)
	{
		smoveoffx = r + 50;
		mmovex = w.Width()-50;
	}
	else
		mmovex = l;
	
	if (t<0)
	{
		smoveoffy = t - 50;
		mmovey = 50;
	}
	else if (b > 0)
	{
		smoveoffy = b + 50;
		mmovey = w.Height()-50;
	}
	else
		mmovey = t;

	if (t < 0 || b > 0 || l < 0 || r > 0)
	{
		current_map->updateMapPosition(current_map->window.left + smoveoffx, current_map->window.top + smoveoffy);
		//load the new values into our coordinates
		this->map_x = current_map->window.left;
		this->map_y = current_map->window.top;
	}

	POINT destp = {mmovex, mmovey};
	this->mouse = destp;
	mainWnd->GetRightPane()->ClientToScreen(&destp);

	SetCursorPos(destp.x, destp.y);

	for (auto& sel : selection)
	{
		//initialize mouse sprite object
		MultiMouseSprite m;
		m.sprite = *sel;
		m.x_offset = sel.screen_num%MAP_COLUMNS * SCREEN_WIDTH  + sel->x - SIDEBAR_WIDTH - cx;
		m.y_offset = sel.screen_num/MAP_COLUMNS * SCREEN_HEIGHT + sel->y - cy;

		//detect screenmatch duplicates
		bool add = true;
		for (auto& mms : current_map->mouseMultiSprites)
			if (   mms.x_offset == m.x_offset 
				&& mms.y_offset == m.y_offset 
				&& mms.sprite.sequence    == m.sprite.sequence 
				&& mms.sprite.frame       == m.sprite.frame
				&& mms.sprite.trim_top    == m.sprite.trim_top 
				&& mms.sprite.trim_bottom == m.sprite.trim_bottom
				&& mms.sprite.trim_left   == m.sprite.trim_left 
				&& mms.sprite.trim_right  == m.sprite.trim_right)
			{
				add = false;
				break;
			}

		//add mouse sprite object to mouse sprite list
		if (add)
			current_map->mouseMultiSprites.push_back(m);

		//delete sprite from its screen
		if (!copy)
			sel.screen()->removeSprite(sel.sprite_num);
	}
}

void MultiSpritePickup::Undo()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	if (!copy)
	{
		//load sprites back to their screens (saved version, not mouse sprite list)
		auto p=selection.begin();
		auto psp=sprites.begin();
		for (; p!=selection.end(); ++p, ++psp)
			p->screen()->addSprite(p->sprite_num, *psp);
	}

	current_map->mouseMultiSprites.clear();

	//load selection from here
	current_map->selectedSprites = selection;
}

}
