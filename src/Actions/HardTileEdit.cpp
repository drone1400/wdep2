#include "StdAfx.h"
#include "HardTileEdit.h"
#include "../Map.h"

namespace actions
{

HardTileEdit::HardTileEdit(int hard_tile_num, TileHardness old_tile_hard, TileHardness new_tile_hard)
	: hard_tile_num(hard_tile_num)
	, old_tile_hard(old_tile_hard)
	, new_tile_hard(new_tile_hard)
{
}

void HardTileEdit::Do()
{
	current_map->hard_tile_selector.hardTile[hard_tile_num] = new_tile_hard;

	current_map->hard_tile_selector.unloadSurface(hard_tile_num);
}

void HardTileEdit::Undo()
{
	current_map->hard_tile_selector.hardTile[hard_tile_num] = old_tile_hard;

	current_map->hard_tile_selector.unloadSurface(hard_tile_num);
}

}
