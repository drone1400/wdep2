#pragma once
#include "Action.h"
#include "../Structs.h"

namespace actions
{

class HardTileTransform : public Action
{
public:
	HardTileTransform(int tile_num, TileHardness old_tile_hard, BYTE hardness);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Transformed Hard Tile"); }

	int tile_num;
	BYTE hardness;
	TileHardness old_tile_hard;
};

}
