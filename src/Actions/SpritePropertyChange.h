#pragma once
#include "Action.h"
#include "../SpriteRef.h"
#include "../Map.h"

namespace actions
{

template <typename TProperty>
class SpritePropertyChange : public Action
{
public:
	SpritePropertyChange(TProperty Sprite::* changedProperty, TProperty newValue, SpriteRef spriteRef)
		: spriteRef(spriteRef)
		, changedProperty(changedProperty)
		, oldValue(spriteRef->*changedProperty)
		, newValue(newValue)
	{
	}

	void Do() override
	{
		current_map->screenMode = EditorSubmode::Sprite;
		current_map->editor_state = EditorState::Screen;
	    hide_all_sprites = false; // make sure sprites are visible

		spriteRef->*changedProperty = newValue;
	}

	void Undo() override
	{
		current_map->screenMode = EditorSubmode::Sprite;
		current_map->editor_state = EditorState::Screen;
	    hide_all_sprites = false; // make sure sprites are visible

		spriteRef->*changedProperty = oldValue;
	}

	const TCHAR* Msg() override { return _T("Changed a property of multiple sprites"); }

private:
	SpriteRef spriteRef;
	TProperty Sprite::* changedProperty;
	TProperty oldValue;
	TProperty newValue;
};

template <typename TProperty>
auto makeSpritePropertyChange(TProperty Sprite::* changedProperty, TProperty newValue, SpriteRef spriteRef)
{
	return SpritePropertyChange<TProperty>(changedProperty, newValue, spriteRef);
}

}
