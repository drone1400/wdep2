#pragma once
#include "Action.h"

namespace actions
{

class HardTileSetDefault : public Action
{
public:
	HardTileSetDefault(int old_index, int new_index, int bmp, int x, int y);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Set default hard tile"); }

	int old_index;
	int new_index;
	int bmp;
	int x, y;
};

}
