#pragma once
#include "Action.h"

namespace actions
{

class HardTileMultipleChange : public Action
{
public:
	HardTileMultipleChange(int width, int height, int first_tile_x, int first_tile_y);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Placed multiple hard tiles"); }

	int width;
	int height;
	int first_tile_x, first_tile_y;
	std::vector<short> new_hard_tiles;
	std::vector<short> old_hard_tiles;
};

}
