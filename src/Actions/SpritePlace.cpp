#include "StdAfx.h"
#include "SpritePlace.h"
#include "../Screen.h"
#include "../Map.h"
using std::vector;

namespace actions
{

SpritePlace::SpritePlace(vector<SpriteRef> spriteRefs, vector<Sprite> sprites, bool stamp)
	: spriteRefs(spriteRefs)
	, sprites(sprites)
	, stamp(stamp)
{
}

void SpritePlace::Do()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	bool add_success = true;
	for (UINT i = 0; i < sprites.size(); i++)
	{
		add_success &= spriteRefs[i].screen()->addSprite(spriteRefs[i].sprite_num, sprites[i]);

		current_map->miniupdated[spriteRefs[i].screen_num] = false;
	}

	if (!add_success)
		Screen::TooManySprites();

	if (!stamp)
		current_map->setMouseSprite(nullptr);
}

void SpritePlace::Undo()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	for (UINT i = 0; i < sprites.size(); i++)
	{
		spriteRefs[i].screen()->removeSprite(spriteRefs[i].sprite_num);
		current_map->miniupdated[spriteRefs[i].screen_num] = false;
	}

	if (stamp)
		return;

	// load the sprite into the mouse sprite
	sprites[0].x -= SIDEBAR_WIDTH;
	current_map->loadMouseSprite(&sprites[0], spriteRefs[0].screen_num);
	sprites[0].x += SIDEBAR_WIDTH;
}

}
