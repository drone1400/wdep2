#pragma once
#include "Action.h"
#include "../SpriteRef.h"

namespace actions
{

class MultiSpriteDelete : public Action
{
public:
	explicit MultiSpriteDelete(std::vector<SpriteRef> spriteRefs);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Deleted multiple sprites"); }

	std::vector<Sprite> sprites;
	std::vector<SpriteRef> spriteRefs;
};

}
