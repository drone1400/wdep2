#pragma once
#include "Action.h"

namespace actions
{

class ScreenCreate : public Action
{
public:
	explicit ScreenCreate(int screen_num);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Created new screen"); }

	int screen_num;
};

}
