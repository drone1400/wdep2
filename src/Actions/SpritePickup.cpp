#include "StdAfx.h"
#include "SpritePickup.h"
#include "../Screen.h"
#include "../Map.h"

namespace actions
{

SpritePickup::SpritePickup(SpriteRef spriteRef, Sprite new_mouse_sprite, bool removeOld /* = true */)
	: spriteRef(spriteRef)
	, new_mouse_sprite(new_mouse_sprite)
	, remove_old(removeOld)
{
	this->use_with_duplicate_detection = detect_screenmatch_duplicates;

	if (use_with_duplicate_detection)
	{
		RECT origBounds;
		spriteRef->getImageBounds(origBounds);
		int scrX = spriteRef.screen_num % MAP_COLUMNS, 
			scrY = spriteRef.screen_num / MAP_COLUMNS;

		int pt = 0;
		struct
		{
			int x, y;
			void Set(int _x, int _y) {x=_x;y=_y;}
		} pts[8];

		if (origBounds.left < SIDEBAR_WIDTH && scrX > 0)
			pts[pt++].Set(-1, 0);
		if (origBounds.right > SCREEN_WIDTH - SIDEBAR_WIDTH && scrX < MAP_COLUMNS - 1)
			pts[pt++].Set(1, 0);

		if (origBounds.top < 0 && scrY > 0)
		{
			pts[pt++].Set(0, -1);
			if (origBounds.left < SIDEBAR_WIDTH && scrX > 0)
				pts[pt++].Set(-1, -1);
			if (origBounds.right > SCREEN_WIDTH - SIDEBAR_WIDTH && scrX < MAP_COLUMNS - 1)
				pts[pt++].Set(1, -1);
		}

		if (origBounds.bottom > SCREEN_HEIGHT && scrY < MAP_ROWS - 1)
		{
			pts[pt++].Set(0, 1);
			if (origBounds.left < SIDEBAR_WIDTH && scrX > 0)
				pts[pt++].Set(-1, 1);
			if (origBounds.right > SCREEN_WIDTH - SIDEBAR_WIDTH && scrX < MAP_COLUMNS - 1)
				pts[pt++].Set(1, 1);
		}

		for (int p = 0; p < pt; p++)
		{
			Screen* sc = current_map->screen[(scrY + pts[p].y) * MAP_COLUMNS + scrX + pts[p].x];
			if (!sc)
				continue;
			for (int i = 0; i < MAX_SPRITES; i++)
				if (   sc->sprite[i] != nullptr
					&& sc->sprite[i]->x == spriteRef->x - SCREEN_WIDTH*pts[p].x
					&& sc->sprite[i]->y == spriteRef->y - SCREEN_HEIGHT*pts[p].y
					&& sc->sprite[i]->sequence == spriteRef->sequence
					&& sc->sprite[i]->frame == spriteRef->frame
					&& sc->sprite[i]->trim_top == spriteRef->trim_top
					&& sc->sprite[i]->trim_bottom == spriteRef->trim_bottom
					&& sc->sprite[i]->trim_left == spriteRef->trim_left
					&& sc->sprite[i]->trim_right == spriteRef->trim_right)
				{
					dup_refs.push_back(spriteRef.at((scrY + pts[p].y) * MAP_COLUMNS + scrX + pts[p].x, i));
					dup_sprites.push_back(*sc->sprite[i]);
					break;
				}
		}
	}
}

void SpritePickup::Do()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
    
	// optionally remove old sprite
	if (remove_old) spriteRef.screen()->removeSprite(spriteRef.sprite_num);

	// load the sprite into the mouse sprite
	new_mouse_sprite.x -= SIDEBAR_WIDTH;
	current_map->loadMouseSprite(&new_mouse_sprite, spriteRef.screen_num);
	new_mouse_sprite.x += SIDEBAR_WIDTH;

	current_map->miniupdated[spriteRef.screen_num] = false;

	if (remove_old && use_with_duplicate_detection)
		for (auto& dup : dup_refs)
			dup.screen()->removeSprite(dup.sprite_num);
}

void SpritePickup::Undo()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	current_map->setMouseSprite(nullptr);

	// place the mouseSprite back on the screen if it was removed before
	if (remove_old) spriteRef.screen()->addSprite(spriteRef.sprite_num, new_mouse_sprite);

	current_map->miniupdated[spriteRef.screen_num] = false;

	if (remove_old && use_with_duplicate_detection)
		for (int i=0; i<dup_refs.size(); i++)
			dup_refs[i].screen()->addSprite(dup_refs[i].sprite_num, dup_sprites[i]);
}

}
