#include "StdAfx.h"
#include "MultiSpritePlace.h"
#include "../Screen.h"
#include "../Map.h"
#include "../Engine.h"

namespace actions
{

MultiSpritePlace::MultiSpritePlace()
{
	mouseX = mouse_x_position;
	mouseY = mouse_y_position;
	hoverX = current_map->hoverRelX;
	hoverY = current_map->hoverRelY;
	hoverScreenX = current_map->hoverScreenX;
	hoverScreenY = current_map->hoverScreenY;
	hoverScreen = current_map->hoverScreen;
	mouse.x = mouse_x_position;
	mouse.y = mouse_y_position;
}

void MultiSpritePlace::Do()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	//save mouse sprite list
	for (auto p = current_map->mouseMultiSprites.begin(); p != current_map->mouseMultiSprites.end(); ++p)
	{
		MultiMouseSprite m;
		m.x_offset = p->x_offset;
		m.y_offset = p->y_offset;
		m.sprite = p->sprite;

		mouseSprites.push_back(m);
	}
	
	if (!current_map->screenmatch)	//only place sprites on the current screen
	{
		for (auto p = current_map->mouseMultiSprites.begin(); p != current_map->mouseMultiSprites.end(); ++p)
		{
			p->sprite.x = mouseX + p->x_offset - (mouseX - hoverX - SIDEBAR_WIDTH);
			p->sprite.y = mouseY + p->y_offset - (mouseY - hoverY);

			if (hoverX > SCREEN_WIDTH) p->sprite.x -= hoverX - SCREEN_WIDTH;
			if (hoverY > SCREEN_HEIGHT) p->sprite.y -= hoverY - SCREEN_HEIGHT;

			RECT r;
		    if (current_map->sequence[p->sprite.sequence] != nullptr)
		    {
		        current_map->sequence[p->sprite.sequence]->getBounds(p->sprite.frame-1, p->sprite.size, r);
		    } else
		    {
		        Sequence::getBoundsFallback(p->sprite.frame-1, p->sprite.size, r);
		    }

		    int frame_width = SPRITE_FALLBACK_WIDTH;
		    int frame_height = SPRITE_FALLBACK_HEIGHT;
		    if (current_map->sequence[p->sprite.sequence] != nullptr)
		    {
		        frame_width = current_map->sequence[p->sprite.sequence]->frame_width[p->sprite.frame-1];
		        frame_height = current_map->sequence[p->sprite.sequence]->frame_height[p->sprite.frame-1];
		    }

			//if the sprite would actually be visible (i.e. not out-of-bounds)
			if (p->sprite.x + r.left < SCREEN_WIDTH + SIDEBAR_WIDTH &&
				p->sprite.x + r.right + frame_width > 0 &&
				p->sprite.y + r.top < SCREEN_HEIGHT &&
				p->sprite.y + r.bottom + frame_height > 0)
			{
				int n = current_map->screen[hoverScreen]->findFirstAvailableSpriteSlot();
				
				placed_sprites.emplace_back(current_map, hoverScreen, n);

				if (current_map->screen[hoverScreen]->addSprite(n, p->sprite))
					current_map->selectedSprites.push_back(SpriteRef{current_map, hoverScreen, n});
			}
		}
	}
	else	//place all mouse sprites, even multiple times if needed (on screen edges)
	{
		for (auto p = current_map->mouseMultiSprites.begin(); p != current_map->mouseMultiSprites.end(); ++p)
		{
			RECT r;
		    if (current_map->sequence[p->sprite.sequence] != nullptr)
		    {
		        current_map->sequence[p->sprite.sequence]->getBounds(p->sprite.frame-1, p->sprite.size, r);
		    } else
		    {
		        Sequence::getBoundsFallback(p->sprite.frame-1, p->sprite.size, r);
		    }

		    RECT sel_rect {
		        mouseX + p->x_offset + r.left,
		        mouseY + p->y_offset + r.top,
		        mouseX + p->x_offset + r.right,
		        mouseY + p->y_offset + r.bottom};

		    if (current_map->bindViewRect(sel_rect) == false)
		        continue;
		    
			int first_x_screen = sel_rect.left / SCREEN_WIDTH_GAP;
			int last_x_screen = sel_rect.right / SCREEN_WIDTH_GAP;
			int first_y_screen = sel_rect.top / SCREEN_HEIGHT_GAP;
			int last_y_screen = sel_rect.bottom / SCREEN_HEIGHT_GAP;

			int cur_screen;
			for (int x_screen = first_x_screen; x_screen <= last_x_screen; x_screen++)
				for (int y_screen = first_y_screen; y_screen <= last_y_screen; y_screen++)
				{
					cur_screen = y_screen * MAP_COLUMNS + x_screen;

					if (cur_screen > NUM_SCREENS || current_map->screen[cur_screen] == nullptr)
						continue;

					if (use_hard_nohit_default_place)
					{
						p->sprite.hardness = !sprite_hard;
						p->sprite.nohit = sprite_nohit;
					}

					p->sprite.x = mouseX + p->x_offset + current_map->window.left - (x_screen * (SCREEN_WIDTH_GAP)) + SIDEBAR_WIDTH;
					p->sprite.y = mouseY + p->y_offset + current_map->window.top - (y_screen * (SCREEN_HEIGHT_GAP));

					p->sprite.x += screen_gap * (cur_screen % MAP_COLUMNS - hoverScreenX);
					p->sprite.y += screen_gap * (cur_screen / MAP_COLUMNS - hoverScreenY);

					int n = current_map->screen[cur_screen]->findFirstAvailableSpriteSlot();
					
					placed_sprites.emplace_back(current_map, cur_screen, n);
					
					if (current_map->screen[cur_screen]->addSprite(n, p->sprite))
						current_map->selectedSprites.push_back(SpriteRef{current_map, cur_screen, n});
				}
		}
	}

	current_map->mouseMultiSprites.clear();
}

void MultiSpritePlace::Undo()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	for (auto& ref : placed_sprites)
		ref.screen()->removeSprite(ref.sprite_num);

	//we just transfer the pointers on undo, no need to copy things
	current_map->mouseMultiSprites = mouseSprites;
	mouseSprites.clear();

	current_map->selectedSprites.clear();
}

}
