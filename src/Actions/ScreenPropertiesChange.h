#pragma once
#include "Action.h"
#include "../Structs.h"

namespace actions
{

class ScreenPropertiesChange : public Action
{
public:
	ScreenPropertiesChange(int screen_num, int old_midi, int new_midi, bool old_indoor, bool new_indoor, BaseScriptFilename old_script, BaseScriptFilename new_script);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Changed screen properties"); }

	int screen_num;
	BaseScriptFilename old_script, new_script;
	int old_midi, new_midi;
	bool old_indoor, new_indoor;
};

}
