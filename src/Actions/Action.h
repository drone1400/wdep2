#pragma once

namespace actions
{

class Action
{
public:
	Action();

	Action(const Action&) = delete;
	Action& operator=(const Action&) = delete;

	Action(Action&&) = default;
	Action& operator=(Action&&) = default;

	virtual ~Action() = default;

	virtual void Do() = 0;
	virtual void Undo() = 0;
	virtual const TCHAR* Msg() = 0;

	int map_x, map_y;
	POINT mouse;
	int vision;
};

}
