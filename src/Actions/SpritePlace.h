#pragma once
#include "Action.h"
#include "../SpriteRef.h"

namespace actions
{

class SpritePlace : public Action
{
public:
	SpritePlace(std::vector<SpriteRef> refs, std::vector<Sprite> sprites, bool stamp);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Placed a sprite"); }

	std::vector<SpriteRef> spriteRefs;
	std::vector<Sprite> sprites;
	bool stamp;
};

}
