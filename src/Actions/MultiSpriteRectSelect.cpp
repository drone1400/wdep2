#include "StdAfx.h"
#include "MultiSpriteRectSelect.h"
#include "../Map.h"

namespace actions
{

MultiSpriteRectSelect::MultiSpriteRectSelect(bool exclusive)
{
	this->old_selection = current_map->selectedSprites;

    RECT sel_rect = current_map->selection_rect;
    if (current_map->bindViewRect(sel_rect) == false)
        return;

	int first_x_screen = sel_rect.left / SCREEN_WIDTH_GAP;
	int last_x_screen = sel_rect.right / SCREEN_WIDTH_GAP;
	int first_y_screen = sel_rect.top / SCREEN_HEIGHT_GAP;
	int last_y_screen = sel_rect.bottom / SCREEN_HEIGHT_GAP;
	
	int minx = sel_rect.left - first_x_screen * SCREEN_WIDTH_GAP;
	int maxx = sel_rect.right - last_x_screen * SCREEN_WIDTH_GAP;
	int miny = sel_rect.top - first_y_screen * SCREEN_HEIGHT_GAP;
	int maxy = sel_rect.bottom - last_y_screen * SCREEN_HEIGHT_GAP;

	for (int x_screen = first_x_screen; x_screen <= last_x_screen; x_screen++)
		for (int y_screen = first_y_screen; y_screen <= last_y_screen; y_screen++)
		{
			int cur_screen = y_screen * MAP_COLUMNS + x_screen;
			if (current_map->screen[cur_screen] == nullptr)
				continue;

			RECT cur_rect = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
			if (x_screen == first_x_screen) cur_rect.left = minx;
			if (x_screen == last_x_screen) cur_rect.right = maxx;
			if (y_screen == first_y_screen) cur_rect.top = miny;
			if (y_screen == last_y_screen) cur_rect.bottom = maxy;

			for (int i=0; i<MAX_SPRITES; i++)
			{
				SpriteRef ref{ current_map, cur_screen, i };
				if (!ref)
					continue;
				
				RECT sprite_rect;
				ref->getImageBounds(sprite_rect);
				if (sprite_rect.left < 0) sprite_rect.left = 0;
				if (sprite_rect.top < 0) sprite_rect.top = 0;
				if (sprite_rect.right > SCREEN_WIDTH) sprite_rect.right = SCREEN_WIDTH;
				if (sprite_rect.bottom > SCREEN_HEIGHT) sprite_rect.bottom = SCREEN_HEIGHT;

				if (!exclusive)
				{
					if (sprite_rect.left < cur_rect.right && sprite_rect.right > cur_rect.left &&
						sprite_rect.top < cur_rect.bottom && sprite_rect.bottom > cur_rect.top)
					{
						newly_selected.push_back(ref);
					}
				}
				else
				{
					if (sprite_rect.left >= cur_rect.left && sprite_rect.right <= cur_rect.right &&
						sprite_rect.top >= cur_rect.top && sprite_rect.bottom <= cur_rect.bottom)
					{
						newly_selected.push_back(ref);
					}
				}
			}
		}
}

void MultiSpriteRectSelect::Do()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	current_map->selectedSprites.insert(current_map->selectedSprites.end(), newly_selected.begin(), newly_selected.end());
	
	//remove duplicates (important because eg trying to remove a sprite twice would cause a null pointer dereference)
	for (auto p = current_map->selectedSprites.begin(); p!=current_map->selectedSprites.end(); ++p)
	{
		auto it = p;
		for (++it; it != current_map->selectedSprites.end();)
			if (it->screen_num == p->screen_num && it->sprite_num == p->sprite_num)
				it = current_map->selectedSprites.erase(it);
			else
				++it;
	}
}

void MultiSpriteRectSelect::Undo()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	current_map->selectedSprites = old_selection;
}

}
