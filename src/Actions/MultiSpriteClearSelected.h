#pragma once
#include "Action.h"
#include "../SpriteRef.h"

namespace actions
{

class MultiSpriteClearSelected : public Action
{
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Cleared selection"); }

	std::vector<SpriteRef> selection;
};

}
