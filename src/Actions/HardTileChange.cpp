#include "StdAfx.h"
#include "HardTileChange.h"
#include "../Screen.h"
#include "../Map.h"

namespace actions
{

HardTileChange::HardTileChange(int first_tile_x, int first_tile_y, int width, int height)
{
	this->first_tile_x = first_tile_x;
	this->first_tile_y = first_tile_y;
	this->width = width;
	this->height = height;
	memset(this->new_hard_tiles, 0, sizeof(this->new_hard_tiles));
	memset(this->old_hard_tiles, 0, sizeof(this->old_hard_tiles));
}

void HardTileChange::Do()
{
	current_map->screenMode = EditorSubmode::Hardbox;
	current_map->editor_state = EditorState::Screen;
	
	for (int m_y = first_tile_y, y = 0; y < height; m_y++, y++)
		for (int m_x = first_tile_x, x = 0; x < width; m_x++, x++)
		{
			int cur_screen = MAP_COLUMNS * (m_y / SCREEN_TILE_HEIGHT) + m_x / SCREEN_TILE_WIDTH;

			if (cur_screen > 767 || cur_screen < 0 || current_map->screen[cur_screen] == nullptr)
				continue;
			
			int x_tile = m_x % SCREEN_TILE_WIDTH;
			int y_tile = m_y % SCREEN_TILE_HEIGHT;

			current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness = new_hard_tiles[x][y];
			current_map->miniupdated[cur_screen] = false;
		}
}

void HardTileChange::Undo()
{
	current_map->screenMode = EditorSubmode::Hardbox;
	current_map->editor_state = EditorState::Screen;
	
	for (int m_y = first_tile_y, y = 0; y < height; m_y++, y++)
		for (int m_x = first_tile_x, x = 0; x < width; m_x++, x++)
		{
			int cur_screen = MAP_COLUMNS * (m_y / SCREEN_TILE_HEIGHT) + m_x / SCREEN_TILE_WIDTH;

			if (cur_screen > 767 || cur_screen < 0 || current_map->screen[cur_screen] == nullptr)
				continue;
			
			int x_tile = m_x % SCREEN_TILE_WIDTH;
			int y_tile = m_y % SCREEN_TILE_HEIGHT;

			current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness = old_hard_tiles[x][y];
		}
}

}
