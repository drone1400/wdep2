#include "StdAfx.h"
#include "SpriteCreate.h"
#include "../Map.h"
#include "../Engine.h"
#include "../MainFrm.h"
#include "../WinDinkeditView.h"

namespace actions
{

SpriteCreate::SpriteCreate(const Sprite* old_mouse_sprite, Sprite new_mouse_sprite)
	: new_mouse_sprite(new_mouse_sprite)
{
	if (old_mouse_sprite != nullptr)
		this->old_mouse_sprite = std::make_unique<Sprite>(*old_mouse_sprite);
	else
		this->old_mouse_sprite = nullptr;
}

void SpriteCreate::Do()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	// now create a sprite with the sequence
	current_map->setMouseSprite(&new_mouse_sprite);

	current_map->getMouseSprite()->x = mouse_x_position;
	current_map->getMouseSprite()->y = mouse_y_position;
}

void SpriteCreate::Undo()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	// change the mouse sprite to the new one
	current_map->setMouseSprite(old_mouse_sprite.get());

	// send the mouse cursor to where p was holding the old sprite before the sprite selector got loaded
	if (old_mouse_sprite != nullptr)
	{
		POINT mousepos = {old_mouse_sprite->x, old_mouse_sprite->y};
		mainWnd->GetRightPane()->ClientToScreen(&mousepos);
		SetCursorPos(mousepos.x, mousepos.y);
	}
}

}
