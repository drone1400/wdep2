#include "StdAfx.h"
#include "TilesChange.h"
#include "../Map.h"

namespace actions
{

TilesChange::TilesChange(int first_x_tile, int first_y_tile, int width, int height)
{
	this->first_x_tile = first_x_tile;
	this->first_y_tile = first_y_tile;
	this->width = width;
	this->height = height;
}

void TilesChange::Do()
{
	current_map->screenMode = EditorSubmode::Tile;
	current_map->editor_state = EditorState::Screen;
	current_map->placeTiles(first_x_tile, first_y_tile, width, height, new_tiles);
}

void TilesChange::Undo()
{
	current_map->screenMode = EditorSubmode::Tile;
	current_map->editor_state = EditorState::Screen;
	current_map->placeTiles(first_x_tile, first_y_tile, width, height, old_tiles);
}

}
