#pragma once
#include "Action.h"
#include "../SpriteRef.h"

namespace actions
{

class MultiSpritePickup : public Action
{
public:
	explicit MultiSpritePickup(bool pickup_copy = false);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Picked up multiple sprites"); }

	std::vector<SpriteRef> selection;
	std::vector<Sprite> sprites;
	bool copy;
};

}
