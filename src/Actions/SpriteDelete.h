#pragma once
#include "Action.h"
#include "../Sprite.h"

namespace actions
{

class SpriteDelete : public Action
{
public:
	explicit SpriteDelete(Sprite mouse_sprite);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Deleted sprite"); }

	Sprite mouse_sprite;
};

}
