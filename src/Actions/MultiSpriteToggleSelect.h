#pragma once
#include "Action.h"
#include "../SpriteRef.h"

namespace actions
{

class MultiSpriteToggleSelect : public Action
{
public:
	explicit MultiSpriteToggleSelect(SpriteRef spriteRef);
	void Do() override;
	void Undo() override { Do(); } //as this is a toggle action, Undo() is the same as Do().
	const TCHAR* Msg() override { return _T("Toggled sprite selection"); }

	SpriteRef spriteRef;
};

}
