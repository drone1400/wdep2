#pragma once
#include "Action.h"
#include "../Structs.h"

namespace actions
{

class HardTileEdit : public Action
{
public:
	HardTileEdit(int hard_tile_num, TileHardness old_tile_hard, TileHardness new_tile_hard);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Hard Tile Edited"); }

	int hard_tile_num;
	TileHardness old_tile_hard;
	TileHardness new_tile_hard;
};

}
