#include "StdAfx.h"
#include "MultiSpriteDelete.h"
#include "../Screen.h"
#include "../Map.h"
using std::vector;

namespace actions
{

MultiSpriteDelete::MultiSpriteDelete(vector<SpriteRef> spriteRefs)
	: spriteRefs(spriteRefs)
{
	transform(begin(spriteRefs), end(spriteRefs), back_inserter(sprites), op_deref());
}

void MultiSpriteDelete::Do()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	for (auto& ref : spriteRefs)
		ref.screen()->removeSprite(ref.sprite_num);

	//clear any selections because they are invalid now
	current_map->selectedSprites.clear();
}

void MultiSpriteDelete::Undo()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	//clear away any selection made since this action was made
	current_map->selectedSprites.clear();

	//add sprites back to the map
	for (int i=0; i<sprites.size(); i++)
		spriteRefs[i].screen()->addSprite(spriteRefs[i].sprite_num, sprites[i]);
	
	//and make those sprites selected again
	current_map->selectedSprites = spriteRefs;
}

}
