#include "StdAfx.h"
#include "MultiSpriteToggleSelect.h"
#include "../Map.h"

namespace actions
{

MultiSpriteToggleSelect::MultiSpriteToggleSelect(SpriteRef spriteRef)
	: spriteRef(spriteRef)
{
}

void MultiSpriteToggleSelect::Do()
{
	current_map->screenMode = EditorSubmode::MultiSprite;
	current_map->editor_state = EditorState::Screen;

	if (!spriteRef)
		return;

	if (!current_map->tryDoMultispriteDeselect(spriteRef))
		current_map->selectedSprites.push_back(spriteRef);
}

}
