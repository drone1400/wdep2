#include "StdAfx.h"
#include "SpriteDelete.h"
#include "../Map.h"
#include "../Engine.h"

namespace actions
{

SpriteDelete::SpriteDelete(Sprite mouse_sprite)
	: mouse_sprite(mouse_sprite)
{
}

void SpriteDelete::Do()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	// should always be a mouse sprite but just in case
	current_map->setMouseSprite(nullptr);
}

void SpriteDelete::Undo()
{
	current_map->screenMode = EditorSubmode::Sprite;
	current_map->editor_state = EditorState::Screen;
    hide_all_sprites = false; // make sure sprites are visible
	
	// change the mouse sprite to the new one
	current_map->setMouseSprite(&mouse_sprite);

	current_map->getMouseSprite()->x = mouse_x_position;
	current_map->getMouseSprite()->y = mouse_y_position;
}

}
