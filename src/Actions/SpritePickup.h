#pragma once
#include "Action.h"
#include "../SpriteRef.h"
#include "../Sprite.h"

namespace actions
{

class SpritePickup : public Action
{
public:
	SpritePickup(SpriteRef spriteRef, Sprite new_mouse_sprite, bool removeOld = true);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Picked sprite up"); }

	SpriteRef spriteRef;
	Sprite new_mouse_sprite;

	bool use_with_duplicate_detection;
	bool remove_old;
	std::vector<SpriteRef> dup_refs;
	std::vector<Sprite> dup_sprites;
};

}
