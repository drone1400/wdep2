#include "StdAfx.h"
#include "HardTileMultipleChange.h"
#include "../Screen.h"
#include "../Map.h"

namespace actions
{

HardTileMultipleChange::HardTileMultipleChange(int width, int height, int first_tile_x, int first_tile_y)
{
	this->width = width;
	this->height = height;
	this->first_tile_x = first_tile_x;
	this->first_tile_y = first_tile_y;
	new_hard_tiles.resize(width*height, 0);
	old_hard_tiles.resize(width*height, 0);
}

void HardTileMultipleChange::Do()
{
	current_map->screenMode = EditorSubmode::Hardbox;
	current_map->editor_state = EditorState::Screen;
	
	for (int m_y = first_tile_y, y = 0; y < height; m_y++, y++)
		for (int m_x = first_tile_x, x = 0; x < width; m_x++, x++)
		{
			int cur_screen = MAP_COLUMNS * (m_y / SCREEN_TILE_HEIGHT) + m_x / SCREEN_TILE_WIDTH;
			
			if (current_map->screen[cur_screen] == nullptr)
				continue;
			
			int x_tile = m_x % SCREEN_TILE_WIDTH;
			int y_tile = m_y % SCREEN_TILE_HEIGHT;

			if (new_hard_tiles[y * width + x] != 0)
			{
				current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness = new_hard_tiles[y * width + x];
				current_map->miniupdated[cur_screen] = false;
			}
		}
}

void HardTileMultipleChange::Undo()
{
	current_map->screenMode = EditorSubmode::Hardbox;
	current_map->editor_state = EditorState::Screen;
	
	for (int m_y = first_tile_y, y = 0; y < height; m_y++, y++)
		for (int m_x = first_tile_x, x = 0; x < width; m_x++, x++)
		{
			int cur_screen = MAP_COLUMNS * (m_y / SCREEN_TILE_HEIGHT) + m_x / SCREEN_TILE_WIDTH;

			if (current_map->screen[cur_screen] == nullptr)
				continue;

			int x_tile = m_x % SCREEN_TILE_WIDTH;
			int y_tile = m_y % SCREEN_TILE_HEIGHT;

			current_map->screen[cur_screen]->tiles[y_tile][x_tile].alt_hardness = old_hard_tiles[y * width + x];
		}
}

}
