#pragma once
#include "Action.h"
#include "../Sprite.h"

namespace actions
{

class SpriteCreate : public Action
{
public:
	SpriteCreate(const Sprite* old_mouse_sprite, Sprite new_mouse_sprite);	
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Created new sprite"); }

	std::unique_ptr<Sprite> old_mouse_sprite;
	Sprite new_mouse_sprite;
};

}
