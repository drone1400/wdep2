#pragma once
#include "Action.h"

class Screen;

namespace actions
{

class ScreenPaste : public Action
{
public:
	ScreenPaste(int screen_num, Screen* new_screen, int midi, bool indoor);
	~ScreenPaste();
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Placed copied screen"); }

	int screen_num;
	Screen* new_screen;
	int midi_num;
	bool indoor;
};

}
