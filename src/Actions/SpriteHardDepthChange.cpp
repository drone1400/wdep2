#include "StdAfx.h"
#include "SpriteHardDepthChange.h"
#include "../sequence.h"
#include "../Map.h"
#include "../PathUtil.h"
using std::ofstream;
using std::ios;

namespace actions
{

SpriteHardDepthChange::SpriteHardDepthChange( int seq, int frame /*0..49*/, int newDepthX, int newDepthY, int newX1, int newY1, int newX2, int newY2 )
{
	this->seq = seq;
	this->frame = frame;
	this->newDepthX = newDepthX;
	this->newDepthY = newDepthY;
	this->newX1 = newX1;
	this->newY1 = newY1;
	this->newX2 = newX2;
	this->newY2 = newY2;

	FRAME_INFO* finfo = current_map->sequence[seq]->frame_info[frame];

	if (finfo != nullptr)
	{
		old_used_seq_info = false;
		oldDepthX = finfo->center_x;
		oldDepthY = finfo->center_y;
		oldX1 = finfo->left_boundary;
		oldY1 = finfo->top_boundary;
		oldX2 = finfo->right_boundary;
		oldY2 = finfo->bottom_boundary;
	}
	else
		old_used_seq_info = true;
}

void SpriteHardDepthChange::Do()
{
	current_map->sequence[seq]->addFrameInfo(frame, newDepthX, newDepthY, newX1, newY1, newX2, newY2);
	done_with_optimize_off = false;

	char line[MAX_LINE_LENGTH];
	if (!optimize_dink_ini)
	{
		done_with_optimize_off = true;

		sprintf_s(line, "set_sprite_info %i %i %i %i %i %i %i %i", seq, frame+1, newDepthX, newDepthY, newX1, newY1, newX2, newY2);

		current_map->dinkIniLines.push_back(line);

		TCHAR backup[MAX_PATH], ini[MAX_PATH];
		wsprintf(ini, _T("%s%s"), (LPCTSTR)current_map->dmod_path, _T("dink.ini"));
		wsprintf(backup, _T("%s%s"), (LPCTSTR)current_map->dmod_path, DINK_INI_BACKUP_NAME);

		if (!FileReadable(backup))
			CopyFile(ini, backup, FALSE);

		ofstream fout(ini, ios::app);
		fout << "\n" << line;
		fout.close();
	}
}

void SpriteHardDepthChange::Undo()
{
	if (!old_used_seq_info)
		current_map->sequence[seq]->addFrameInfo(frame, oldDepthX, oldDepthY, oldX1, oldY1, oldX2, oldY2);
	else
	{
		delete current_map->sequence[seq]->frame_info[frame];
		current_map->sequence[seq]->frame_info[frame] = nullptr;
	}

	if (done_with_optimize_off)	//optimize_dink_ini was off when Do() was last called
	{
		TCHAR backup[MAX_PATH], ini[MAX_PATH];
		wsprintf(ini, _T("%s%s"), (LPCTSTR)current_map->dmod_path, _T("dink.ini"));
		wsprintf(backup, _T("%s%s"), (LPCTSTR)current_map->dmod_path, DINK_INI_BACKUP_NAME);

		if (!FileReadable(backup))
		{
			//mainWnd->setStatusText("Undo Action: Changed hardness/depth dot of a sprite [FAILED!]");
			return;
		}
		
		current_map->dinkIniLines.pop_back();

		_tremove(ini);
		CopyFile(backup, ini, FALSE);

		ofstream fout(ini, ios::app);
		for (auto it=current_map->dinkIniLines.begin(); it != current_map->dinkIniLines.end(); ++it)
			fout << "\n" << *it;
		fout.close();
	}
}

}
