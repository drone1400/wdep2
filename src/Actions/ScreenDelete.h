#pragma once
#include "Action.h"

class Screen;

namespace actions
{

class ScreenDelete : public Action
{
public:
	ScreenDelete(int screen_num, Screen* old_screen, int midi, bool indoor);
	~ScreenDelete();
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Deleted screen"); }

	int screen_num;
	Screen* old_screen;
	int midi_num;
	bool indoor;
};

}
