#pragma once
#include "Action.h"
#include "../Sprite.h"
#include "../Map.h"

namespace actions
{

template <typename TProperty>
class MultiSpritePropertyChange : public Action
{
public:
	MultiSpritePropertyChange(TProperty Sprite::* changedProperty, TProperty newValue)
	{
		this->changedProperty = changedProperty;
		this->newValue = newValue;

		selected_sprites = current_map->selectedSprites;

		for (auto& sel : selected_sprites)
			oldValues.push_back(sel->*changedProperty);
	}

	void Do() override
	{
		current_map->screenMode = EditorSubmode::MultiSprite;
		current_map->editor_state = EditorState::Screen;

		for (auto& sel : selected_sprites)
			sel->*changedProperty = newValue;
	}

	void Undo() override
	{
		current_map->screenMode = EditorSubmode::MultiSprite;
		current_map->editor_state = EditorState::Screen;

		auto itOld = oldValues.begin();
		auto itSel = selected_sprites.begin();
		for (; itOld != oldValues.end(); ++itOld, ++itSel)
			*itSel->*changedProperty = *itOld;
	}

	const TCHAR* Msg() override { return _T("Changed a property of multiple sprites"); }

private:
	std::vector<SpriteRef> selected_sprites;
	TProperty Sprite::* changedProperty;
	std::vector<TProperty> oldValues;
	TProperty newValue;
};

template <typename TProperty>
auto makeMultiSpritePropertyChange(TProperty Sprite::* changedProperty, TProperty newValue)
{
	return new MultiSpritePropertyChange<TProperty>(changedProperty, newValue);
}

}
