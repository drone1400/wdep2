#pragma once
#include "Action.h"
#include "../Structs.h"

namespace actions
{

class TilesChange : public Action
{
public:
	TilesChange(int first_x_tile, int first_y_tile, int width, int height);
	void Do() override;
	void Undo() override;
	const TCHAR* Msg() override { return _T("Placed tiles on screen"); }

	int first_x_tile, first_y_tile;
	int width, height;
	TILEDATA new_tiles[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2];
	TILEDATA old_tiles[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2];
};

}
