#include "StdAfx.h"
#include "ScreenDelete.h"
#include "../Screen.h"
#include "../Map.h"
#include "../Minimap.h"

namespace actions
{

ScreenDelete::ScreenDelete(int screen_num, Screen* old_screen, int midi, bool indoor)
{
	this->screen_num = screen_num;
	this->old_screen = old_screen;
	this->midi_num = midi;
	this->indoor = indoor;
}

ScreenDelete::~ScreenDelete()
{
	delete old_screen;
}

void ScreenDelete::Do()
{
	delete current_map->screen[screen_num];
	current_map->screen[screen_num] = nullptr;
	current_map->midi_num[screen_num] = 0;
	current_map->indoor[screen_num] = 0;

	current_map->miniupdated[screen_num] = false;
	current_map->minimap->renderMapSquare(screen_num);
}

void ScreenDelete::Undo()
{
	current_map->screen[screen_num] = new Screen(old_screen);
	current_map->indoor[screen_num] = indoor;
	current_map->midi_num[screen_num] = midi_num;

	current_map->minimap->renderMapSquare(screen_num);
	current_map->miniupdated[screen_num] = false;
}

}
