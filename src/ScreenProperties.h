#pragma once
#include "resource.h"

class ScreenProperties : public CDialog
{
public:
	ScreenProperties(CWnd* pParent = nullptr);

	enum { IDD = IDD_SCREEN_PROPERTIES };
	BOOL	m_inside;
	int		m_midi;
	CString	m_script;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support
	afx_msg void OnButton1();
	afx_msg void OnButton2();

	DECLARE_MESSAGE_MAP()
};