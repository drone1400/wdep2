// this is part of the Map class and includes the I/O functionality
#include "StdAfx.h"
#include "Globals.h"
#include "Map.h"
#include "Screen.h"
#include "Sequence.h"
#include "Tile.h"
#include "HardTileSelector.h"
#include "Structs.h"
#include "SpriteLibrary.h"
#include "PathUtil.h"
#include <ios>

#include "CommonFile.h"
#include "SimpleLog.h"
using std::ifstream;
using std::ofstream;
using std::ios;
using std::ios_base;
using std::endl;
using std::vector;


#define DINK_HEADER_NAME		"Smallwood"

struct DUMMY_HARD_TILE
{
	BYTE data[TILEWIDTH][TILEHEIGHT + 1];	// 1 extra garbage byte
};

#define HARD_DAT_MAX_TS_INDEX	128
#define HARD_DAT_EOF_JUNK_INTS	2752

// NOTE: not using this one anymore...
struct DUMMY_TILE_HARDNESS
{
	int data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
	int garbage[32];
};


void Map::open_dmod() //throws
{
	parse_dink_dat();
	read_tile_bmps();
	check_dirty_dink_ini_quit();
	parse_dink_ini();
	parse_hard_dat();

	memset(&miniupdated, 0, sizeof(miniupdated));
}

void Map::backup_dmod_old()
{
	// check dmod base path exists
	if (!FolderExists(dmod_path))
	{
		// if base path is invalid, can not do anything
		Log_Error("DMOD base path invalid while attempting to backup files... This should never happen...");
		Log_Error(dmod_path,LOG_FORMAT_CONTINUE);
		return;
	}

	// create WDED metadata subdirectory
	CString wdePath;
	Path_ConcatIntoAndNormalize(wdePath, dmod_path, _T(".wded"));
	if (!FolderExists(wdePath))
	{
		if (CreateDirectory(wdePath, nullptr) == FALSE)
		{
			Log_Error("DMOD Could not create WDE subdirectory...");
			Log_Error(wdePath,LOG_FORMAT_CONTINUE);
			return;
		}
	}

	// create backup directory
	CString backupDir;
	CString backupDirBase;
	int backupindex = 1;
	Path_ConcatIntoAndNormalize(backupDirBase, wdePath, _T("dmod.bak"));
	backupDir.SetString(backupDirBase);
	while (FolderExists(backupDir))
	{
		backupDir.SetString(backupDirBase);
		backupDir.AppendFormat(_T("%d"),backupindex);
		backupindex++;
	}
	if (CreateDirectory(backupDir, nullptr) == FALSE)
	{
		Log_Error("DMOD Could not create backup subdirectory...");
		Log_Error(backupDir,LOG_FORMAT_CONTINUE);
		return;
	}

	// old dmod files
	CString dinkiniOld;
	Path_ConcatIntoAndNormalize(dinkiniOld, dmod_path,_T("dink.ini"));
	CString dinkdatOld;
	Path_ConcatIntoAndNormalize(dinkdatOld, dmod_path,_T("dink.dat"));
	CString mapdatOld;
	Path_ConcatIntoAndNormalize(mapdatOld, dmod_path,_T("map.dat"));
	CString harddatOld;
	Path_ConcatIntoAndNormalize(harddatOld, dmod_path,_T("hard.dat"));

	// backup dmod files
	CString dinkiniBak;
	Path_ConcatIntoAndNormalize(dinkiniBak, backupDir,_T("dink.ini"));
	CString dinkdatBak;
	Path_ConcatIntoAndNormalize(dinkdatBak, backupDir,_T("dink.dat"));
	CString mapdatBak;
	Path_ConcatIntoAndNormalize(mapdatBak, backupDir,_T("map.dat"));
	CString harddatBak;
	Path_ConcatIntoAndNormalize(harddatBak, backupDir,_T("hard.dat"));

	
	
	if(FileReadable(dinkiniOld))
	{
		if (CopyFile(dinkiniOld, dinkiniBak, TRUE) == 0)
		{
			Log_Error(_T("Failed to backup dink.ini"));
		}
	}

	if(FileReadable(dinkdatOld))
	{
		if (CopyFile(dinkdatOld, dinkdatBak, TRUE) == 0)
		{
			Log_Error(_T("Failed to backup dink.dat"));
		}
	}

	if(FileReadable(mapdatOld))
	{
		if (CopyFile(mapdatOld, mapdatBak, TRUE) == 0)
		{
			Log_Error(_T("Failed to backup map.dat"));
		}
	}

	if(FileReadable(harddatOld))
	{
		if (CopyFile(harddatOld, harddatBak, TRUE) == 0)
		{
			Log_Error(_T("Failed to backup hard.dat"));
		}
	}
}


//Save any changes to the dmod - throws on error.
void Map::save_dmod()
{
	save_dmod_at(dmod_path);

	sprite_library.storeList();
}

void Map::save_dmod_at(CString const & basePath)
{
	if (!FolderExists(basePath))
	{
		Log_Error("DMOD can not be saved at location, base path not found...");
		Log_Error(basePath, LOG_FORMAT_CONTINUE);
		return;
	}

	CString dinkini;
	Path_ConcatIntoAndNormalize(dinkini, basePath,_T("dink.ini"));
	CString dinkdat;
	Path_ConcatIntoAndNormalize(dinkdat, basePath,_T("dink.dat"));
	CString mapdat;
	Path_ConcatIntoAndNormalize(mapdat, basePath,_T("map.dat"));
	CString harddat;
	Path_ConcatIntoAndNormalize(harddat, basePath,_T("hard.dat"));
	
	if (optimize_dink_ini)
	{
		save_dink_ini(dinkini);
	}

	save_dink_dat(dinkdat);
	save_map_dat(mapdat);
	save_hard_dat(harddat);
}

int Map::deallocate_sequences()
{
	for (int i = 0; i < MAX_SEQUENCES; i++)
	{
		if (sequence[i])
		{
			delete sequence[i];
			sequence[i] = nullptr;
		}
	}
	return true;
}

int Map::release_tile_bmps()
{
	for (int i = 0; i < MAX_TILE_BMPS; i++)
	{
		if (tileBmp[i])
		{
			delete tileBmp[i];
			tileBmp[i] = nullptr;
		}
	}

	return true;
}

int Map::deallocate_map()
{
	for (int i = 0; i < NUM_SCREENS; i++)
	{
		if (screen[i])
		{
			delete screen[i];
			screen[i] = nullptr;
		}
	}
	return true;
}

void Map::read_tile_bmps(void)
{
	for (int i = 0; i < MAX_TILE_BMPS; i++)	
		tileBmp[i] = new Tile(i + 1);
}

void Map::save_hard_dat(CString const& filename)
{
	ofstream stream(filename, ios::out | ios::binary);
	if (!stream) 
	{ 
		MB_ShowError(_T("Could not open hard.dat!"));
		throw 1; 
	} 
	stream.exceptions(ofstream::failbit | ofstream::badbit);

	vector<BYTE> garbage(58);
 
	DUMMY_HARD_TILE hard_tile;

	// --- IMPORTANT NOTE REGARDING TILE #0 ---
	// due to the way alt-hardness works in game, you can't ever assign hardness at index #0 as an alternative hardness on a map screen
	// you can only use hardness index #0 by assigning it directly as the default hardness for a tile in a tilesheet
	// the game seems to usually use it as a default empty tile, aka, when a tile sheet has a hardness index of 0, it is empty
	// so it makes sense to always write hardness index #0 as empty in the hard.dat file and not let it be user editable
	
	memset(&hard_tile, 0, sizeof(DUMMY_HARD_TILE)); 
	stream.write((char*) &hard_tile, sizeof(DUMMY_HARD_TILE));
		
	// write out useless data 
	stream.write((char*) &garbage[0], 58);

	// write tiles #1 through #799
	for (int i = 1; i < NUM_HARD_TILES; i++) 
	{ 
		// revert to the messed up file format 
		for (int y = 0; y < TILEHEIGHT; y++)  
			for (int x = 0; x < TILEWIDTH; x++) 
				hard_tile.data[x][y] = hard_tile_selector.hardTile[i][y][x];
 
		stream.write((char*) &hard_tile, sizeof(DUMMY_HARD_TILE));
		
		// write out useless data 
		stream.write((char*) &garbage[0], 58);
	} 


	int tsIndex = 0;
	int tsX = 0;
	int tsY = 0;
	for (int tsid = 0; tsid < MAX_TILE_BMPS; tsid++)
	{
		for (int y = 0, hidx=0; y < TILESHEET_TILE_HEIGHT; y++)
		{
			for (int x = 0; x < TILESHEET_TILE_WIDTH; x++,hidx++)
			{
				int value = tileBmp[tsid]->tile_hardness[y][x];
				
				if (hidx < HARD_DAT_MAX_TS_INDEX)
				{
					// only save 128 bytes per tile sheet from stream
					stream.write(reinterpret_cast<char*>(&value), sizeof(int));
				}
			}
		}
	}

	// write out the 2752 junk int values at the end of the file too...
	// these are not used by any dink engine as far as i'm aware, but they are there in the original file format nonetheless
	for (int i = 0; i < HARD_DAT_EOF_JUNK_INTS; i++)
	{
		int value = 0;
		stream.write(reinterpret_cast<char*>(&value), sizeof(int));
	}

	stream.flush();
	stream.close();
}

void Map::parse_hard_dat(void)
{
	/*
	hard.dat
	file size: 2,118,400
	800 entries
	block size of each tile: 2550
	size between blocks: 58
	first block begins at beginning of file
	column major order
	each pixel separated by 50 bytes of data followed by 1 byte = 0
	z is 1, normal hardness
	a is 2, water hardness
	s is 3, ??? hardness
	*/

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)dmod_path, _T("hard.dat"));

	ifstream stream(filename, ios::in | ios::binary);
	if (!stream)
	{
		MB_ShowError(_T("Could not open hard.dat!"));
		throw 1;
	}
	stream.exceptions(ifstream::failbit | ifstream::badbit | ifstream::eofbit);

	DUMMY_HARD_TILE hard_tile;

	BYTE *dest_ptr;
	
	for (int i = 0; i < NUM_HARD_TILES; i++)
	{
		stream.read((char*) &hard_tile, sizeof(hard_tile));

		dest_ptr = hard_tile_selector.hardTile[i][0].data();

		// fix the messed up file format
		for (int y = 0; y < TILEHEIGHT; y++)		
			for (int x = 0; x < TILEWIDTH; x++)
			{
				*dest_ptr = hard_tile.data[x][y];
				dest_ptr++;
			}
		
		stream.seekg(58, ios_base::cur);  // skip useless data
	}

	// IMPORTANT NOTE: in previous versions of WDE, the hard.dat data would only read/write 96 blocks of tile hard index values per tile sheet
	// from what i understand, there are hard.dat files out there that might have missing values on the last tile sheet...
	// supposedly, some freedinkedit implementations save hard.dat files with the last tile sheet using only 96 int values instead of 128 as is expected
	// so to make this compatible with any implementation, we just read all the tile sheets int by int, and assign a 0 value to the missing ones

	int tsIndex = 0;
	int tsX = 0;
	int tsY = 0;
	for (int tsid = 0; tsid < MAX_TILE_BMPS; tsid++)
	{
		for (int y = 0, hidx=0; y < TILESHEET_TILE_HEIGHT; y++)
		{
			for (int x = 0; x < TILESHEET_TILE_WIDTH; x++,hidx++)
			{
				int value = 0;
				
				if (hidx < HARD_DAT_MAX_TS_INDEX)
				{
					// only read 128 bytes per tile sheet from stream
					stream.read(reinterpret_cast<char*>(&value), sizeof(int));
					// if data was not read ok, reset value to 0
					if (!stream) value = 0;
				}

				tileBmp[tsid]->tile_hardness[y][x] = value;
			}
		}
	}
}

void Map::parse_dink_dat(void)
{
	int screen_order[NUM_SCREENS];

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)dmod_path, _T("dink.dat"));

	ifstream stream(filename, ios::in | ios::binary);
	if (!stream)
	{
		// dink.dat not found, so clear the memory used by it
		memset(screen_order, 0, sizeof(int) * NUM_SCREENS);
		memset(midi_num, 0, sizeof(int) * NUM_SCREENS);
		memset(indoor, 0, sizeof(int) * NUM_SCREENS);

		// clear map.dat
		memset(screen, 0, sizeof(Screen*) * (NUM_SCREENS+1));

		throw 0;
	}
	stream.exceptions(ifstream::failbit | ifstream::badbit | ifstream::eofbit);

	stream.seekg(24);

	// read in the order the screens were created
	// this lets us know where in the map.dat each screen is located
	stream.read((char*) screen_order, sizeof(int) * NUM_SCREENS);
	
	stream.seekg(4, std::ios_base::cur);

	// read in midi numbers for each screen
	stream.read((char*) midi_num, sizeof(int) * NUM_SCREENS);
	
	stream.seekg(4, std::ios_base::cur);

	// read in whether the screen is indoor or outdoor
	stream.read((char*) indoor, sizeof(int) * NUM_SCREENS);

	parse_map_dat(screen_order);
}

void Map::parse_map_dat(int screen_order[NUM_SCREENS])
{
	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)dmod_path, _T("map.dat"));

	ifstream stream(filename, ios::in | ios::binary);
	if (!stream)
	{
		MB_ShowError(_T("Cannot open map.dat!"));
		throw 1;
	}
	stream.exceptions(ifstream::failbit | ifstream::badbit | ifstream::eofbit);

	vector<DUMMY_SPRITE> sprite_data(MAX_SPRITES);

	DUMMY_TILE tile_data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];	

	for (int i = 0; i < NUM_SCREENS; i++)
	{
		// find the next screen to load
		int map_location = screen_order[i] - 1;
		if (map_location == -1)
		{
			screen[i] = nullptr;
			continue;
		}
		
		// go to that spot in the map file to load
		stream.seekg(map_location * SCREEN_DATA_SIZE, ios_base::beg);

		// advance to first tile
		stream.seekg(20, ios_base::cur);

		// read in the tiles		
		screen[i] = new Screen;

		// read in the tile data
		stream.read((char*)tile_data, sizeof(DUMMY_TILE) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH);

		for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)		
			for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
			{
				int temp = tile_data[y][x].tile;
				// make the map format easier to read
				screen[i]->tiles[y][x].bmp = short(temp / TILESHEET_ID_MULTIPLIER);
				if (screen[i]->tiles[y][x].bmp < 0 || screen[i]->tiles[y][x].bmp > 40)
				{
					screen[i]->tiles[y][x].bmp = 30;
				}
				screen[i]->tiles[y][x].x = short((temp % TILESHEET_ID_MULTIPLIER) % 12);
				screen[i]->tiles[y][x].y = short(temp % TILESHEET_ID_MULTIPLIER) / 12;

				screen[i]->tiles[y][x].alt_hardness = short(tile_data[y][x].alt_hardness);
			}

		// advance to sprite data
		stream.seekg(540, ios_base::cur);

		// read in a sprite block
		stream.read((char*)&sprite_data[0], sizeof(DUMMY_SPRITE) * MAX_SPRITES);

		// read in the sprite data
		for (int cur_sprite = 0; cur_sprite < MAX_SPRITES; cur_sprite++)
		{
			// check if there's anything in the sprite block
			if (sprite_data[cur_sprite].sprite_exist == 1)
			{
				// found a sprite, now allocate the structure for it
				screen[i]->sprite[cur_sprite] = new Sprite(sprite_data[cur_sprite]);
			}
		}

		stream.read(screen[i]->script.s, BASE_SCRIPT_MAX_LENGTH);
	}
}

void Map::save_dink_ini(CString const& filename)
{
	int cur_seq;
	char buffer[MAX_PATH];

	// open the dink.ini file
	ofstream dink_ini;
	dink_ini.exceptions(ofstream::failbit | ofstream::badbit);
	dink_ini.open(filename);

	for (cur_seq = 1; cur_seq <= MAX_SEQUENCES; cur_seq++)
	{
		if (sequence[cur_seq] != nullptr)
		{
			if (sequence[cur_seq]->now == true)
				dink_ini << "load_sequence_now " << sequence[cur_seq]->graphics_path << " " <<	cur_seq << " ";
			else
				dink_ini << "load_sequence " << sequence[cur_seq]->graphics_path << " " <<	cur_seq << " ";
			
			//do we need to print all this?
			if (sequence[cur_seq]->frame_delay ||
				sequence[cur_seq]->center_x ||
				sequence[cur_seq]->center_y ||
				sequence[cur_seq]->left_boundary ||
				sequence[cur_seq]->top_boundary ||
				sequence[cur_seq]->right_boundary ||
				sequence[cur_seq]->bottom_boundary)
			{
				//yes we do...
				dink_ini << sequence[cur_seq]->frame_delay << " " <<
				sequence[cur_seq]->center_x << " " <<
				sequence[cur_seq]->center_y << " " <<
				sequence[cur_seq]->left_boundary << " " <<
				sequence[cur_seq]->top_boundary << " " <<
				sequence[cur_seq]->right_boundary << " " <<
				sequence[cur_seq]->bottom_boundary << " ";
			}

			//print special attributes if needed
			if (sequence[cur_seq]->type == BLACK)
				dink_ini << "BLACK";

			else if (sequence[cur_seq]->type == NOTANIM)
				dink_ini << "NOTANIM";

			else if (sequence[cur_seq]->type == LEFTALIGN)
				dink_ini << "LEFTALIGN";

			dink_ini << "\n";
		}
	}

	dink_ini << "\n\n";

	for (cur_seq = 1; cur_seq < MAX_SEQUENCES; cur_seq++)
	{
		if (sequence[cur_seq] != nullptr)
		{
			for (int cur_frame = 1; cur_frame < MAX_FRAMES; cur_frame++)
			{
				if (sequence[cur_seq]->frame_info[cur_frame-1] != nullptr)
				{
					sprintf_s(buffer, "set_sprite_info %i %i %i %i %i %i %i %i", cur_seq, cur_frame, sequence[cur_seq]->frame_info[cur_frame-1]->center_x,
						sequence[cur_seq]->frame_info[cur_frame-1]->center_y,
						sequence[cur_seq]->frame_info[cur_frame-1]->left_boundary,
						sequence[cur_seq]->frame_info[cur_frame-1]->top_boundary,
						sequence[cur_seq]->frame_info[cur_frame-1]->right_boundary,
						sequence[cur_seq]->frame_info[cur_frame-1]->bottom_boundary);

					dink_ini << buffer << endl;
				}
			}
		}
	}

	dink_ini << "\n\n";

	for (cur_seq = 1; cur_seq < MAX_SEQUENCES; cur_seq++)
	{
		if (sequence[cur_seq] != nullptr)
		{
			for (int cur_frame = 1; cur_frame < MAX_FRAMES; cur_frame++)
			{
				if (sequence[cur_seq]->set_frame_delay[cur_frame-1] != 0)
				{
					sprintf_s(buffer, "set_frame_delay %i %i %i", cur_seq, cur_frame, sequence[cur_seq]->set_frame_delay[cur_frame-1]);
					dink_ini << buffer << endl;
				}
				if (sequence[cur_seq]->set_frame_special[cur_frame-1] != 0)
				{
					sprintf_s(buffer, "set_frame_special %i %i %i", cur_seq, cur_frame, sequence[cur_seq]->set_frame_special[cur_frame-1]);
					dink_ini << buffer << endl;
				}
				if (sequence[cur_seq]->set_frame_frame_seq[cur_frame] != 0)
				{
					sprintf_s(buffer, "set_frame_frame %i %i %i", cur_seq, cur_frame, sequence[cur_seq]->set_frame_frame_seq[cur_frame]);
					dink_ini << buffer;

					if (sequence[cur_seq]->set_frame_frame_seq[cur_frame] != -1)
						dink_ini << " " << sequence[cur_seq]->set_frame_frame_frame[cur_frame];

					dink_ini << endl;
				}
			}
		}
	}
	dink_ini.close();
}

void Map::parse_dink_ini()
{
	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)dmod_path, _T("dink.ini"));

	// open the dink.ini file
	ifstream dink_ini(filename);

	char input_line[MAX_LINE_LENGTH];
	char parse_inputs[MAX_PARSE_INPUTS][MAX_LINE_LENGTH];

	int num_tokens = 0;

	int num_special_frames = 0;

	while (dink_ini.getline(input_line, MAX_LINE_LENGTH))
	{
		// now let's process the text from this line

		// check for comments or blank lines and skip them
		if (input_line[0] == ';' || input_line[0] == '/' || input_line[0] == '\0')
			continue;

		//Clear the data from the previous line
		for (int i = 1; i < MAX_PARSE_INPUTS; i++)
		{
			strcpy_s(parse_inputs[i], "");
		}

		// parse line into up to MAX_PARSE_INPUTS strings
		char* pos = input_line;
		for (num_tokens = 0; num_tokens < MAX_PARSE_INPUTS && pos[0] != '\0'; num_tokens++)
		{
			char* c = strchr(pos, ' ');
			if (c == NULL)
			{
				strcpy(parse_inputs[num_tokens], pos);
				num_tokens++;
				break;
			}
			else
			{
				*c = '\0';
				strcpy(parse_inputs[num_tokens], pos);
				pos = c+1;
			}

		}

		if (num_tokens > 2)
		{
			// process command: load_sequence_now
			if (_stricmp("load_sequence_now", parse_inputs[0]) == 0 || _stricmp("load_sequence", parse_inputs[0]) == 0)
			{
				bool now;

				if (_stricmp("load_sequence_now", parse_inputs[0]) == 0)
					now = true;
				else
					now = false;

				int frame_delay = 0;
				int center_x = 0;
				int center_y = 0;
				int left_boundary = 0;
				int top_boundary = 0;
				int right_boundary = 0;
				int bottom_boundary = 0;
				int type = 0;

				int sequence_num = atoi(parse_inputs[2]);

				if (num_tokens > 3)
				{
					if (_stricmp("BLACK", parse_inputs[3]) == 0)
					{
						type = BLACK;
					}
					else if (_stricmp("NOTANIM", parse_inputs[3]) == 0)
					{
						type = NOTANIM;
					}
					else if (_stricmp("LEFTALIGN", parse_inputs[3]) == 0)
					{
						type = LEFTALIGN;
					}
					else
					{
						frame_delay = atoi(parse_inputs[3]);
						center_x = atoi(parse_inputs[4]);
						center_y = atoi(parse_inputs[5]);
						left_boundary = atoi(parse_inputs[6]);
						top_boundary = atoi(parse_inputs[7]);
						right_boundary = atoi(parse_inputs[8]);
						bottom_boundary = atoi(parse_inputs[9]);
					}
				}


				if (sequence[sequence_num] != nullptr)
					continue;

				sequence[sequence_num] = new Sequence(sequence_num + 1, parse_inputs[1], frame_delay, type,
						center_x, center_y, left_boundary, top_boundary, right_boundary, bottom_boundary, now);
			}
			else if (_stricmp("set_sprite_info", parse_inputs[0]) == 0)
			{
				int cur_sequence = atoi(parse_inputs[1]);
				// check if sequence exists first
				if (sequence[cur_sequence] == nullptr)
					continue;
				
				int frame_num = atoi(parse_inputs[2]) - 1;

				sequence[cur_sequence]->addFrameInfo(frame_num,
					atoi(parse_inputs[3]),
					atoi(parse_inputs[4]),
					atoi(parse_inputs[5]),
					atoi(parse_inputs[6]),
					atoi(parse_inputs[7]),
					atoi(parse_inputs[8]));

				num_special_frames++;
			}
			else if (_stricmp("set_frame_frame", parse_inputs[0]) == 0)
			{
				int dest_sequence = atoi(parse_inputs[1]);
				int dest_frame = atoi(parse_inputs[2]) - 1;
				int cur_sequence = atoi(parse_inputs[3]);
				int cur_frame = atoi(parse_inputs[4]) - 1;

				if (sequence[dest_sequence] == nullptr)
					continue;

				sequence[dest_sequence]->set_frame_frame_seq[dest_frame] = cur_sequence;

				if (cur_sequence != -1)
					sequence[dest_sequence]->set_frame_frame_frame[dest_frame] = cur_frame;
			}
			else if (_stricmp("set_frame_delay", parse_inputs[0]) == 0)
			{
				int cur_sequence = atoi(parse_inputs[1]);
				int cur_frame = atoi(parse_inputs[2]);
				int delay = atoi(parse_inputs[3]);

				if (sequence[cur_sequence] == nullptr)
					continue;

				sequence[cur_sequence]->set_frame_delay[cur_frame-1] = delay;
			}
			else if (_stricmp("set_frame_special", parse_inputs[0]) == 0)
			{
				int cur_sequence = atoi(parse_inputs[1]);
				int cur_frame = atoi(parse_inputs[2]);
				int hit = atoi(parse_inputs[3]);

				if (sequence[cur_sequence] == nullptr)
					continue;

				sequence[cur_sequence]->set_frame_special[cur_frame-1] = hit;
			}
		}
	} // end while loop

	if (num_special_frames >= MAX_FRAME_INFOS)
	{
		CString setSpriteInfoCountError;
		setSpriteInfoCountError.Format(_T("You have exceeded the maximum number of \"set_sprite_info\" commands by %d. \
			These will still work in WinDinkedit but will be ignored by Dinkedit and the Dink engine. \
			Enable Optimize Dink.ini in the options menu and WDE will attempt to fix this on the next save."), num_special_frames - MAX_FRAME_INFOS + 1);
		MB_ShowWarning(setSpriteInfoCountError);
	}
}

//Checks for unsaved dink.ini stuff - throws if the open should stop.
void Map::check_dirty_dink_ini_quit()
{
	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)dmod_path, DINK_INI_BACKUP_NAME);

	if (FileReadable(filename))
	{
		int res = MB_ShowGeneric(_T("The dink.ini (hardness and depth dot data) of this DMod was edited in a previous session that closed unexpectedly. \
								Do you wish to keep the edits that were made?"), _T("Warning"), MB_ICONWARNING | MB_YESNOCANCEL);
		if (res == IDCANCEL)
			throw 0;
		
		if (res == IDYES)
		{
			_tremove(filename);
		}
		else if (res == IDNO)
		{
			TCHAR ini[MAX_PATH];
			wsprintf(ini, _T("%s%s"), (LPCTSTR)dmod_path, _T("dink.ini"));
			_tremove(ini);
			_trename(filename, ini);
		}
	}
}


void Map::save_dink_dat(CString const& filename)
{
	vector<BYTE> dummy_array(2240, 0);

	ofstream stream(filename, ios::out | ios::binary);
	if (!stream) throw 0;
	stream.exceptions(ofstream::failbit | ofstream::badbit | ofstream::eofbit);

	stream.write(DINK_HEADER_NAME, sizeof(DINK_HEADER_NAME));

	// write out garbage
	stream.write((char*) &dummy_array[0], 24 - sizeof(DINK_HEADER_NAME));
	
	// get the order the screens were created
	int screen_order[NUM_SCREENS];
	
	// first fix the numbers so they are ordered
	int screen_count = 1;
	for (int i = 0; i < NUM_SCREENS; i++)
		if (screen[i] != nullptr)
			screen_order[i] = screen_count++;
		else
			screen_order[i] = 0;	

	// write out the order the screens were created
	stream.write((char*) &screen_order, sizeof(int) * NUM_SCREENS);

	// write out garbage
	stream.write((char*) &dummy_array[0], 4);

	// write out midi numbers for each screen
	stream.write((char*) midi_num, sizeof(int) * NUM_SCREENS);

	// write out garbage
	stream.write((char*) &dummy_array[0], 4);

	// write out whether the screen is indoor or outdoor
	stream.write((char*) indoor, sizeof(int) * NUM_SCREENS);

	// write out extra garbage
	stream.write((char*) &dummy_array[0], 2240);
}


void Map::save_map_dat(CString const& filename)
{
	DUMMY_SPRITE sprite_data;

	BYTE dummy_array[1024];
	memset(dummy_array, 0, 1024);
	
	//DUMMY_SPRITE sprite_data[MAX_SPRITES];
	DUMMY_TILE tile_data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
	memset(tile_data, 0, SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH * sizeof(DUMMY_TILE));

	// open the map file
	ofstream stream(filename, ios::out | ios::binary);
	if (!stream) throw 0;
	stream.exceptions(ofstream::failbit | ofstream::badbit | ofstream::eofbit);

	for (int i = 0; i < NUM_SCREENS; i++)
	{
		if (screen[i] == nullptr)
			continue;

		// write out garbage
		stream.write((char*) &dummy_array, 20);
		
		// fill in tile output matrix
		for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
		{
			for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
			{
				// tile number
				tile_data[y][x].tile = (screen[i]->tiles[y][x].bmp << 7) +
										screen[i]->tiles[y][x].x +
										screen[i]->tiles[y][x].y * 12;

				// tile hardness
				tile_data[y][x].alt_hardness = screen[i]->tiles[y][x].alt_hardness;
			}
		}

		// write out the tile information for the screen
		stream.write((char*) tile_data, SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH * sizeof(DUMMY_TILE));
		
		// write out more garbage
		stream.write((char*) &dummy_array, 540);

		// write out the sprite data
		for (int cur_sprite = 0; cur_sprite < MAX_SPRITES; cur_sprite++)
		{
			// check if there's anything in the sprite block
			if (screen[i]->sprite[cur_sprite] != nullptr)
			{
				screen[i]->sprite[cur_sprite]->formatOutput(sprite_data);

				// write out a sprite block
				stream.write((char*) &sprite_data, sizeof(sprite_data));
			}
			else
			{
				// write out a sprite block
				stream.write((char*) &dummy_array, sizeof(DUMMY_SPRITE));
			}
		}

		stream.write(screen[i]->script.s, BASE_SCRIPT_MAX_LENGTH);
		
		// write out garbage
		stream.write((char*) &dummy_array, 1019);
	}
}
