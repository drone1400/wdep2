#pragma once
#include "Common.h"
#include "Const.h"
#include "Structs.h"
class Sprite;

class Screen
{
public:
	Screen();
	Screen(Screen* copy_screen);
	~Screen();

	Sprite* getSpriteAtXy(int x, int y, int &sprite_num);
	bool addSprite(int sprite_num, Sprite const& new_sprite);
	void removeSprite(int sprite_num);
	int findFirstAvailableSpriteSlot();
	int drawTiles(int x_offset, int y_offset, RECT* cliprect = nullptr);
	int drawSprites(int x_offset, int y_offset, RECT &clip_rect, int drawn_screen = -1, SpriteRenderOptions * render_options = nullptr);
	int drawHardTiles(int x_offset, int y_offset);
	int drawHardSprites(int x_offset, int y_offset, RECT &clip_rect);

	TILEDATA tiles[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
	Sprite* sprite[MAX_SPRITES];

	BaseScriptFilename script;

	static void TooManySprites(void)
	{
		MB_ShowError(_T("Dink only allows 100 sprites per screen. Please delete some and try again."));
	}
};