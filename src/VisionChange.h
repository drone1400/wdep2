#pragma once
#include "resource.h"

class VisionChange : public CDialog
{
public:
	VisionChange(CWnd* pParent = nullptr);

	enum { IDD = IDD_VISION };
	int		m_vision;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};