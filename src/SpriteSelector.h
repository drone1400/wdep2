#pragma once
#include "Const.h"

class Map;

class SpriteSelector
{
public:
	SpriteSelector();
	~SpriteSelector();
	int render();
	int drawGrid(int x, int y);
	void getSprite();
	void nextPage();
	void prevPage();
	void createList(Map* map);
	void resizeScreen();

	bool showFrames;
	int currentFrame, currentSequence;

private:
	int max_pics;
	int selectorScreen;
	int max_sprite;
	int sprites_per_row;
	int sprites_per_column;
	int pics_displayed;
	int spriteSelectorList[MAX_SEQUENCES];
};