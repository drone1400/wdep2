#include "StackHelper.h"

#include "../Common.h"
#include "../Globals.h"
#include "../Map.h"
#include "../SimpleLog.h"

// NOTE:
// See these posts about the whole PreventSetUnhandledExceptionFilter thing
//     http://blog.kalmbachnet.de/?postid=75
//     http://blog.kalmbach-software.de/2008/04/02/unhandled-exceptions-in-vc8-and-above-for-x86-and-x64/
//     http://blog.kalmbach-software.de/2013/05/23/improvedpreventsetunhandledexceptionfilter/


static BOOL PreventSetUnhandledExceptionFilter()
{
	HMODULE hKernel32 = LoadLibrary(_T("kernel32.dll"));
	if (hKernel32 == NULL) return FALSE;
	void *pOrgEntry = GetProcAddress(hKernel32, "SetUnhandledExceptionFilter");
	if (pOrgEntry == NULL) return FALSE;
 
#ifdef _M_IX86
	// Code for x86:
	// 33 C0                xor         eax,eax  
	// C2 04 00             ret         4 
	unsigned char szExecute[] = { 0x33, 0xC0, 0xC2, 0x04, 0x00 };
#elif _M_X64
	// 33 C0                xor         eax,eax 
	// C3                   ret  
	unsigned char szExecute[] = { 0x33, 0xC0, 0xC3 };
#else
#error "The following code only works for x86 and x64!"
#endif

	// change memory protection
	DWORD dwOldProtect = 0;
	BOOL bProt = VirtualProtect(pOrgEntry, sizeof(szExecute),PAGE_EXECUTE_READWRITE, &dwOldProtect);
	
	// write magic thingy
	SIZE_T bytesWritten = 0;
	BOOL bRet = WriteProcessMemory(GetCurrentProcess(),
	  pOrgEntry, szExecute, sizeof(szExecute), &bytesWritten);

	// restore memory protection if needed
	if ((bProt != FALSE) && (dwOldProtect != PAGE_EXECUTE_READWRITE))
	{
		DWORD dwOldProtect2 = 0;
		VirtualProtect(pOrgEntry, sizeof(szExecute), dwOldProtect, &dwOldProtect2);
	}

	return bRet;
}

class StackWalkerLogger : public StackWalker
{
protected:
	virtual void OnOutput(LPCSTR szText)
	{
		Log_Error(szText);
	}
};

static LONG WINAPI MyUnhandledExceptionFilter(_EXCEPTION_POINTERS *lpTopLevelExceptionFilter)
{
	Log_Error(_T("OH NO! WIN DINK EDIT CRASHED! STACK TRACE BELOW"));
	Log_Error(_T("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- "));

	StackWalkerLogger stackbro;
	stackbro.ShowCallstack(GetCurrentThread(), lpTopLevelExceptionFilter->ContextRecord);

	Log_Error(_T("---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- "));

	Log_Flush();

	if (current_map != nullptr)
	{
		// try to save the dmod i guess?...
		current_map->backup_dmod_old(); // backup old dmod files
		current_map->save_dmod(); // try saving anyway
	}
	
	
	FatalAppExit(0, _T("WDEP2 has crashed! The Stack Trace is logged in the latest log file in the 'logs' subfolder. Send it to drone1400 on TDN and maybe he can fix it!"));
	return EXCEPTION_CONTINUE_SEARCH;
}

static BOOL isInitializedUnhandledExceptionFilter = FALSE;

void InitializeUnhandledExceptionFilter() {

	if (isInitializedUnhandledExceptionFilter == TRUE)
	{
		Log_Info(_T("Unhandled Exception Filter is already initialized!"));
		return;
	}
	
	SetUnhandledExceptionFilter(MyUnhandledExceptionFilter);
	
	BOOL bRet = PreventSetUnhandledExceptionFilter();
	_tprintf(_T("Prevented: %d"), bRet);

	if (!bRet)
	{
		Log_Info(_T("Failed to initialize unhandled Exception Filter!"));
	} else
	{
		Log_Info(_T("Initialized Unhandled Exception Filter!"));
		isInitializedUnhandledExceptionFilter = TRUE;
	}
}