#include "stdafx.h"
#include "ScriptDialog.h"
#include "Map.h"


IMPLEMENT_DYNAMIC(CScriptDialog, CDialog)

CScriptDialog::CScriptDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(CScriptDialog::IDD, pParent)
	, m_script(_T(""))
{

}

CScriptDialog::~CScriptDialog()
{
}

void CScriptDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_Script, m_script);
	DDV_MaxChars(pDX, m_script, EightDotThreeFilename::capacity());
}


BEGIN_MESSAGE_MAP(CScriptDialog, CDialog)
	ON_BN_CLICKED(IDC_BROWSE_SCRIPT, &CScriptDialog::OnClickedBrowseScript)
END_MESSAGE_MAP()


void CScriptDialog::OnClickedBrowseScript()
{
	CFileDialog dialog(true, _T(".c"), nullptr, 0, _T("C Files (*.c)|*.c|All Files (*.*)|*.*||"));

	CString scriptdir = current_map->dmod_path;
	scriptdir += _T("story");
	dialog.m_ofn.lpstrInitialDir = (LPCTSTR)scriptdir;

	if (dialog.DoModal() == IDOK)
	{
		m_script = dialog.GetFileName();
		m_script = m_script.Left(m_script.GetLength()-2); //remove ext
		UpdateData(FALSE);
	}
}