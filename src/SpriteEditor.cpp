#include "StdAfx.h"
#include "Globals.h"
#include "Colors.h"
#include "CommonSdl.h"
#include "Map.h"
#include "sprite.h"
#include "SpriteEditor.h"
#include "Actions/SpriteHardDepthChange.h"


SpriteEditor::SpriteEditor()
{
	Reset();
}

void SpriteEditor::DrawSprite()
{
	seq = current_map->mouseSprite->sequence;
	frame = current_map->mouseSprite->frame;
	width = current_map->sequence[seq]->frame_width[frame-1] * zoom / 100;
	height = current_map->sequence[seq]->frame_height[frame-1] * zoom / 100;
	x_offset = (current_map->window_width - width) / 2;
	y_offset = (current_map->window_height - height) / 2;

	RECT editor_bounds;
	editor_bounds.top = y_offset;
	editor_bounds.bottom = y_offset + height;
	editor_bounds.left = x_offset;
	editor_bounds.right = x_offset + width;

	drawFilledBox(editor_bounds, COLOR_WHITE);
	drawTexture(current_map->sequence[seq]->frame_image[frame-1], nullptr, &editor_bounds);
}

void SpriteEditor::DrawHardness()
{
	if (!depth_x && !depth_y)
	{
		current_map->sequence[seq]->getCenter(frame-1, depth_x, depth_y);
	}
    
	if (x1 || x2 || y1 || y2)
	{
		hardness_bounds.left = (x1 + depth_x) * zoom / 100;
		hardness_bounds.right = (x2 + depth_x) * zoom / 100;
		hardness_bounds.top = (y1 + depth_y) * zoom / 100;
		hardness_bounds.bottom = (y2 + depth_y) * zoom / 100;
	}
	else
	{
		if (current_map->sequence[seq]->frame_info[frame-1])
		{
			//use frame hardness
			depth_y = current_map->sequence[seq]->frame_info[frame-1]->center_y;
			depth_x = current_map->sequence[seq]->frame_info[frame-1]->center_x;
			x1 = current_map->sequence[seq]->frame_info[frame-1]->left_boundary * zoom / 100;
			x2 = current_map->sequence[seq]->frame_info[frame-1]->right_boundary * zoom / 100;
			y1 = current_map->sequence[seq]->frame_info[frame-1]->top_boundary * zoom / 100;
			y2 = current_map->sequence[seq]->frame_info[frame-1]->bottom_boundary * zoom / 100;
		}
		else if (current_map->sequence[seq]->left_boundary || 
			current_map->sequence[seq]->right_boundary || 
			current_map->sequence[seq]->top_boundary)
		{
			//use sequence hardness
			depth_y = current_map->sequence[seq]->center_y;
			depth_x = current_map->sequence[seq]->center_x;
			x1 = current_map->sequence[seq]->left_boundary * zoom / 100;
			x2 = current_map->sequence[seq]->right_boundary * zoom / 100;
			y1 = current_map->sequence[seq]->top_boundary * zoom / 100;
			y2= current_map->sequence[seq]->bottom_boundary * zoom / 100;
		}
		else
		{
			//use default hardness
			x1 = -current_map->sequence[seq]->frame_width[frame-1]  / 4  * zoom / 100;
			y1 = -current_map->sequence[seq]->frame_height[frame-1] / 10 * zoom / 100;
			x2 =  current_map->sequence[seq]->frame_width[frame-1]  / 4  * zoom / 100;
			y2 =  current_map->sequence[seq]->frame_height[frame-1] / 10 * zoom / 100;
		}
	}

	int x = x_offset + depth_x * zoom / 100;
	int y = y_offset + depth_y * zoom / 100;

	hardness_bounds.left += x_offset;
	hardness_bounds.right += x_offset;
	hardness_bounds.top += y_offset;
	hardness_bounds.bottom += y_offset;

	RECT depth_dot = {x-1, y-1, x+2, y+2};

    SDL_BlendMode blendModeOld;
    SDL_GetRenderDrawBlendMode(sdl_renderer, &blendModeOld);

    // switch to SDL_BLENDMODE_BLEND for transparency
    SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);
    
	//TODO: make these colors named?
	drawFilledBox(hardness_bounds, SDL_Color{ 128, 128, 128, hardness_alpha_sprite });
	drawFilledBox(depth_dot, SDL_Color{ 255, 255, 0, hardness_alpha_sprite });

    SDL_SetRenderDrawBlendMode(sdl_renderer, blendModeOld);
}

void SpriteEditor::Save()
{
	if (edited)
	{
		edited = false;
		
		current_map->undo_buffer->PushAndDo<actions::SpriteHardDepthChange>(seq, frame-1, depth_x, depth_y, x1, y1, x2, y2);
	}

	Reset();	
}

void SpriteEditor::SetHardness(int left, int top, int right, int bottom)
{
	//set it edited, and make sure the bounds aren't backwards
	edited = true;
	if (right < left)	//then swap them
		std::swap(left, right);

	if (bottom < top)	//then swap them
		std::swap(top, bottom);

	x1 = ((left - x_offset  ) * 100 / zoom - depth_x);
	x2 = ((right - x_offset ) * 100 / zoom - depth_x);
	y1 = ((top - y_offset   ) * 100 / zoom - depth_y);
	y2 = ((bottom - y_offset) * 100 / zoom - depth_y);
}

void SpriteEditor::SetDepth(int x, int y)
{
	//set it to edited, and do the math of figuring out the depth
	edited = true;

	int temp = depth_x - (x - x_offset) * 100 / zoom;

	x1 += temp;
	x2 += temp;

	temp = depth_y - (y - y_offset) * 100 / zoom;

	y2 += temp;
	y1 += temp;

	depth_x = (x - x_offset) * 100 / zoom;
	depth_y = (y - y_offset) * 100 / zoom;
}

void SpriteEditor::DrawEditor(bool withHardness)	//RW: draws a background, then the sprite, then the hardness and depth dot if needed.
{
	drawClearAll(color_map_screen_grid);

	spriteeditor->DrawSprite();

	if (withHardness)
		spriteeditor->DrawHardness();
}

void SpriteEditor::Reset()
{
	memset(&hardness_bounds, 0, sizeof(RECT));
	x1 = 0;
	x2 = 0;
	y1 = 0;
	y2 = 0;
	depth_x = 0;
	depth_y = 0;
	edited = false;
	zoom = 100;
}