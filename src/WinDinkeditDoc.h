#pragma once

class CWinDinkeditDoc : public CDocument
{
protected: // create from serialization only
	CWinDinkeditDoc();
	DECLARE_DYNCREATE(CWinDinkeditDoc)

public:
	BOOL OnNewDocument() override;
	BOOL OnOpenDocument(LPCTSTR lpszPathName) override;
	void OnCloseDocument() override;
	BOOL CanCloseFrame(CFrameWnd* pFrame) override;
protected:
	BOOL SaveModified() override;
	void SetModifiedFlag(BOOL bModified) override;

public:
	virtual ~CWinDinkeditDoc();
#ifdef _DEBUG
	void AssertValid() const override;
	void Dump(CDumpContext& dc) const override;
#endif

protected:
	DECLARE_MESSAGE_MAP()
};