#pragma once
#include "resource.h"

class OpenDmod : public CDialog
{
// Construction
public:
	OpenDmod(CWnd* pParent = nullptr);   // standard constructor
	void setDmodNamePtr(TCHAR *buffer);

	enum { IDD = IDD_OPEN_DMOD };	

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support
	void OnOK() override;
	DECLARE_MESSAGE_MAP()
private:
	TCHAR *dmod_selected;
public:
	virtual BOOL OnInitDialog() override;
	CListCtrl m_dmod_list;
	afx_msg void OnNMDblclkList2(NMHDR *pNMHDR, LRESULT *pResult);
};
