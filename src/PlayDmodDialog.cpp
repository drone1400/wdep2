#include "stdafx.h"
#include "PlayDmodDialog.h"
#include "Common.h"
#include "Globals.h"
#include "Map.h"
#include "PathUtil.h"
#include "SimpleLog.h"


CPlayDmodDialog::CPlayDmodDialog(CWnd* pParent /*=nullptr*/) : CDialog(CPlayDmodDialog::IDD, pParent)	
{
	m_check_windowed = FALSE;
	m_check_debug = FALSE;
	m_check_sound = FALSE;
	m_check_truecolor = FALSE;
	m_check_joystick = FALSE;
	m_check_pathquot = FALSE;
}

void CPlayDmodDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_PLAYDMOD_WINDOWED, m_check_windowed);
	DDX_Check(pDX, IDC_PLAYDMOD_DEBUG, m_check_debug);
	DDX_Check(pDX, IDC_PLAYDMOD_SOUND, m_check_sound);
	DDX_Check(pDX, IDC_PLAYDMOD_TRUECOLOR, m_check_truecolor);
	DDX_Check(pDX, IDC_PLAYDMOD_JOYSTICK, m_check_joystick);
	DDX_Check(pDX, IDC_PLAYDMOD_PATHQUOT, m_check_pathquot);
}

BEGIN_MESSAGE_MAP(CPlayDmodDialog, CDialog)
END_MESSAGE_MAP()


void CPlayDmodDialog::OnOK()
{
	Log_Info(_T("PLAY DMOD - Attempting to play DMOD:"));
	Log_Info(loaded_dmod_path, LOG_FORMAT_CONTINUE);
	Log_Info(dink_exe_path, LOG_FORMAT_CONTINUE);
	
	
	CString params;
	params.SetString(_T(" -nomovie"));

	UpdateData(true);

	if (m_check_windowed) params.Append(_T(" -window"));
	if (m_check_debug) params.Append(_T(" -debug"));
	if (!m_check_sound) params.Append( _T(" -nosound"));
	if (m_check_truecolor) params.Append( _T(" -truecolor"));
	if (!m_check_joystick) params.Append( _T(" -nojoy"));
	params.Append( _T(" -game "));

	CString exeBase;
	Path_ConcatIntoAndNormalize(exeBase, dink_exe_path, _T(".."));
	
	CString muhNewPath;
	if (Path_MakeRelative(exeBase, loaded_dmod_path, muhNewPath))
	{
		Log_Info(_T("PLAY DMOD - DMOD path was made relative:"));
		Log_Info(muhNewPath, LOG_FORMAT_CONTINUE);
	}
	if (m_check_pathquot) params.Append(_T("\""));
	params.Append( muhNewPath);
	if (m_check_pathquot) params.Append(_T("\""));

	Log_Info(_T("PLAY DMOD - Final game launch parameters are:"));
	Log_Info(params, LOG_FORMAT_CONTINUE);
	
	try
	{
		current_map->save_dmod();
	}
	catch(...)
	{
		MB_ShowError(_T("Failed to save the dmod."));
		return;
	}

	HINSTANCE runresult = ShellExecute(nullptr, _T("open"), dink_exe_path, params, nullptr, SW_SHOW);

	if (int(runresult) < 32)
	{
		CString dmodPlayError;
		dmodPlayError.Format(_T("The DMOD could not be run.\n\nCommand was:\n%s%s\n\nError Code: %i"), dink_exe_path, params, int(runresult));
		MB_ShowError(dmodPlayError);
	}

	//close dialog
	//CDialog::OnOK();
}

//RW: note the byref parameters.
void CPlayDmodDialog::GetCheckBoxStatus(BOOL& truecolor, BOOL& windowed, BOOL& sound, BOOL& joystick, BOOL& debug, BOOL& quot)
{
	truecolor = m_check_truecolor;
	windowed = m_check_windowed;
	sound = m_check_sound;
	joystick = m_check_joystick;
	debug = m_check_debug;
	quot = m_check_pathquot;
}

void CPlayDmodDialog::SetCheckBoxStatus(BOOL truecolor, BOOL windowed, BOOL sound, BOOL joystick, BOOL debug, BOOL quot)
{
	m_check_truecolor = truecolor;
	m_check_windowed = windowed;
	m_check_sound = sound;
	m_check_joystick = joystick;
	m_check_debug = debug;
	m_check_pathquot = quot;
}