#pragma once

//for functions that want individual values
#define EXPAND_SDL_COLOR(color)  color.r, color.g, color.b, color.a

void setColors();

extern SDL_Color color_tile_grid;
extern SDL_Color color_mouse_box;
extern SDL_Color color_blank_screen_fill;
extern SDL_Color color_tile_sel_border;
extern SDL_Color color_tile_sel_error;
extern SDL_Color color_tile_sel_error_back;
extern SDL_Color color_tile_grid_in_boundary;
extern SDL_Color color_tile_grid_out_boundary;
extern SDL_Color color_map_screen_grid;
extern SDL_Color color_minimap_screen_selected;
extern SDL_Color color_screen_selected_box;
extern SDL_Color color_sprite_info_text;
extern SDL_Color color_sprite_hover_box;
extern SDL_Color color_sprite_hover_box_raw;
extern SDL_Color color_match_duplicate_hover_box;
extern SDL_Color color_sprite_selected_box;
extern SDL_Color color_sprite_hover_selected_box;
extern SDL_Color color_rect_select_normal_box;
extern SDL_Color color_rect_select_exclusive_box;

extern SDL_Color color_hard_edit_cursor;
extern SDL_Color color_hard_edit_border;
extern SDL_Color color_hard_edit_notex;

extern SDL_Color color_sprite_fallback_back;
extern SDL_Color color_sprite_fallback_fore;

extern SDL_Color color_sprite_hardness;
extern SDL_Color color_warp_sprite_hardness;
extern SDL_Color color_normal_tile_hardness;
extern SDL_Color color_low_tile_hardness;
extern SDL_Color color_other_tile_hardness;

extern SDL_Color color_polygon_fill_hardness;
