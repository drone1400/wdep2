#include "stdafx.h"
#include "NewDmodDialog.h"
#include "Common.h"
#include "CommonFile.h"


NewDmodDialog::NewDmodDialog(CWnd* pParent /*=nullptr*/) : CDialog(NewDmodDialog::IDD, pParent)
{
	m_description = _T("");
	m_directory = _T("");
	m_dmod_name = _T("");
	m_author = _T("");
	m_email_website = _T("");
}

void NewDmodDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_DMOD_DESCRIPTION, m_description);
	DDX_Text(pDX, IDC_DMOD_DIRECTORY_NAME, m_directory);
	DDX_Text(pDX, IDC_DMOD_TITLE, m_dmod_name);
	DDX_Text(pDX, IDC_AUTHOR, m_author);
	DDX_Text(pDX, IDC_EMAIL_WEBSITE, m_email_website);
	DDV_MaxChars(pDX, m_directory, 20);
}

void NewDmodDialog::setDmodNamePtr(TCHAR *buffer)
{
	dmod_name = buffer;
}


BEGIN_MESSAGE_MAP(NewDmodDialog, CDialog)
END_MESSAGE_MAP()


void NewDmodDialog::OnOK() 
{
	UpdateData();
	
	if (m_directory.GetLength() == 0)
	{
		MB_ShowError(_T("You must specify a dmod directory name."));
		return;
	}
	
	if (CreateNewDmod(m_directory, m_dmod_name, m_author, m_email_website, m_description) == false)
	{
		CDialog::OnCancel();
	}
	else
	{
		_tcscpy(dmod_name, m_directory.GetBuffer(0));
		CDialog::OnOK();
	}
}


BOOL NewDmodDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_DMOD_DIRECTORY_NAME)->SetFocus();

	return FALSE;	
}