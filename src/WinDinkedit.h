// WinDinkedit.h : main header file for the WINDINKEDIT application
#pragma once
#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


class CWinDinkeditApp : public CWinAppEx
{
public:
	CWinDinkeditApp();

	BOOL InitInstance() override;
	CDocument* OpenDocumentFile(LPCTSTR lpszFileName) override;
	virtual void OnFileOpen();

	afx_msg void OnAppAbout();
	afx_msg void OnNewDmod();
	
	DECLARE_MESSAGE_MAP()
	int ExitInstance() override;
};