#include "stdafx.h"
#include "ImportScreen.h"
#include "Globals.h"
#include "Map.h"
#include "ImportMap.h"
#include "Common.h"
#include "CommonFile.h"
#include "PathUtil.h"


ImportScreen::ImportScreen(CWnd* pParent /*=nullptr*/) : CDialog(ImportScreen::IDD, pParent)
{
}

void ImportScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);	
	DDX_Control(pDX, IDC_DMOD_IMPORT_LIST, m_dmod_list);	
}

BEGIN_MESSAGE_MAP(ImportScreen, CDialog)		
	ON_NOTIFY(NM_DBLCLK, IDC_DMOD_IMPORT_LIST, &ImportScreen::OnNMDblclkDmodImportList)
END_MESSAGE_MAP()


BOOL ImportScreen::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_dmod_list.SetFocus();
	m_dmod_list.SetFocus();	
	m_dmod_list.SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT);

	CRect listrect;
	m_dmod_list.GetWindowRect(&listrect);
	m_dmod_list.InsertColumn(0, _T("DMod folder"), LVCFMT_LEFT, 100, 0);
	m_dmod_list.InsertColumn(1, _T("DMod title"), LVCFMT_CENTER, 100, 1);

	PopulateDmodClistCtrl(&m_dmod_list);

	if ((m_dmod_list.GetStyle() & WS_VSCROLL) != 0)
		m_dmod_list.SetColumnWidth(1, listrect.Width()-105-GetSystemMetrics(SM_CXVSCROLL));
	else
		m_dmod_list.SetColumnWidth(1, listrect.Width()-105);

	if (current_map->import_dmod_path.GetLength() != 0)	
	{
		CString dmodFolderName;
		if (Path_GetLastToken(current_map->import_dmod_path, dmodFolderName))
		{
			LVFINDINFO listiteminfo;
			listiteminfo.flags = LVFI_PARTIAL | LVFI_STRING;
			listiteminfo.psz = (LPCTSTR) dmodFolderName;
			int selIndex = m_dmod_list.FindItem(&listiteminfo);

			LVITEM lvi;		
			lvi.mask	= LVIF_STATE;
			lvi.state	= LVIS_SELECTED | LVIS_FOCUSED;
			lvi.stateMask   = LVIS_SELECTED | LVIS_FOCUSED;	
			m_dmod_list.SetItemState(selIndex, &lvi);
		}
	}
	
	return FALSE;	//RW: return false because we set a focus.      
}

// load the dmod import screen
void ImportScreen::OnOK()
{
	POSITION pos;

	// return if an invalid dmod name is selected
	pos = m_dmod_list.GetFirstSelectedItemPosition();
	if (pos == nullptr)
	{
		MB_ShowError(_T("You must select a dmod from the list!"));
		return;
	}

	CString dmod_name_entered;
	dmod_name_entered = m_dmod_list.GetItemText(m_dmod_list.GetNextSelectedItem(pos), 0);

	ConcatInto(current_map->import_dmod_path, dink_dmods_path, _T("\\"), dmod_name_entered, _T("\\"));
	
	if (current_map->import_map == nullptr)
	{
		current_map->import_map = new ImportMap();
		if (current_map->import_map->load_failed)
		{
			delete current_map->import_map;
			current_map->import_map = nullptr;
			MB_ShowError(_T("Failed to open the selected dmod to import from!"));
		}
		else
			current_map->editor_state = EditorState::ScreenImporter;
	}

	CDialog::OnOK();
}


void ImportScreen::OnNMDblclkDmodImportList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnOK();
	*pResult = 0;
}