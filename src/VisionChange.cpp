#include "stdafx.h"
#include "VisionChange.h"


VisionChange::VisionChange(CWnd* pParent /*=nullptr*/) : CDialog(VisionChange::IDD, pParent)
{
	m_vision = 0;
}

void VisionChange::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_Vision, m_vision);
}

BEGIN_MESSAGE_MAP(VisionChange, CDialog)
END_MESSAGE_MAP()