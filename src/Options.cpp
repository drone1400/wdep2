#include "stdafx.h"
#include "Options.h"
#include "Globals.h"


Options::Options(CWnd* pParent /*=nullptr*/) : CDialog(Options::IDD, pParent)
	, m_show_progress(show_minimap_progress)
	, m_screen_gap(screen_gap)
	, m_max_undos(max_undo_level)
	, m_autosave_time(auto_save_time)
	, m_dink_ini(optimize_dink_ini)
	, m_brush_size(tile_brush_size)
	, m_fast_minimap(fast_minimap_update)
	, m_hover_sprite_info(hover_sprite_info)
	, m_hover_sprite_hardness(hover_sprite_hardness)
	, m_help_text(help_text)
	, m_offset(snapto_offset)
	, m_autoscript(autoscript)
	, m_minimap_detail(2)
	, m_sprite_preview_size(sprite_selector_bmp_width / 20)		//RW: sprite preview size is sprite_selector_bmp_width / 20 - we want increments of 20.
    , m_hardtile_transparency(hardness_alpha_tile)
    , m_hardsprite_transparency(hardness_alpha_sprite)
	, m_invert_vertical_wheel(mouse_wheel_updown_invert)
	, m_invert_shiftvertical_wheel(mouse_wheel_updown_shift_invert)
	, m_invert_horizontal_wheel(mouse_wheel_leftright_invert)
	, m_spritestamp_hard_nohit(use_hard_nohit_default_stamp)
	, m_spriteplace_hard_nohit(use_hard_nohit_default_place)
    , m_statusbar_sidebar_offset(use_statusbar_sidebar_offset)
{
	for (int i=0; i<=6; i++)
		if (minimap_detail_levels[i] >= square_width)
		{
			m_minimap_detail = i;
			break;
		}
}

BOOL Options::OnInitDialog()
{
	CDialog::OnInitDialog();

	CSliderCtrl* slider = (CSliderCtrl*) this->GetDlgItem(IDC_SLIDERMMDETAIL);
	slider->SetRange(1, 6);
	slider->SetPos(1);	//RW: I don't know why this is needed, but without this the initial position of the slider is crap.	
	slider->SetPos(m_minimap_detail);
	
	slider = (CSliderCtrl*) this->GetDlgItem(IDC_SLIDERSSPREVIEWSIZE);
	slider->SetRange(1,10);
	slider->SetPos(1);	//RW: I don't know why this is needed, but without this the initial position of the slider is crap.
	slider->SetPos(m_sprite_preview_size);

    slider = (CSliderCtrl*) this->GetDlgItem(IDC_SLIDER_HARDTILE_TRANSPARENCY);
    slider->SetRange(HARDTILE_TRANSPARENCY_MIN, HARDTILE_TRANSPARENCY_MAX);
    slider->SetPos(1);	//RW: I don't know why this is needed, but without this the initial position of the slider is crap.	
    slider->SetPos(m_hardtile_transparency);

    slider = (CSliderCtrl*) this->GetDlgItem(IDC_SLIDER_HARDSPRITE_TRANSPARENCY);
    slider->SetRange(HARDSPRITE_TRANSPARENCY_MIN, HARDSPRITE_TRANSPARENCY_MAX);
    slider->SetPos(1);	//RW: I don't know why this is needed, but without this the initial position of the slider is crap.	
    slider->SetPos(m_hardsprite_transparency);

	return TRUE;
}

void Options::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_SCREEN_GAP, m_screen_gap);
	DDV_MinMaxUInt(pDX, m_screen_gap, 0, 20);
	DDX_Text(pDX, IDC_MAX_UNDOS, m_max_undos);
	DDV_MinMaxUInt(pDX, m_max_undos, 0, 3000);
	DDX_Text(pDX, IDC_AUTO_SAVE, m_autosave_time);
	DDV_MinMaxUInt(pDX, m_autosave_time, 0, 20);
	DDX_Text(pDX, IDC_SNAPTO_OFFSET, m_offset);
	DDV_MinMaxUInt(pDX, m_offset, 0, 512);
	DDX_Check(pDX, IDC_SHOW_PROGRESS, m_show_progress);
	DDX_Check(pDX, IDC_DINK_INI, m_dink_ini);
	DDX_Text(pDX, IDC_BRUSH_SIZE, m_brush_size);
	DDV_MinMaxInt(pDX, m_brush_size, 1, 25);
	DDX_Check(pDX, IDC_FAST_MINIMAP, m_fast_minimap);
	DDX_Check(pDX, IDC_HOVER_SPRITE_INFO, m_hover_sprite_info);
	DDX_Check(pDX, IDC_CHECK2, m_hover_sprite_hardness);
	DDX_Check(pDX, IDC_HELP_TEXT, m_help_text);
	DDX_Check(pDX, IDC_AUTOSCRIPT, m_autoscript);
	DDX_Slider(pDX, IDC_SLIDERMMDETAIL, m_minimap_detail);
	DDV_MinMaxInt(pDX, m_minimap_detail, 1, 6);
	DDX_Slider(pDX, IDC_SLIDERSSPREVIEWSIZE, m_sprite_preview_size);
	DDV_MinMaxInt(pDX, m_sprite_preview_size, 1, 10);
    DDX_Slider(pDX, IDC_SLIDER_HARDTILE_TRANSPARENCY, m_hardtile_transparency);
    DDV_MinMaxInt(pDX, m_hardtile_transparency, HARDTILE_TRANSPARENCY_MIN, HARDTILE_TRANSPARENCY_MAX);
    DDX_Slider(pDX, IDC_SLIDER_HARDSPRITE_TRANSPARENCY, m_hardsprite_transparency);
    DDV_MinMaxInt(pDX, m_hardsprite_transparency, HARDSPRITE_TRANSPARENCY_MIN, HARDSPRITE_TRANSPARENCY_MAX);
	DDX_Check(pDX, IDC_MOUSE_VERT_INVERT, m_invert_vertical_wheel);
	DDX_Check(pDX, IDC_MOUSE_SHIFTVERT_INVERT, m_invert_shiftvertical_wheel);
	DDX_Check(pDX, IDC_MOUSE_HORIZ_INVERT, m_invert_horizontal_wheel);
	DDX_Check(pDX, IDC_PLACE_HARD_NOHIT, m_spriteplace_hard_nohit);
	DDX_Check(pDX, IDC_STAMP_HARD_NOHIT, m_spritestamp_hard_nohit);
	DDX_Check(pDX, IDC_STATUSBAR_SIDEBAROFFSET, m_statusbar_sidebar_offset);
}


BEGIN_MESSAGE_MAP(Options, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_DEFAULTS, &Options::OnBnClickedButtonDefaults)
END_MESSAGE_MAP()


void Options::OnBnClickedButtonDefaults()
{
	m_autosave_time = 0;
	m_autoscript = TRUE;
	m_brush_size = 1;
	m_dink_ini = FALSE;
	m_fast_minimap = TRUE;
	m_help_text = TRUE;
	m_hover_sprite_hardness = FALSE;
	m_hover_sprite_info = FALSE;
	m_invert_horizontal_wheel = FALSE;
	m_invert_shiftvertical_wheel = FALSE;
	m_invert_vertical_wheel = FALSE;
	m_max_undos = DEFAULT_MAX_UNDO_LEVEL;
	m_minimap_detail = 2;
	m_offset = 16;
	m_screen_gap = DEFAULT_SCREEN_GAP;
	m_show_progress = TRUE;
	m_sprite_preview_size = 2;
	m_spritestamp_hard_nohit = TRUE;
	m_spriteplace_hard_nohit = FALSE;
    m_hardtile_transparency = 255;
    m_hardsprite_transparency = 255;
    m_statusbar_sidebar_offset = FALSE;

	UpdateData(FALSE);
}