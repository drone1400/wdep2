// WinDinkeditDoc.cpp : implementation of the CWinDinkeditDoc class
#include "stdafx.h"
#include "WinDinkeditDoc.h"
#include "Common.h"
#include "PathUtil.h"
#include "Globals.h"
#include "Map.h"
#include "MainFrm.h"
#include "SimpleLog.h"
#include "WinDinkeditView.h"


IMPLEMENT_DYNCREATE(CWinDinkeditDoc, CDocument)

BEGIN_MESSAGE_MAP(CWinDinkeditDoc, CDocument)
END_MESSAGE_MAP()


CWinDinkeditDoc::CWinDinkeditDoc()
{
	// TODO: add one-time construction code here
	SetModifiedFlag(false);
}

CWinDinkeditDoc::~CWinDinkeditDoc()
{
}

BOOL CWinDinkeditDoc::OnNewDocument()
{
	Log_Info(_T("CDocument - OnNewDocument"));
	
	if (!CDocument::OnNewDocument())
		return FALSE;

	SetTitle(_T("Untitled"));
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


#ifdef _DEBUG
void CWinDinkeditDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CWinDinkeditDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


BOOL CWinDinkeditDoc::OnOpenDocument(LPCTSTR dmod_name) 
{
	Log_Info(_T("CDocument - OnOpenDocument"));
	Log_Info(dmod_name, LOG_FORMAT_CONTINUE);
	
	CString dmod_path;	
	dmod_path.SetString(dmod_name);

	//check if hard.dat is writable... for example, if the dmods are in an admin-only location and we're not running as admin, then we should fail here with a helpful error message, instead of failing when the user is trying to save their work
	CString hard_dat;
	ConcatInto(hard_dat, dmod_path, _T("\\hard.dat"));
	if (!FileReadable(hard_dat) || !FileWritable(hard_dat))
	{
		MB_ShowError(_T("The dmod you are trying to open is not writable.\n\nIf your dmods are installed to a restricted location, try running WinDinkedit as administrator."));
		return FALSE;
	}

	// check if a map exists already and kill it if one does
	if (current_map)
	{
		delete current_map;
		current_map = nullptr;
	}

	// dmod being opened has not been modified yet
	SetModifiedFlag(FALSE);

	// pass filename to dink game
	Map* new_map = nullptr;
	try 
	{
		new_map = new Map(dmod_path);
		new_map->finishLoading();
		current_map = new_map;
		mainWnd->setStatusText(_T("Dmod loaded successfully."));
	}
	catch (...)
	{		
		mainWnd->setStatusText(_T("Dmod loading failed."));
		mainWnd->GetRightPane()->Invalidate(TRUE);
		
		MB_ShowError(_T("Dmod loading failed."));

		delete new_map;
		return FALSE;
	}

	// set loaded dmod path for playtesting
	loaded_dmod_path.SetString(dmod_path);
	
	return TRUE;
}

void CWinDinkeditDoc::OnCloseDocument() 
{
	Log_Info(_T("CDocument - OnCloseDocument"));
	
	if (current_map)
	{
		delete current_map;
		current_map = nullptr;
	}
	
	CDocument::OnCloseDocument();
}

BOOL CWinDinkeditDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	return SaveModified();
}

BOOL CWinDinkeditDoc::SaveModified() 
{
	Log_Info(_T("CDocument - SaveModified"));
	
	if (current_map != nullptr && IsModified())
	{
		int res = MB_ShowGeneric(_T("The current DMod has unsaved modifications. Save them?"),
			_T("WDEP2 - Save DMOD?"), MB_ICONQUESTION | MB_YESNOCANCEL);
		if (res == IDCANCEL)
			return FALSE;
		if (res == IDNO)
			return TRUE;
		if (res == IDYES)
			try
			{
				current_map->save_dmod();
				return TRUE;
			}
			catch(...)
			{
				int errRes = MB_ShowGeneric(_T("Failed to save dmod.\nWarning! Data loss or corruption may occur if forcefully closed.\n\nClose anyway?"),
					_T("WDEP2 - Error"), MB_YESNO | MB_ICONERROR);
				return errRes == IDYES;
			}
	}

	return TRUE;  //the operation may continue (FALSE=do not close!).
}

void CWinDinkeditDoc::SetModifiedFlag(BOOL bModified)
{
	CDocument::SetModifiedFlag(bModified);

	CString const& title = GetTitle();
	if (bModified)
	{
		if (!title.IsEmpty() && title[0] != _T('*'))
		{
			CString new_title;
			ConcatInto(new_title, _T("*"), title);
			SetTitle(new_title);
		}
	}
	else
	{
		if (!title.IsEmpty() && title[0] == _T('*'))
			SetTitle(title.Mid(1));
	}
}
