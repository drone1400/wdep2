#pragma once
#include "Const.h"

class Tile
{
public:
	Tile(int file_source);
	~Tile();

	int loadSurface();

	bool render(RECT dest_rect, int tileX, int tileY, RECT* cliprect = nullptr);
	bool renderAll(int x, int y, int width, int height);
	
	int width, height;  // size of bitmap
	int file_location;

	SDL_Texture* image;
	
	// stores the tile hardness id that is associated with each square
	int tile_hardness[TILESHEET_TILE_HEIGHT][TILESHEET_TILE_WIDTH];
};