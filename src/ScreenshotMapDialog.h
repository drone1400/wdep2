#pragma once
#include "resource.h"
#include "Structs.h"

class CScreenshotMapDialog : public CDialog
{
public:
	CScreenshotMapDialog(CWnd* pParent);   // standard constructor
	
	enum { IDD = IDD_SCREENSHOTMAP_DIALOG };
    BOOL    m_renderInvisible;
    BOOL    m_renderIndoor;
    CString m_sequenceFilters;
	
public:
	void GetData(SpriteRenderOptions& options);
	void SetData(const SpriteRenderOptions& options);

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

protected:
	void OnOK() override;

	DECLARE_MESSAGE_MAP()
};
