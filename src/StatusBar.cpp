#include "StdAfx.h"
#include "StatusBar.h"


IMPLEMENT_DYNAMIC(CProgStatusBar, CStatusBar)
BEGIN_MESSAGE_MAP(CProgStatusBar, CStatusBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


CProgStatusBar::CProgStatusBar()
{
}

CProgStatusBar::~CProgStatusBar()
{
}

// Status bar created: create progress bar too.
int CProgStatusBar::OnCreate(LPCREATESTRUCT lpcs)
{
	lpcs->style |= WS_CLIPCHILDREN;
	VERIFY(CStatusBar::OnCreate(lpcs)==0);
	VERIFY(m_wndProgBar.Create(WS_CHILD, CRect(), this, 1));
	m_wndProgBar.SetRange(0,100);
	return 0;
}

// Status bar was sized: adjust size of progress bar to same as first pane (ready message).
// Note that the progress bar may or may not be visible.
void CProgStatusBar::OnSize(UINT nType, int cx, int cy)
{
	CStatusBar::OnSize(nType, cx, cy); // call base class
	CRect rc;								  // rectangle 
	GetItemRect(0, &rc);					  // item 0 = first pane, "ready" message
	m_wndProgBar.MoveWindow(&rc,FALSE);// move progress bar
}

// Set progress bar position. pct is an integer from 0 to 100:
//  0 = hide progress bar and display ready message (done);
// >0 = (assemed 0-100) set progress bar position.
// You should call this from your main frame to update progress. (See Mainfrm.cpp)
void CProgStatusBar::OnProgress(UINT pct)
{
	CProgressCtrl& pc = m_wndProgBar;
	DWORD dwOldStyle = pc.GetStyle();
	DWORD dwNewStyle = dwOldStyle;
	if (pct > 0)
		dwNewStyle |= WS_VISIBLE;
	else
		dwNewStyle &= ~WS_VISIBLE;

	if (dwNewStyle != dwOldStyle) 
	{
		// change state of hide/show
		SetWindowText(nullptr);								// clear old text
		SetWindowLong(pc.m_hWnd, GWL_STYLE, dwNewStyle);	// change style
	}

	// set progress bar position
	pc.SetPos(pct);
}

void CProgStatusBar::SetRange(short min, short max)
{
	m_wndProgBar.SetRange(min,max);
}