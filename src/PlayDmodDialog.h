#pragma once
#include "resource.h"

class CPlayDmodDialog : public CDialog
{
public:
	CPlayDmodDialog(CWnd* pParent);   // standard constructor
	
	enum { IDD = IDD_PLAYDMOD_DIALOG };
	BOOL	m_check_windowed;
	BOOL	m_check_debug;
	BOOL	m_check_sound;
	BOOL	m_check_truecolor;
	BOOL	m_check_joystick;
	BOOL	m_check_pathquot;
	
public:
	void GetCheckBoxStatus(BOOL& truecolor, BOOL& windowed, BOOL& sound, BOOL& joystick, BOOL& debug, BOOL& quot);	//RW: this gets the checkbox status in one simple call.
	void SetCheckBoxStatus(BOOL truecolor, BOOL windowed, BOOL sound, BOOL joystick, BOOL debug, BOOL quot);	//RW: this sets the checkbox status in one simple call.

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

protected:
	void OnOK() override;

	DECLARE_MESSAGE_MAP()
};