#include "stdafx.h"
#include "SpriteLibraryEntry.h"


SpriteLibraryEntry::SpriteLibraryEntry(CWnd* pParent /*=nullptr*/) : CDialog(SpriteLibraryEntry::IDD, pParent)
{
	m_sprite_name = _T("");
	DialogTitle = nullptr;
}

void SpriteLibraryEntry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_Name, m_sprite_name);
	DDV_MaxChars(pDX, m_sprite_name, 19);
}

BEGIN_MESSAGE_MAP(SpriteLibraryEntry, CDialog)
END_MESSAGE_MAP()


BOOL SpriteLibraryEntry::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (DialogTitle != nullptr)
		SetWindowText(DialogTitle);

	return TRUE;
}