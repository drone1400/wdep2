#pragma once
#include "resource.h"


class CBrainDialog : public CDialog
{
	DECLARE_DYNAMIC(CBrainDialog)

public:
	CBrainDialog(int start_brain = 0, CWnd* pParent = nullptr);   // standard constructor
	virtual ~CBrainDialog();

// Dialog Data
	enum { IDD = IDD_BRAIN_DIALOG };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	afx_msg void OnCbnSelchangeBrain();
	DECLARE_MESSAGE_MAP()

	CComboBox m_brainDropdown;
	CEdit m_brainTextbox;

	void setBrain(int brain);
public:
	int m_brain;
	BOOL OnInitDialog() override;
};