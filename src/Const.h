#pragma once

//use this by by pass MFC key codes.
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

#define HARDTILE_TRANSPARENCY_MIN   64
#define HARDTILE_TRANSPARENCY_MAX   255

#define HARDSPRITE_TRANSPARENCY_MIN   64
#define HARDSPRITE_TRANSPARENCY_MAX   255

#define SPRITE_FALLBACK_SQUARE  4
#define SPRITE_FALLBACK_WIDTH  32
#define SPRITE_FALLBACK_HEIGHT 32

// defined by the dink engine and can't be changed...
#define SCREEN_WIDTH		600		// width in pixels, not memory size
#define SCREEN_HEIGHT		400

#define TILESHEET_WIDTH		600
#define TILESHEET_HEIGHT	550

// NOTE: max sequences is something like 1300 in YeOldeDink i think?
// changing this to 2000 for now because it probably won't break anything... i think...
#define MAX_SEQUENCES_OLD	1000
#define MAX_SEQUENCES		2000
// NOTE: changed max frames to 99, this should work under DinkHD but will not work with older Dink versions...
#define MAX_FRAMES_OLD		50
#define MAX_FRAMES			99
#define MAX_FRAME_INFOS		500
#define NUM_HARD_TILES		800

#define NUM_SCREENS			768
#define MAP_COLUMNS			32
#define MAP_ROWS			24

#define SCREEN_TILE_WIDTH	12
#define SCREEN_TILE_HEIGHT	8

#define TILESHEET_TILE_WIDTH	12
#define TILESHEET_TILE_HEIGHT	11
#define TILESHEET_ID_MULTIPLIER	128

#define MAX_TILE_BMPS		41

#define TILEWIDTH			50
#define TILEHEIGHT			50

#define SIDEBAR_WIDTH		20

#define MAX_SPRITES			100	// only 99 used, 100 available in map.dat though

#define SCREEN_DATA_SIZE	31280

#define BASE_SCRIPT_MAX_LENGTH	21

// not defined by the dink engine...
#define DEFAULT_MAX_UNDO_LEVEL	250
#define DEFAULT_SCREEN_GAP	    3
#define DEFAULT_AUTOSAVE_TIME   0

#define VISIONS_TRACKED				256

#define MOUSE_BOX_LINE_WIDTH		1
#define SPRITE_SELECTOR_BMP_GAP		4
#define TILE_SELECTOR_GAP			4

enum ColorKey
{
	COLORKEY_NONE,
	COLORKEY_WHITE,
	COLORKEY_BLACK,
};

enum class EditorState
{
	Screen,
	Minimap,
	SpriteSelector,
	TileSelector,
	HardTileSelector,
	ScreenImporter,
	TileHardnessEditor,
	SpriteHardnessEditor
};

enum class EditorSubmode
{
	Sprite,
	Tile,
	Hardbox,
	MultiSprite
};

enum class SpriteType : short
{
	Background,
	Foreground,
	Invisible
};

#define MAX_PARSE_INPUTS	10
#define MAX_LINE_LENGTH		500		//RW: this is the maximum length of a line in dink.ini

#define BLACK		1
#define NOTANIM		2
#define LEFTALIGN	3

//the 8 base colors
#define COLOR_BLACK      (SDL_Color{  0,   0,   0, 255})
#define COLOR_RED        (SDL_Color{255,   0,   0, 255})
#define COLOR_YELLOW     (SDL_Color{255, 255,   0, 255})
#define COLOR_LIME       (SDL_Color{  0, 255,   0, 255})
#define COLOR_CYAN       (SDL_Color{  0, 255, 255, 255})
#define COLOR_BLUE       (SDL_Color{  0,   0, 255, 255})
#define COLOR_MAGENTA    (SDL_Color{255,   0, 255, 255})
#define COLOR_WHITE      (SDL_Color{255, 255, 255, 255})
			       
//and a few extra	       
#define COLOR_ORANGE     (SDL_Color{255, 128,   0, 255})
#define COLOR_GREEN      (SDL_Color{  0, 128,   0, 255})
// transparent background colors
#define COLOR_BCK_BLACK1 (SDL_Color{0x10, 0x10, 0x10, 0x80})

#define NUM_HELP_COLORS (sizeof(helpcolors)/sizeof(helpcolors[0]))	//RW: number of colors in the helpcolors array

//RW: the name of the WDE configuration file
#define CONFIG_FILE_NAME _T("wdep2cnf.ini")

//The dink.ini backup file name
#define DINK_INI_BACKUP_NAME _T("wdep2.orig.dink.ini")

//RW: the mouse wheel directions
#define MOUSE_WHEEL_LEFT	0
#define MOUSE_WHEEL_UP		1
#define MOUSE_WHEEL_RIGHT	2
#define MOUSE_WHEEL_DOWN	3

//timers
#define TIMER_AUTO_SAVE 1
#define TIMER_MOUSE_CHECK 2
#define TIMER_INDICATOR_SCREEN_POS_DELAY 3

#define MAX_DMOD_NAME_LENGTH	20

// ffcreate stuff
#define FF_DATA_BUFFER_SIZE	1024
