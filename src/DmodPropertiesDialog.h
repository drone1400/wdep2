#pragma once
#include "resource.h"

class DmodPropertiesDialog : public CDialog
{
public:
	DmodPropertiesDialog(CWnd* pParent = nullptr);   // standard constructor

	enum { IDD = IDD_DMOD_PROPERTIES };
	CString	m_author;
	CString	m_description;
	CString	m_dmod_title;
	CString	m_email_website;
	
protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support
	void OnOK() override;
	DECLARE_MESSAGE_MAP()
public:
	BOOL OnInitDialog() override;
};