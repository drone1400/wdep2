#pragma once
#include "resource.h"

class ScriptDecompressor : public CDialog
{
	DECLARE_DYNAMIC(ScriptDecompressor)

public:
	ScriptDecompressor(CWnd* pParent = nullptr);   // standard constructor
	virtual ~ScriptDecompressor();

// Dialog Data
	enum { IDD = IDD_DECOMPRESS_SCRIPTS };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	BOOL m_delete_d_files;
};