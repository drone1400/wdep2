#pragma once

void compensateScreenGap(int &x, int &y);

// ------------------------------------------------------------------------------------------------------------------------------
// Message Box helpers, they all show a message box using the main window's handle
//

CString& ConcatInto(CString& into, CString const& s1, CString const& s2, CString const& s3 = _T(""), CString const& s4 = _T(""));

// generic messagebox, returns result
int MB_ShowGeneric(CString const& message, CString const& caption, UINT buttons);

// generic error messagebox
void MB_ShowError(CString const& message);
// generic error messagebox that concatenates multiple strings
void MB_ShowError(CString const& message1, CString const& message2, CString const& message3 = _T(""), CString const& message4 = _T(""));

// generic warning message box
void MB_ShowWarning(CString const& message);

// generic warning messagebox that concatenates multiple strings
void MB_ShowWarning(CString const& message1, CString const& message2, CString const& message3 = _T(""), CString const& message4 = _T(""));

//
// ------------------------------------------------------------------------------------------------------------------------------