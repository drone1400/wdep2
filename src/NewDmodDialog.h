#pragma once
#include "resource.h"

class NewDmodDialog : public CDialog
{
public:
	NewDmodDialog(CWnd* pParent = nullptr);   // standard constructor
	void setDmodNamePtr(TCHAR *buffer);

	enum { IDD = IDD_NEW_DMOD };
	CString	m_description;
	CString	m_directory;
	CString	m_dmod_name;
	CString	m_author;
	CString	m_email_website;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support
	void OnOK() override;

	DECLARE_MESSAGE_MAP()

private:
	TCHAR *dmod_name;
public:
	BOOL OnInitDialog() override;
};
