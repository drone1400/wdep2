#pragma once
#include "Structs.h"
#include <cstdint>

// needs to be of size 220
struct DUMMY_SPRITE
{
	std::int32_t x;
	std::int32_t y;
	std::int32_t sequence;
	std::int32_t frame;
	std::int32_t type;
	std::int32_t size;
	bool sprite_exist;
	std::int32_t rotation;	// completely unusable
	std::int32_t special;
	std::int32_t brain;
	EightDotThreeFilename script;
	EightDotThreeFilename hit;
	EightDotThreeFilename die;
	EightDotThreeFilename talk;
	std::int32_t speed;
	std::int32_t base_walk;
	std::int32_t base_idle;
	std::int32_t base_attack;
	std::int32_t base_hit;	// usable via DinkC, sp_base_hit
	std::int32_t timing;
	std::int32_t depth;
	std::int32_t hardness;
	std::int32_t trim_left;
	std::int32_t trim_top;
	std::int32_t trim_right;
	std::int32_t trim_bottom;
	std::int32_t warp_enabled;
	std::int32_t warp_screen;
	std::int32_t warp_x;
	std::int32_t warp_y;
	std::int32_t touch_sequence;
	std::int32_t base_death;
	std::int32_t gold_dropped;	// usable via DinkC, sp_gold
	std::int32_t hitpoints;
	std::int32_t strength;	// ????? doesn't work
	std::int32_t defense;
	std::int32_t experience;
	std::int32_t sound;
	std::int32_t vision;
	std::int32_t nohit;
	std::int32_t touch_damage;

	std::int32_t garbage[5];
};
static_assert(sizeof(DUMMY_SPRITE) == 220, "DUMMY_SPRITE must be 220 bytes -- per map.dat format.");

class Sprite
{
public:
	Sprite() = default;
	Sprite(const Sprite&) = default;
	Sprite& operator=(const Sprite&) = default;

	Sprite(const DUMMY_SPRITE &sprite_data);

	void formatOutput(DUMMY_SPRITE &sprite_data);
	BOOL getImageBounds(RECT &bounds);
	BOOL getImageBoundsFallback(RECT &bounds);

    void SetFrom(const DUMMY_SPRITE & sprite_data);
    void SetFrom(const Sprite & sprite_data);

	int x;
	int y;
	int sequence;
	int frame;
	SpriteType type;
	int size;
	int special;
	int brain;
	EightDotThreeFilename script;
	int speed;
	int base_walk;
	int base_idle;
	int base_attack;
	int base_hit;
	int timing;
	int depth;
	int hardness;
	int trim_left;
	int trim_top;
	int trim_right;
	int trim_bottom;
	int warp_enabled;
	int warp_screen;
	int warp_x;
	int warp_y;
	int touch_sequence;
	int base_death;
	int hitpoints;
	int strength;
	int defense;
	int experience;
	int gold_dropped;
	int sound;
	int nohit;
	int touch_damage;

	int vision;
};

struct MultiMouseSprite
{
	Sprite sprite;
	int x_offset, y_offset;		//offset of sprite from mouse
};
