#pragma once
#include "resource.h"

class Sprite;

class SpriteProperties : public CDialog
{
public:
	SpriteProperties(Sprite* backingSprite, CWnd* pParent = nullptr);
	virtual BOOL OnInitDialog();

	enum { IDD = IDD_SPRITE_PROPERTIES };
	INT     m_x;
	INT     m_y;
	INT     m_vision;
	INT     m_touch_damage;
	CString	m_script;
	BOOL    m_nohit;
	BOOL    m_hard;
	BOOL    m_enable_warp;
	INT     m_base_walk;
	INT     m_base_death;
	INT     m_base_attack;
	INT     m_depth_que;
	INT     m_base_idle;
	INT     m_base_hit;
	INT     m_type;
	INT     m_trim_left;
	INT     m_trim_right;
	INT     m_trim_top;
	INT     m_trim_bottom;
	INT     m_gold;
	INT     m_experience;
	INT     m_strength;
	INT     m_defense;
	INT     m_hitpoints;
	INT     m_sequence;
	INT     m_size;
	INT     m_sound;
	INT     m_speed;
	INT     m_frame;
	INT     m_timing;
	INT     m_touch_sequence;
	INT     m_warp_screen;
	INT     m_warp_x;
	INT     m_warp_y;
	INT     m_brain;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	void saveSpriteData();
	void setBrain(int brain);

private:
	Sprite* sprite;

	CComboBox m_brainDropdown;
	CEdit m_brainTextbox;

protected:
	afx_msg void OnEditSpriteScript();
	afx_msg void OnBrowseScript();
	afx_msg void OnAuto1();
	afx_msg void OnCbnSelchangeBrain();

	DECLARE_MESSAGE_MAP()
};