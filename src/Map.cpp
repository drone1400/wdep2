#include "StdAfx.h"
#include "Globals.h"
#include "Map.h"

#include <SDL_image.h>

#include "Screen.h"
#include "Sequence.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "Engine.h"
#include "Minimap.h"
#include "Colors.h"
#include "Structs.h"
#include "Common.h"
#include "CommonSdl.h"
#include "PathUtil.h"
#include "SimpleLog.h"
#include "Undo.h"
#include "SpriteEditor.h"
#include "StringUtil.h"
#include "Actions/MultiSpritePickup.h"
#include "Actions/MultiSpriteToggleSelect.h"
#include "Actions/MultiSpriteClearSelected.h"
#include "Actions/MultiSpriteDelete.h"
#include "Actions/MultiSpritePlace.h"
#include "Actions/SpritePlace.h"
#include "Actions/SpritePickup.h"
#include "Actions/ScreenCreate.h"
#include "Actions/ScreenDelete.h"
#include "Actions/ScreenPaste.h"
#include "Actions/ScreenPropertiesChange.h"
#include "Actions/SpritePropertiesChange.h"

namespace actions
{
	class MultiSpriteClearSelected;
}

using std::ofstream;
using std::ios;
using std::vector;

Map::Map(CString dmod_path)
    : isScreenFocusMode(false)
 {
	undo_buffer = new UndoStack();
	
	this->dmod_path.SetString(dmod_path);
	this->dmod_path.AppendChar(_T('\\'));

	minimap = nullptr;

	need_resizing = true;
	cur_vision = 0;
	screenmatch = false;
	editor_state = EditorState::Minimap;
	screenMode = EditorSubmode::Tile;
	hardTileSelectorReturnState = EditorState::Screen;

	hoverSpriteNum = -1;
	findSpriteNum = -1;
	findSpriteLastMouseX = 0;
	findSpriteLastMouseY = 0;

	hoverScreenX = 0;
	hoverScreenY = 0;
	hoverScreen = 0;
	
	hoverRelX = 0;
	hoverRelY = 0;
	hoverScreenOriginX = 0;
	hoverScreenOriginY = 0;
	hoverTileX = 0;
	hoverTileY = 0;
	hoverTileData = {0,0,0,0};

	mouseSprite = nullptr;

	rect_select_active = false;
	rect_select_exclusive = false;
	memset(&selection_rect, 0, sizeof(RECT));

	// set the map position to the upper left corner, necessary???
	window.left = 0;
	window.top = 0;

	//zero out the sequence memory
	memset(sequence, 0, (MAX_SEQUENCES+1) * sizeof(Sequence*));

	memset(vision_used, 0, VISIONS_TRACKED);

	import_map = nullptr;	//RW: without this the Import Map dialog would never show up

	for (int i=0; i<NUM_SCREENS; i++)		//RW: without this minimap wouldn't be rendered in fast map update mode
		miniupdated[i] = false;

	spriteeditor = new SpriteEditor();

	if (auto_save_time)
		SetTimer(mainWnd->GetSafeHwnd(), TIMER_AUTO_SAVE, auto_save_time * 60000, nullptr);
}

void Map::finishLoading() //throws
{
	open_dmod();

	minimap = new Minimap(this);
	sprite_library.readList(dmod_path);
	sprite_selector.createList(this);
	mainWnd->GetLeftPane()->PopulateTree(this);
	Game_Main();
}

Map::~Map()
{
	if (auto_save_time)
		KillTimer(mainWnd->GetSafeHwnd(), TIMER_AUTO_SAVE);

	if (minimap)
	{
		delete minimap;
		minimap = nullptr;
	}

	if (undo_buffer)
	{
		delete undo_buffer;
		undo_buffer = nullptr;
	}

	if (mouseSprite)
	{
		delete mouseSprite;
		mouseSprite = nullptr;
	}

	if (spriteeditor)
	{
		delete spriteeditor;
		spriteeditor = nullptr;
	}

	deallocate_map();
	hard_tile_selector.unloadAllBitmaps();
	release_tile_bmps();
	deallocate_sequences();

	//delete the dink.ini backup file
	TCHAR backup[MAX_PATH];
	wsprintf(backup, _T("%s%s"), (LPCTSTR)this->dmod_path, DINK_INI_BACKUP_NAME);
	_tremove(backup);
}

// update the coordinates of the view window
void Map::updateMapPosition(int x, int y)
{
	window.left = x;
	window.top = y;

    int x_min = MIN_MAP_X;
    int y_min = MIN_MAP_Y;
    int x_max = MAX_MAP_X;
    int y_max = MAX_MAP_Y;

    // experimental idea to allow scrolling the map beyond the edge...
    // not sure if useful or good idea, for now not using it
    // int x_min = MIN_MAP_X_EXTENDED;
    // int y_min = MIN_MAP_Y_EXTENDED;
    // int x_max = MAX_MAP_X_EXTENDED;
    // int y_max = MAX_MAP_Y_EXTENDED;

    if (isScreenFocusMode)
    {
        // allow user to scroll up to 8 screens out of bounds... 
        int range = 8;
        
        // change map limits...
        int focus_x = screenFocusIndex % MAP_COLUMNS;
        int focus_y = screenFocusIndex / MAP_COLUMNS;

        int offset_x = focus_x * SCREEN_WIDTH_GAP;
        int offset_y = focus_y * SCREEN_HEIGHT_GAP;
        x_min = offset_x - range * SCREEN_WIDTH;
        y_min = offset_y - range * SCREEN_HEIGHT;
        x_max = offset_x + (range + 1) * SCREEN_WIDTH;
        y_max = offset_y + (range + 1) * SCREEN_HEIGHT;
        
    }

	// verify coordinates are within bounds
	if (window.left < x_min)
	{
		window.left = x_min;
	}
	if (window.top < y_min)
	{
		window.top = y_min;
	}

	window.right = window.left + window_width;
	window.bottom = window.top + window_height;

	if (window.right > x_max)
	{
		window.left -=  window.right - x_max;
		window.right = x_max;
	}
	if (window.bottom > y_max)
	{
		window.top -=  window.bottom - y_max;
		window.bottom = y_max;
	}

	screens_displayed.left = window.left / SCREEN_WIDTH_GAP;
	screens_displayed.right = window.right / SCREEN_WIDTH_GAP;
	screens_displayed.top = window.top / SCREEN_HEIGHT_GAP;
	screens_displayed.bottom = window.bottom / SCREEN_HEIGHT_GAP;

	screenOffsetX = -(window.left % SCREEN_WIDTH_GAP);
	screenOffsetY = -(window.top % SCREEN_HEIGHT_GAP);
}

void Map::updateHoverPosition(int x, int y)
{
	hoverMouseX = x;
	hoverMouseY = y;
	
	if (isScreenFocusMode)
	{
		hoverScreen = screenFocusIndex;
		hoverScreenY = hoverScreen / MAP_COLUMNS;
		hoverScreenX = hoverScreen - hoverScreenY * MAP_COLUMNS;

		hoverScreenOriginX = SCREEN_WIDTH_GAP * hoverScreenX;
		hoverScreenOriginY = SCREEN_HEIGHT_GAP * hoverScreenY;
		hoverRelX = x + window.left - hoverScreenOriginX;
		hoverRelY = y + window.top - hoverScreenOriginY;

		if (hoverRelX < 0 || hoverRelY < 0 ||
			hoverRelX >= SCREEN_WIDTH || hoverRelY >= SCREEN_HEIGHT)
		{
			hoverTileData = {0,0,0,0};
			hoverTileX = 0;
			hoverTileY = 0;
		} else
		{
			hoverTileX = hoverRelX / TILEWIDTH;
			hoverTileY = hoverRelY / TILEHEIGHT;
			if (screen[hoverScreen] != nullptr)
			{
				hoverTileData = screen[hoverScreen]->tiles[hoverTileY][hoverTileX];
			} else
			{
				hoverTileData = {0,0,0,0};
			}
		}

		const int deltaFind = 40;
		if (findSpriteNum >= 0 && findSpriteNum < MAX_SPRITES &&
			abs(findSpriteLastMouseX - hoverMouseX) < deltaFind &&
			abs(findSpriteLastMouseY - hoverMouseY) < deltaFind)
		{
			// currently showing found sprite as hover sprite...
			hoverSpriteNum = findSpriteNum;

			
			Game_Main();
		} else
		{
		    // identify hovered sprite...
		    int new_hover_sprite_num = getHoverSpriteNum();
		    
			// cursor moved, update hover sprite and find sprite values...
			if (new_hover_sprite_num >= 0 && new_hover_sprite_num < MAX_SPRITES)
			{
				findSpriteLastMouseX = hoverMouseX;
				findSpriteLastMouseY = hoverMouseY;
				findSpriteNum = new_hover_sprite_num;
			} else
			{
				findSpriteNum = -1;
			}
			
	
		    if (new_hover_sprite_num != hoverSpriteNum)
		    {
		        hoverSpriteNum = new_hover_sprite_num;
		        Game_Main();
		    }
		}
	} else
	{
	    POINT point = {x, y};
	    if (bindViewPoint(point) == false)
	    {
	        // not hovering over valid screen...
	        findSpriteNum = -1;
	        hoverSpriteNum = -1;
	        hoverScreen = -1;
	        hoverScreenX = -1;
	        hoverScreenY = -1;
	        hoverScreenOriginX = -1;
	        hoverScreenOriginY = -1;
	        hoverRelX = 0;
	        hoverRelY = 0;
	        return;
	    }
	    
		findSpriteNum = -1;
		hoverScreenX = point.x / SCREEN_WIDTH_GAP;
		hoverScreenY = point.y / SCREEN_HEIGHT_GAP;
	    
	    if (hoverScreenX < 0) hoverScreenX = 0;
	    if (hoverScreenX >= MAP_COLUMNS) hoverScreenX = MAP_COLUMNS - 1;
	    if (hoverScreenY < 0) hoverScreenY = 0;
	    if (hoverScreenY >= MAP_ROWS) hoverScreenY = MAP_ROWS - 1;
	    
		hoverScreenOriginX = SCREEN_WIDTH_GAP * hoverScreenX;
		hoverScreenOriginY = SCREEN_HEIGHT_GAP * hoverScreenY;
		hoverScreen = hoverScreenY * MAP_COLUMNS + hoverScreenX;

		hoverRelX = point.x % SCREEN_WIDTH_GAP;
		hoverRelY = point.y % SCREEN_HEIGHT_GAP;

		hoverTileX = hoverRelX / TILEWIDTH;
		hoverTileY = hoverRelY / TILEHEIGHT;
	    
		if (screen[hoverScreen] != nullptr &&
		    hoverTileX >= 0 && hoverTileX <= SCREEN_TILE_WIDTH &&
		    hoverTileY >= 0 && hoverTileY <= SCREEN_TILE_HEIGHT)
		{
			hoverTileData = screen[hoverScreen]->tiles[hoverTileY][hoverTileX];
		} else
		{
			hoverTileData = {0,0,0,0};
		}

		// identify hovered sprite...
		int new_hover_sprite_num = getHoverSpriteNum();
	    if (new_hover_sprite_num != hoverSpriteNum)
	    {
	        hoverSpriteNum = new_hover_sprite_num;
	        Game_Main();
	    }
	    
	}

	
}

void Map::cursorToNextSprite(bool previous)
{
	if (isScreenFocusMode == false) return;
	if (editor_state != EditorState::Screen) return;
	if (screenMode != EditorSubmode::Sprite) return;
	if (mouseSprite) return;
	if (screen[hoverScreen] == nullptr) return;

	Screen * scr = screen[hoverScreen];
	
	int spr_num = findSpriteNum;
	if (spr_num < 0) spr_num = 0;
	if (spr_num > MAX_SPRITES) spr_num = MAX_SPRITES - 1;

	spr_num +=  + (previous ? (-1) : (1));
	if (spr_num < 0) spr_num = MAX_SPRITES - 1;
	if (spr_num > MAX_SPRITES) spr_num = 0;

	int spr_num_start = spr_num;

	while(true)
	{
		if (scr->sprite[spr_num] != nullptr)
		{
			findSpriteNum = spr_num;
			
			Sprite * spr = scr->sprite[spr_num];
			int sprx = spr->x;
			int spry = spr->y;

			Sequence * seq;
			seq = current_map->sequence[spr->sequence];
			int fridx = spr->frame - 1;
			if (seq != nullptr && fridx >= 0 && fridx < MAX_FRAMES)
			{
				RECT hardbox;
				int frw = seq->frame_width[fridx];
				int frh = seq->frame_height[fridx];
				if (((seq->center_x == 0) && (seq->center_y == 0)) ||
					((seq->type != NOTANIM) && (seq->left_boundary == 0) && (seq->top_boundary == 0) && (seq->right_boundary == 0) && (seq->bottom_boundary == 0)))
				{
					hardbox.left = spr->x - frw / 4;
					hardbox.top		= spr->y - frh / 10;
					hardbox.right		= spr->x + frw / 4;
					hardbox.bottom	= spr->y + frh / 10;
				}
				else
				{
					hardbox.left		= spr->x + seq->left_boundary;
					hardbox.top		= spr->y + seq->top_boundary;
					hardbox.right		= spr->x + seq->right_boundary;
					hardbox.bottom	= spr->y + seq->bottom_boundary;
				}

				sprx = (hardbox.left + hardbox.right) / 2;
				spry = (hardbox.top + hardbox.bottom) / 2;
			}

			// not sure why but need to offset by the ingame sidebar width
			sprx -= SIDEBAR_WIDTH;

			int deltax = sprx - hoverRelX;
			int deltay = spry - hoverRelY;

			updateMapPosition(window.left + deltax, window.top + deltay);
			updateHoverPosition(hoverMouseX, hoverMouseY);
			return;
		}
		
		if (previous) spr_num--;
		else spr_num++;
		
		if (spr_num < 0) spr_num = MAX_SPRITES - 1;
		if (spr_num >= MAX_SPRITES) spr_num = 0;
		

		// we're back to where we started but we haven't found anything... exit
		if (spr_num == spr_num_start)
			return;
	}
}

void Map::setMouseSprite(const Sprite* sprite)
{
	if (sprite)
	{
		if (!mouseSprite)
			mouseSprite = new Sprite(*sprite);
		else
			*mouseSprite = *sprite;
	}
	else
	{
		if (mouseSprite)
		{
			delete mouseSprite;
			mouseSprite = nullptr;
		}
	}
}

void Map::setVision(int new_vision)
{
	cur_vision = new_vision;
	// immediately update everything after setting vision...
	Game_Main();
}

// triggered by an undo action
void Map::placeTiles(int first_x_tile, int first_y_tile, int width, int height, 
					 TILEDATA tile_set[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2])
{
	int cur_screen, x_tile, y_tile;
	for (int y = first_y_tile, y_temp = 0; y_temp < height; y++, y_temp++)
	{
		for (int x = first_x_tile, x_temp = 0; x_temp < width; x++, x_temp++)
		{
			cur_screen = MAP_COLUMNS * (y / SCREEN_TILE_HEIGHT) + x / SCREEN_TILE_WIDTH;
			if (screen[cur_screen] == nullptr)
				continue;
			x_tile = x % SCREEN_TILE_WIDTH;
			y_tile = y % SCREEN_TILE_HEIGHT;

			miniupdated[cur_screen] = false;

			screen[cur_screen]->tiles[y_tile][x_tile] = tile_set[x_temp][y_temp];
		}
	}
}

RECT Map::getWindowScreenRectRadius(int screen_idx, int radius) const
{
	int y_screen = screen_idx / MAP_COLUMNS;
	int x_screen = screen_idx - y_screen * MAP_COLUMNS;
	int scr_left = x_screen * SCREEN_WIDTH_GAP - window.left;
	int scr_top = y_screen * SCREEN_HEIGHT_GAP - window.top;
	int scr_right = scr_left + SCREEN_WIDTH;
	int scr_bottom = scr_top + SCREEN_HEIGHT;

	if (radius > 1)
	{
		scr_left -= (radius -1) * SCREEN_WIDTH;
		scr_right += (radius -1) * SCREEN_WIDTH;
		scr_top -= (radius -1) * SCREEN_HEIGHT;
		scr_bottom += (radius -1) * SCREEN_HEIGHT;
	}

	return {scr_left, scr_top, scr_right, scr_bottom}; 
}

bool Map::checkSpriteOffScreenRadius(RECT sprite_bounds, int screen_idx, int radius)
{
	RECT rect = getWindowScreenRectRadius(screen_idx, radius);

	if (sprite_bounds.right < rect.left ||
		sprite_bounds.left >= rect.right ||
		sprite_bounds.bottom < rect.top ||
		sprite_bounds.top >= rect.bottom)
	{
		return true;
	}

	return false;
}



int Map::placeSprite(int x, int y, bool stamp)
{
	bool sprite_placed = false;
	vector<SpriteRef> sprite_refs;
	vector<Sprite> sprite_list;

	RECT sprite_bounds;
	if (mouseSprite->getImageBounds(sprite_bounds) != TRUE &&
	    mouseSprite->getImageBoundsFallback(sprite_bounds) != TRUE)
		return false;

	int i_screen,  x_screen, y_screen;
    
    POINT point {x + window.left, y + window.top};
	
	if (isScreenFocusMode)
	{
		x_screen = screenFocusIndex % MAP_COLUMNS;
		y_screen = screenFocusIndex / MAP_COLUMNS;
		i_screen = screenFocusIndex;
	}
	else
	{
		x_screen = (x + window.left) / SCREEN_WIDTH_GAP;
		y_screen = (y + window.top) / SCREEN_HEIGHT_GAP;
		i_screen = y_screen * MAP_COLUMNS + x_screen;

	    if (bindMapPoint(point) == false)
	        return false;
	}

	// if sprite is more than two neighbouring screen away, don't place it...
	if (checkSpriteOffScreenRadius(sprite_bounds, i_screen, 3))
		return false;

	bool sprite_out_of_bounds = checkSpriteOffScreenRadius(sprite_bounds, i_screen, 1);

	if (sprite_out_of_bounds && isScreenFocusMode == false)
		return false;
	
	// add main sprite
	{
		if (screen[i_screen] == nullptr)
			return false;

		Sprite new_sprite = *mouseSprite;

		new_sprite.x = point.x - (x_screen * SCREEN_WIDTH_GAP) + SIDEBAR_WIDTH;
		new_sprite.y = point.y - (y_screen * SCREEN_HEIGHT_GAP);

		//set the hardness and nohit to the default specified by the user if needed
		if ((stamp && use_hard_nohit_default_stamp) || (!stamp && use_hard_nohit_default_place))
		{
			new_sprite.hardness = !sprite_hard;
			new_sprite.nohit = sprite_nohit;
		}

		// make sure it returns true before deleting mouseSprite
		compensateScreenGap(new_sprite.x, new_sprite.y);
		sprite_placed = true;

		sprite_refs.emplace_back(this, i_screen, screen[i_screen]->findFirstAvailableSpriteSlot());
		sprite_list.push_back(new_sprite);
	
	
		//check script with database
		if (autoscript == true) 
		{
			TCHAR read_sequence[256];
			TCHAR return_string[256];
			TCHAR file_path[MAX_PATH];
			wsprintf(read_sequence, _T("%i"), new_sprite.sequence);
			wsprintf(file_path, _T("%s\\bscripts.ini"), (LPCTSTR)WDE_path);
			if (GetPrivateProfileString(_T("Script"), read_sequence, 0, return_string, 256, file_path) > 0)
				EightDotThreeFilename::from_tchar(return_string, new_sprite.script);
		}
	}
	
	if (!sprite_placed)
		return false;
	
	// add screenmatched sprites
	if (screenmatch && (isScreenFocusMode == false || sprite_out_of_bounds == false))
	{
		Sequence* cur_seq = sequence[mouseSprite->sequence];
		int cur_frame = mouseSprite->frame - 1;

		if (cur_seq == nullptr || cur_seq->frame_image[cur_frame] == nullptr)
			return false;

		sprite_bounds.left += SIDEBAR_WIDTH;
		sprite_bounds.right += SIDEBAR_WIDTH;

	    RECT bounds = sprite_bounds;
	    if (bindViewRect(bounds) == false)
	        return false;

		int first_x_screen = bounds.left / SCREEN_WIDTH_GAP;
		int last_x_screen = bounds.right / SCREEN_WIDTH_GAP;
		int first_y_screen = bounds.top / SCREEN_HEIGHT_GAP;
		int last_y_screen = bounds.bottom / SCREEN_HEIGHT_GAP;

		for (int scr_x = first_x_screen; scr_x <= last_x_screen; scr_x++)
		{
		    if (scr_x < 0 || scr_x >= MAP_COLUMNS)
		        continue;
		    
		    for (int scr_y = first_y_screen; scr_y <= last_y_screen; scr_y++)
		    {
		        if (scr_y < 0 || scr_y >= MAP_ROWS)
		            continue;
		        
		        int scr = scr_y * MAP_COLUMNS + scr_x;

		        if (scr == i_screen)
		            continue;

		        if (scr > NUM_SCREENS)
		            continue;

		        if (screen[scr] == nullptr)
		            continue;
			
		        Sprite new_sprite = *mouseSprite;

		        //set the hardness and nohit to the default specified by the user if needed
		        if ((stamp && use_hard_nohit_default_stamp) || (!stamp && use_hard_nohit_default_place))
		        {
		            new_sprite.hardness = !sprite_hard;
		            new_sprite.nohit = sprite_nohit;
		        }

		        // add sprite to screen
		        new_sprite.x = point.x - (scr_x * SCREEN_WIDTH_GAP) + SIDEBAR_WIDTH;
		        new_sprite.y = point.y - (scr_y * SCREEN_HEIGHT_GAP);
		        compensateScreenGap(new_sprite.x, new_sprite.y);

		        sprite_refs.emplace_back(this, scr, screen[scr]->findFirstAvailableSpriteSlot());
		        sprite_list.push_back(new_sprite);

		        if (autoscript == true) 
		        {
		            TCHAR read_sequence[256];
		            TCHAR return_string[256];
		            TCHAR file_path[MAX_PATH];
		            wsprintf(read_sequence, _T("%i"), new_sprite.sequence);
		            wsprintf(file_path, _T("%s\\bscripts.ini"), (LPCTSTR)WDE_path);
					
		            if (GetPrivateProfileString(_T("Script"), read_sequence, 0, return_string, 256, file_path) > 0)
		                EightDotThreeFilename::from_tchar(return_string, new_sprite.script);
		        }
		    }
		}
	}

	// create the action and then execute to actually place the sprites
	undo_buffer->PushAndDo<actions::SpritePlace>(sprite_refs, sprite_list, stamp);

	return true;
}

// centers the mouse on the sprites position
void Map::loadMouseSprite(const Sprite* sprite, int screen_num)
{
	// create a new mouse sprite if none exists already
	if (mouseSprite == nullptr)
		mouseSprite = new Sprite();

	*mouseSprite = *sprite;

	sprite_selector.currentSequence = mouseSprite->sequence;
	sprite_selector.currentFrame = mouseSprite->frame;
	sprite_selector.showFrames = true;

	int screen_x = screen_num % MAP_COLUMNS;
	int screen_y = screen_num / MAP_COLUMNS;
	
	int x_offset = (screen_x * SCREEN_WIDTH_GAP) - window.left;
	int y_offset = (screen_y * SCREEN_HEIGHT_GAP) - window.top;

    if (isScreenFocusMode)
    {
        int gap_correction_x = 0;
        int gap_correction_y = 0;
        int x_val = mouseSprite->x;
        int y_val = mouseSprite->y;
        if (x_val < 0)
        {
            gap_correction_x = -screen_gap;
        }
        if (x_val > SCREEN_WIDTH)
        {
            gap_correction_x = screen_gap;
        }
        if (y_val < 0)
        {
            gap_correction_y = -screen_gap;
        }
        if (y_val > SCREEN_HEIGHT)
        {
            gap_correction_y = screen_gap;
        }

        // basically, due to screen gap compensation in other parts of the code, the sprite needs to be offset when picked up,
        // or the coordinates will be offset by the screen gap size when it is picked up out of the bounds of the screen...
        mouseSprite->x += gap_correction_x;
        mouseSprite->y += gap_correction_y;
        
    }

    mouseSprite->x += x_offset;
    mouseSprite->y += y_offset;

	// center the mouse on the sprite
	SetCursorPos(mouseSprite->x + mapRect.left, mouseSprite->y + mapRect.top);
}

// stores sprite in mouseSprite
bool Map::tryDoSpritePickup(bool removeOld /* = true */)
{
	if (screen[hoverScreen] == nullptr)
		return false;
	
	int sprite_num;
	//Sprite* temp_sprite = screen[hoverScreen]->getSprite(hoverRelX, hoverRelY, sprite_num);
	// doing this instead to make sure we pick up the sprite indicated by the hover rectangle...
	sprite_num = hoverSpriteNum;
    if (sprite_num < 0 || sprite_num >= MAX_SPRITES)
        return false;
	Sprite * temp_sprite = screen[hoverScreen]->sprite[hoverSpriteNum];
	if (temp_sprite == nullptr)
		return false;
	
	undo_buffer->PushAndDo<actions::SpritePickup>(SpriteRef(this, hoverScreen, sprite_num), *temp_sprite, removeOld);
    
	hoverSpriteNum = -1;
	
	return true;
}

Sprite* Map::getHoverSprite()
{
    if (hoverSpriteNum < 0 || hoverSpriteNum >= MAX_SPRITES)
        return nullptr;

    // in focus mode, only get sprites from focused screen...
    if (isScreenFocusMode)
    {
        if (screen[screenFocusIndex] == nullptr)
            return nullptr;

        return screen[screenFocusIndex]->sprite[hoverSpriteNum];
    }

    // make sure the screen is valid
    if (screen[hoverScreen] == nullptr)
        return nullptr;
    
    return screen[hoverScreen]->sprite[hoverSpriteNum];
}


// returns pointer to sprite that was clicked on
int Map::getHoverSpriteNum()
{
	// make sure the screen is valid
	if (screen[hoverScreen] == nullptr)
		return -1;

    int sprite_num = -1;
    
	// in focus mode, only get sprites from focused screen...
	if (isScreenFocusMode)
	{
		if (screen[screenFocusIndex] == nullptr)
			return -1;

	    
		screen[screenFocusIndex]->getSpriteAtXy(hoverRelX, hoverRelY, sprite_num);
	    return sprite_num;
	}

	screen[hoverScreen]->getSpriteAtXy(hoverRelX, hoverRelY, sprite_num);
    return sprite_num;
}

void Map::drawScreenAt(int screen_index, int x_total_offset, int y_total_offset, RECT clip_rect)
{
    // sanity check screen index...
    if (screen_index < 0 || screen_index >= NUM_SCREENS)
        return;
    
	if (screen[screen_index] != nullptr)
	{
		// render tiles
		screen[screen_index]->drawTiles(x_total_offset, y_total_offset);

	    if (hide_all_sprites == false)
	    {
	        // render sprites
	        screen[screen_index]->drawSprites(x_total_offset, y_total_offset, clip_rect, screen_index);
	    }

		// draw hardness
		if (displayhardness)
		{
			screen[screen_index]->drawHardTiles(x_total_offset, y_total_offset);
		    if (hide_all_sprites == false)
		    {
		        screen[screen_index]->drawHardSprites(x_total_offset, y_total_offset, clip_rect);
		    }
		}

		// add screen to minimap
		if (update_minimap)
		{   
			drawScreen(screen_index);
			//minimap->image->Blt(&minimap_dest_rect, lpddsback, &minimap_src_rect, DDBLT_WAIT, nullptr);
		}

		if (screenMode == EditorSubmode::MultiSprite && rect_select_active)	//draw selection rectangle in multi-sprite mode
		{
			RECT dest = selection_rect;
			if (!rect_select_exclusive)
				drawBox(dest, color_rect_select_normal_box, 1);
			else
				drawBox(dest, color_rect_select_exclusive_box, 1);
		}
	}
	else
	{
		// draw a blank screen on the main screen
		drawFilledBox(clip_rect, color_blank_screen_fill);
	}
}

int Map::drawScreens()
{
	memset(new_vision_used, 0, VISIONS_TRACKED);

	// sanity check i suppose...
	if (screenFocusIndex < 0 || screenFocusIndex >= NUM_SCREENS  || 
        !(editor_state == EditorState::Screen && screenMode == EditorSubmode::Sprite))
	{
	    if (isScreenFocusMode)
	    {
	        isScreenFocusMode = false;
	        updateMapPosition(window.left, window.top);
	    }
	}

	if (isScreenFocusMode)
	{
		int y_tl_screen = screens_displayed.top;
		int x_tl_screen = screens_displayed.left;
		
		int i_screen = screenFocusIndex;
		int y_screen = i_screen / MAP_COLUMNS;
		int x_screen = i_screen - y_screen * MAP_COLUMNS;
		
		int x_total_offset = screenOffsetX + (x_screen - x_tl_screen) * SCREEN_WIDTH_GAP;
		int y_total_offset = screenOffsetY + (y_screen - y_tl_screen) * SCREEN_HEIGHT_GAP;

		RECT clip_rect = {0, 0, window_width, window_height};

		drawScreenAt(i_screen, x_total_offset, y_total_offset, clip_rect);
	}
	else
	{
		int y_total_offset = screenOffsetY;
		for (int y_screen = screens_displayed.top; y_screen <= screens_displayed.bottom; y_screen++)
		{
		    if (y_screen < 0 || y_screen >= MAP_ROWS)
		    {
		        y_total_offset += SCREEN_HEIGHT_GAP;
		        continue;
		    }
		    
			int i_screen = y_screen * MAP_COLUMNS + screens_displayed.left;
			RECT clip_rect = {0, y_total_offset, 0, y_total_offset + SCREEN_HEIGHT};

			if (clip_rect.top < 0) clip_rect.top = 0;
			if (clip_rect.bottom > window_height) clip_rect.bottom = window_height;
			if (clip_rect.top >= clip_rect.bottom)
			{
				y_total_offset += SCREEN_HEIGHT_GAP;
				continue;
			}

			int x_total_offset = screenOffsetX;
			for (int x_screen = screens_displayed.left; x_screen <= screens_displayed.right; x_screen++)
			{
			    if (x_screen >= 0 && x_screen < MAP_COLUMNS)
			    {
			        clip_rect.left = x_total_offset;
			        clip_rect.right = x_total_offset + SCREEN_WIDTH;

			        if (clip_rect.left < 0) clip_rect.left = 0;
			        if (clip_rect.right > window_width) clip_rect.right = window_width;
			        if (clip_rect.left < clip_rect.right)
			        {
			            drawScreenAt(i_screen, x_total_offset, y_total_offset, clip_rect);
			        }
			    }
				i_screen++;
				x_total_offset += SCREEN_WIDTH_GAP;
			}
		    
			y_total_offset += SCREEN_HEIGHT_GAP;
		}
	}
	
	RECT boundsRaw;
	RECT boundsFixed;
	BOOL boundsMatch = true;
	RECT clip_box = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
	RECT clip_box_duplicate_sprite = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
	int x_offset=0, y_offset=0;

	if (isScreenFocusMode)
	{
		const int offx = hoverScreenOriginX - window.left;
		const int offy = hoverScreenOriginY - window.top;
		clip_box = { 0 - offx, 0 - offy, window_width - offx, window_height - offy};
	}

    Sprite * hover_sprite = getHoverSprite();

	// draw a box around the sprite the mouse is on top of
	if (screenMode == EditorSubmode::Sprite && hover_sprite != nullptr && mouseSprite == nullptr)
	{
		if (hover_sprite->getImageBounds(boundsRaw) != TRUE)
		    hover_sprite->getImageBoundsFallback(boundsRaw);
		boundsFixed = boundsRaw;
		//hoverSprite->getImageBounds(boundsFixed);
		
		if (fixBounds(boundsFixed, clip_box))
		{
			x_offset = (hoverScreenX - screens_displayed.left) * SCREEN_WIDTH_GAP + screenOffsetX;
			y_offset = (hoverScreenY - screens_displayed.top) * SCREEN_HEIGHT_GAP + screenOffsetY;

			boundsRaw.left += x_offset;
			boundsRaw.right += x_offset;
			boundsRaw.top += y_offset;
			boundsRaw.bottom += y_offset;
			
			boundsFixed.left += x_offset;
			boundsFixed.right += x_offset;
			boundsFixed.top += y_offset;
			boundsFixed.bottom += y_offset;

			boundsMatch = boundsRaw.left == boundsFixed.left &&
							boundsRaw.right == boundsFixed.right &&
							boundsRaw.top == boundsFixed.top &&
							boundsRaw.bottom == boundsFixed.bottom;

			// draw hover sprite hardness before sprite bounding boxes and info!
			if (hoverSpriteNum >= 0 && hoverSpriteNum < 100 && hover_sprite_hardness)
			{
			    if (sequence[hover_sprite->sequence] != nullptr)
			    {
			        // NOTE: why was this commented out previously? hmmm..
			        RECT clip_box_hover_hard = {0, 0, this->window_width, this->window_height};
			        sequence[hover_sprite->sequence]->clipRenderHardness(x_offset, y_offset, hover_sprite, clip_box_hover_hard);
			    }
			}

			if (!boundsMatch)
			{
				// draw additional bounding rectangle on raw bounds...
				// drawBox(boundsRaw, color_sprite_hover_box_raw, 1);
			}

			// draw bounds rectangle
			drawBox(boundsFixed, color_sprite_hover_box, 1);

			if (detect_screenmatch_duplicates)
			{
				RECT hoverBounds, spBounds;
				if (hover_sprite->getImageBounds(hoverBounds) != TRUE)
				    hover_sprite->getImageBoundsFallback(hoverBounds);

				int pt = 0;
				struct 
				{
					int x, y;
					void Set(int _x, int _y) {x=_x;y=_y;}
				} pts[8];
				
				if (hoverScreenX > 0)
					pts[pt++].Set(-1, 0);
				if (hoverScreenX < MAP_COLUMNS - 1)
					pts[pt++].Set(1, 0);
				
				if (hoverScreenY > 0)
				{
					pts[pt++].Set(0, -1);
					if (hoverScreenX > 0)
						pts[pt++].Set(-1, -1);
					if (hoverScreenX < MAP_COLUMNS - 1)
						pts[pt++].Set(1, -1);
				}

				if (hoverScreenY < MAP_ROWS - 1)
				{
					pts[pt++].Set(0, 1);
					if (hoverScreenX > 0)
						pts[pt++].Set(-1, 1);
					if (hoverScreenX < MAP_COLUMNS - 1)
						pts[pt++].Set(1, 1);
				}

				for (int p = 0; p < pt; p++)
				{
				    int scr_num = (hoverScreenY + pts[p].y) * MAP_COLUMNS + hoverScreenX + pts[p].x;
					Screen* sc = screen[scr_num];
					if (!sc)
						continue;
					for (int i=0; i<MAX_SPRITES; i++)
						if (sc->sprite[i] != nullptr && sc->sprite[i]->x == hover_sprite->x - SCREEN_WIDTH*pts[p].x && sc->sprite[i]->y == hover_sprite->y - SCREEN_HEIGHT*pts[p].y
							&& sc->sprite[i]->sequence == hover_sprite->sequence && sc->sprite[i]->frame == hover_sprite->frame
							&& sc->sprite[i]->trim_top == hover_sprite->trim_top && sc->sprite[i]->trim_bottom == hover_sprite->trim_bottom 
							&& sc->sprite[i]->trim_left == hover_sprite->trim_left && sc->sprite[i]->trim_right == hover_sprite->trim_right)
						{
							if (sc->sprite[i]->getImageBounds(spBounds) != TRUE)
							    sc->sprite[i]->getImageBoundsFallback(spBounds);
//							if (fixBounds(spBounds, clip_box_duplicate_sprite))
							{
								// NOTE: must not override old offset value as it is needed for drawing hover sprite info!
								int x_offset_new = (hoverScreenX + pts[p].x - screens_displayed.left) * SCREEN_WIDTH_GAP + screenOffsetX;
								int y_offset_new = (hoverScreenY + pts[p].y - screens_displayed.top) * SCREEN_HEIGHT_GAP + screenOffsetY;

								spBounds.left += x_offset_new;
								spBounds.right += x_offset_new;
								spBounds.top += y_offset_new;
								spBounds.bottom += y_offset_new;

								drawBox(spBounds, color_match_duplicate_hover_box, 1);
								break;
							}
						}
				}
			}

			// draw hover sprite info at the very end!
			if (hoverSpriteNum >= 0 && hoverSpriteNum < 100 && hover_sprite_info)
			{
			    if (sequence[hover_sprite->sequence] == nullptr ||
			        sequence[hover_sprite->sequence]->drawSpriteInfo(x_offset, y_offset, hover_sprite, hoverSpriteNum+1) != TRUE)
			    {
			        Sequence::drawSpriteInfoFallback(x_offset, y_offset, hover_sprite, hoverSpriteNum+1);
			    }
			}
		}
	}
	else if (screenMode == EditorSubmode::MultiSprite)
	{
		for (auto p=selectedSprites.begin(); p!=selectedSprites.end(); ++p)
		{
			if ((*p)->getImageBounds(boundsFixed) != TRUE)
			    (*p)->getImageBoundsFallback(boundsFixed);
			
			if (fixBounds(boundsFixed, clip_box))
			{
				x_offset = (p->screen_num % MAP_COLUMNS - screens_displayed.left) * SCREEN_WIDTH_GAP + screenOffsetX;
				y_offset = (p->screen_num / MAP_COLUMNS - screens_displayed.top) * SCREEN_HEIGHT_GAP + screenOffsetY;

				boundsFixed.left += x_offset;
				boundsFixed.right += x_offset;
				boundsFixed.top += y_offset;
				boundsFixed.bottom += y_offset;

				drawBox(boundsFixed, color_sprite_selected_box, 1);
			}
		}

		if (hover_sprite != nullptr && mouseMultiSprites.size() == 0)	//draw hover box if we are hovering but don't have anything picked up
		{
			if (hover_sprite->getImageBounds(boundsFixed) != TRUE)
		        hover_sprite->getImageBoundsFallback(boundsFixed);

			if (fixBounds(boundsFixed, clip_box))
			{
				x_offset = (hoverScreenX - screens_displayed.left) * SCREEN_WIDTH_GAP + screenOffsetX;
				y_offset = (hoverScreenY - screens_displayed.top) * SCREEN_HEIGHT_GAP + screenOffsetY;

				boundsFixed.left += x_offset;
				boundsFixed.right += x_offset;
				boundsFixed.top += y_offset;
				boundsFixed.bottom += y_offset;

				if (isSelectedMultisprite(SpriteRef{ this, hoverScreen, hoverSpriteNum }))
					drawBox(boundsFixed, color_sprite_hover_selected_box, 1);
				else
					drawBox(boundsFixed, color_sprite_hover_box, 1);
			}
		}
	}

	// track the first 256 visions used on screen
	if (memcmp(new_vision_used, vision_used, VISIONS_TRACKED) != 0)
	{
		// update the left panel
		memcpy(vision_used, new_vision_used, VISIONS_TRACKED);
	
		mainWnd->GetLeftPane()->UpdateVisionTree();
	}
	
	return true;
}


void Map::trimSprite(RECT trim)
{
	Sprite new_sprite = *mouseSprite;

	if (sequence[new_sprite.sequence] == nullptr)
		return;

	RECT image_bounds;
	if (mouseSprite->getImageBounds(image_bounds) == false)
		return;

	int max_x = image_bounds.right - image_bounds.left;
	int max_y = image_bounds.bottom - image_bounds.top;

	if (new_sprite.trim_right == 0)
	{
		new_sprite.trim_left = 0;
		new_sprite.trim_top = 0;
		new_sprite.trim_right = max_x;
		new_sprite.trim_bottom = max_y;
	}

	new_sprite.trim_left += (short)trim.left;
	new_sprite.trim_top += (short)trim.top;
	new_sprite.trim_right += (short)trim.right;
	new_sprite.trim_bottom += (short)trim.bottom;

	if (new_sprite.trim_left < 0)
		new_sprite.trim_left = 0;

	if (new_sprite.trim_top < 0)
		new_sprite.trim_top = 0;

	if (new_sprite.trim_right > max_x)
		new_sprite.trim_right = max_x;

	if (new_sprite.trim_bottom > max_y)
		new_sprite.trim_bottom = max_y;

	if (new_sprite.trim_left == 0 && new_sprite.trim_top == 0 && new_sprite.trim_right == max_x && new_sprite.trim_bottom == max_y)
	{
		// remove all trimming attributes from the sprite.
		new_sprite.trim_left = 0;
		new_sprite.trim_top = 0;
		new_sprite.trim_right = 0;
		new_sprite.trim_bottom = 0;
	}

	changeSpriteProperties(SpriteRef{ this, -1, -1 }, new_sprite, *mouseSprite);
}

void Map::changeSpriteProperties(SpriteRef spriteRef, Sprite new_sprite, Sprite old_sprite) //TODO: inline
{
	undo_buffer->PushAndDo<actions::SpritePropertiesChange>(old_sprite, new_sprite, spriteRef);
}

void Map::screenshot(const TCHAR* filename)
{
	SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target
	SDL_SetRenderTarget(sdl_renderer, editor_state == EditorState::Minimap ? minimap->image : nullptr);

	if (PrintDmodScreen(filename))
		MB_ShowGeneric(_T("Screenshot generated in the dmod folder."), _T("WDEP2 - Success"), MB_OK | MB_ICONINFORMATION);
	else
		MB_ShowError(_T("Screenshot could not be generated."));

	SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target
}

void Map::screenshotV2(const CString& filename, SpriteRenderOptions options)
{
    SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target


    // create temporary screen texture to render each screen to
    SDL_Texture* screen_texture = SDL_CreateTexture(sdl_renderer, SDL_GetWindowPixelFormat(sdl_window), SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);

    if (screen_texture == nullptr)
    {
        CString msg = CString(_T("Could not initialize texture for screenshot rendering..."));
        Log_Error(msg);
        MB_ShowError(msg);
        return;
    }

    const int32_t pixel_format = SDL_PIXELFORMAT_BGR24;
    const int32_t map_width_px = SCREEN_WIDTH * MAP_COLUMNS;
    const int32_t map_height_px = SCREEN_HEIGHT * MAP_ROWS;
    const int32_t byte_count = map_width_px * map_height_px * SDL_BYTESPERPIXEL(pixel_format);

    byte * pixels = new byte[byte_count];
    const int32_t map_surface_pitch = map_width_px * SDL_BYTESPERPIXEL(pixel_format);
    
    SDL_Surface* map_surface = SDL_CreateRGBSurfaceWithFormatFrom(pixels, map_width_px, map_height_px, SDL_BITSPERPIXEL(pixel_format), map_surface_pitch, pixel_format);

    if (map_surface == nullptr)
    {
        SDL_DestroyTexture(screen_texture);
        
        CString msg = CString(_T("Could not initialize texture for screenshot rendering..."));
        Log_Error(msg);
        MB_ShowError(msg);
        return;
    }



    SDL_Color no_screen = {0,0,0,0};
    SDL_Rect screen_sdl_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
    RECT screen_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

    // we will now render screens to the temp screen texture
    SDL_SetRenderTarget(sdl_renderer, screen_texture);

    int32_t scr_idx = 0;
    mainWnd->m_wndStatusBar.SetRange(1,768);

    const int32_t screen_byte_width = SCREEN_WIDTH * SDL_BYTESPERPIXEL(pixel_format);
    
    for (int32_t map_y = 0; map_y < MAP_ROWS; map_y++)
    {
        byte * pixel_row_ptr = static_cast<byte*>(map_surface->pixels);
        pixel_row_ptr += map_y * SCREEN_HEIGHT * map_surface_pitch;
        byte * pixel_scr_ptr = pixel_row_ptr;
        
        for (int32_t map_x = 0; map_x < MAP_COLUMNS; map_x++, scr_idx++)
        {
            drawClearAll(no_screen);
            
            if (screen[scr_idx] != nullptr &&
                (options.renderIndoor || indoor[scr_idx] == 0))
            {
                screen[scr_idx]->drawTiles(0, 0, &screen_rect);
                screen[scr_idx]->drawSprites(0, 0, screen_rect, scr_idx, &options);
            }

            // render texture to surface
            SDL_RenderReadPixels(sdl_renderer, &screen_sdl_rect, pixel_format, pixel_scr_ptr, map_surface_pitch);

            // go right 1 screen
            pixel_scr_ptr += screen_byte_width;

            mainWnd->m_wndStatusBar.OnProgress(scr_idx);
            mainWnd->m_wndStatusBar.OnProgress(scr_idx-1);	//RW: ugly hack to keep Windows Vista/7 progress bar correctly updated.
            mainWnd->m_wndStatusBar.OnProgress(scr_idx);
        }
    }
    
    SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target
    SDL_DestroyTexture(screen_texture);             // we're done with the texture

    std::string utf8_filename = cs2s(filename);
    // NOTE: IMG_SavePNG fails for an image of this size... it seems it only works for something up to 1/4th of the dink map size!
    //if (IMG_SavePNG_RW(map_surface, SDL_RWFromFile(utf8_filename.c_str(), "wb"), SDL_TRUE) != 0)
    if (SDL_SaveBMP_RW(map_surface, SDL_RWFromFile(utf8_filename.c_str(), "wb"), SDL_TRUE) != 0)
    {
        const char * err = SDL_GetError();
        SDL_FreeSurface(map_surface);
        mainWnd->setStatusText(_T("Failed to render Screenshot!!!"));
        MB_ShowError(_T("Failed to render screenshot!"));
    } else
    {
        mainWnd->setStatusText(_T("Finished Rendering Screenshot!"));
    }

    delete pixels;
}

//function used to draw one screen on the minimap
void Map::drawScreen(int cur_screen)
{
	if (miniupdated[cur_screen] && fast_minimap_update)
		return;

	SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target

	//rects for where to draw and from where
	RECT src_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT}; //set for whole buffer
	RECT dst_rect;

	//calculate where the screen is on the minimap row and col
	int row_num = (cur_screen / MAP_COLUMNS) + 1;
	int col_num = (cur_screen % MAP_COLUMNS) + 1;

	//set the rect to the screen on the minimap
	dst_rect.left = (col_num - 1) * square_width;
	dst_rect.right = col_num * square_width;
	dst_rect.top = (row_num - 1) * square_height;
	dst_rect.bottom = row_num * square_height;

	SDL_SetRenderTarget(sdl_renderer, temp_texture);
	drawClearAll(COLOR_BLACK);

	//if the screen is actually there, draw tiles and sprite
	if (screen[cur_screen] != nullptr)
	{
		screen[cur_screen]->drawTiles(0, 0);
		screen[cur_screen]->drawSprites(0, 0, src_rect);
	}
	miniupdated[cur_screen] = true;

	//blit the buffer to the minimap
	SDL_SetRenderTarget(sdl_renderer, minimap->image);
	drawTexture(temp_texture, &src_rect, &dst_rect);

	SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target
}

// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// Screen stuff
// -------------------------------------------------------------------------------------------------------------------------------------------------------------


// creates a new blank screen on the map
bool Map::tryDoScreenCreate(int screenNum)
{
	if (screenNum >= 0 && screenNum < NUM_SCREENS && screen[screenNum] == nullptr)
	{
		undo_buffer->PushAndDo<actions::ScreenCreate>(screenNum);
		return true;
	}
	return false;
}

// deletes the screen from the map
bool Map::tryDoScreenDelete(int screenNum)
{
	if (screenNum >= 0 && screenNum < NUM_SCREENS && screen[screenNum] != nullptr)
	{
		undo_buffer->PushAndDo<actions::ScreenDelete>(screenNum, new Screen(screen[screenNum]), midi_num[screenNum], indoor[screenNum]);
		return true;
	}
	return false;
}

// places a copied screen onto the map
bool Map::tryDoScreenPaste(int screenNum, Screen* screenSource, int midiNum, bool isIndoor)
{
	if (screenNum >= 0 && screenNum < NUM_SCREENS && screen[screenNum] == nullptr && screenSource != nullptr)
	{
		undo_buffer->PushAndDo<actions::ScreenPaste>(screenNum, new Screen(screenSource), midiNum, isIndoor);
		return true;
	}
	return false;
}

void Map::changeScreenProperties(int screenNum, const TCHAR* script, int midiNum, bool isIndoor)
{
	if (screenNum >= 0 && screenNum < NUM_SCREENS && screen[screenNum] != nullptr) {
		BaseScriptFilename new_script;
		BaseScriptFilename::from_tchar(script, new_script);
		undo_buffer->PushAndDo<actions::ScreenPropertiesChange>(screenNum, midi_num[screenNum], midiNum, indoor[screenNum], isIndoor, screen[screenNum]->script, new_script);
	}
}

bool Map::tryDoScreenCopyTemp(int screenNum)
{
	screenTempNum = screenNum;
	if (screenTempNum >= 0 && screenTempNum < NUM_SCREENS && screen[screenTempNum] != nullptr)
	{
		screenTempMidiNum = midi_num[screenTempNum];
		screenTempIsInside = indoor[screenTempNum];
		return true;
	}

	screenTempClear();
	return false;
}

bool Map::tryDoScreenPasteTemp(int screenNum)
{
	if (screenTempNum >= 0 && screenTempNum < NUM_SCREENS && screen[screenTempNum] != nullptr)
		return tryDoScreenPaste(screenNum, screen[screenTempNum], screenTempMidiNum, screenTempIsInside);
	return false;
}

void Map::screenTempClear()
{
	screenTempNum = -1;
	screenTempMidiNum = -1;
	screenTempIsInside = false;
}



// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// Multisprite stuff
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

bool Map::tryDoMultispritePickup(bool pickupCopy)
{
	// can be performed if in screen - multisprite mode, there currently are selected sprites but no sprites are selected in the cursor
	if (editor_state == EditorState::Screen &&
		screenMode == EditorSubmode::MultiSprite &&
		!selectedSprites.empty() &&
		mouseMultiSprites.empty())
	{
		undo_buffer->PushAndDo<actions::MultiSpritePickup>(pickupCopy);
		return true;
	}
	return false;
}

bool Map::tryDoMultispriteClearSelection()
{
	// can be performed if in screen - multisprite mode and there currently are selected sprites
	if (editor_state == EditorState::Screen &&
		screenMode == EditorSubmode::MultiSprite &&
		!selectedSprites.empty())
	{
		undo_buffer->PushAndDo<actions::MultiSpriteClearSelected>();
		return true;
	}
	return false;
}

bool Map::tryDoMultispriteDeleteSelection()
{
	// can be performed if in screen - multisprite mode and there currently are selected sprites
	if (editor_state == EditorState::Screen &&
		screenMode == EditorSubmode::MultiSprite &&
		!selectedSprites.empty())
	{
		undo_buffer->PushAndDo<actions::MultiSpriteDelete>(selectedSprites);
		return true;
	}
	return false;
}


bool Map::tryDoMultispritePlaceMouseSelection()
{
	// can be performed if in screen - multisprite mode and there currently are selected sprites
	if (editor_state == EditorState::Screen &&
		screenMode == EditorSubmode::MultiSprite &&
		screen[hoverScreen] != nullptr &&
		!mouseMultiSprites.empty())
	{
		updateHoverPosition(mouse_x_position, mouse_y_position);
		undo_buffer->PushAndDo<actions::MultiSpritePlace>();
		return true;
	}
	return false;
}



// Toggles the selected state of one sprite in multi-selection mode
bool Map::tryDoMultispriteSelectToggle()
{
	if (screen[hoverScreen] == nullptr)
		return false;
	
	int num;
	Sprite* s = screen[hoverScreen]->getSpriteAtXy(hoverRelX, hoverRelY, num);
	if (s == nullptr)
		return false;
	
	undo_buffer->PushAndDo<actions::MultiSpriteToggleSelect>(SpriteRef{ this, hoverScreen, num });
	return true;
}

bool Map::tryDoMultispriteDeselect(SpriteRef spriteRef)
{
	auto it = find(selectedSprites.begin(), selectedSprites.end(), spriteRef);
	if (it == selectedSprites.end())
		return false;

	std::swap(*it, *(selectedSprites.end()-1));
	selectedSprites.pop_back();
	return true;
}

bool Map::isSelectedMultisprite(SpriteRef spriteRef)
{
	return find(selectedSprites.begin(), selectedSprites.end(), spriteRef) != selectedSprites.end();
}


// -------------------------------------------------------------------------------------------------------------------------------------------------------------
// Generic Edit commands
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

void Map::cmdEditCopy()
{
	if (editor_state == EditorState::Minimap)
	{
		if (tryDoScreenCopyTemp(minimap->getHoverScreen()))
		{
			mainWnd->setStatusText(_T("Copied minimap screen data!"));
		}
	}
	else if (editor_state == EditorState::Screen)
	{
		if (screenMode == EditorSubmode::Sprite)
		{
			if (current_map->getMouseSprite() == nullptr)
			{
				current_map->tryDoSpritePickup(false);
			}
		}
		else if (screenMode == EditorSubmode::MultiSprite) tryDoMultispritePickup(true);
		else if (screenMode == EditorSubmode::Tile) tile_selector.storeScreenTiles();
		else if (screenMode == EditorSubmode::Hardbox) hard_tile_selector.storeScreenTiles();
	} else if (editor_state == EditorState::TileSelector)
	{
		if (screenMode == EditorSubmode::Tile) current_map->tile_selector.storeTileSheetTiles();
	}
}
bool Map::canCmdEditCopy()
{
	return editor_state == EditorState::Minimap || editor_state == EditorState::Screen || editor_state == EditorState::TileSelector;
}
void Map::cmdEditCut()
{
	if (editor_state == EditorState::Screen)
	{
		if (screenMode == EditorSubmode::Sprite)
		{
			if (current_map->getMouseSprite() == nullptr)
			{
				current_map->tryDoSpritePickup(true);
			}
		}
		else if (screenMode == EditorSubmode::MultiSprite) tryDoMultispritePickup(false);
	}
}
bool Map::canCmdEditCut()
{
	return editor_state == EditorState::Screen;
}
void Map::cmdEditPaste()
{
	if (editor_state == EditorState::Minimap)
	{
		tryDoScreenPasteTemp(minimap->getHoverScreen());
	} else if (editor_state == EditorState::Screen)
	{
		if (screenMode == EditorSubmode::Sprite)
		{
			if (current_map->getMouseSprite() != nullptr)
			{
				current_map->placeSprite(mouse_x_position, mouse_y_position, false);
			}
		}
		else if (screenMode == EditorSubmode::MultiSprite) tryDoMultispritePlaceMouseSelection();
		else if (screenMode == EditorSubmode::Tile) tile_selector.tryDoPlaceTiles();
		else if (screenMode == EditorSubmode::Hardbox) hard_tile_selector.tryDoPlaceTiles();
	}
}
bool Map::canCmdEditPaste()
{
	return editor_state == EditorState::Screen || editor_state == EditorState::Minimap;
}

void Map::cmdEditUndo()
{
	if (editor_state == EditorState::Screen ||
		editor_state == EditorState::Minimap ||
		editor_state == EditorState::TileSelector)
	{
		undo_buffer->Undo();
	}
}
bool Map::canCmdEditUndo()
{
	return undo_buffer->CanUndo();
}

void Map::cmdEditRedo()
{
	if (editor_state == EditorState::Screen ||
		editor_state == EditorState::Minimap ||
		editor_state == EditorState::TileSelector)
	{
		undo_buffer->Redo();
	}
}
bool Map::canCmdEditRedo()
{
	return undo_buffer->CanRedo();
}

bool Map::bindViewRect(RECT & view_rect) const
{
    if (view_rect.left > view_rect.right)
        std::swap(view_rect.left, view_rect.right);

    if (view_rect.top > view_rect.bottom)
        std::swap(view_rect.top, view_rect.bottom);

    POINT tl = {window.left + view_rect.left, window.top + view_rect.top};
    POINT br = {window.left + view_rect.right, window.top + view_rect.bottom};

    bool onScreenTl = bindMapPoint(tl);
    bool onScreenBr = bindMapPoint(br);

    view_rect.left = tl.x;
    view_rect.top = tl.y;
    view_rect.right = br.x;
    view_rect.bottom = br.y;

    return onScreenTl || onScreenBr;
}


bool Map::bindMapRect(RECT & view_rect) const
{
    if (view_rect.left > view_rect.right)
        std::swap(view_rect.left, view_rect.right);

    if (view_rect.top > view_rect.bottom)
        std::swap(view_rect.top, view_rect.bottom);

    POINT tl = {view_rect.left, view_rect.top};
    POINT br = {view_rect.right, view_rect.bottom};

    bool onScreenTl = bindMapPoint(tl);
    bool onScreenBr = bindMapPoint(br);

    view_rect.left = tl.x;
    view_rect.top = tl.y;
    view_rect.right = br.x;
    view_rect.bottom = br.y;

    return onScreenTl || onScreenBr;
}

bool Map::bindViewPoint(POINT & view_point) const
{
    bool onScreen = true;
    
    int x = window.left + view_point.x;
    if (x < MIN_MAP_X)
    {
        x = MIN_MAP_X;
        onScreen = false;
    }
    if (x >= MAX_MAP_X)
    {
        x = MAX_MAP_X - 1;
        onScreen = false;
    }

    int y = window.top + view_point.y;
    if (y < MIN_MAP_Y)
    {
        y = MIN_MAP_Y;
        onScreen = false;
    }
    if (y >= MAX_MAP_Y)
    {
        y = MAX_MAP_Y - 1;
        onScreen = false;
    }

    view_point.x = x;
    view_point.y = y;

    return onScreen;
}

bool Map::bindMapPoint(POINT & view_point) const
{
    bool onScreen = true;
    
    int x = view_point.x;
    if (x < MIN_MAP_X)
    {
        x = MIN_MAP_X;
        onScreen = false;
    }
    if (x >= MAX_MAP_X)
    {
        x = MAX_MAP_X - 1;
        onScreen = false;
    }

    int y = view_point.y;
    if (y < MIN_MAP_Y)
    {
        y = MIN_MAP_Y;
        onScreen = false;
    }
    if (y >= MAX_MAP_Y)
    {
        y = MAX_MAP_Y - 1;
        onScreen = false;
    }

    view_point.x = x;
    view_point.y = y;

    return onScreen;
}


void Map::generateSpriteReport() {
    CString report_path;
    Path_ConcatIntoAndNormalize(report_path, dmod_path, "sprite_report.txt");

    std::ofstream report;
    report.open(report_path, std::ios::out|std::ios::trunc);

    if (report.is_open() == false) return;

    Log_PrintTime(report);
    report << "Started report...\n";

    class SPRITE_REPORT
    {
    public:
        std::string msg;
        int spriteNum;
        int spriteSeq;
        int spriteFrame;
        int spriteX;
        int spriteY;
        
        SPRITE_REPORT (int sprite_num, int sprite_seq, int sprite_frame, int sprite_x, int sprite_y, const std::string& msg )
            : spriteNum(sprite_num), spriteSeq(sprite_seq), spriteFrame(sprite_frame), spriteX(sprite_x), spriteY(sprite_y),
            msg(msg)
        {
            
        }
    }; 

    for (int screen_num = 0; screen_num < NUM_SCREENS; screen_num++)
    {
        if (screen[screen_num] != nullptr)
        {
            std::vector<SPRITE_REPORT> errors;
            
            for (int sprite_num = 0; sprite_num < MAX_SPRITES; sprite_num++)
            {
                if (screen[screen_num]->sprite[sprite_num] != nullptr)
                {
                    Sprite * sprite = screen[screen_num]->sprite[sprite_num];
                    Sequence * seq = nullptr;

                    if (sprite->script.s[0] != '\0' && sprite->warp_enabled)
                    {
                        SPRITE_REPORT spr_rep { sprite_num + 1, sprite->sequence, sprite->frame, sprite->x, sprite->y,
                            "ERROR - Sprite has both script and warp enabled! This can cause some errors to happen in DinkHD and possibly other engines too."};
                        errors.emplace_back(spr_rep);
                    }
                    
                    if (sprite->sequence < 1 || sprite->sequence >= MAX_SEQUENCES)
                    {
                        SPRITE_REPORT spr_rep { sprite_num + 1, sprite->sequence, sprite->frame, sprite->x, sprite->y,
                            "ERROR - Bad sequence data"};
                        errors.emplace_back(spr_rep);
                    } else if (sequence[sprite->sequence] == nullptr)
                    {
                        SPRITE_REPORT spr_rep { sprite_num + 1, sprite->sequence, sprite->frame, sprite->x, sprite->y,
                            "ERROR - Sequence data not found in dink.ini???"};
                        errors.emplace_back(spr_rep);
                    } else
                    {
                        seq = sequence[sprite->sequence];
                    }
                    if (seq != nullptr)
                    {
                        if (sprite->frame < 1 || sprite->frame >= MAX_FRAMES)
                        {
                            SPRITE_REPORT spr_rep { sprite_num + 1, sprite->sequence, sprite->frame, sprite->x, sprite->y,
                            "ERROR - Bad frame data"};
                            errors.emplace_back(spr_rep);
                        } else
                        {
                            RECT bounds;

                            if (seq->getBounds(sprite->frame -1, sprite->size, bounds) == false)
                            {
                                SPRITE_REPORT spr_rep { sprite_num + 1, sprite->sequence, sprite->frame, sprite->x, sprite->y,
                            "ERROR - could not determine sprite bounds?!"};
                                errors.emplace_back(spr_rep);
                            } else
                            {
                                bounds.left += sprite->x - SIDEBAR_WIDTH;
                                bounds.right += sprite->x - SIDEBAR_WIDTH;
                                bounds.top += sprite->y;
                                bounds.bottom += sprite->y;

                                if (bounds.right < 0 || bounds.left > SCREEN_WIDTH ||
                                    bounds.bottom < 0 || bounds.top > SCREEN_HEIGHT)
                                {
                                    SPRITE_REPORT spr_rep { sprite_num + 1, sprite->sequence, sprite->frame, sprite->x, sprite->y,
                                "ERROR - SPRITE OUT OF BOUNDS!"};
                                    errors.emplace_back(spr_rep);
                                }
                            }
                        }
                    }
                }
            }

            

            if (errors.empty() == false)
            {
                report << "Screen "<<screen_num + 1<<" had "<<errors.size()<< " sprite related errors!\n";
                for (const auto& err : errors)
                {
                    report << "    sprite_num=" << err.spriteNum << ", x=" <<err.spriteX<<", y="<<err.spriteY<<", seq="<<err.spriteSeq<<", frame="<<err.spriteFrame<<", "<<err.msg<<"\n";
                }
                report <<"\n";
                report <<"\n";
            }
        }
    }

    report.flush();
    report.close();

    MB_ShowGeneric(_T("Sprite report generated in the dmod folder."), _T("WDEP2 - Success"), MB_OK | MB_ICONINFORMATION);
}