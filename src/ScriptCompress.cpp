#include "StdAfx.h"
#include <stdio.h>
#include "Globals.h"
#include "Map.h"
#include <iterator>
#include "Common.h"
#include "ScriptCompress.h"

using std::ifstream;
using std::ofstream;
using std::ios;
using std::istreambuf_iterator;
using std::vector;

// NOTE: drone1400 - moved the script compress/decompress functions from Tools.cpp here
// these are some black magic as far as I'm concerned and I'm scared to touch them right now


static void ScriptCompress(TCHAR *file_name, BOOL remove_comments)  //Throws exception on error.
{
	vector<BYTE> left(TABLE_SIZE), right(TABLE_SIZE);

	ifstream stream(file_name, ios::in | ios::binary);
	if (!stream) throw 0;
	
	vector<int> big_buffer(BIG_BUFFER_SIZE);
	vector<int> mini_buffer(MINI_BUFFER_SIZE);
	
	vector<BYTE> buffer;
	buffer.insert(buffer.begin(), istreambuf_iterator<char>(stream), istreambuf_iterator<char>());
	if (stream.fail()) throw 0;
	
// remove all comments?
	int total_amount_read = buffer.size(); //TEMP!
	int i = 0, j = 0;

	if (remove_comments)
	{
		for (i = 0; j < total_amount_read - 1; i++)
		{
			if (buffer[i] == '/' && buffer[i + 1] == '/')
			{
				i += 2;
				total_amount_read -= 2;
				// skip the rest of the line
				while (j < total_amount_read)
				{
					if (buffer[i] == 13 || buffer[i] == '\n')
					{
						i--;
						break;
					}
					i++;
					total_amount_read--;
				}
			}
			else
			{
				buffer[j] = buffer[i];
				j++;
			}
		}
	}

	if (j < total_amount_read)
	{
		buffer[j] = buffer[i];
	}

// create the table now
	int max_value;
	int max_value_location;
	int used_pairs = 0;

	BYTE left_char, right_char;

	int location;
	BYTE newchar;
	
	fill(big_buffer.begin(), big_buffer.end(), 0);   // clear the big buffer
	fill(mini_buffer.begin(), mini_buffer.end(), 0); // clear the small buffer

	// count the instances of each pair
	auto sourceIt = buffer.begin();
	for (i = 0; i < total_amount_read - 1; i++)
	{
		location = ((int)*sourceIt << 8) + (int)*++sourceIt;
		
		big_buffer[location]++;
		mini_buffer[location >> 4]++;
	}

	// create the table and compress
	for (used_pairs = 0; used_pairs < TABLE_SIZE - 1; used_pairs++)
	{
		max_value = 0;
		auto bufferIt = mini_buffer.begin();
		
		// find the most common pair now
		for (i = 0; i < MINI_BUFFER_SIZE; i++)
		{
			if (*bufferIt > max_value)
			{
				for (int x = 0; x < 16; x++)
				{
					location = (i << 4) + x;
					if (big_buffer[location] > max_value)
					{
						max_value = big_buffer[location];
						max_value_location = location;
					}
				}
			}
			++bufferIt;
		}

		// add the pair to the table
		if (max_value > 2)
		{
			left_char = left[used_pairs] = max_value_location >> 8;
			right_char = right[used_pairs] = max_value_location & 0xFF;
		}
		else
			break;

		// replace all instances of that pair in the buffer
		sourceIt = buffer.begin();
		auto destIt = buffer.begin();
		newchar = used_pairs + TABLE_SIZE;
		for (i = 0; i < total_amount_read - 1; i++)
		{
			if (*sourceIt == left_char && *(sourceIt + 1) == right_char)
			{
				// remove left pair
				if (i != 0)
				{
					location = (*(sourceIt - 1) << 8) + *sourceIt;
					big_buffer[location]--;
					mini_buffer[location >> 4]--;

					// add new pair
					location = (*(sourceIt - 1) << 8) + newchar;
					big_buffer[location]++;
					mini_buffer[location >> 4]++;
				}
				// remove right pair
				if (i != total_amount_read - 2)
				{
					location = (*(sourceIt + 1) << 8) + *(sourceIt + 2);
					big_buffer[location]--;
					mini_buffer[location >> 4]--;

					// add new pair
					location = (newchar << 8) + *(sourceIt + 2);
					big_buffer[location]++;
					mini_buffer[location >> 4]++;
				}

				*destIt = newchar;
				++sourceIt;
				total_amount_read--;
			}
			else
			{
				*destIt = *sourceIt;
			}
			++sourceIt;
			++destIt;
		}

		if (i == total_amount_read - 1)
			*destIt = *sourceIt;

		// remove middle pairs
		big_buffer[max_value_location] = 0;
		mini_buffer[max_value_location >> 4] -= max_value;
	}

// write out the file now
	file_name[_tcslen(file_name) - 1] = _T('d');

	ofstream outstream(file_name, ios::out | ios::binary);
	if (!outstream) throw 0;
	
	BYTE num_pairs = used_pairs + TABLE_SIZE;

	outstream.write((char*)&num_pairs, 1);

	for (i = 0; i < used_pairs; i++)
	{
		outstream.write((char*)&left[i], 1);
		outstream.write((char*)&right[i], 1);
	}

	outstream.write((char*) buffer.data(), total_amount_read);
}

void ScriptCompressAll(BOOL delete_file, BOOL remove_comments)
{
	HANDLE hFind;
	WIN32_FIND_DATA dataFind;
	BOOL bMoreFiles = TRUE;

	TCHAR buffer[MAX_PATH];
	wsprintf(buffer, _T("%sstory\\*.c"), (LPCTSTR)current_map->dmod_path);

	hFind = FindFirstFile(buffer, &dataFind);

	int failed = 0;
	// get all the *.c files in the directory
	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)
	{
		wsprintf(buffer, _T("%sstory\\%s"), (LPCTSTR)current_map->dmod_path, dataFind.cFileName);
		try
		{
			ScriptCompress(buffer, remove_comments);
			if (delete_file)
			{
				wsprintf(buffer, _T("%sstory\\%s"), (LPCTSTR)current_map->dmod_path, dataFind.cFileName);
				_tremove(buffer);
			}
		}
		catch (...)
		{
			failed++;
		}

		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	if (failed > 0)
	{
		CString compressError;
		compressError.Format(_T("Could not compress %d files."), failed);
		MB_ShowError(compressError);
	}

	FindClose(hFind);
}

static bool ScriptDecompress(TCHAR* filename)
{
	FILE* fin = _tfopen(filename, _T("rb"));
	if (fin == nullptr)	
		return false;	

	CString cfilename = filename;
	cfilename.SetAt(cfilename.GetLength() - 1, 'c');
	FILE* fout = _tfopen((LPCTSTR)cfilename, _T("r"));
	if (fout != nullptr)
	{
		fclose(fin);
		fclose(fout);
		return false;
	}

	fout = _tfopen((LPCTSTR)cfilename, _T("wt"));
	if (fout == nullptr)
	{
		fclose(fin);
		return false;
	}
	

	//RW: byte-pair decode algorithm:
	BYTE s[16], pairs[128][2];
	int i=0, c;

	if ((c = getc(fin)) > 127)
		fread(pairs,2,c-128,fin);
	else if (c!='\r')
		fputc(c, fout);

	while (1)
	{
		if (i!=0) 
			c = s[--i];
		else if ((c = getc(fin)) == EOF)
			break;

		if (c > 127)
		{
			s[i++] = pairs[c-128][1];
			s[i++] = pairs[c-128][0];
		}
		else if (c!='\r')
			fputc(c, fout);
	}

	fclose(fout);
	fclose(fin);
	return true;
}

void ScriptDecompressAll(bool delete_files)	//RW: convert all .d files to .c
{
	HANDLE hFind;
	WIN32_FIND_DATA dataFind;
	BOOL bMoreFiles = TRUE;

	TCHAR buffer[MAX_PATH];
	wsprintf(buffer, _T("%sstory\\*.d"), (LPCTSTR)current_map->dmod_path);

	hFind = FindFirstFile(buffer, &dataFind);

	// get all the .d files in the directory
	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)
	{
		wsprintf(buffer, _T("%sstory\\%s"), (LPCTSTR)current_map->dmod_path, dataFind.cFileName);
		if (ScriptDecompress(buffer) == true)		
			if (delete_files)
			{
				wsprintf(buffer, _T("%sstory\\%s"), (LPCTSTR)current_map->dmod_path, dataFind.cFileName);
				_tremove(buffer);
			}		

		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	FindClose(hFind);
}