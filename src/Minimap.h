#pragma once

#define MINI_MAP_WIDTH		(square_width * MAP_COLUMNS)
#define MINI_MAP_HEIGHT		(square_height * MAP_ROWS)

#define SQUARE_EMPTY_NAME	_T("S06.bmp")
#define SQUARE_EMPTY_NAME_PNG	_T("S06.png")
#define SQUARE_EMPTY		0
#define SQUARE_USED_NAME	_T("S07.bmp")
#define SQUARE_USED_NAME_PNG	_T("S07.png")
#define SQUARE_USED			1
#define SQUARE_MIDI_NAME	_T("S12.bmp")
#define SQUARE_MIDI_NAME_PNG	_T("S12.png")
#define SQUARE_MIDI			2
#define SQUARE_INDOOR_NAME	_T("S13.bmp")
#define SQUARE_INDOOR_NAME_PNG	_T("S13.png")
#define SQUARE_INDOOR		3

class Map;

class Minimap
{
public:
	explicit Minimap(Map* map);
	~Minimap();

	void drawSquares(Map* map = nullptr);
	int drawMap();
	void toggleDetailing();
	void renderMapSquare(int cur_screen, Map* map = nullptr);
	int renderImportMap(int screen_order[], int midi_num[], int indoor[]);
	void render();

	bool loadSquare(CString const& dmod_path, const TCHAR *filename, int square_num);
	void loadScreen();

	void loadSurface(Map* map);
	void updateHoverPosition(int x, int y);
	void resizeScreen();

	SDL_Texture* image;
	int getHoverScreen() { return hover_screen; }

private:
	bool isDetailed;
	SDL_Texture* mapSquareImage[4];
	int hover_x_screen, hover_y_screen, hover_screen;
	double x_scale, y_scale;
};