#pragma once
#include "resource.h"

class FastfileExtract : public CDialog
{
	DECLARE_DYNAMIC(FastfileExtract)

public:
	FastfileExtract(CWnd* pParent = nullptr);   // standard constructor
	virtual ~FastfileExtract();

// Dialog Data
	enum { IDD = IDD_FFEXTRACT };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	BOOL m_delete_dirff;
	afx_msg void OnBnClickedOk();

	//returns number of errors
	int FFExtract(const TCHAR* path);
};