﻿#include "StdAfx.h"
#include "SimpleLog.h"

#include <codecvt>
#include <locale>

std::wstring s2ws(const std::string& str)
{
	using convert_typeX = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.from_bytes(str);
}

std::string ws2s(const std::wstring& wstr)
{
	using convert_typeX = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.to_bytes(wstr);
}

CString s2cs(std::string const& utf8StrIn)
{
	CString mfcStrOut;
#ifdef UNICODE
	const std::wstring wstr(s2ws(utf8StrIn));
	mfcStrOut.SetString(wstr.c_str());	
#else
	// TODO, is this actually correct?...
	mfcStrOut.SetString(utf8StrIn.c_str());
#endif
	return mfcStrOut;
}

std::string cs2s(CString const& mfcStrIn)
{
	std::string utf8StrOut;
#ifdef UNICODE
	const std::wstring wstr(mfcStrIn);
	const std::string str(ws2s(wstr));
	utf8StrOut.assign(str);
#else
	// TODO, is this actually correct?...
	utf8StrOut.assign(mfcStrIn);
#endif
	return utf8StrOut;
}