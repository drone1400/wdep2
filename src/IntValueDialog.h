#pragma once
#include "resource.h"

class CIntValueDialog : public CDialog
{
	DECLARE_DYNAMIC(CIntValueDialog)

public:
	CIntValueDialog(const TCHAR* title = _T(""), int startvalue = 0, int min = MININT, int max = MAXINT, CWnd* pParent = nullptr);   // standard constructor
	virtual ~CIntValueDialog();

// Dialog Data
	enum { IDD = IDD_INT_VALUE_DIALOG };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int m_value;
	BOOL OnInitDialog() override;

private:
	int min;
	int max;
	TCHAR title[30];
};