#include "stdafx.h"
#include "ScriptDecompressor.h"


IMPLEMENT_DYNAMIC(ScriptDecompressor, CDialog)

ScriptDecompressor::ScriptDecompressor(CWnd* pParent /*=nullptr*/)
	: CDialog(ScriptDecompressor::IDD, pParent)
	, m_delete_d_files(false)
{
}

ScriptDecompressor::~ScriptDecompressor()
{
}

void ScriptDecompressor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_DELETE_D_FILES, m_delete_d_files);
}


BEGIN_MESSAGE_MAP(ScriptDecompressor, CDialog)
END_MESSAGE_MAP()
