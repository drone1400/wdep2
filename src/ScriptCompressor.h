#pragma once
#include "resource.h"

class ScriptCompressor : public CDialog
{
public:
	ScriptCompressor(CWnd* pParent = nullptr);   // standard constructor

	enum { IDD = IDD_COMPRESS_SCRIPTS };
	BOOL	m_delete_c_files;
	BOOL	m_remove_comments;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};