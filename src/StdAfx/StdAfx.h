// stdafx.h : include file for standard system include files, or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

//define the lowest Windows version we want to support
#ifdef WDEP_WIN_XP
#define _WIN32_WINNT _WIN32_WINNT_WINXP
#define MAXINT 2147483647
#define MININT -2147483648
typedef struct IUnknown IUnknown;
#else
#define _WIN32_WINNT _WIN32_WINNT_VISTA
#endif

//exclude rarely-used stuff from Windows headers
#define VC_EXTRALEAN

//disable the warnings about old C functions being insecure
#define _CRT_SECURE_NO_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxcview.h>
#include <afxdisp.h>        // MFC Automation classes
//#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxcontrolbars.h>     // MFC support for ribbons and control bars

#define SDL_MAIN_HANDLED
#include <SDL.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <deque>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <type_traits>
#include <assert.h>
