#pragma once
#include "Globals.h"
#include "Structs.h"
#define HARD_TILE_SELECTOR_BMP_GAP  4
#define MAX_TILE_BOX_POINTS         4

class HardTileSelector
{
public:
	HardTileSelector();
	~HardTileSelector();
	int render();
	void drawHardTileSelectorGrid(int x, int y);
	int getTile(int x, int y);
	int renderHardness(int x, int y, int tileWidth, int tileHeight, int &hard_tile_num, bool transparent);
	int loadSurface(int hard_tile_num);
	void unloadSurface(int hard_tile_num);
	int updateScreenGrid(int x, int y);
	int updateScreenGrid(const RECT &mouse_box);
	int revertTile();
	int tryDoPlaceTiles();
	int storeScreenTiles();
	int renderScreenTileHover(int x, int y);
	int nextPage();
	int prevPage();
	int resizeScreen();
	int screenRender();
	void hardTileEditorPrepareTile(int hardIndex, TILEDATA tileData);
	void hardTileEditorLoadFromScreen();
	void hardTileEditorSaveAndReturn();
	void hardTileEditorDiscardAndReturn();
	void hardTileEditorClearAllHardness();
	void hardTileEditorRender(int x, int y, BOOL enableTileTexture);
	
	void TransformHardTile(BYTE hardness);
	void placeTileBoxPoint(int x, int y);
	void placeTileBox();
	void unloadAllBitmaps();
	void SetDefaultHardTile();

	TileHardness hardTile[NUM_HARD_TILES];
	

	int tile_box_points;

	// selection data
	void getSelectionSize(int& xTiles, int& yTiles);
	void getSelectedTileAt(int x, int y, int& hardIndex);
	void getCurrentTileData(TILEDATA& data);
	void getCurrentTileHardIndex(int& hardIndex);

private:
	void drawHardTileToTempTexture(BOOL enableTileTexture);
	
	int selectorScreen;
	
	int tile_box_x[MAX_TILE_BOX_POINTS], tile_box_y[MAX_TILE_BOX_POINTS];

	int tiles_per_row, tiles_per_column;
	int pics_displayed;
	int max_pics;

	// selection data
	int hard_tile_set[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2];
	int x_tiles_selected;
	int y_tiles_selected;
	
	// grid for screenmode
	int screen_first_x_tile, screen_last_x_tile, screen_first_y_tile, screen_last_y_tile;

	int cur_hard_num, drag_hardness;
	TILEDATA cur_tile;
	TileHardness cur_hardness;

	bool anchorRight, anchorBottom;

	SDL_Texture* hardTileSurface[NUM_HARD_TILES];
	TILEDATA tile;
};