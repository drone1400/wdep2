#include "stdafx.h"
#include "BrainDialog.h"

IMPLEMENT_DYNAMIC(CBrainDialog, CDialog)

CBrainDialog::CBrainDialog(int start_brain, CWnd* pParent /*=nullptr*/)
	: CDialog(CBrainDialog::IDD, pParent)
{
	m_brain = start_brain;
}

CBrainDialog::~CBrainDialog()
{
}

void CBrainDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_Custom_Brain, m_brain);
	DDV_MinMaxInt(pDX, m_brain, 0, MAXINT);
	DDX_Control(pDX, IDC_BRAIN, m_brainDropdown);
	DDX_Control(pDX, IDC_Custom_Brain, m_brainTextbox);
}


BEGIN_MESSAGE_MAP(CBrainDialog, CDialog)
	ON_CBN_SELCHANGE(IDC_BRAIN, &CBrainDialog::OnCbnSelchangeBrain)
END_MESSAGE_MAP()

void CBrainDialog::setBrain(int brain)
{
	m_brain = brain;

	if (brain == -1)
	{
		m_brainDropdown.SetCurSel(-1); //deselect to signify that different sprites have different brains currently
		m_brainTextbox.EnableWindow(FALSE);
	}
	else if (brain >= 0 && brain < 18)
	{
		m_brainDropdown.SetCurSel(brain);
		m_brainTextbox.EnableWindow(FALSE);
	}
	else
	{
		m_brainDropdown.SetCurSel(18); //"custom"
		m_brainTextbox.EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}

BOOL CBrainDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	setBrain(m_brain);
	if (m_brain >= 0 && m_brain < 18)
		m_brainDropdown.SetFocus();
	else
		m_brainTextbox.SetFocus();

	return FALSE;
}

void CBrainDialog::OnCbnSelchangeBrain()
{
	int sel = m_brainDropdown.GetCurSel();
	if (sel == CB_ERR)
		return;

	setBrain(sel);
}
