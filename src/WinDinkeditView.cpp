// WinDinkeditView.cpp : implementation of the CWinDinkeditView class
#include "stdafx.h"
#include "WinDinkeditDoc.h"
#include "WinDinkeditView.h"
#include "MainFrm.h"
#include "Engine.h"
#include "Globals.h"
#include "Map.h"
#include "Screen.h"
#include "SpriteProperties.h"
#include "ScreenProperties.h"
#include "Interface.h"
#include "Structs.h"
#include "SpriteLibrary.h"
#include "SpriteLibraryEntry.h"
#include "Sprite.h"
#include "IntValueDialog.h"
#include "ScriptDialog.h"
#include "BrainDialog.h"
#include "Actions/SpritePropertyChange.h"
#include "Actions/MultiSpritePropertyChange.h"


IMPLEMENT_DYNCREATE(CWinDinkeditView, CView)

BEGIN_MESSAGE_MAP(CWinDinkeditView, CView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
#ifndef WDEP_WIN_XP
	ON_WM_MOUSEHWHEEL()
#endif
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_COMMAND(ID_SCREEN_PROPERTIES, OnScreenProperties)
	ON_COMMAND(ID_DELETE_SCREEN, OnDeleteScreen)
	ON_COMMAND(IDM_SPRITE_PROPERTIES, OnSpriteProperties)
	ON_COMMAND(IDM_NEW_SCREEN, OnNewScreen)
	ON_COMMAND(IDM_VIEW_TILE_MODE, OnViewTileMode)
	ON_COMMAND(IDM_VIEW_TILESET_1, OnViewTileset1)
	ON_COMMAND(IDM_VIEW_TILESET_2, OnViewTileset2)
	ON_COMMAND(IDM_VIEW_TILESET_3, OnViewTileset3)
	ON_COMMAND(IDM_VIEW_TILESET_4, OnViewTileset4)
	ON_COMMAND(IDM_VIEW_TILESET_5, OnViewTileset5)
	ON_COMMAND(IDM_VIEW_TILESET_6, OnViewTileset6)
	ON_COMMAND(IDM_VIEW_TILESET_7, OnViewTileset7)
	ON_COMMAND(IDM_VIEW_TILESET_8, OnViewTileset8)
	ON_COMMAND(IDM_VIEW_TILESET_9, OnViewTileset9)
	ON_COMMAND(IDM_VIEW_TILESET_10, OnViewTileset10)
	ON_COMMAND(IDM_VIEW_TILESET_11, OnViewTileset11)
	ON_COMMAND(IDM_VIEW_HARDBOX_MODE, OnViewHardboxMode)
	ON_COMMAND(IDM_SET_WARP_BEGIN, OnSetWarpBegin)
	ON_COMMAND(IDM_SET_WARP_END, OnSetWarpEnd)
	ON_COMMAND(IDM_STORE_SPRITE, OnStoreSprite)
	ON_COMMAND(IDM_COPY_SCREEN, OnCopyScreen)
	ON_COMMAND(IDM_PASTE_SCREEN, OnPasteScreen)
	ON_COMMAND(IDM_REVERT_HARD_TILE, OnRevertHardTile)
	ON_COMMAND(IDM_TRANSFORM_HARDNESS_NORMAL, OnTransformHardnessNormal)
	ON_COMMAND(IDM_TRANSFORM_HARDNESS_LOW, OnTransformHardnessLow)
	ON_COMMAND(IDM_TRANSFORM_HARDNESS_UNKNOWN, OnTransformHardnessUnknown)
	ON_COMMAND(IDM_SET_DEFAULT_HARD_TILE, OnSetDefaultHardTile)
	ON_COMMAND(IDM_EDIT_HARDNESS, OnEditHardTile)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_TILESET_1, &CWinDinkeditView::OnUpdateViewTileset1)
	ON_COMMAND(ID_SPRITEMENU_HARD, &CWinDinkeditView::OnSpritemenuHard)
	ON_COMMAND(ID_SPRITEMENU_NOHIT, &CWinDinkeditView::OnSpritemenuNohit)
	ON_COMMAND(ID_MULTISPRITE_HARD, &CWinDinkeditView::OnMultispriteHard)
	ON_UPDATE_COMMAND_UI(ID_MULTISPRITE_HARD, &CWinDinkeditView::OnUpdateMultispriteHard)
	ON_COMMAND(ID_MULTISPRITE_NOHIT, &CWinDinkeditView::OnMultispriteNohit)
	ON_UPDATE_COMMAND_UI(ID_MULTISPRITE_NOHIT, &CWinDinkeditView::OnUpdateMultispriteNohit)
	ON_COMMAND(ID_MULTISPRITE_BACKGROUND, &CWinDinkeditView::OnMultispriteBackground)
	ON_UPDATE_COMMAND_UI(ID_MULTISPRITE_BACKGROUND, &CWinDinkeditView::OnUpdateMultispriteBackground)
	ON_COMMAND(ID_MULTISPRITE_FOREGROUND, &CWinDinkeditView::OnMultispriteForeground)
	ON_UPDATE_COMMAND_UI(ID_MULTISPRITE_FOREGROUND, &CWinDinkeditView::OnUpdateMultispriteForeground)
	ON_COMMAND(ID_MULTISPRITE_INVISIBLE, &CWinDinkeditView::OnMultispriteInvisible)
	ON_UPDATE_COMMAND_UI(ID_MULTISPRITE_INVISIBLE, &CWinDinkeditView::OnUpdateMultispriteInvisible)
	ON_COMMAND(ID_MULTISPRITE_BRAIN, &CWinDinkeditView::OnMultispriteBrain)
	ON_COMMAND(ID_MULTISPRITE_TOUCHDAMAGE, &CWinDinkeditView::OnMultispriteTouchdamage)
	ON_COMMAND(ID_MULTISPRITE_VISION, &CWinDinkeditView::OnMultispriteVision)
	ON_COMMAND(ID_MULTISPRITE_SIZE, &CWinDinkeditView::OnMultispriteSize)
	ON_COMMAND(ID_MULTISPRITE_SCRIPT, &CWinDinkeditView::OnMultispriteScript)
	ON_COMMAND(ID_MODE_MULTISPRITE, &CWinDinkeditView::OnModeMultisprite)
END_MESSAGE_MAP()


CWinDinkeditView::CWinDinkeditView()
{
	// TODO: add construction code here
}

CWinDinkeditView::~CWinDinkeditView()
{
	if (current_map)
	{
		delete current_map;
		current_map = nullptr;
	}
}

BOOL CWinDinkeditView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}


void CWinDinkeditView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

//	view_window = this;
	// TODO: You may populate your ListView with items by directly accessing its list control through a call to GetListCtrl().
}


#ifdef _DEBUG
void CWinDinkeditView::AssertValid() const
{
	CView::AssertValid();
}

void CWinDinkeditView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CWinDinkeditDoc* CWinDinkeditView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CWinDinkeditDoc)));
	return (CWinDinkeditDoc*)m_pDocument;
}
#endif //_DEBUG


void CWinDinkeditView::gainFocus()
{
	SetFocus();
}


void CWinDinkeditView::OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct)
{
	//TODO: add code to react to the user changing the view style of your window
}

void CWinDinkeditView::OnMButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	mouseMiddlePressed(point.x, point.y);

	CView::OnMButtonDown(nFlags, point);
}

void CWinDinkeditView::OnMButtonUp(UINT nFlags, CPoint point) 
{
	mouseMiddleReleased(point.x, point.y);

	CView::OnMButtonUp(nFlags, point);
}

void CWinDinkeditView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	mouseLeftPressed(point.x, point.y);
	
	CView::OnLButtonDown(nFlags, point);
}

void CWinDinkeditView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	mouseLeftReleased(point.x, point.y);

	CView::OnLButtonUp(nFlags, point);
}

void CWinDinkeditView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	mouseLeftDoubleClick(point.x, point.y);
	
	CView::OnLButtonDblClk(nFlags, point);
}

void CWinDinkeditView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	mouseRightPressed(point.x, point.y, this);
	CView::OnRButtonDown(nFlags, point);
}

void CWinDinkeditView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CView::OnRButtonUp(nFlags, point);
}

void CWinDinkeditView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	//snap to grid
	if (snapto_grid == true)
	{
		point.x = ((int)(point.x/snapto_offset) * snapto_offset);
		point.y = ((int)(point.y/snapto_offset) * snapto_offset);
	}

	mouseMove(point.x, point.y);

	CView::OnMouseMove(nFlags, point);
}


void CWinDinkeditView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	keyPressed((BYTE)nChar);

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CWinDinkeditView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{	
	keyReleased((BYTE)nChar);

	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CWinDinkeditView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

	GetWindowRect(&mapRect);
	need_resizing = true;
}

void CWinDinkeditView::OnMove(int x, int y) 
{
	CView::OnMove(x, y);
	
	// TODO: Add your message handler code here
	GetWindowRect(&mapRect);
}

// called from mainframe
int CWinDinkeditView::windowMoved(int x, int y)
{
	GetWindowRect(&mapRect);

	return true;
}

// context menus
Sprite* sprite_clicked = nullptr;
int screen_num_clicked = 0;
int sprite_num_clicked = 0;
Sprite* begin_warp = nullptr;

void CWinDinkeditView::displaySpriteMenu(Sprite* sprite, int screen_num, int sprite_num, int x, int y)
{
	sprite_clicked = sprite;
	screen_num_clicked = screen_num;
	sprite_num_clicked = sprite_num;

	CMenu menuPopup;
	menuPopup.LoadMenu(IDR_SPRITE_MENU);
	
	CMenu *submenu = menuPopup.GetSubMenu(0);

	//don't allow begin if we have a mousesprite
	if (current_map->getMouseSprite())
	{
		//gray out the menu for 'set warp begin', 'Hard', 'Nohit'.
		submenu->EnableMenuItem(IDM_SET_WARP_BEGIN, MF_GRAYED);

		submenu->EnableMenuItem(ID_SPRITEMENU_HARD, MF_GRAYED);
		submenu->EnableMenuItem(ID_SPRITEMENU_NOHIT, MF_GRAYED);
	}
	else
	{
		//only allow to set warp end if possible
		if (begin_warp == nullptr)
			submenu->EnableMenuItem(IDM_SET_WARP_END, MF_GRAYED);

	    Sprite * hover_sprite = current_map != nullptr ? current_map->getHoverSprite() : nullptr;
		if (hover_sprite != nullptr)
		{
			submenu->EnableMenuItem(ID_SPRITEMENU_HARD, MF_ENABLED);
			submenu->CheckMenuItem(ID_SPRITEMENU_HARD, (!sprite->hardness) ? MF_CHECKED : MF_UNCHECKED);
			submenu->EnableMenuItem(ID_SPRITEMENU_NOHIT, MF_ENABLED);
			submenu->CheckMenuItem(ID_SPRITEMENU_NOHIT, (sprite->nohit) ? MF_CHECKED : MF_UNCHECKED);
		}
		else
		{
			submenu->EnableMenuItem(ID_SPRITEMENU_HARD, MF_GRAYED);
			submenu->EnableMenuItem(ID_SPRITEMENU_NOHIT, MF_GRAYED);
		}
	}

	submenu->TrackPopupMenu(TPM_LEFTALIGN, x + mapRect.left, y + mapRect.top, this);
}

void CWinDinkeditView::displayScreenMenu(int screen, int x, int y)
{
	screen_num_clicked = screen;

	if (current_map->screen[screen_num_clicked] == nullptr)
	{
		CMenu menuPopup;
		menuPopup.LoadMenu(IDR_NEW_SCREEN);
		menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, x + mapRect.left, y + mapRect.top, this);
	}
	else
	{
		//use different menu if in hardness mode
		if (current_map->screenMode == EditorSubmode::Hardbox)
		{
			//click the mouse so we only have one tile selected.
			mouseLeftPressed(x,y);
			mouseLeftReleased(x,y);

			//load the hard box menu into CMenu Variable
			CMenu menuPopup;
			menuPopup.LoadMenu(IDR_HARD_MENU);

			//lets check if the tile can be reverted. If not, don't display the menu

			//get the current tile
		    POINT point {x + current_map->window.left, y + current_map->window.top};
		    bool bind_ok = current_map->bindMapPoint(point);
			int x_tile = (point.x % SCREEN_WIDTH_GAP) / TILEWIDTH;
			int y_tile = (point.y % SCREEN_HEIGHT_GAP) / TILEHEIGHT;

			//if there isn't a alternate hardness, it can't be reverted to default.
			if (bind_ok == false || current_map->screen[screen_num_clicked]->tiles[y_tile][x_tile].alt_hardness == 0)
			{
				//now, load the sub menu into the pointer
				CMenu *submenu;
				submenu = menuPopup.GetSubMenu(0);

				//now, gray out the menu for 'revert hard tile'.
				submenu->EnableMenuItem(IDM_REVERT_HARD_TILE,MF_BYCOMMAND|MF_GRAYED);
			} else
			// if there is an alternate hardness, we can't set default...
			{
			    CMenu *submenu;
			    submenu = menuPopup.GetSubMenu(0);
			    submenu->EnableMenuItem(IDM_SET_DEFAULT_HARD_TILE, MF_BYCOMMAND|MF_GRAYED);
			}
			
			//only allow if possible to set warp end.
			if (begin_warp == nullptr || current_map->screenMode != EditorSubmode::Sprite)
			{
				//now, load the sub menu into the pointer
				CMenu *submenu;
				submenu = menuPopup.GetSubMenu(0);

				//now, gray out the menu for 'set warp end'.
				submenu->EnableMenuItem(IDM_SET_WARP_END,MF_BYCOMMAND|MF_GRAYED);
			}

			//display menu and track what the user clicks.
			menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, x + mapRect.left, y + mapRect.top, this);
		} 
		else 
		{
			CMenu menuPopup;
			menuPopup.LoadMenu(IDR_SCREEN_MENU);

			//only allow if possible to set warp end.
			if (begin_warp == nullptr || current_map->screenMode != EditorSubmode::Sprite)
			{
				//now, load the sub menu into the pointer
				CMenu *submenu;
				submenu = menuPopup.GetSubMenu(0);

				//now, gray out the menu for 'set warp end'.
				submenu->EnableMenuItem(IDM_SET_WARP_END,MF_BYCOMMAND|MF_GRAYED);
			}
			menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, x + mapRect.left, y + mapRect.top, this);
		}
	}
}

void CWinDinkeditView::displayMultiSpriteMenu(int x, int y)
{
	CMenu menu;
	menu.LoadMenu(IDR_MULTI_SPRITE_MENU);
	menu.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, x + mapRect.left, y + mapRect.top, mainWnd);
}

void CWinDinkeditView::OnSpriteProperties() 
{	
	Sprite temp_sprite = *sprite_clicked;
	SpriteProperties dlg(&temp_sprite, this);
	
	auto result = dlg.DoModal();
	if (result == IDOK)
	{
		current_map->changeSpriteProperties(SpriteRef{ current_map, screen_num_clicked, sprite_num_clicked }, temp_sprite, *sprite_clicked);
		current_map->miniupdated[screen_num_clicked] = false;
	}
}

void CWinDinkeditView::OnStoreSprite() 
{
	// create the dialog box
	SpriteLibraryEntry newdlg(this);

	// now show the dialog box
	int retcode = newdlg.DoModal();

	// check if ok or cancel was pressed
	if (retcode == IDOK)
		current_map->sprite_library.addSprite(sprite_clicked, static_cast<LPCTSTR>(newdlg.m_sprite_name));
}

void CWinDinkeditView::OnViewTileMode() 
{
    if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
    {
        current_map->screenMode = EditorSubmode::Tile;
    }
}

void CWinDinkeditView::OnViewHardboxMode() 
{
    if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
    {
        current_map->screenMode = EditorSubmode::Hardbox;
    }
}

void CWinDinkeditView::OnModeMultisprite()
{
    if (current_map != nullptr && current_map->editor_state == EditorState::Screen)
    {
        current_map->screenMode = EditorSubmode::MultiSprite;
    }
}

void CWinDinkeditView::OnViewTileset1() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('1');
	}
}

void CWinDinkeditView::OnViewTileset2() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('2');
	}
}

void CWinDinkeditView::OnViewTileset3() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('3');
	}
}

void CWinDinkeditView::OnViewTileset4() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('4');
	}
}

void CWinDinkeditView::OnViewTileset5() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('5');
	}
}

void CWinDinkeditView::OnViewTileset6() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('6');
	}
}

void CWinDinkeditView::OnViewTileset7() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('7');
	}
}

void CWinDinkeditView::OnViewTileset8() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('8');
	}
}

void CWinDinkeditView::OnViewTileset9() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('9');
	}
}

void CWinDinkeditView::OnViewTileset10() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('0');
	}
}

void CWinDinkeditView::OnViewTileset11() 
{
	if(current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)
	{
	    keyEvalTileSelectorPage('U');
	}
}

void CWinDinkeditView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	GetWindowRect(&mapRect);
	Game_Main();
}

void CWinDinkeditView::OnSetWarpBegin() 
{
	begin_warp = sprite_clicked;
}

void CWinDinkeditView::OnSetWarpEnd() 
{
	if (begin_warp != nullptr)
	{
		if (current_map->screen[screen_num_clicked] != nullptr)
		{
			begin_warp->warp_screen = screen_num_clicked + 1;
			begin_warp->warp_x = current_map->hoverRelX + SIDEBAR_WIDTH;
			begin_warp->warp_y = current_map->hoverRelY;
			begin_warp->warp_enabled = true;
			begin_warp->hardness = 0;
		}
	}	
}


BOOL CWinDinkeditView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	if (sizeScroll.cy > 0)
	{
		keyPressed(VK_DOWN);
		keyReleased(VK_DOWN);
	}

	if (sizeScroll.cy < 0)
	{
		keyPressed(VK_UP);
		keyReleased(VK_UP);
	}
	
	return CView::OnScrollBy(sizeScroll, bDoScroll);
}

BOOL CWinDinkeditView::OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll) 
{
	// TODO: Add your specialized code here and/or call the base class 
	
	return CView::OnScroll(SB_LINEDOWN | SB_LINEUP , nPos, TRUE);
}


#include <fstream>
using std::ifstream;
using std::ofstream;

void CWinDinkeditView::OnNewScreen() 
{
	current_map->tryDoScreenCreate(screen_num_clicked);
}

void CWinDinkeditView::OnDeleteScreen() 
{
	current_map->tryDoScreenDelete(screen_num_clicked);
}

void CWinDinkeditView::OnScreenProperties() 
{
	if (current_map->screen[screen_num_clicked] == nullptr)
		return;

	// create the dialog box
	ScreenProperties newdlg(this);
	newdlg.m_script = current_map->screen[screen_num_clicked]->script.s;
	newdlg.m_midi = current_map->midi_num[screen_num_clicked];
	newdlg.m_inside = current_map->indoor[screen_num_clicked];

	// now show the dialog box
	int retcode = newdlg.DoModal();

	// check if ok or cancel was pressed
	if (retcode == IDOK)
	{
		current_map->changeScreenProperties(screen_num_clicked, (LPCTSTR) newdlg.m_script, newdlg.m_midi, newdlg.m_inside);
	}
}

void CWinDinkeditView::OnCopyScreen() 
{
	if (current_map != nullptr) current_map->tryDoScreenCopyTemp(screen_num_clicked);
}

void CWinDinkeditView::OnPasteScreen() 
{
	if (current_map != nullptr) current_map->tryDoScreenPasteTemp(screen_num_clicked);
}

void CWinDinkeditView::OnRevertHardTile()
{
	current_map->hard_tile_selector.revertTile();
}

void CWinDinkeditView::OnTransformHardnessNormal()
{
	current_map->hard_tile_selector.TransformHardTile(1);
}

void CWinDinkeditView::OnTransformHardnessLow()
{
	current_map->hard_tile_selector.TransformHardTile(2);
}

void CWinDinkeditView::OnTransformHardnessUnknown()
{
	current_map->hard_tile_selector.TransformHardTile(3);
}

void CWinDinkeditView::OnSetDefaultHardTile()
{
	current_map->hard_tile_selector.SetDefaultHardTile();
}

void CWinDinkeditView::OnEditHardTile()
{
    if (current_map != nullptr && current_map->editor_state == EditorState::Screen && current_map->screenMode == EditorSubmode::Hardbox)
    {
        current_map->hard_tile_selector.hardTileEditorLoadFromScreen();
    }
}


BOOL CWinDinkeditView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if (zDelta > 0)	//RW: we are scrolling "up"
	{
		if ((nFlags & MK_SHIFT) == 0)	//RW: shift is not pressed
			mouseWheel(mouse_wheel_updown_invert ? MOUSE_WHEEL_DOWN : MOUSE_WHEEL_UP);
		else
			mouseWheel(mouse_wheel_updown_shift_invert ? MOUSE_WHEEL_RIGHT : MOUSE_WHEEL_LEFT);
	}
	else
	{
		if ((nFlags & MK_SHIFT) == 0)	//RW: shift is not pressed
			mouseWheel(mouse_wheel_updown_invert ? MOUSE_WHEEL_UP : MOUSE_WHEEL_DOWN);
		else
			mouseWheel(mouse_wheel_updown_shift_invert ? MOUSE_WHEEL_LEFT : MOUSE_WHEEL_RIGHT);
	}

	return CListView::OnMouseWheel(nFlags, zDelta, pt);
}


void CWinDinkeditView::OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// This feature requires Windows Vista or greater.
	
	if (zDelta > 0)
		mouseWheel(mouse_wheel_leftright_invert ? MOUSE_WHEEL_LEFT : MOUSE_WHEEL_RIGHT);
	else
		mouseWheel(mouse_wheel_leftright_invert ? MOUSE_WHEEL_RIGHT : MOUSE_WHEEL_LEFT);

	CListView::OnMouseHWheel(nFlags, zDelta, pt);
}



void CWinDinkeditView::OnUpdateViewTileset1(CCmdUI *pCmdUI)
{
	if (pCmdUI->m_pSubMenu != nullptr)	//pCmdUI refers to the parent of the tile set selection submenu
	{
		if (current_map!=nullptr && current_map->screenMode == EditorSubmode::Tile && current_map->editor_state != EditorState::TileHardnessEditor)	
			pCmdUI->m_pMenu->EnableMenuItem(pCmdUI->m_nIndex, MF_BYPOSITION | MF_ENABLED);
		else
			pCmdUI->m_pMenu->EnableMenuItem(pCmdUI->m_nIndex, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);
	}
	else 
		pCmdUI->Enable(current_map!=nullptr);
}


void CWinDinkeditView::OnSpritemenuHard()
{
	if (sprite_clicked != nullptr)
		current_map->undo_buffer->PushAndDo<actions::SpritePropertyChange<int>>(&Sprite::hardness, (int)!sprite_clicked->hardness, SpriteRef{ current_map, screen_num_clicked, sprite_num_clicked });
}


void CWinDinkeditView::OnSpritemenuNohit()
{
	if (sprite_clicked != nullptr)
	{
		current_map->undo_buffer->PushAndDo<actions::SpritePropertyChange<int>>(&Sprite::nohit, (int)!sprite_clicked->nohit, SpriteRef{ current_map, screen_num_clicked, sprite_num_clicked });
	}
}


void CWinDinkeditView::OnUpdateMultispriteHard(CCmdUI *pCmdUI)
{
	bool hard = false;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		hard = !(*p)->hardness;
	
	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if (!(*p)->hardness != hard)
		{
			pCmdUI->SetRadio();
			return;
		}

	pCmdUI->SetCheck(hard);
}


void CWinDinkeditView::OnMultispriteHard()
{
	auto& sel = current_map->selectedSprites.front();
	if (sel)
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<int>>(&Sprite::hardness, sel->hardness);
}


void CWinDinkeditView::OnUpdateMultispriteNohit(CCmdUI *pCmdUI)
{
	bool nohit = false;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		nohit = (*p)->nohit;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if ((*p)->nohit != nohit)
		{
			pCmdUI->SetRadio();
			return;
		}

	pCmdUI->SetCheck(nohit);
}


void CWinDinkeditView::OnMultispriteNohit()
{
	auto& sel = current_map->selectedSprites.front();
	if (sel)
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<int>>(&Sprite::nohit, sel->nohit);
}


void CWinDinkeditView::OnUpdateMultispriteBackground(CCmdUI *pCmdUI)
{
	bool background = false;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		background = (*p)->type == SpriteType::Background;

	for (++p; p != current_map->selectedSprites.end(); ++p)
		if ((*p)->type == SpriteType::Background != background)
		{
			pCmdUI->SetRadio();
			return;
		}

	pCmdUI->SetCheck(background);
}


void CWinDinkeditView::OnMultispriteBackground()
{
	current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<SpriteType>>(&Sprite::type, SpriteType::Background);
}


void CWinDinkeditView::OnUpdateMultispriteForeground(CCmdUI *pCmdUI)
{
	bool foreground = false;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		foreground = (*p)->type == SpriteType::Foreground;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if (((*p)->type == SpriteType::Foreground) != foreground)
		{
			pCmdUI->SetRadio();
			return;
		}

	pCmdUI->SetCheck(foreground);
}


void CWinDinkeditView::OnMultispriteForeground()
{
	current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<SpriteType>>(&Sprite::type, SpriteType::Foreground);
}


void CWinDinkeditView::OnUpdateMultispriteInvisible(CCmdUI *pCmdUI)
{
	bool invisible = false;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		invisible = (*p)->type == SpriteType::Invisible;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if (((*p)->type == SpriteType::Invisible) != invisible)
		{
			pCmdUI->SetRadio();
			return;
		}

	pCmdUI->SetCheck(invisible);
}


void CWinDinkeditView::OnMultispriteInvisible()
{
	current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<SpriteType>>(&Sprite::type, SpriteType::Invisible);
}


void CWinDinkeditView::OnMultispriteBrain()
{
	int brain = 0;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		brain = (*p)->brain;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if ((*p)->brain != brain)
		{
			brain = -1;
			break;
		}

	CBrainDialog dlg(brain);
	if (dlg.DoModal() == IDOK)
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<int>>(&Sprite::brain, dlg.m_brain);
}


void CWinDinkeditView::OnMultispriteTouchdamage()
{
	int touchdamage = 0;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		touchdamage = (*p)->touch_damage;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if ((*p)->touch_damage != touchdamage)
		{
			touchdamage = -1;
			break;
		}

	CIntValueDialog dlg(_T("Touch damage:"), touchdamage, 0);
	if (dlg.DoModal() == IDOK)
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<int>>(&Sprite::touch_damage, dlg.m_value);
}


void CWinDinkeditView::OnMultispriteVision()
{
	int vision = 0;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		vision = (*p)->vision;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if ((*p)->vision != vision)
		{
			vision = -1;
			break;
		}

	CIntValueDialog dlg(_T("Vision:"), vision, 0, VISIONS_TRACKED-1);
	if (dlg.DoModal() == IDOK)
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<int>>(&Sprite::vision, dlg.m_value);
}


void CWinDinkeditView::OnMultispriteSize()
{
	int size = 100;

	auto p = current_map->selectedSprites.begin();
	if (*p)
		size = (*p)->size;

	for (++p; p!=current_map->selectedSprites.end(); ++p)
		if ((*p)->size != size)
		{
			size = -1;
			break;
		}

	CIntValueDialog dlg(_T("Size:"), size, 1);
	if (dlg.DoModal() == IDOK)
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<int>>(&Sprite::size, dlg.m_value);
}


void CWinDinkeditView::OnMultispriteScript()
{
	CScriptDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		EightDotThreeFilename script;
		EightDotThreeFilename::from_tchar((LPCTSTR)dlg.m_script, script);
		current_map->undo_buffer->PushAndDo<actions::MultiSpritePropertyChange<EightDotThreeFilename>>(&Sprite::script, script);
	}
}
