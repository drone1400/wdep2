#include "stdafx.h"
#include "ScriptCompressor.h"


ScriptCompressor::ScriptCompressor(CWnd* pParent /*=nullptr*/) : CDialog(ScriptCompressor::IDD, pParent)
{
	m_delete_c_files = FALSE;
	m_remove_comments = FALSE;
}

void ScriptCompressor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_DELETE_C_FILES, m_delete_c_files);
	DDX_Check(pDX, IDC_REMOVE_COMMENTS, m_remove_comments);
}

BEGIN_MESSAGE_MAP(ScriptCompressor, CDialog)
END_MESSAGE_MAP()