#pragma once
#define KEY_BACKQUOTE    VK_OEM_3		// '`'
#define KEY_LEFTBRACKET  VK_OEM_4		// '['
#define KEY_RIGHTBRACKET VK_OEM_6		// ']'
class CWinDinkeditView;

void initializeInput();

BOOL keyEvalTileSelectorPage(BYTE key);

//Call with capital char for letter keys (e.g. 'A' for the A key, never 'a')!
void keyPressed(BYTE key);
void keyReleased(BYTE key);

void mouseLeftPressed(int x, int y);
void mouseLeftReleased(int x, int y);
void mouseRightPressed(int x, int y, CWinDinkeditView* view_window);
void mouseLeftDoubleClick(int x, int y);
void mouseMove(int x, int y);
void mouseMiddlePressed(int x, int y);
void mouseMiddleReleased(int x, int y);
void mouseWheel(int direction);

void editScript(CString filename);

//let the outside world know when the screen is being dragged around
extern bool screen_move;