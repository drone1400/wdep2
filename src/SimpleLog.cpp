#include <fstream>
#include <iomanip>
#include <ctime>
#include "stdafx.h"
#include "SimpleLog.h"
#include "atlstr.h"
#include "PathUtil.h"
#include "CommonFile.h"
using std::ifstream;
using std::ofstream;
using std::ios;
using std::istreambuf_iterator;
using std::vector;

// ---------------------------------------------------------------------------------------------------------------
// ---- Static members
#define ATXTLVL_CRITICAL	"CRITICAL ";
#define ATXTLVL_ERROR		"ERROR    ";
#define ATXTLVL_WARNING		"WARNING  ";
#define ATXTLVL_INFO		"INFO     ";
#define ATXTLVL_DEBUG		"DEBUG    ";
#define ATXTLVL_VERBOSE		"VERBOSE  ";
#define ATXTLVL_DEFAULT		"???????? ";
#define ATXT_FULL_PADDING	"                           "
#define ATXT_SMALL_PADDING	"    "
#define PRINT_TIME_FORMAT	"%Y-%m-%d %H-%M-%S "


static int log_level_enable = 0;
static std::ofstream logfs;


static void LogPrintCStringUtf8(CString const& msg, int level, int format)
{
	if (!logfs.is_open()) return;            // make sure log is open!!

	static std::vector<char> utf8convbuf;

	if (format & LOG_FORMAT_HEADER_PADDING_ONLY)
	{
		// only write full padding space in place of header
		logfs << ATXT_FULL_PADDING;
	} else if (format & LOG_FORMAT_SMALL_PADDING_ONLY)
	{
		// only write small padding space in place of header
		logfs << ATXT_SMALL_PADDING;
	} else if (format & LOG_FORMAT_HEADER)
	{
		// write full header with timestamp and level
		Log_PrintTime(logfs);
		switch (level)
		{
			case LOG_ENABLE_CRITICAL: logfs << ATXTLVL_CRITICAL
			break;
			case LOG_ENABLE_ERROR: logfs << ATXTLVL_ERROR
			break;
			case LOG_ENABLE_WARNING: logfs << ATXTLVL_WARNING
			break;
			case LOG_ENABLE_INFO: logfs << ATXTLVL_INFO
			break;
			case LOG_ENABLE_DEBUG: logfs << ATXTLVL_DEBUG
			break;
			case LOG_ENABLE_VERBOSE: logfs << ATXTLVL_VERBOSE
			break;
			default:  logfs << ATXTLVL_DEFAULT
			break;
		}
	}
	
	ConvertToUtf8(msg, utf8convbuf);
	logfs << utf8convbuf.data();
	if (format & LOG_FORMAT_DOUBLE_NEW_LINE)
	{
		logfs << std::endl << std::endl;
	} else if (format & LOG_FORMAT_NEW_LINE)
	{
		logfs << std::endl;
	}
	
	logfs.flush();
}
// ---------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------
// ---- "public" members


void Log_PrintTime(std::ofstream& stream)
{
    if (!stream.is_open()) return;            // make sure log is open!!
	
    time_t time = std::time({});
    tm tm{};
    if (localtime_s(&tm, &time) != 0) return;   // this probably wouldn't happen?...
	
    stream << std::put_time(&tm, PRINT_TIME_FORMAT);
}

void Log_Initialize(CString basePath, int levels /*= LOG_ENABLE_ALL*/)
{
	log_level_enable = levels;

	// logs will be written in a logs folder to keep things tidy
	basePath.AppendChar(DIRSEPC);
	basePath.Append(_T("logs"));

	// make folder if it does not exist
	if (!FolderExists(basePath)) CreateDirectory(basePath, nullptr);
	
	time_t time = std::time({});
	tm tm{};
	if (localtime_s(&tm, &time) == 0)
	{
		// use current time as filename
		
		char timestr[std::size("yyyy-mm-dd_hh-mm-ss")];
		std::strftime(std::data(timestr), std::size(timestr), "%Y-%m-%d_%H-%M-%S", &tm);

		USES_CONVERSION;
		basePath.AppendChar(DIRSEPC);
		basePath.Append(A2T(timestr));
		basePath.Append(_T("_wdep2_applog.log"));
	} else // fallback if local time buggers out...
	{
		basePath.AppendChar(DIRSEPC);
		basePath.Append(_T("wdep2_applog.log"));
	}

	// close log if it's already open
	if (logfs.is_open()) logfs.close();

	// open new log
	logfs.open(basePath, std::ios::out|std::ios::app);

	// set locale
	//logfs.imbue(std::locale("en_US.utf8"));
	//logfs.imbue(std::locale("en_US"));

	// announce that log is initialized...
	Log_PrintTime(logfs);
	logfs << "LOG_INIT" << std::endl;
}

void Log_Flush()
{
	if (!logfs.is_open()) return;

	logfs.flush();
}

void Log_Close()
{
	if (logfs.is_open()) logfs.close();
}

void Log_Critical(CString const& msg, int format /*= LOG_FORMAT_STANDARD*/)
{
	if (!logfs.is_open()) return;                                        // make sure log is open!!
	if ((log_level_enable & LOG_ENABLE_CRITICAL) == 0) return;           // make sure log level is enabled
	LogPrintCStringUtf8(msg, LOG_ENABLE_CRITICAL, format);
}

void Log_Error(CString const& msg, int format /*= LOG_FORMAT_STANDARD*/)
{
	if (!logfs.is_open()) return;                                        // make sure log is open!!
	if ((log_level_enable & LOG_ENABLE_ERROR) == 0) return;              // make sure log level is enabled
	LogPrintCStringUtf8(msg, LOG_ENABLE_ERROR, format);
}

void Log_Warning(CString const& msg, int format /*= LOG_FORMAT_STANDARD*/)
{
	if (!logfs.is_open()) return;                                        // make sure log is open!!
	if ((log_level_enable & LOG_ENABLE_WARNING) == 0) return;            // make sure log level is enabled
	LogPrintCStringUtf8(msg, LOG_ENABLE_WARNING, format);
}

void Log_Info(CString const& msg, int format /*= LOG_FORMAT_STANDARD*/)
{
	if (!logfs.is_open()) return;                                        // make sure log is open!!
	if ((log_level_enable & LOG_ENABLE_INFO) == 0) return;               // make sure log level is enabled
	LogPrintCStringUtf8(msg, LOG_ENABLE_INFO, format);
}

void Log_Debug(CString const& msg, int format /*= LOG_FORMAT_STANDARD*/)
{
	if (!logfs.is_open()) return;                                        // make sure log is open!!
	if ((log_level_enable & LOG_ENABLE_DEBUG) == 0) return;              // make sure log level is enabled
	LogPrintCStringUtf8(msg, LOG_ENABLE_DEBUG, format);
}

void Log_Verbose(CString const& msg, int format /*= LOG_FORMAT_STANDARD*/)
{
	if (!logfs.is_open()) return;                                        // make sure log is open!!
	if ((log_level_enable & LOG_ENABLE_VERBOSE) == 0) return;            // make sure log level is enabled
	LogPrintCStringUtf8(msg, LOG_ENABLE_VERBOSE, format);
}

void ConvertToUtf8(CString const& mfcString, std::vector<char>& utfString)
{
	int buf_len = WideCharToMultiByte(CP_UTF8, 0, mfcString, -1, nullptr, 0, nullptr, nullptr);
	utfString.resize(buf_len);
	WideCharToMultiByte(CP_UTF8, 0, mfcString, -1, utfString.data(), buf_len, nullptr, nullptr);
}
