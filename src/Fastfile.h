#pragma once
#include "resource.h"

class Fastfile : public CDialog
{
public:
	Fastfile(CWnd* pParent = nullptr);   // standard constructor

	enum { IDD = IDD_FFCREATE };
	BOOL	m_delete_graphics;

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();

	void FFCreate(const TCHAR* path);
};