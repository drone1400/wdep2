#pragma once
class Sprite;
class CWinDinkeditDoc;

class CWinDinkeditView : public CListView
{
public:
	void displaySpriteMenu(Sprite* sprite, int screen_num, int sprite_num, int x, int y);
	void displayScreenMenu(int screen, int x, int y);
	void displayMultiSpriteMenu(int x, int y);
	void gainFocus();

protected: // create from serialization only
	CWinDinkeditView();
	DECLARE_DYNCREATE(CWinDinkeditView)

public:
	CWinDinkeditDoc* GetDocument();
	int windowMoved(int x, int y);

	BOOL PreCreateWindow(CREATESTRUCT& cs) override;
	BOOL OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll = TRUE) override;
	protected:
	void OnInitialUpdate() override; // called first time after construct
	BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE) override;

public:
	virtual ~CWinDinkeditView();
#ifdef _DEBUG
	void AssertValid() const override;
	void Dump(CDumpContext& dc) const override;
#endif

public:
	// mouse events
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt);
	// keyboard events
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnScreenProperties();
	afx_msg void OnDeleteScreen();
	afx_msg void OnSpriteProperties();
	afx_msg void OnNewScreen();
	
	afx_msg void OnViewTileMode();
	afx_msg void OnViewTileset1();
	afx_msg void OnViewTileset10();
	afx_msg void OnViewTileset11();
	afx_msg void OnViewTileset3();
	afx_msg void OnViewTileset4();
	afx_msg void OnViewTileset5();
	afx_msg void OnViewTileset6();
	afx_msg void OnViewTileset7();
	afx_msg void OnViewTileset8();
	afx_msg void OnViewTileset9();
	afx_msg void OnViewTileset2();
	afx_msg void OnPaint();
	afx_msg void OnViewHardboxMode();
	afx_msg void OnSetWarpBegin();
	afx_msg void OnSetWarpEnd();
	afx_msg void OnStoreSprite();
	afx_msg void OnCopyScreen();
	afx_msg void OnPasteScreen();
	afx_msg void OnRevertHardTile();
	afx_msg void OnTransformHardnessNormal();
	afx_msg void OnTransformHardnessLow();
	afx_msg void OnTransformHardnessUnknown();
	afx_msg void OnSetDefaultHardTile();
	afx_msg void OnEditHardTile();
	afx_msg void OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateViewTileset1(CCmdUI *pCmdUI);
	afx_msg void OnSpritemenuHard();
	afx_msg void OnSpritemenuNohit();
	afx_msg void OnUpdateMultispriteHard(CCmdUI *pCmdUI);
	afx_msg void OnMultispriteHard();
	afx_msg void OnUpdateMultispriteNohit(CCmdUI *pCmdUI);
	afx_msg void OnMultispriteNohit();
	afx_msg void OnUpdateMultispriteBackground(CCmdUI *pCmdUI);
	afx_msg void OnMultispriteBackground();
	afx_msg void OnUpdateMultispriteForeground(CCmdUI *pCmdUI);
	afx_msg void OnMultispriteForeground();
	afx_msg void OnUpdateMultispriteInvisible(CCmdUI *pCmdUI);
	afx_msg void OnMultispriteInvisible();
	afx_msg void OnMultispriteBrain();
	afx_msg void OnMultispriteTouchdamage();
	afx_msg void OnMultispriteVision();
	afx_msg void OnMultispriteSize();
	afx_msg void OnMultispriteScript();
	afx_msg void OnModeMultisprite();
};

#ifndef _DEBUG  // debug version in WinDinkeditView.cpp
inline CWinDinkeditDoc* CWinDinkeditView::GetDocument()
   { return (CWinDinkeditDoc*)m_pDocument; }
#endif