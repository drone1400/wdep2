#include "StdAfx.h"
#include "WinDinkeditView.h"
#include "MainFrm.h"
#include "Globals.h"
#include "common.h"
#include "CommonSdl.h"
#include "Engine.h"
#include "ImportMap.h"
#include "Minimap.h"
#include "Map.h"
#include "SpriteSelector.h"
#include "SpriteEditor.h"
#include "TileSelector.h"
#include "Interface.h"
#include "Structs.h"
#include "Undo.h"
#include "ViewCompressedScript.h"
#include "Actions/SpriteDelete.h"
#include "Actions/MultiSpriteRectSelect.h"

#define SCREEN_ARROW_MOVEMENT_SMALL		1
#define SCREEN_ARROW_MOVEMENT_MEDIUM	10
#define SCREEN_ARROW_MOVEMENT_BIG		50
#define SCREEN_WHEEL_MOVEMENT			50
#define TRIM_SMALL	1
#define TRIM_MEDIUM	5
#define TRIM_BIG	10

static bool keys_pressed[256];
bool screen_move = false;
int map_x_origin, map_y_origin; // used for moving the map around


void initializeInput()
{
	memset(keys_pressed, 0, sizeof(keys_pressed));
}

static void keyMoveApplyModifier(int& keyMove, BOOL positive)
{
	if (keys_pressed[VK_CONTROL]) //RW: if Ctrl is pressed then only move 1 pixel.
		keyMove += positive ? SCREEN_ARROW_MOVEMENT_SMALL : - SCREEN_ARROW_MOVEMENT_SMALL;
	else if (keys_pressed[VK_SHIFT]) //RW: if Shift is pressed then move a larger amount.
		keyMove += positive ? SCREEN_ARROW_MOVEMENT_BIG : - SCREEN_ARROW_MOVEMENT_BIG;
	else
		keyMove += positive ? SCREEN_ARROW_MOVEMENT_MEDIUM : - SCREEN_ARROW_MOVEMENT_MEDIUM;
}

// returns TRUE if commands were executed
BOOL keyEvalTileSelectorPage(BYTE key)
{
    if (current_map == nullptr)
        return FALSE;
    
	if (key == 'U' || key == KEY_BACKQUOTE)
	{
		// switch to tile sheet #41
		current_map->tile_selector.selectScreen(10);
		return TRUE;
	}
	
	// check if key to switch to tile selector 
	if ((key >= '0') && (key <= '9'))
	{
		int new_render = int(key) - int('0') - 1;

		if (new_render == -1) // 0 = 10
			new_render = 9;

		current_map->tile_selector.selectScreen(new_render);
		return TRUE;
	}
	return FALSE;
}

static BOOL keyEvalMinimapArrowNavigation(BYTE key)
{
	int x, y;
	RECT r;
	
	mainWnd->GetRightPane()->GetWindowRect(&r);
	x = (current_map->minimap->getHoverScreen() % MAP_COLUMNS) * (r.right - r.left) / MAP_COLUMNS + (r.right - r.left) / (2 * MAP_COLUMNS);
	y = (current_map->minimap->getHoverScreen() / MAP_COLUMNS) * (r.bottom - r.top) / MAP_ROWS + (r.bottom - r.top) / (2 * MAP_ROWS);

	switch (key)
	{
	case VK_RIGHT:
		if (current_map->minimap->getHoverScreen() % MAP_COLUMNS < MAP_COLUMNS - 1) 
			x += (r.right - r.left) / MAP_COLUMNS; //RW: if this is not the rightmost screen
	break;
	case VK_LEFT:
		if (current_map->minimap->getHoverScreen() % MAP_COLUMNS > 0)
			x -= (r.right - r.left) / MAP_COLUMNS; //RW: if this is not the leftmost screen
	break;
	case VK_DOWN:
		if (current_map->minimap->getHoverScreen() / MAP_COLUMNS < MAP_ROWS - 1) 
			y += (r.bottom - r.top) / MAP_ROWS; //RW: if this is not the bottommost screen
	break;
	case VK_UP:
		if (current_map->minimap->getHoverScreen() / MAP_COLUMNS > 0)
			y -= (r.bottom - r.top) / MAP_ROWS; //RW: if this is not the topmost screen
	break;
	default:
		return false;
	}

	SetCursorPos(r.left + x, r.top + y);
	return true;
}

// returns TRUE if commands were executed
static BOOL keyEvalNumpadCursorNavigation(BYTE key)
{
	//move the mouse pointer using the numpad
	if (key >= VK_NUMPAD0 && key <= VK_NUMPAD9)
	{
		int movex = 0, movey = 0;
		int k = key - VK_NUMPAD0;

		if (k == 0)
		{
			INPUT inp[] = {
				{INPUT_MOUSE, {0, 0, 0,MOUSEEVENTF_LEFTDOWN, 0, (ULONG_PTR)nullptr}}, {INPUT_MOUSE, {0, 0, 0,MOUSEEVENTF_LEFTUP, 0, (ULONG_PTR)nullptr}}
			};
			SendInput(2, inp, sizeof(INPUT));
			return TRUE;
		}
		else if (k == 5)
		{
			INPUT inp[] = {
				{INPUT_MOUSE, {0, 0, 0,MOUSEEVENTF_RIGHTDOWN, 0, (ULONG_PTR)nullptr}}, {INPUT_MOUSE, {0, 0, 0,MOUSEEVENTF_RIGHTUP, 0, (ULONG_PTR)nullptr}}
			};
			SendInput(2, inp, sizeof(INPUT));
			return TRUE;
		}
		
		if (k == 7 || k == 8 || k == 9)	keyMoveApplyModifier(movey, FALSE);
		if (k == 1 || k == 2 || k == 3)	keyMoveApplyModifier(movey, TRUE);
		if (k == 7 || k == 4 || k == 1)	keyMoveApplyModifier(movex, FALSE);
		if (k == 9 || k == 6 || k == 3)	keyMoveApplyModifier(movex, TRUE);

		INPUT inp = {INPUT_MOUSE, {movex, movey, 0,MOUSEEVENTF_MOVE, 0, (ULONG_PTR)nullptr}};
		SendInput(1, &inp, sizeof(INPUT));

		return TRUE;
	}

	return FALSE;
}

void keyPressed(BYTE key)
{
	if (!current_map)
		return;

	//this improves performance while you have to hold a key down - a LOT.
	if ((key == VK_CONTROL && keys_pressed[VK_CONTROL]) ||
		(key == VK_SHIFT && keys_pressed[VK_SHIFT]) ||
		(key == VK_MENU && keys_pressed[VK_MENU]) ||
		(key == VK_SPACE && keys_pressed[VK_SPACE]) ||
		(key == 'I' && keys_pressed['I']) ||
		(key == 'X' && keys_pressed['X']) ||
		(key == 'Z' && keys_pressed['Z']))
		return;

	// set key to down so we can tell when it is released
	keys_pressed[key] = true;

	if (key == VK_F12) //RW: toggle help text
	{
		help_text = !help_text;

		Game_Main();
		return;
	}

	if (key == VK_F5) //RW: change help text color
	{
		help_text_color_index++;
		if (help_text_color_index >= NUM_HELP_COLORS)
			help_text_color_index = 0;

		help_text_color = helpcolors[help_text_color_index];

		Game_Main();
		return;
	}

	// check if numpad navigation needs to be performed
	if (keyEvalNumpadCursorNavigation(key))
	{
		Game_Main();
		return;
	}

	// uuuh... wtf is the point of this ready text again?
	//mainWnd->setStatusText(_T("Ready"));

	switch (current_map->editor_state)
	{
	case EditorState::Screen:
		{
			// check if need to go to tile selector
			if (keyEvalTileSelectorPage(key))
			{
				Game_Main();
				return;
			}
			
			switch (key)
			{
			// navigation keys
			case VK_LEFT:
			case VK_RIGHT:
			case VK_UP:
			case VK_DOWN:
				if (!keys_pressed['Z'] && !keys_pressed['X'])
				{
					int x_move = 0;
					int y_move = 0;
					
					if (keys_pressed[VK_UP]) keyMoveApplyModifier(y_move, FALSE);
					if (keys_pressed[VK_DOWN]) keyMoveApplyModifier(y_move, TRUE);
					if (keys_pressed[VK_LEFT]) keyMoveApplyModifier(x_move, FALSE);
					if (keys_pressed[VK_RIGHT]) keyMoveApplyModifier(x_move, TRUE);

					if (x_move != 0 || y_move != 0)
					{
						current_map->updateMapPosition(current_map->window.left + x_move, current_map->window.top + y_move);
					}
				}
				break;
			
			// show screen selector (minimap)
			case VK_TAB:
			case VK_ESCAPE:
				current_map->editor_state = EditorState::Minimap;
			    if (current_map->isScreenFocusMode)
			    {
			        current_map->isScreenFocusMode = false;
			        current_map->updateMapPosition(current_map->window.left, current_map->window.top);
			    }
				break;

			case VK_SPACE:
				displayhardness = true;
				break;

			case 'D': // toggle by default hard sprite mode
				if (current_map->screenMode != EditorSubmode::Hardbox)
				{
					sprite_hard = !sprite_hard;
				}
				break;
			case 'J':
			    if (current_map->screenMode != EditorSubmode::Sprite &&
			        current_map->screenMode != EditorSubmode::MultiSprite)
			    {
			        hide_all_sprites = !hide_all_sprites;
			    } else
			    {
			        hide_all_sprites = false;
			    }
			    break;
			case 'H': // switch to hard mode
				current_map->screenMode = EditorSubmode::Hardbox;
				//current_map->editor_state = EditorState::Screen;
				displayhardness = true;
				break;

			case 'I': // show sprite info
				show_sprite_info = true;
				break;

			case 'L': // import screen
				mainWnd->OnImportScreen();
				break;

			case 'M': // toggle screen match mode or duplicate detection
				if (!keys_pressed[VK_SHIFT])
					current_map->screenmatch = !current_map->screenmatch;
				else
					detect_screenmatch_duplicates = !detect_screenmatch_duplicates;
				break;

			case 'R': // switch to sprite mode
				current_map->screenMode = EditorSubmode::Sprite;
			    hide_all_sprites = false; // make sure sprites are visible
				//current_map->editor_state = EditorState::Screen;
				break;

			case 'T': // switch to tile mode
				current_map->screenMode = EditorSubmode::Tile;
				//current_map->editor_state = EditorState::Screen;
				break;

			// change vision
			case 'V':
				mainWnd->OnVision();
				break;

			case 'Y': // switch to multi sprite mode
				current_map->screenMode = EditorSubmode::MultiSprite;
				//current_map->editor_state = EditorState::Screen;
				current_map->selectedSprites.erase(
					remove_if(current_map->selectedSprites.begin(), current_map->selectedSprites.end(), is_falselike()),
					current_map->selectedSprites.end()
				);
				break;
			}

			if (current_map->screenMode == EditorSubmode::Sprite)
			{
				if (current_map->getMouseSprite() != nullptr)
				{
					
					// check if the mouse sprite needs trimming
					int trim_amount = keys_pressed[VK_SHIFT]
						? TRIM_BIG
						: keys_pressed[VK_CONTROL]
							? TRIM_SMALL
							: TRIM_MEDIUM;

					if (keys_pressed['Q'])
					{
						if (current_map->sequence[current_map->mouseSprite->sequence] != nullptr &&
							current_map->sequence[current_map->mouseSprite->sequence]->frame_image[current_map->mouseSprite->frame-1] != nullptr)
						{
							current_map->editor_state = EditorState::SpriteHardnessEditor;
						}
					}

					RECT trim = {0, 0, 0, 0};

					if (keys_pressed['Z'])
					{
						if (keys_pressed[VK_UP])
							trim.top = -trim_amount;
						if (keys_pressed[VK_DOWN])
							trim.top = trim_amount;
						if (keys_pressed[VK_LEFT])
							trim.left = -trim_amount;
						if (keys_pressed[VK_RIGHT])
							trim.left = trim_amount;
					}

					if (keys_pressed['X'])
					{
						if (keys_pressed[VK_UP])
							trim.bottom = -trim_amount;
						if (keys_pressed[VK_DOWN])
							trim.bottom = trim_amount;
						if (keys_pressed[VK_LEFT])
							trim.right = -trim_amount;
						if (keys_pressed[VK_RIGHT])
							trim.right = trim_amount;
					}

					if (trim.left || trim.top || trim.right || trim.bottom)
					{
						current_map->trimSprite(trim);
					}

					// delete sprite
					if (key == VK_DELETE || key == 'B')
					{
						if (current_map->getMouseSprite())
						{
							current_map->undo_buffer->PushAndDo<actions::SpriteDelete>(*current_map->getMouseSprite());
						}
					}
					else if (key == 'S') // stamp sprite
					{
						if (!keys_pressed[VK_CONTROL])
							current_map->placeSprite(mouse_x_position, mouse_y_position, true);
					}
					else if (key == VK_RETURN) //RW: Enter key
					{
						current_map->placeSprite(mouse_x_position, mouse_y_position, false);
					}
					else if (key == KEY_RIGHTBRACKET) //RW: use '[' and ']' to change sprite size.
					{
						current_map->mouseSprite->size += 5;
					}
					else if (key == KEY_LEFTBRACKET && current_map->mouseSprite->size > 5)
					{
						current_map->mouseSprite->size -= 5;
					}
				} else
				{
					if (key == VK_F3)
					{
						current_map->cursorToNextSprite(keys_pressed[VK_SHIFT]);
					}
				}

				// show sprite selector
				if (key == 'E')
				{
					mainWnd->setStatusText(_T("Loading sprite selector..."));
					current_map->editor_state = EditorState::SpriteSelector;
				} else if (key == 'F')
				{
					if (current_map->isScreenFocusMode) {
						current_map->isScreenFocusMode = false;
					    current_map->updateMapPosition(current_map->window.left, current_map->window.top);
					} else
					{
						if (current_map->screen[current_map->hoverScreen] != nullptr)
						{
							current_map->findSpriteNum = -1; 
							current_map->isScreenFocusMode = true;
							current_map->screenFocusIndex = current_map->hoverScreen;
						    current_map->updateMapPosition(current_map->window.left, current_map->window.top);
						}
					}
				}
			}
			else if (current_map->screenMode == EditorSubmode::MultiSprite)
			{
				switch (key)
				{
				case 'C': current_map->tryDoMultispriteClearSelection(); break;
				case 'B':
				case VK_DELETE: current_map->tryDoMultispriteDeleteSelection(); break;
				case 'P': current_map->tryDoMultispritePickup(keys_pressed[VK_SHIFT]); break;
				case VK_RETURN: current_map->tryDoMultispritePlaceMouseSelection(); break;
				case VK_CONTROL:
					if (current_map->rect_select_active)
						current_map->rect_select_exclusive = true;
					break;
				}
			}
			else if (current_map->screenMode == EditorSubmode::Tile && !keys_pressed[VK_CONTROL])
			{
				switch (key)
				{
				case 'C': current_map->tile_selector.storeScreenTiles(); break; // copy tile(s) 
				case 'S': current_map->tile_selector.tryDoPlaceTiles(); break; // stamp tile(s)
				}
			}
			else if (current_map->screenMode == EditorSubmode::Hardbox && !keys_pressed[VK_CONTROL])
			{
				switch (key)
				{
				case 'E':
					current_map->editor_state = EditorState::HardTileSelector;
					current_map->hardTileSelectorReturnState = EditorState::Screen;
					break;
				case 'C':
					current_map->hard_tile_selector.storeScreenTiles();
					break;
				case 'D':
					current_map->hard_tile_selector.SetDefaultHardTile();
					break;
				case 'S':
					current_map->hard_tile_selector.tryDoPlaceTiles();
					break;
				case 'Q':
					current_map->hard_tile_selector.hardTileEditorLoadFromScreen();
					break;
				case 'B':
				case VK_DELETE:
					current_map->hard_tile_selector.revertTile();
					break;
				}
			}
			break;
		}

	case EditorState::Minimap:
		{
			switch (key)
			{
				case VK_RETURN:         //Load the screen the mouse is over
					current_map->minimap->loadScreen();
				break;
				case VK_TAB:            // show screen
					current_map->editor_state = EditorState::Screen;
				break;
				case VK_SPACE:          // render minimap
					current_map->minimap->toggleDetailing();
				break;
				case 'B':               //Delete the screen the mouse hovers over
				case VK_DELETE:
					current_map->tryDoScreenDelete(current_map->minimap->getHoverScreen());
				break;
				case 'L':               // import screen
					mainWnd->OnImportScreen();
				break;
				case VK_RIGHT:          // minimap navigation
				case VK_LEFT:
				case VK_UP:
				case VK_DOWN:
					keyEvalMinimapArrowNavigation(key);
				break;
			}
	        break;
		}

	case EditorState::SpriteSelector:
		{
			switch (key)
			{
			case VK_ESCAPE: //show screen
				if (current_map->sprite_selector.showFrames)
					current_map->sprite_selector.showFrames = false;
				else
					current_map->editor_state = EditorState::Screen;
				break;
			case VK_PRIOR: //Page Up
				current_map->sprite_selector.prevPage();
				break;
			case VK_NEXT: //Page Down
				current_map->sprite_selector.nextPage();
				break;
			}
			break;
		}

	case EditorState::TileSelector:
		{
			// check if need to go to another tile selector
			if (keyEvalTileSelectorPage(key))
			{
				Game_Main();
				return;
			}
		
			switch (key)
			{
				case VK_ESCAPE: // return to main mode
					current_map->editor_state = EditorState::Screen;
				break;
				case VK_SPACE:
					displayhardness = true;
				break;
				case 'H':
					current_map->screenMode = EditorSubmode::Hardbox;
					displayhardness = true;
				break;
				case 'T':
					current_map->screenMode = EditorSubmode::Tile;
				break;

			case VK_RETURN:
				current_map->tile_selector.storeTileSheetTiles();
				current_map->editor_state = EditorState::Screen;
				current_map->screenMode = EditorSubmode::Tile;
				break;

			case VK_PRIOR:
				current_map->tile_selector.prevPage();
				break;

			case VK_NEXT:
				current_map->tile_selector.nextPage();
				break;
			}

			if (current_map->screenMode == EditorSubmode::Tile)
			{
				switch(key)
				{
					case 'C': // copy tiles into buffer
						current_map->tile_selector.storeTileSheetTiles();
						break;
				}
			}
			else if (current_map->screenMode == EditorSubmode::Hardbox)
			{
				switch(key)
				{
				case VK_DELETE:
				case 'B':
					current_map->tile_selector.tryDoHardTileSetDefaultClearOnHover();
					break;
				case 'D':
					current_map->tile_selector.tryDoHardTileSetDefaultOnHover();
					break;
				case 'E':
					current_map->editor_state = EditorState::HardTileSelector;
					current_map->hardTileSelectorReturnState = EditorState::TileSelector;
					break;
				case 'Q':
					current_map->tile_selector.switchHardTileEditModeOnHover();
					break;
				
				}
			}
			break;
		}
	case EditorState::HardTileSelector:
		{
			switch (key)
			{
			case VK_ESCAPE: // return to previous mode
				current_map->editor_state = current_map->hardTileSelectorReturnState;
				break;

			case VK_PRIOR: // prev page
				current_map->hard_tile_selector.prevPage();
				break;

			case VK_NEXT: // next page
				current_map->hard_tile_selector.nextPage();
				break;
			}
			break;
		}

	case EditorState::ScreenImporter:
		{
			// exit map importer
			if (key == VK_ESCAPE)
				removeImportMap();
			break;
		}

	case EditorState::TileHardnessEditor:
		{
			switch (key)
			{
			case VK_TAB:
				current_map->hard_tile_selector.hardTileEditorDiscardAndReturn();
				break;
			case VK_ESCAPE: // return to previous mode
				current_map->hard_tile_selector.hardTileEditorSaveAndReturn();
				break;

			case VK_SPACE: // hide texture when space held
				hardedit_show_texture = FALSE;
				break;
			case VK_DELETE:
				case 'B':
				current_map->hard_tile_selector.hardTileEditorClearAllHardness();
				break;

			case VK_SUBTRACT: // decrease brush size
			case VK_OEM_MINUS:
				if (tile_brush_size > 1) tile_brush_size--;
				break;

			case VK_ADD: // increase brush size
			case VK_OEM_PLUS:
				if (tile_brush_size < 25) tile_brush_size++;
			}

			break;
		}
	case EditorState::SpriteHardnessEditor:
		{
			switch (key)
			{
			case VK_ESCAPE: // return previous mode
				spriteeditor->Save();
				current_map->screenMode = EditorSubmode::Sprite;
				current_map->editor_state = EditorState::Screen;
			    hide_all_sprites = false; // make sure sprites are visible
				break;

			case VK_PRIOR: //Page Up
				spriteeditor->zoom += 5;
				break;

			case VK_NEXT: //Page Down
				if (spriteeditor->zoom > 5) spriteeditor->zoom -= 5;
				break;

			case VK_HOME:
				spriteeditor->zoom = 100;
				break;

			case VK_TAB:
				spriteeditor->Reset();
				current_map->screenMode = EditorSubmode::Sprite;
				current_map->editor_state = EditorState::Screen;
			    hide_all_sprites = false; // make sure sprites are visible
				break;
			}
			break;
		}
	}

	Game_Main();
}

void keyReleased(BYTE key)
{
	if (!current_map)
		return;

	keys_pressed[key] = false;

	if (current_map->editor_state == EditorState::TileHardnessEditor)
	{
		if (!keys_pressed[VK_SPACE]) hardedit_show_texture = TRUE;
	}
	else if ((current_map->editor_state == EditorState::Screen || current_map->editor_state == EditorState::TileSelector)
		&& current_map->screenMode != EditorSubmode::Hardbox)
			displayhardness = false;

	if (key == VK_SHIFT)
	{
		current_map->hard_tile_selector.tile_box_points = 0;
	}

	if (current_map->rect_select_active)
	{
		if (key == VK_CONTROL)
			current_map->rect_select_exclusive = false;

		if (key == VK_SHIFT)
			current_map->rect_select_active = false;
	}

	show_sprite_info = false;
	screen_move = false;
	Game_Main();
}

void mouseLeftPressed(int x, int y)
{
	if (!current_map)
		return;

	SetTimer(mainWnd->GetSafeHwnd(), TIMER_MOUSE_CHECK, 250, nullptr);

	//RW: without this, placing a sprite using mouse is not really affected by "snap to grid"
	if (current_map->mouseSprite != nullptr && snapto_grid == true)
	{
		x = ((int)(x / snapto_offset) * snapto_offset);
		y = ((int)(y / snapto_offset) * snapto_offset);
	}

	mouseMove(x, y);

	switch (current_map->editor_state)
	{
	case EditorState::Screen:
		// prepare the map to be moved
		if (keys_pressed[VK_CONTROL] && !(keys_pressed[VK_SHIFT] && current_map->screenMode == EditorSubmode::MultiSprite))
		{
			screen_move = true;
			map_x_origin = current_map->window.left; // set original x location
			map_y_origin = current_map->window.top; // set original y location
			mouse_x_origin = x;
			mouse_y_origin = y;
		}
		else
		{
			// might be a sprite, check the screens
			if (current_map->screenMode == EditorSubmode::Sprite)
			{
				if (current_map->getMouseSprite() == nullptr)
				{
					current_map->tryDoSpritePickup();
				}
				else
				{
					// there is a mouse sprite, now place it on the screen if possible
					if (current_map->placeSprite(x, y, false))
					{
						// mouse sprite was placed, update the screen
						Game_Main();
					}
				}
			}
			else if (current_map->screenMode == EditorSubmode::MultiSprite)
			{
				if (keys_pressed[VK_SHIFT])
				{
					current_map->selection_rect.left = x;
					current_map->selection_rect.top = y;
				    // should also write to bottom right corner
				    // otherwise a quick single shift click can lead to selecting unwanted sprites
				    // because the rect will remember the previous bottom right coordinates instead
				    current_map->selection_rect.right = x;
				    current_map->selection_rect.bottom = y;
				    
					current_map->rect_select_active = true;
					if (keys_pressed[VK_CONTROL])
						current_map->rect_select_exclusive = true;
					Game_Main();
				}
				else
				{
					if (current_map->mouseMultiSprites.size() == 0)
					{
						current_map->tryDoMultispriteSelectToggle();
						Game_Main();
					}
					else
						current_map->tryDoMultispritePlaceMouseSelection();
				}
			}
			else if (current_map->screenMode == EditorSubmode::Tile)
			{
				// didn't catch a sprite, start making a box and set start location
				draw_box = true;
				mouse_x_origin = x;
				mouse_y_origin = y;

				current_map->tile_selector.updateScreenGrid(x, y);
				Game_Main();
			}
			else if (current_map->screenMode == EditorSubmode::Hardbox)
			{
				if (keys_pressed[VK_SHIFT])
				{
					current_map->hard_tile_selector.placeTileBoxPoint(x, y);
				}
				else
				{
					draw_box = true;
					mouse_x_origin = x;
					mouse_y_origin = y;

					current_map->hard_tile_selector.updateScreenGrid(x, y);
					Game_Main();
				}
			}
		}
		break;

	case EditorState::Minimap:
		// go to tile square
		current_map->minimap->loadScreen();
		break;

	case EditorState::SpriteSelector:
		// try to catch a sprite
		current_map->sprite_selector.getSprite();
		Game_Main();
		break;

	case EditorState::TileSelector:
		// only enable multiple selection in tile mode...
		if (current_map->screenMode == EditorSubmode::Tile)
		{
			draw_box = true;
			mouse_x_origin = x;
			mouse_y_origin = y;
		} else
		{
			draw_box = false;
		}

		current_map->tile_selector.selectTileSheetTileXY(x, y);
		Game_Main();
		break;

	case EditorState::HardTileSelector:
		current_map->hard_tile_selector.getTile(x, y);
		Game_Main();
		break;

	case EditorState::ScreenImporter:
		current_map->import_map->tileClicked();
		break;

	case EditorState::TileHardnessEditor:
		draw_box = true;
		current_map->hard_tile_selector.hardTileEditorRender(x, y, hardedit_show_texture);
		break;

	case EditorState::SpriteHardnessEditor:
		draw_box = true;
		mouse_x_origin = x;
		mouse_y_origin = y;
		break;
	}
}


void mouseLeftReleased(int x, int y)
{
	if (!current_map)
		return;

	if (draw_box)
	{
		draw_box = false;

		if (current_map->editor_state == EditorState::SpriteHardnessEditor)
		{
			spriteeditor->SetHardness(mouse_x_origin, mouse_y_origin, x, y);
		}

		Game_Main();
	}

	if (current_map->rect_select_active && current_map->editor_state == EditorState::Screen && current_map->screenMode == EditorSubmode::MultiSprite)
	{
		current_map->undo_buffer->PushAndDo<actions::MultiSpriteRectSelect>(keys_pressed[VK_CONTROL]);
	}

	screen_move = false;
	current_map->rect_select_active = false;
	current_map->rect_select_exclusive = false;
}

void mouseMiddlePressed(int x, int y)
{
	switch (current_map->editor_state)
	{
	case EditorState::Screen:
		{
			screen_move = true;
			map_x_origin = current_map->window.left; // set original x location
			map_y_origin = current_map->window.top; // set original y location
			mouse_x_origin = x;
			mouse_y_origin = y;
			mouseMove(x, y);
			break;
		}
	}
}


void mouseMiddleReleased(int x, int y)
{
	screen_move = false;
}

void mouseLeftDoubleClick(int x, int y)
{
	if (!current_map)
		return;
	// possibly create a screen
	if (current_map->editor_state == EditorState::Screen)
	{
		// calculate screen mouse is over
		int x_screen = (current_map->window.left + x) / (SCREEN_WIDTH_GAP);
		int y_screen = (current_map->window.top + y) / (SCREEN_HEIGHT_GAP);
		int cur_screen = y_screen * MAP_COLUMNS + x_screen;

		current_map->tryDoScreenCreate(cur_screen);
	}
}

void mouseRightPressed(int x, int y, CWinDinkeditView* view_window)
{
	if (!current_map)
		return;

	KillTimer(mainWnd->GetSafeHwnd(), TIMER_MOUSE_CHECK);

	mouseMove(x, y);

	switch (current_map->editor_state)
	{
	case EditorState::Screen:
		// adjust properties of the sprite
		if (current_map->screenMode == EditorSubmode::Sprite)
		{
			// there's no mouse sprite, try to find a sprite and display it's properties
			Sprite* mouseSprite = current_map->getMouseSprite();
			if (mouseSprite == nullptr)
			{
				int sprite_num = current_map->hoverSpriteNum;
				Sprite* hover_sprite = current_map->getHoverSprite();
				if (hover_sprite != nullptr)
				{
					view_window->displaySpriteMenu(hover_sprite, current_map->hoverScreen, sprite_num, x, y);
				}
				else
				{
					view_window->displayScreenMenu(current_map->hoverScreen, x, y);
				}
			}
			// there is a mouse sprite, display the properties of that sprite
			else
			{
				view_window->displaySpriteMenu(mouseSprite, current_map->hoverScreen, -1, x, y);
			}
		}
		else if (current_map->screenMode == EditorSubmode::MultiSprite)
		{
			if (current_map->mouseMultiSprites.empty())
			{
				if (current_map->selectedSprites.empty())
				{
				    int sprite_num = current_map->hoverSpriteNum;
				    Sprite* hover_sprite = current_map->getHoverSprite();

					if (hover_sprite != nullptr)
						view_window->displaySpriteMenu(hover_sprite, current_map->hoverScreen, sprite_num, x, y);
					else
						view_window->displayScreenMenu(current_map->hoverScreen, x, y);
				}
				else
					view_window->displayMultiSpriteMenu(x, y);
			}
		}
		else
		{
			view_window->displayScreenMenu(current_map->hoverScreen, x, y);
		}
		break;
	case EditorState::Minimap:
		{
			int screen = current_map->minimap->getHoverScreen();
			view_window->displayScreenMenu(screen, x, y);
			break;
		}
	case EditorState::SpriteSelector: break;
	case EditorState::TileSelector: break;
	case EditorState::HardTileSelector: break;
	case EditorState::ScreenImporter: break;
	case EditorState::TileHardnessEditor: break;
	case EditorState::SpriteHardnessEditor:
		spriteeditor->SetDepth(x, y);
		Game_Main(); //RW: draw the depth dot immediately in its new position, do not wait for a mouse move.
		break;
	}
}

void mouseMove(int x, int y)
{
	if (current_map == nullptr)
		return;

	mouse_x_position = x;
	mouse_y_position = y;

	switch (current_map->editor_state)
	{
	case EditorState::Screen:
		{
			current_map->updateHoverPosition(x, y);

			// move the map around
			if (!(GetAsyncKeyState(VK_LBUTTON) || GetAsyncKeyState(VK_MBUTTON) || GetSystemMetrics(SM_SWAPBUTTON) && GetAsyncKeyState(VK_RBUTTON))) {
				// if left button or middle button have been released, stop moving screen
				screen_move = false;
			}
			else if (screen_move)
			{
				//if the mouse is out of the map, break out of switch
				if (x < 1 || x >= current_map->window_width || y < 1 || y >= current_map->window_height)
					break;

				current_map->updateMapPosition(map_x_origin - (x - mouse_x_origin), map_y_origin - (y - mouse_y_origin));
			}

			switch (current_map->screenMode)
			{
			case EditorSubmode::Tile:
				if (draw_box)
				{
					RECT tile_box = {mouse_x_origin, mouse_y_origin, x, y};
					current_map->tile_selector.updateScreenGrid(tile_box);
				}
				break;

			case EditorSubmode::Sprite:
				{
					// update the mouse position
					Sprite* mouseSprite = current_map->getMouseSprite();

					if (mouseSprite != nullptr)
					{
						mouseSprite->x = x;
						mouseSprite->y = y;
					}
					else
					{
						bool redraw_needed = false;

						//this is a hack: we allow switching modes even if sprites are picked up... and they keep following the mouse even in other modes... this is probably a bad idea, but whatever for now
						if (!current_map->mouseMultiSprites.empty())
							redraw_needed = true;

						if (screen_move)
							redraw_needed = true;

						if (!redraw_needed)
							return;
					}
					break;
				}

			case EditorSubmode::MultiSprite:
				if (current_map->rect_select_active)
				{
					current_map->selection_rect.right = x;
					current_map->selection_rect.bottom = y;
				}
				break;

			case EditorSubmode::Hardbox:
				if (keys_pressed[VK_SHIFT])
				{
					//current_map->hard_tile_selector.placeTileBoxMouse(x, y);
				}
				else if (draw_box)
				{
					RECT tile_box = {mouse_x_origin, mouse_y_origin, x, y};
					current_map->hard_tile_selector.updateScreenGrid(tile_box);
				}
				break;
			}
			break;
		}

	case EditorState::Minimap:
		// displays screen coordinates of mouse pointer
		current_map->minimap->updateHoverPosition(x, y);
		break;

	case EditorState::SpriteSelector: break;

	case EditorState::TileSelector:
		if (draw_box)
		{
			if (current_map->screenMode == EditorSubmode::Tile)
			{
				current_map->tile_selector.selectTileSheetTileXY(mouse_x_origin, x, mouse_y_origin, y);
			} else if (current_map->screenMode == EditorSubmode::Hardbox)
			{
				current_map->tile_selector.selectTileSheetTileXY(x, y);
			}
		}
		break;

	case EditorState::HardTileSelector: break;


	case EditorState::ScreenImporter:
		// displays screen coordinates of mouse pointer
		current_map->minimap->updateHoverPosition(x, y);
		break;

	case EditorState::TileHardnessEditor:
		// NOTE drone: this was commented out... maybe i should investigate later what it does lol
		//current_map->hard_tile_selector.hardrender(x,y);
		break;

	case EditorState::SpriteHardnessEditor:
		if (draw_box)
		{
			RECT hard_box;
			hard_box.top = mouse_y_origin;
			hard_box.left = mouse_x_origin;
			hard_box.right = x;
			hard_box.bottom = y;

			spriteeditor->DrawSprite();
			drawFilledBox(hard_box, SDL_Color{128, 128, 128, 255});
		}
		break;
	}

	Game_Main();
}

//RW: this function gets called when the user turns the mouse wheel
void mouseWheel(int direction)
{
	if (!current_map) return;

	switch (current_map->editor_state)
	{
	case EditorState::Screen:
		switch (direction)
		{
		case MOUSE_WHEEL_LEFT:
			current_map->updateMapPosition(current_map->window.left - SCREEN_WHEEL_MOVEMENT, current_map->window.top);
			break;
		case MOUSE_WHEEL_UP:
			current_map->updateMapPosition(current_map->window.left, current_map->window.top - SCREEN_WHEEL_MOVEMENT);
			break;
		case MOUSE_WHEEL_RIGHT:
			current_map->updateMapPosition(current_map->window.left + SCREEN_WHEEL_MOVEMENT, current_map->window.top);
			break;
		case MOUSE_WHEEL_DOWN:
			current_map->updateMapPosition(current_map->window.left, current_map->window.top + SCREEN_WHEEL_MOVEMENT);
			break;
		}
		break;

	case EditorState::Minimap: break;

	case EditorState::SpriteSelector:
		if (direction == MOUSE_WHEEL_DOWN || direction == MOUSE_WHEEL_RIGHT)
			current_map->sprite_selector.nextPage();
		else
			current_map->sprite_selector.prevPage();
		break;

	case EditorState::TileSelector:
		if (direction == MOUSE_WHEEL_DOWN || direction == MOUSE_WHEEL_RIGHT)
			current_map->tile_selector.nextPage();
		else
			current_map->tile_selector.prevPage();
		break;

	case EditorState::ScreenImporter: break;

	case EditorState::HardTileSelector:
		if (direction == MOUSE_WHEEL_DOWN || direction == MOUSE_WHEEL_RIGHT)
			current_map->hard_tile_selector.nextPage();
		else
			current_map->hard_tile_selector.prevPage();
		break;

	case EditorState::TileHardnessEditor:
		if ((direction == MOUSE_WHEEL_DOWN || direction == MOUSE_WHEEL_RIGHT) && tile_brush_size > 1)
			tile_brush_size--;
		if ((direction == MOUSE_WHEEL_UP || direction == MOUSE_WHEEL_LEFT) && tile_brush_size < 25)
			tile_brush_size++;
		break;

	case EditorState::SpriteHardnessEditor:
		if (direction == MOUSE_WHEEL_DOWN || direction == MOUSE_WHEEL_RIGHT)
		{
			if (spriteeditor->zoom > 10)
				spriteeditor->zoom -= 10;
		}
		else
			spriteeditor->zoom += 10;
		break;
	}

	Game_Main();
}

// opens up the default editor for a script
void editScript(CString filename)
{
	TCHAR full_path[250];
	int length = filename.GetLength();
	if (filename[length - 2] == _T('.') && filename[length - 1] == _T('d')) //RW: we are trying to open a .d file
	{
		wsprintf(full_path, _T("%sstory\\%s"), (LPCTSTR)current_map->dmod_path, (LPCTSTR)filename);

		try
		{
			ViewCompressedScript vcsdlg(full_path);
			vcsdlg.DoModal();
		}
		catch (...)
		{
			MB_ShowError(_T("Could not open the file."));
		}
	}
	else //RW: we are opening a .c file
	{
		wsprintf(full_path, _T("%sstory\\%s.c"), (LPCTSTR)current_map->dmod_path, (LPCTSTR)filename);

		HINSTANCE temp = ShellExecute(nullptr, _T("open"), full_path, nullptr, nullptr, SW_SHOWNORMAL);

		if ((int)temp < 32)
			ShellExecute(nullptr, _T("open"), _T("notepad"), full_path, nullptr, SW_SHOWNORMAL);
	}
}
