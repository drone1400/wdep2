#include "StdAfx.h"
#include "MainFrm.h"
#include "WinDinkeditView.h"
#include "Globals.h"
#include "Engine.h"
#include "Sequence.h"
#include "Minimap.h"
#include "Screen.h"
#include "Map.h"
#include "SpriteSelector.h"
#include "SpriteEditor.h"
#include "TileSelector.h"
#include "Colors.h"
#include "ImportMap.h"
#include "Interface.h"
#include "Common.h"
#include "CommonSdl.h"
#include "resource.h"

bool draw_box = false;
int mouse_x_position, mouse_y_position;
int mouse_x_origin, mouse_y_origin;

static void Game_InitSpriteFallbackTexture()
{
    if (temp_sprite_fallback != nullptr)
    {
        SDL_DestroyTexture(temp_sprite_fallback);
    }


    temp_sprite_fallback = SDL_CreateTexture(sdl_renderer, SDL_GetWindowPixelFormat(sdl_window), SDL_TEXTUREACCESS_TARGET, SPRITE_FALLBACK_WIDTH, SPRITE_FALLBACK_HEIGHT);

    // render to temp texture
    SDL_SetRenderTarget(sdl_renderer, temp_sprite_fallback);

    // draw a square grid...
    
    SDL_Rect sdl_dest_rect1 {0, 0, SPRITE_FALLBACK_WIDTH, SPRITE_FALLBACK_HEIGHT};
    SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color_sprite_fallback_back));
    SDL_RenderFillRect(sdl_renderer, &sdl_dest_rect1);

    bool offset = false;
    
    for (int32_t x = 0; x < SPRITE_FALLBACK_WIDTH; x+= SPRITE_FALLBACK_SQUARE)
    {
        for (int32_t y = offset ? SPRITE_FALLBACK_SQUARE : 0; y < SPRITE_FALLBACK_HEIGHT; y+= 2 * SPRITE_FALLBACK_SQUARE)
        {
            SDL_Rect sdl_dest_rect2 = { x, y, SPRITE_FALLBACK_SQUARE, SPRITE_FALLBACK_SQUARE};
            SDL_SetRenderDrawColor(sdl_renderer, EXPAND_SDL_COLOR(color_sprite_fallback_fore));
            SDL_RenderFillRect(sdl_renderer, &sdl_dest_rect2);
        }

        offset = !offset;
    }


    SDL_SetRenderTarget(sdl_renderer, nullptr);
}

void Game_Init()
{
	sdl_window = SDL_CreateWindowFrom(mainWnd->GetRightPane()->GetSafeHwnd());
	if (!sdl_window)
		goto fatal;

	//note: we create the renderer without SDL_RENDERER_PRESENTVSYNC, because vsync would make generating the detailed minimap slower, the way it's done currently
	sdl_renderer = SDL_CreateRenderer(sdl_window, -1, 0); //note: SDL prioritizes creating a hardware-accelerated renderer if possible
	if (!sdl_renderer)
		goto fatal;

	temp_texture = SDL_CreateTexture(sdl_renderer, SDL_GetWindowPixelFormat(sdl_window), SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);
	if (!temp_texture)
		goto fatal;
	
	if (!loadBitmapFont())
		MB_ShowWarning(_T("Failed to load bitmap font!\nOn-screen help won't be available."));

	setColors();

    Game_InitSpriteFallbackTexture();

	initializeInput();

	return;

fatal:
	CString fatalError;
	const char* sdl_err = SDL_GetError();
	if (!sdl_err || !*sdl_err)
		sdl_err = "<unknown error>";
	fatalError.Format(_T("Cannot start WinDinkedit.\n\nFailed to initialize the rendering subsystem.\n\nThe internal error was:\n%hs"), sdl_err);
	MB_ShowGeneric(fatalError,_T("WDEP2 - FATAL ERROR!"), MB_OK | MB_ICONERROR);
	exit(1);
}


void RestoreSurfaces()
{
	memset(current_map->miniupdated,0,sizeof(current_map->miniupdated));

	for (int i = 0; i < MAX_SEQUENCES; i++)
	{
		for (int y = 0; y < MAX_FRAMES; y++)
		{
			if (current_map->sequence[i])
			{
				if (current_map->sequence[i]->frame_image[y])
				{
					//I have no idea if it's necessary to destroy and recreate the textures on device reset, or if it's enough to just re-fill them with data.
					//But since this is supposed to be a very rare occurrence anyway, the performance difference shouldn't matter. So we take the safer route.
					SDL_DestroyTexture(current_map->sequence[i]->frame_image[y]);
					current_map->sequence[i]->frame_image[y] = nullptr;

					current_map->sequence[i]->loadFrame(y);
				}
			}
		}
	}

	unloadBitmapFont();
	loadBitmapFont();

    Game_InitSpriteFallbackTexture();
    

	//the easiest way to deal with the minimap is to just create it anew
	delete current_map->minimap;
	current_map->minimap = new Minimap(current_map);
}

void resize_map_window(int new_width, int new_height)
{
	need_resizing = false;
	
	// reset the map position, only right and bottom side change
	current_map->window_width = new_width;
	current_map->window_height = new_height;
	current_map->updateMapPosition(current_map->window.left, current_map->window.top);

	current_map->sprite_selector.resizeScreen();
	current_map->hard_tile_selector.resizeScreen();
	current_map->minimap->resizeScreen();
}


void Game_Shutdown()
{
	unloadBitmapFont();

	SDL_DestroyRenderer(sdl_renderer);
	SDL_DestroyWindow(sdl_window);

	sdl_renderer = nullptr;
	sdl_window = nullptr;
}


int Game_Main(void)
{
	// if a dmod isn't open or the screen doesn't have focus don't do anything
	if (!current_map)
	{
		SDL_SetRenderTarget(sdl_renderer, nullptr); //we want to draw to the window
		drawClearAll(COLOR_WHITE);
		SDL_RenderPresent(sdl_renderer);

		return false;
	}

	// check if back buffer needs to be resized
	if (need_resizing)
	{
		int new_width = mapRect.right - mapRect.left;
		int new_height = mapRect.bottom - mapRect.top;
	
		//if the window is too small to resize the back buffer return so nothing draws. Otherwise the program will crash.
		if (new_width <= 1 || new_height <= 1)
			return false;

		// make sure it needs resizing first
		if (new_width != current_map->window_width || new_height != current_map->window_height)
		{
			// now resize the map
			resize_map_window(new_width, new_height);
		}
	}

	SDL_Event sdl_event;
	while (SDL_PollEvent(&sdl_event))
	{
		if (sdl_event.type == SDL_RENDER_TARGETS_RESET) //we must re-fill textures we use as render targets
		{
			// We only have two textures we use as render targets in the entire program (search for SDL_TEXTUREACCESS_TARGET).
			// One is temp_texture, which is only used to pre-compose certain views, and holds only transient data, so it doesn't need to be re-created.
			// The other one is the minimap image, we have to fill that.

			current_map->minimap->drawSquares();

			// Other than that, the tile hardness editor is wonky, so we need to refill temp_texture with it.
			// drone: this doesn't seem to be needed, the same code is called when drawing the editor, so the texture will just be refreshed then
			// if (current_map->editor_state == EditorState::TileHardnessEditor)
			// {
			// 	SDL_SetRenderTarget(sdl_renderer, temp_texture);
			// 	current_map->hard_tile_selector.drawHardTile(hardedit_show_texture);
			// }

		    // just in case...
		    Game_InitSpriteFallbackTexture();
		}
		else if (sdl_event.type == SDL_RENDER_DEVICE_RESET) //we must re-create all textures
		{
			RestoreSurfaces();
		}
	}

	//we want to draw to the window
	SDL_SetRenderTarget(sdl_renderer, nullptr);

	// draw the main screen
	if (current_map->editor_state == EditorState::Screen)
	{
		if (current_map->screenMode != EditorSubmode::Sprite && current_map->isScreenFocusMode)
		{
			current_map->isScreenFocusMode = false;
		    current_map->updateMapPosition(current_map->window.left, current_map->window.top);
		}

		if (current_map->isScreenFocusMode)
		{
			drawClearAll(color_blank_screen_fill);
			RECT focus_rect = current_map->getWindowScreenRectRadius(current_map->screenFocusIndex, 3);
			
			drawFilledBox(focus_rect, color_map_screen_grid);
		} else
		{
			drawClearAll(color_map_screen_grid);
		}
		
		// draw the screen(s)
		current_map->drawScreens();

		if (current_map->screenMode == EditorSubmode::Tile)
		{
			if (draw_box)
			{
				RECT dest = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};
				drawBox(dest, color_mouse_box, 1);
			}
			else
			{
				current_map->tile_selector.renderScreenTileHover(mouse_x_position, mouse_y_position);
			}
			current_map->tile_selector.renderScreenTileGrid();

			ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 130);
			ScreenText(_T("Change mode: 'R' - Sprite, 'Y' - Multi-sprite,  'T' - Tile,  'H' - Hardness mode."), 110);
		    ScreenText(_T("Press 'J' to toggle all sprite visibility."),90);
			ScreenText(_T("Pres '1'-'9', '0', or 'U' to open tile selector."), 70);
			ScreenText(_T("Click to select a tile (click and drag for multiple tiles)."), 50);
			ScreenText(_T("Press 'C' or 'CTRL+C' to copy tiles, press 'S' or 'CTRL+V' to stamp the tiles."), 30);
		}
		else if (current_map->screenMode == EditorSubmode::Hardbox)
		{
			if (draw_box)
			{
				RECT dest = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};
				drawBox(dest, color_mouse_box, 1);
			} 
			else 
			{
				current_map->hard_tile_selector.renderScreenTileHover(mouse_x_position, mouse_y_position);
			}
			current_map->hard_tile_selector.screenRender();
		
			ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 130);
			ScreenText(_T("Change mode: 'R' - Sprite, 'Y' - Multi-sprite,  'T' - Tile,  'H' - Hardness mode."), 110);
		    ScreenText(_T("Press 'J' to toggle all sprite visibility."),90);
			ScreenText(_T("Click to select a tile. Press 'C' or 'CTRL+C' to copy tiles. Press 'E' to open the hardness selector."), 70);
			ScreenText(_T("Press 'D' to set chosen hard tile default for selected graphical tile."), 50);
			ScreenText(_T("Press 'Q' to edit the copied hardness, press 'S' or 'CTRL+V' to stamp the tiles."), 30);
		} 
		else if (current_map->screenMode == EditorSubmode::Sprite)
		{
		    if (current_map->isScreenFocusMode)
		    {
		        ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 250);
		        ScreenText(_T("Change mode: 'R' - Sprite, 'Y' - Multi-sprite,  'T' - Tile,  'H' - Hardness mode."), 230);
		        ScreenText(_T("Press 'F3' or 'SHIFT + F3' to select next/previous sprite"),210);
		        ScreenText(_T("Press 'F' to toggle single screen Sprite Focus mode"),190);
		        ScreenText(_T("Press 'E' to open sprite selector."), 170);
		        ScreenText(_T("Pickup sprite by left clicking or 'CTRL+X' while hovering over. 'CTRL+C' picks up a copy'!"),150);
		        ScreenText(_T("Left click or 'CTRL+V' to place picked up sprite, or 'S' to stamp a copy of it."),130);
		        ScreenText(_T("Right click a sprite to view its properties."),110);
		        ScreenText(_T("'Q' to edit hardness and depth dot. Use '[' and ']' to resize."),90);
		        ScreenText(_T("Hold down 'Z' or 'X' then use arrow keys to trim picked sprite."), 70);
		        ScreenText(_T("Press 'M' to toggle screen match, 'Shift+M' for screenmatch duplicates detection."), 50);
		        ScreenText(_T("Right click for screen details. Hold space bar to see hardness, 'I' for sprite info."), 30);
		    } else
		    {
		        ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 230);
		        ScreenText(_T("Change mode: 'R' - Sprite, 'Y' - Multi-sprite,  'T' - Tile,  'H' - Hardness mode."), 210);
		        ScreenText(_T("Press 'F' to toggle single screen Sprite Focus mode"),190);
		        ScreenText(_T("Press 'E' to open sprite selector."), 170);
		        ScreenText(_T("Pickup sprite by left clicking or 'CTRL+X' while hovering over. 'CTRL+C' picks up a copy'!"),150);
		        ScreenText(_T("Left click or 'CTRL+V' to place picked up sprite, or 'S' to stamp a copy of it."),130);
		        ScreenText(_T("Right click a sprite to view its properties."),110);
		        ScreenText(_T("'Q' to edit hardness and depth dot. Use '[' and ']' to resize."),90);
		        ScreenText(_T("Hold down 'Z' or 'X' then use arrow keys to trim picked sprite."), 70);
		        ScreenText(_T("Press 'M' to toggle screen match, 'Shift+M' for screenmatch duplicates detection."), 50);
		        ScreenText(_T("Right click for screen details. Hold space bar to see hardness, 'I' for sprite info."), 30);
		    }
		}
		else if (current_map->screenMode == EditorSubmode::MultiSprite)
		{
			ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 130);
			ScreenText(_T("Change mode: 'R' - Sprite, 'Y' - Multi-sprite,  'T' - Tile,  'H' - Hardness mode."), 110);
			ScreenText(_T("Click a sprite to toggle its selected state. Press 'C' to clear away all selections."), 90);
			ScreenText(_T("Press 'SHIFT' and drag a box to select intersected sprites, 'CTRL+SHIFT' for contained ones."), 70);
			ScreenText(_T("Press 'P' or 'CTRL+X' to pick up selected sprites, 'SHIFT+P' or 'CTRL+C' to pick up a copy."), 50);
			ScreenText(_T("Press 'Enter' or 'CTRL+V' or click to put picked sprites down. Press 'Delete' to delete all selected sprites."), 30);
		}
	}
	// check if minimap needs to be shown
	else if (current_map->editor_state == EditorState::Minimap)
	{
		// draw a box around the selected tile
		current_map->minimap->render();

		ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 90);
		ScreenText(_T("Press 'CTRL+C' to copy a selected existing screen and 'CTRL+V' to paste it in an empty slot."), 70);
		ScreenText(_T("Press space bar to toggle detail minimap. Click a square to view it."), 50);
		ScreenText(_T("Right click on any map square to select its details."), 30);
	}
	// check if sprite selector should be shown
	else if (current_map->editor_state == EditorState::SpriteSelector)
	{
		// fills the back buffer with black
		drawClearAll(color_map_screen_grid);
		current_map->sprite_selector.render();
		current_map->sprite_selector.drawGrid(mouse_x_position, mouse_y_position);

		ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 70);
		ScreenText(_T("Use PageDown/PageUp to navigate."), 50);
		ScreenText(_T("Click a square to select sprite sequence."), 30);
	}
	// check if tile selector should be shown
	else if (current_map->editor_state == EditorState::TileSelector)
	{
		drawClearAll(color_map_screen_grid);
		current_map->tile_selector.renderTileSheet();

		if (draw_box)
		{
			RECT dest = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};
			drawBox(dest, color_mouse_box, 1);
		}
		else
		{
			current_map->tile_selector.renderTileSheetTileHover(mouse_x_position, mouse_y_position);
		}

		if (current_map->screenMode == EditorSubmode::Tile)
		{
			ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 110);
			ScreenText(_T("Change mode: T - Tile,  H - Hardness mode, ESC - return to map screen"), 90);
			ScreenText(_T("Click or draw a box to select tiles. 'C' - copy tiles."), 70);
			ScreenText(_T("WARNING: Last 4 tiles on the bottom row of a tile sheet can not be used!"), 30);
		} else if (current_map->screenMode == EditorSubmode::Hardbox)
		{
			ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 130);
			ScreenText(_T("Change mode: T - Tile,  H - Hardness mode, ESC - return to map screen"), 110);
			ScreenText(_T("'E' - open the hardness selector, 'D' - set selected hardness as default, 'Q' - edit hardness under cursor"), 90);
			ScreenText(_T("'B' or 'DEL' - clear default hardness to empty."), 70);
			ScreenText(_T("'Q' - edit selected hardness. WARNING: You can't edit a cleared hardness!"), 50);
			ScreenText(_T("WARNING: Last 4 tiles on the bottom row of a tile sheet can not be used!"), 30);
		}
	}
	// check if sprite selector should be shown
	else if (current_map->editor_state == EditorState::HardTileSelector)
	{
		drawClearAll(color_map_screen_grid);
		current_map->hard_tile_selector.render();
		current_map->hard_tile_selector.drawHardTileSelectorGrid(mouse_x_position, mouse_y_position);
	}
	else if (current_map->editor_state == EditorState::ScreenImporter)
	{
		current_map->import_map->render();

		if (current_map->import_map->source_tile == -1)
		{
			ScreenText(_T("Click on the screen you want to import."), 50);
			ScreenText(_T("Press Escape to cancel."), 30);
		}
		else
		{
			ScreenText(_T("Click on the empty screen you want to import to."), 50);
			ScreenText(_T("Press Escape to cancel."), 30);
		}
	}
	else if (current_map->editor_state == EditorState::TileHardnessEditor)
	{
		drawClearAll(color_map_screen_grid);
		current_map->hard_tile_selector.hardTileEditorRender(mouse_x_position, mouse_y_position, hardedit_show_texture);

		ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 130);
		ScreenText(_T("Click and paint to change the hardness. "), 110);
		ScreenText(_T("Hold Control for low hardness, or Shift for unknown hardness. Hold Space to hide texture."), 90);
		ScreenText(_T("Click same hardness to go in erase mode. 'B' or 'Delete' to clear all hardness. "), 70);
		ScreenText(_T("Press Escape to save and exit, Tab to exit without saving."), 50);
		ScreenText(_T("WARNING: HardIndex #0 can not be edited! A red cross will be displayed."), 30);
		
	}
	else if (current_map->editor_state == EditorState::SpriteHardnessEditor)
	{				
		if (draw_box)	//RW: mouse is held down (user is drawing the hardness rectangle)
		{
			spriteeditor->DrawEditor(false);
			RECT hard_box = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};	//RW: left, top, right, bottom					
			drawFilledBox(hard_box, SDL_Color{ 128, 128, 128, 255 });
		}
		else
			spriteeditor->DrawEditor(true);
				
		TCHAR s[80];

		ScreenText(_T("Press F12 to toggle this text, F5 to change its color."), 90);
		ScreenText(_T("Click and drag to change the hard box. Right click to change the Depth Dot."), 70);
		wsprintf(s, _T("Use Page Up and Page Down to zoom, Home to reset (now at %d%%)."), spriteeditor->zoom);
		ScreenText(s, 50);
		ScreenText(_T("Press Escape to save and exit, Tab to exit without saving."), 30);
	}

	SDL_RenderPresent(sdl_renderer);

	return true;
}
