#pragma once

// .c -> .d stuff
#define MIN_BUFFER_SIZE		1024
#define BIG_BUFFER_SIZE		(1 << 16)
#define TABLE_SIZE			128
#define MINI_BUFFER_SIZE	(1 << 12)

void ScriptCompressAll(BOOL delete_file, BOOL remove_comments);
void ScriptDecompressAll(bool delete_files);