#pragma once

void Game_Init();
void Game_Shutdown();
int Game_Main();

void resize_map_window(int new_width, int new_height);

class SpriteSelector;
class TileSelector;

extern bool draw_box;
extern int mouse_x_position, mouse_y_position;
extern int mouse_x_origin, mouse_y_origin;