#pragma once

// function for loading bitmaps using SDL_image
SDL_Texture* loadBitmapImage(LPCTSTR szBitmap, int& width, int& height, ColorKey color_key=COLORKEY_NONE);

SDL_Texture* loadBitmap(LPCTSTR szBitmap, int& width, int& height, ColorKey color_key=COLORKEY_NONE);
SDL_Texture* loadBitmapStream(FILE *input_file, int& new_width, int& new_height, ColorKey color_key=COLORKEY_NONE);