#pragma once

#define LOG_ENABLE_CRITICAL			0x01
#define LOG_ENABLE_ERROR			0x02
#define LOG_ENABLE_WARNING			0x04
#define LOG_ENABLE_INFO				0x08
#define LOG_ENABLE_DEBUG			0x10
#define LOG_ENABLE_VERBOSE			0x20
#define LOG_ENABLE_NONE				0x00
#define LOG_ENABLE_ALL				0xFF

#define LOG_FORMAT_RAW							0x00
#define LOG_FORMAT_HEADER						0x01
#define LOG_FORMAT_HEADER_PADDING_ONLY			0x02
#define LOG_FORMAT_SMALL_PADDING_ONLY			0x04
#define LOG_FORMAT_NEW_LINE						0x10
#define LOG_FORMAT_DOUBLE_NEW_LINE				0x20
#define LOG_FORMAT_STANDARD						(LOG_FORMAT_HEADER | LOG_FORMAT_NEW_LINE)
#define LOG_FORMAT_CONTINUE						(LOG_FORMAT_SMALL_PADDING_ONLY | LOG_FORMAT_NEW_LINE)
#define LOG_FORMAT_CONTINUE_END					(LOG_FORMAT_SMALL_PADDING_ONLY | LOG_FORMAT_DOUBLE_NEW_LINE)


void Log_PrintTime(std::ofstream& stream);

void Log_Initialize(CString basePath, int levels = LOG_ENABLE_ALL);
void Log_Close();
void Log_Flush();

void Log_Critical(CString const& msg, int format = LOG_FORMAT_STANDARD);
void Log_Error(CString const& msg, int format = LOG_FORMAT_STANDARD);
void Log_Warning(CString const& msg, int format = LOG_FORMAT_STANDARD);
void Log_Info(CString const& msg, int format = LOG_FORMAT_STANDARD);
void Log_Debug(CString const& msg, int format = LOG_FORMAT_STANDARD);
void Log_Verbose(CString const& msg, int format = LOG_FORMAT_STANDARD);

// formerly in tools.h
void ConvertToUtf8(CString const& mfcString, std::vector<char>& utfString);