#include "stdafx.h"
#include "IntValueDialog.h"


IMPLEMENT_DYNAMIC(CIntValueDialog, CDialog)

CIntValueDialog::CIntValueDialog(const TCHAR* title /*=""*/, int startvalue /*= 0*/, int min /*=MININT*/, int max /*=MAXINT*/, CWnd* pParent /*=nullptr*/)
	: CDialog(IDD, pParent)
	, m_value(0)
{
	this->min = min;
	this->max = max;
	if (startvalue < min || startvalue > max)
		m_value = min;
	else
		m_value = startvalue;
	_tcscpy_s(this->title, title);
}

CIntValueDialog::~CIntValueDialog()
{
}

void CIntValueDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_VALUE_BOX, m_value);
	DDV_MinMaxInt(pDX, m_value, min, max);
}


BEGIN_MESSAGE_MAP(CIntValueDialog, CDialog)
END_MESSAGE_MAP()


BOOL CIntValueDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	GetDlgItem(IDC_VALUE_BOX)->SetFocus();
	SetWindowText(title);
	return FALSE;
}