#include "StdAfx.h"
#include "Minimap.h"
#include "Globals.h"
#include "ddutil.h"
#include "Map.h"
#include "Engine.h"
#include "Screen.h"
#include "MainFrm.h"
#include "Colors.h"
#include "Common.h"
#include "CommonSdl.h"

Minimap::Minimap(Map* map)
{
	image = nullptr;
	mapSquareImage[0] = nullptr;
	mapSquareImage[1] = nullptr;
	mapSquareImage[2] = nullptr;
	mapSquareImage[3] = nullptr;

	loadSurface(map);

	hover_screen = 0;
	hover_x_screen = 0;
	hover_y_screen = 0;
	
	isDetailed = false;
}

Minimap::~Minimap()
{
	if (image != nullptr)
		SDL_DestroyTexture(image);

	for (int i = 0; i < 4; i++)
	{
		if (mapSquareImage[i] != nullptr)
			SDL_DestroyTexture(mapSquareImage[i]);
	}
}

bool Minimap::loadSquare(CString const& dmod_path, const TCHAR *filename, int square_num)
{
	// load the map squares
	TCHAR bmp_file[MAX_PATH];

	int dummy = 0;
	
	wsprintf(bmp_file, _T("%sTiles\\%s"), (LPCTSTR)dmod_path, filename);
	if ((mapSquareImage[square_num] = loadBitmapImage(bmp_file, dummy, dummy, COLORKEY_BLACK)) == nullptr)
	{
		// try loading from the dink dmod directory
		if (is_dink_core_path_override_enabled) {
			wsprintf(bmp_file, _T("%s\\Tiles\\%s"), (LPCTSTR)dink_core_path_override, filename);
		} else {
			wsprintf(bmp_file, _T("%s\\Tiles\\%s"), (LPCTSTR)dink_core_path, filename);
		}
		
		if ((mapSquareImage[square_num] = loadBitmapImage(bmp_file, dummy, dummy, COLORKEY_BLACK)) == nullptr)
			return false;
	}

	return true;
}

void Minimap::loadSurface(Map* map)
{
	image = SDL_CreateTexture(sdl_renderer, SDL_GetWindowPixelFormat(sdl_window), SDL_TEXTUREACCESS_TARGET, MINI_MAP_WIDTH, MINI_MAP_HEIGHT);

    bool loaded1 = loadSquare(map->dmod_path, SQUARE_EMPTY_NAME_PNG, SQUARE_EMPTY);
    if (!loaded1)
        loaded1 =  loadSquare(map->dmod_path, SQUARE_EMPTY_NAME, SQUARE_EMPTY);

    bool loaded2 = loadSquare(map->dmod_path, SQUARE_USED_NAME_PNG, SQUARE_USED);
    if (!loaded2)
        loaded2 =  loadSquare(map->dmod_path, SQUARE_USED_NAME, SQUARE_USED);

    bool loaded3 = loadSquare(map->dmod_path, SQUARE_MIDI_NAME_PNG, SQUARE_MIDI);
    if (!loaded3)
        loaded3 =  loadSquare(map->dmod_path, SQUARE_MIDI_NAME, SQUARE_MIDI);

    bool loaded4 = loadSquare(map->dmod_path, SQUARE_INDOOR_NAME_PNG, SQUARE_INDOOR);
    if (!loaded4)
        loaded4 =  loadSquare(map->dmod_path, SQUARE_INDOOR_NAME, SQUARE_INDOOR);

    bool loaded = loaded1 && loaded2 && loaded3 && loaded4;

    
	if (!loaded)
		MB_ShowError(_T("Could not load the minimap square images!"));
	
	drawSquares(map);
}

void Minimap::toggleDetailing()
{
	if (isDetailed == false)
	{
		drawMap();
	} else
	{
		drawSquares();
	}
}


// draws the entire minimap
int Minimap::drawMap()
{
	SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target

	RECT dst_rect;

	dst_rect.top = 0;
	dst_rect.bottom = square_height;

	int cur_screen = 0;

	// make clip box for use when rendering each screen	
	RECT screen_box = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

	mainWnd->m_wndStatusBar.SetRange(1,768);

	for (int y = 0; y < MAP_ROWS; y++)
	{
		dst_rect.left = 0;
		dst_rect.right = square_width;
		for (int x = 0; x < MAP_COLUMNS; x++, cur_screen++)
		{
			if (KEY_DOWN(VK_ESCAPE))
				return true;

			//skip if it doesn't need to be updated and if the option is enabled
			if (!current_map->miniupdated[cur_screen] || !fast_minimap_update)
			{
				if (current_map->screen[cur_screen] != nullptr)
				{
					SDL_SetRenderTarget(sdl_renderer, temp_texture); //TODO: temp_texture is the analogue of lpddshidden not lpddsscreentemp, but maybe we can get away with this
					drawClearAll(COLOR_BLACK);
					
					current_map->screen[cur_screen]->drawTiles(0, 0, &screen_box);
					current_map->screen[cur_screen]->drawSprites(0, 0, screen_box);

					SDL_SetRenderTarget(sdl_renderer, image);
					drawTexture(temp_texture, &screen_box, &dst_rect);
					
					if (show_minimap_progress)
					{
						SDL_SetRenderTarget(sdl_renderer, nullptr);
						drawTexture(image, nullptr, nullptr);
						SDL_RenderPresent(sdl_renderer);
					}
				}
				else
				{
					SDL_SetRenderTarget(sdl_renderer, image);
					drawFilledBox(dst_rect, SDL_Color{ 0, 0, 0, 255 });
				}

				current_map->miniupdated[cur_screen] = true;
			}
			dst_rect.left += square_width;
			dst_rect.right += square_width;

			mainWnd->m_wndStatusBar.OnProgress(cur_screen);
			mainWnd->m_wndStatusBar.OnProgress(cur_screen-1);	//RW: ugly hack to keep Windows Vista/7 progress bar correctly updated.
			mainWnd->m_wndStatusBar.OnProgress(cur_screen);
		}
		dst_rect.top += square_height;
		dst_rect.bottom += square_height;
	}
	mainWnd->setStatusText(_T("Finished Minimap Detailing"));

	SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target
	isDetailed = true;
	return 1;
}

// draws purple or red tile on minimap for target screen
void Minimap::renderMapSquare(int cur_screen, Map* map /*= nullptr*/)
{
	if (map == nullptr)
		map = current_map;

	SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target
	SDL_SetRenderTarget(sdl_renderer, image);

	int x_square = cur_screen % MAP_COLUMNS;
	int y_square = cur_screen / MAP_COLUMNS;

	RECT dest = {0, 0, square_width, 0};
	dest.left = x_square * square_width;
	dest.top = y_square * square_height;
	dest.right = dest.left + square_width;
	dest.bottom = dest.top + square_height;

	// draw blank screen tile
	if (map->screen[cur_screen] == nullptr)
	{
		drawTexture(mapSquareImage[SQUARE_EMPTY], nullptr, &dest);
	}	
	else
	{
		drawTexture(mapSquareImage[SQUARE_USED], nullptr, &dest);
		if (map->midi_num[cur_screen] != 0)
			drawTexture(mapSquareImage[SQUARE_MIDI], nullptr, &dest);
		if (map->indoor[cur_screen] == 1)
			drawTexture(mapSquareImage[SQUARE_INDOOR], nullptr, &dest);
	}

	SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target
}

// draws purple and red tiles for entire minimap
void Minimap::drawSquares(Map* map /*= nullptr*/)
{
	if (map == nullptr)
		map = current_map;

	//setting the render target here is not exactly necessary because renderMapSquare() also does it, but we do it for performance (it's potentially faster to switch to the same target)
	SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target
	SDL_SetRenderTarget(sdl_renderer, image);

	//if we're drawing the purple and red squares, then we might be overwriting the detailed one... so let's make sure we can draw the detailed one again later
	for (int i = 0; i < NUM_SCREENS; i++)
		map->miniupdated[i] = false;

	drawClearAll(COLOR_BLACK);

	for (int screen = 0; screen < NUM_SCREENS; screen++)
		renderMapSquare(screen, map);

	SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target

	isDetailed = false;
}

// used when importing a screen from another map
int Minimap::renderImportMap(int screen_order[], int midi_num[], int indoor[])
{
	SDL_Texture* orig_target = SDL_GetRenderTarget(sdl_renderer); //save the render target
	SDL_SetRenderTarget(sdl_renderer, image);

	drawClearAll(COLOR_BLACK);

	int cur_screen = 0;

	RECT dest = {0, 0, square_width, square_height};
	for (int y = 0; y < MAP_ROWS; y++)
	{
		dest.right = square_width;
		dest.left = 0;
		for (int x = 0; x < MAP_COLUMNS; x++)
		{
			if (screen_order[cur_screen] == 0)
			{
				drawTexture(mapSquareImage[SQUARE_EMPTY], nullptr, &dest);
			}
			else
			{
				drawTexture(mapSquareImage[SQUARE_USED], nullptr, &dest);
				if (midi_num[cur_screen] != 0)
					drawTexture(mapSquareImage[SQUARE_MIDI], nullptr, &dest);
				if (indoor[cur_screen] == 1)
					drawTexture(mapSquareImage[SQUARE_INDOOR], nullptr, &dest);
			}

			dest.left += square_width;
			dest.right += square_width;

			cur_screen++;
		}
		dest.bottom += square_height;
		dest.top += square_height;
	}

	SDL_SetRenderTarget(sdl_renderer, orig_target); //restore the render target

	return true;
}

// draws the minimap onto the screen
void Minimap::render()
{
	assert(SDL_GetRenderTarget(sdl_renderer) == nullptr);

	SDL_RenderCopy(sdl_renderer, image, nullptr, nullptr);

	//draw a box around the currently highlighted screen
	RECT dest = { int(x_scale * double(hover_x_screen)),
		int(y_scale * double(hover_y_screen)), 
		int(x_scale * double(hover_x_screen + 1)),
		int(y_scale * double(hover_y_screen + 1)) };
	drawBox(dest, color_minimap_screen_selected);
}

void Minimap::updateHoverPosition(int x, int y)
{
// displays screen coordinates of mouse pointer
	hover_x_screen = int(double(x) / x_scale);
	hover_y_screen = int(double(y) / y_scale);
	hover_screen = hover_y_screen * MAP_COLUMNS + hover_x_screen;
}

// calculates the x and y location of the screen
void Minimap::loadScreen()
{
	int x_location = hover_x_screen * (SCREEN_WIDTH_GAP) + SCREEN_WIDTH / 2 - (current_map->window_width / 2);
	int y_location = hover_y_screen * (SCREEN_HEIGHT_GAP) + SCREEN_HEIGHT / 2 - (current_map->window_height / 2);

	current_map->updateMapPosition(x_location, y_location);

	current_map->editor_state = EditorState::Screen;
	Game_Main();
}

void Minimap::resizeScreen()
{
	x_scale = double(current_map->window_width) / MAP_COLUMNS;
	y_scale = double(current_map->window_height) / MAP_ROWS;

	// the minimap image gets invalidated by the resize, so let's make sure we can render it again
	for (int i = 0; i < NUM_SCREENS; i++)
		current_map->miniupdated[i] = false;
}