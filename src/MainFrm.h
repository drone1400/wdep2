#pragma once
#include "StatusBar.h"
class CWinDinkeditView;
class CLeftView;

class CMainFrame : public CFrameWnd
{
public:
	int setStatusText(const TCHAR *text);
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

protected:
	CSplitterWnd m_wndSplitter;

public:

	afx_msg void OnTimer(UINT_PTR nIdEvent);

	BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) override;
	BOOL PreCreateWindow(CREATESTRUCT& cs) override;

public:
	virtual ~CMainFrame();
	CWinDinkeditView* GetRightPane();
	CLeftView* GetLeftPane();
#ifdef _DEBUG
	void AssertValid() const override;
	void Dump(CDumpContext& dc) const override;
#endif

public:  // control bar embedded members
	CProgStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CReBar      m_wndReBar;
	CProgressCtrl m_wndProgress;
	CDialogBar      m_wndDlgBar;

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnMove(int x, int y);
	afx_msg void OnDmodProperties();
	afx_msg void OnUpdateDmodProperties(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewStyles(CCmdUI* pCmdUI);
	afx_msg void OnViewStyle(UINT nCommandID);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileSave();
	afx_msg void OnFilePlaygame();
	afx_msg void OnUpdateFilePlaygame(CCmdUI *pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditRedo(CCmdUI *pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAutoUpdateMinimap(CCmdUI *pCmdUI);
	afx_msg void OnAutoUpdateMinimap();
	afx_msg void OnUpdateSettingsSnaptogrid(CCmdUI *pCmdUI);
	afx_msg void OnSettingsSnaptogrid();
	afx_msg void OnUpdateFileOpendmodfolder(CCmdUI *pCmdUI);
	afx_msg void OnFileOpendmodfolder();
	afx_msg void OnUpdateIndicatorMousePos(CCmdUI* pCmdUI);
	afx_msg void OnUpdateIndicatorVision(CCmdUI* pCmdUI);
	afx_msg void OnUpdateIndicatorSpriteId(CCmdUI *pCmdUI);
	afx_msg void OnUpdateIndicatorSpriteSequence(CCmdUI *pCmdUI);
	afx_msg void OnUpdateIndicatorSpriteFrame(CCmdUI *pCmdUI);
	afx_msg void OnUpdateIndicatorTileId(CCmdUI* pCmdUI);
	afx_msg void OnUpdateIndicatorTileHard(CCmdUI* pCmdUI);
	afx_msg void OnFileDinkInstallation();
	afx_msg void OnUpdateFileDinkInstallation(CCmdUI *pCmdUI);
	afx_msg void OnFileCloseDMod();
	afx_msg void OnUpdateFileCloseDMod(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDuplicateDetect(CCmdUI *pCmdUI);
	afx_msg void OnDuplicateDetect();
	afx_msg void OnOptions();
	afx_msg void OnUpdateOptions(CCmdUI *pCmdUI);
	afx_msg void OnScreenShot();
	afx_msg void OnUpdateScreenShot(CCmdUI *pCmdUI);
	afx_msg void OnCompressScripts();
	afx_msg void OnUpdateCompressScripts(CCmdUI *pCmdUI);
	afx_msg void OnDecompressScripts();
	afx_msg void OnUpdateDecompressScripts(CCmdUI *pCmdUI);
	afx_msg void OnScreenMatch();
	afx_msg void OnUpdateScreenMatch(CCmdUI *pCmdUI);
	afx_msg void OnFfcreate();
	afx_msg void OnUpdateFfcreate(CCmdUI *pCmdUI);
	afx_msg void OnToolsFfextract();
	afx_msg void OnUpdateToolsFfextract(CCmdUI *pCmdUI);
	afx_msg void OnVision();
	afx_msg void OnUpdateVision(CCmdUI *pCmdUI);
	afx_msg void OnSpriteHardness();
	afx_msg void OnUpdateSpriteHardness(CCmdUI *pCmdUI);
    afx_msg void OnSpriteReport();
	afx_msg void OnImportScreen();
	afx_msg void OnUpdateImportScreen(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpriteReport(CCmdUI *pCmdUI);
	afx_msg void OnViewMinimap();
	afx_msg void OnUpdateViewMinimap(CCmdUI *pCmdUI);
	afx_msg void OnViewSpriteMode();
	afx_msg void OnUpdateViewSpriteMode(CCmdUI *pCmdUI);
	afx_msg void OnToolsSpritenohitbydefault();
	afx_msg void OnUpdateToolsSpritenohitbydefault(CCmdUI *pCmdUI);

private:
	BOOL timerIndicatorScreenPosDelayRunning;
};

extern CMainFrame* mainWnd;