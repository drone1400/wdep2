#include "stdafx.h"
#include "Fastfile.h"
#include "Globals.h"
#include <io.h>
#include <sys/stat.h>
#include "MainFrm.h"
#include "Map.h"
using std::vector;

Fastfile::Fastfile(CWnd* pParent /*=nullptr*/) : CDialog(Fastfile::IDD, pParent)
{	
	m_delete_graphics = FALSE;
}

void Fastfile::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_DELETE_GRAPHICS, m_delete_graphics);
}

BEGIN_MESSAGE_MAP(Fastfile, CDialog)
	ON_BN_CLICKED(IDOK, &Fastfile::OnBnClickedOk)
END_MESSAGE_MAP()

void Fastfile::OnBnClickedOk()
{
	UpdateData();

	TCHAR path[MAX_PATH];
	wsprintf(path, _T("%sGraphics\\"), (LPCTSTR)current_map->dmod_path);

	mainWnd->setStatusText(_T("Packing..."));
	BeginWaitCursor();
	FFCreate(path);
	EndWaitCursor();
	mainWnd->setStatusText(_T("Done packing graphics!"));

	CDialog::OnOK();
}

// compare function used for sorting
bool compare_filenames(Img arg1, Img arg2 )
{
	return _stricmp(arg1.name.s, arg2.name.s) < 0;
}

void Fastfile::FFCreate(const TCHAR* path)
{
	HANDLE hFind;
	WIN32_FIND_DATA dataFind;
	BOOL bMoreFiles = TRUE;
	int i;

	TCHAR buffer[MAX_PATH];

	_tcscpy_s(buffer, path);
	_tcscat_s(buffer, _T("*.bmp"));

	hFind = FindFirstFile(buffer, &dataFind);

	vector<Img> file_list;

	// get all the *.bmp files in the directory
	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)
	{
		Img f { 0, "" };
		if (EightDotThreeFilename::from_tchar(dataFind.cFileName, f.name))
			file_list.push_back(f);

		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	FindClose(hFind);

	_tcscpy_s(buffer, path);
	_tcscat_s(buffer, _T("*.*"));

	hFind = FindFirstFile(buffer, &dataFind);
	bMoreFiles = TRUE;

	// get all the directories
	while (hFind != INVALID_HANDLE_VALUE && bMoreFiles == TRUE)
	{
		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 && dataFind.cFileName[0] != '.')
		{
			_tcscpy_s(buffer, path);
			_tcscat_s(buffer, dataFind.cFileName);
			_tcscat_s(buffer, _T("\\"));

			FFCreate(buffer);
		}

		bMoreFiles = FindNextFile(hFind, &dataFind);
	}

	FindClose(hFind);

	if (file_list.size() == 0)
		return;

	std::sort(file_list.begin(), file_list.end(), compare_filenames);	//Sort the filenames alphabetically

	FILE *output, *input;

	_tcscpy_s(buffer, path);
	_tcscat_s(buffer, _T("dir.ff"));

	//Show the user that something is actually happening
	mainWnd->setStatusText(buffer);

	if ((output = _tfopen(buffer, _T("wb"))) == nullptr)
		return;

	int temp = file_list.size() + 1;
	fwrite(&temp, sizeof(int), 1, output);

	Img dummy;
	memset(&dummy, 0, sizeof(Img));
	for (i = 0; i < file_list.size() + 1; i++)
		fwrite(&dummy, 17, 1, output);

	BYTE data_buffer[FF_DATA_BUFFER_SIZE];

	int file_offset_location = 4 + (file_list.size() + 1) * 17;

	for (i = 0; i < file_list.size(); i++)
	{
		_tcscpy_s(buffer, path);
		_tcscat_s(buffer, file_list[i].name.as_tchar());

		file_list[i].offset = file_offset_location;

		if ((input = _tfopen(buffer, _T("rb"))) == nullptr)
		{
			fclose(output);
			return;
		}

		int amount_read;
		while ((amount_read = fread(data_buffer, 1, FF_DATA_BUFFER_SIZE, input)) != 0)
		{
			file_offset_location += amount_read;
			fwrite(data_buffer, 1, amount_read, output);
		}

		fclose(input);

		// delete file if option has been selected
		if (m_delete_graphics)
		{
			// remove read only attribute
			if (_tchmod(buffer, _S_IREAD | _S_IWRITE) == 0)	//if successful permission change
				_tremove(buffer);
		}
	}

	// go to fourth byte in file
	fseek(output, sizeof(int), SEEK_SET);

	for (i = 0; i < file_list.size(); i++)
		fwrite(&file_list[i], 17, 1, output);

	dummy.offset = file_offset_location;

	fwrite(&dummy, 17, 1, output);

	fclose(output);
}