﻿#include "StdAfx.h"
#include "resource.h"
#include "Common.h"
#include "Globals.h"
#include "Map.h"
#include "Colors.h"
#include "MainFrm.h"
#include "PathUtil.h"
#include "CommonFile.h"
#include "SimpleLog.h"

using std::vector;
using std::ifstream;
using std::ofstream;
using std::endl;

void compensateScreenGap(int &x, int &y)
{
	// compensate for screen gap
	x -= SIDEBAR_WIDTH;
	if (x > SCREEN_WIDTH)
	{
		x -= screen_gap;

		if (x < SCREEN_WIDTH)
			x = SCREEN_WIDTH;
	}

	if (x < 0)
	{
		x += screen_gap;

		if (x > 0)
			x = 0;
	}
	x += SIDEBAR_WIDTH;

	if (y > SCREEN_HEIGHT)
	{
		y -= screen_gap;

		if (y < SCREEN_HEIGHT)
			y = SCREEN_HEIGHT;
	}

	if (y < 0)
	{
		y += screen_gap;

		if (y > 0)
			y = 0;
	}
}

// -------------------------------------------------------------------------------------------------------------------------------------
// Message Box Helper functions
// -------------------------------------------------------------------------------------------------------------------------------------

CString& ConcatInto(CString& into, CString const& s1, CString const& s2, CString const& s3 /*=_T("")*/, CString const& s4 /*=_T("")*/)
//not very efficient, but super convenient
{
	into.SetString(s1);
	into.Append(s2);
	into.Append(s3);
	into.Append(s4);
	return into;
}

int MB_ShowGeneric(CString const& message, CString const& caption, UINT buttons)
{
	return MessageBox(mainWnd->GetSafeHwnd(), message, caption, buttons);
}

void MB_ShowError(CString const& message)
{
	MessageBox(mainWnd->GetSafeHwnd(), message, _T("WDEP2 - Error"), MB_OK | MB_ICONERROR);
}

void MB_ShowError(CString const& message1, CString const& message2, CString const& message3, CString const& message4)
{
    CString message;
    ConcatInto(message, message1, message2, message3, message4);
    MessageBox(mainWnd->GetSafeHwnd(), message, _T("WDEP2 - Error"), MB_OK | MB_ICONERROR);
}

void MB_ShowWarning(CString const& message)
{
	MessageBox(mainWnd->GetSafeHwnd(), message, _T("WDEP2 - Warning"), MB_OK | MB_ICONWARNING);
}
void MB_ShowWarning(CString const& message1, CString const& message2, CString const& message3, CString const& message4)
{
	CString message;
	ConcatInto(message, message1, message2, message3, message4);
	MessageBox(mainWnd->GetSafeHwnd(), message, _T("WDEP2 - Warning"), MB_OK | MB_ICONWARNING);
}