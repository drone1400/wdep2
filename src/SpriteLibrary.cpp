#include "StdAfx.h"
#include "Globals.h"
#include "SpriteLibrary.h"
#include "Common.h"
#include "Map.h"
#include "LeftView.h"
#include "MainFrm.h"

void SpriteLibrary::addSprite(Sprite* cur_sprite, TStdString const& new_name)
{
	if (doesExist(new_name))
	{
		MB_ShowError(_T("Cannot change name, entry with that name already exists!"));
		return;
	}

	if (new_name.size()+1 > MAX_SPRITE_NAME_LENGTH)
	{
		MB_ShowError(_T("New name too long!"));
		return;
	}

	library[new_name] = *cur_sprite;

	mainWnd->GetLeftPane()->UpdateSpriteLibraryTree(current_map->sprite_library);
}

Sprite SpriteLibrary::getSprite(TStdString const& name)
{
	auto it = library.find(name);
	if (it == library.end())
		throw "Invalid sprite library entry name.";

	Sprite new_mouse_sprite(it->second);
	new_mouse_sprite.vision = current_map->cur_vision;

	return new_mouse_sprite;
}

void SpriteLibrary::removeSprite(TStdString const& name)
{
	library.erase(name);
	mainWnd->GetLeftPane()->UpdateSpriteLibraryTree(current_map->sprite_library);
}

bool SpriteLibrary::doesExist(TStdString const& name)
{
	return library.find(name) != library.end();
}

void SpriteLibrary::renameSprite(TStdString const& old_name, TStdString const& new_name)
{
	if (doesExist(new_name))
	{
		MB_ShowError(_T("Cannot change name, entry with that name already exists!"));
		return;
	}
	
	if (new_name.size() + 1 > MAX_SPRITE_NAME_LENGTH)
	{
		MB_ShowError(_T("New name too long!"));
		return;
	}

	auto it = library.find(old_name);
	if (it == library.end())
		return;

	Sprite s = it->second;

	library.erase(it);
	library[new_name] = s;

	mainWnd->GetLeftPane()->UpdateSpriteLibraryTree(current_map->sprite_library);
}

void SpriteLibrary::storeList()
{
	FILE *stream;

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)current_map->dmod_path, _T("library.dat"));

	if ((stream = _tfopen(filename, _T("wb"))) == nullptr)
		return;

	int len = library.size();
	fwrite(&len, sizeof(int), 1, stream);

	for (auto& p : library)
	{
		char name[MAX_SPRITE_NAME_LENGTH];
		#ifdef UNICODE
		WideCharToMultiByte(CP_ACP, 0, p.first.c_str(), -1, name, MAX_SPRITE_NAME_LENGTH, nullptr, nullptr);
		#else
		strcpy(name, p.first.c_str());
		#endif

		fwrite(name, sizeof(char), MAX_SPRITE_NAME_LENGTH, stream);
		fwrite(&p.second, sizeof(Sprite), 1, stream);
	}

	fclose(stream);
}

void SpriteLibrary::readList(CString const& dmod_path)
{
	FILE *stream;

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s%s"), (LPCTSTR)dmod_path, _T("library.dat"));

	if ((stream = _tfopen(filename, _T("rb"))) == nullptr) //not having a sprite library is not a problem - we just return.
		return;

	int len;
	fread(&len, sizeof(int), 1, stream);
	
	for (int i = 0; i < len; i++)
	{
		char name[MAX_SPRITE_NAME_LENGTH];
		Sprite s;
		
		fread(name, sizeof(char), MAX_SPRITE_NAME_LENGTH, stream);
		fread(&s, sizeof(Sprite), 1, stream);

		TCHAR tName[MAX_SPRITE_NAME_LENGTH];
		#ifdef UNICODE
		MultiByteToWideChar(CP_ACP, 0, name, -1, tName, MAX_SPRITE_NAME_LENGTH);
		#else
		strcpy(tName, name);
		#endif

		library[tName] = s;
	}
	
	fclose(stream);
}
