#pragma once
#include "Globals.h"
#include "HardTileSelector.h"
#include "SpriteSelector.h"
#include "TileSelector.h"
#include "SpriteLibrary.h"
#include "Structs.h"
#include "Sprite.h"
#include "SpriteRef.h"
#include "sequence.h"
#include "Undo.h"

#define MIN_MAP_X (0)
#define MIN_MAP_Y (0)
#define MAX_MAP_X (MAP_COLUMNS * (SCREEN_WIDTH + screen_gap) - screen_gap)
#define MAX_MAP_Y (MAP_ROWS * (SCREEN_HEIGHT + screen_gap) - screen_gap)

#define SCREEN_WIDTH_GAP    (SCREEN_WIDTH + screen_gap)
#define SCREEN_HEIGHT_GAP   (SCREEN_HEIGHT + screen_gap)

#define MIN_MAP_X_EXTENDED (MIN_MAP_X - 1 * SCREEN_WIDTH_GAP)
#define MIN_MAP_Y_EXTENDED (MIN_MAP_Y - 1 * SCREEN_HEIGHT_GAP)
#define MAX_MAP_X_EXTENDED (MAX_MAP_X + 1 * SCREEN_WIDTH_GAP)
#define MAX_MAP_Y_EXTENDED (MAX_MAP_Y + 1 * SCREEN_HEIGHT_GAP)

class Minimap;
class Screen;
class UndoStack;
struct UNDO_TILES_CHANGE;
class Sequence;
class ImportMap;

class Map
{
public:
	Map(CString dmod_path);
	~Map();

	// various user commands
	void cmdEditCopy();
	bool canCmdEditCopy();
	void cmdEditCut();
	bool canCmdEditCut();
	void cmdEditPaste();
	bool canCmdEditPaste();
	void cmdEditUndo();
	bool canCmdEditUndo();
	void cmdEditRedo();
	bool canCmdEditRedo();

	// editor actions
	// NOTE: these return true if action was performed and pushed to the undo buffer, false otherwise
	bool tryDoMultispritePickup(bool pickupCopy);
	bool tryDoMultispriteClearSelection();
	bool tryDoMultispriteDeleteSelection();
	bool tryDoMultispritePlaceMouseSelection();
	bool tryDoSpritePickup(bool removeOld = true);

	void drawScreenAt(int screen_index, int x_total_offset, int y_total_offset, RECT clip_rect);
	int drawScreens();
	int placeSprite(int x, int y, bool stamp);

    // clamps rectangle view coordinates to map min/max coordinates
    // returns false if the view rect is completely off screen
    bool bindViewRect(RECT & view_rect) const;

    // clamps rectangle map coordinates to map min/max coordinates
    // returns false if the view rect is completely off screen
    bool bindMapRect(RECT & map_rect) const;

    // clamps point view coordinates to map min/max coordinates
    // returns false if the point is off screen
    bool bindViewPoint(POINT & view_point) const;

    // clamps point map coordinates to map min/max coordinates
    // returns false if the point is off screen
    bool bindMapPoint(POINT & map_point) const;

	RECT getWindowScreenRectRadius(int screen_idx, int radius) const;
	bool checkSpriteOffScreenRadius(RECT sprite_bounds, int screen_idx, int radius);

    Sprite* getHoverSprite();
	void updateMapPosition(int x, int y);
	void updateHoverPosition(int x, int y);
	void cursorToNextSprite(bool previous);
	void finishLoading();

	bool tryDoScreenDelete(int screenNum);
	bool tryDoScreenCreate(int screenNum);
	bool tryDoScreenPaste(int screenNum, Screen* screenSource, int midiNum, bool isIndoor);
	bool tryDoScreenPasteTemp(int screenNum);
	bool tryDoScreenCopyTemp(int screenNum);
	void screenTempClear();
	
	void changeScreenProperties(int screenNum, const TCHAR* script, int midiNum, bool isIndoor);
	void changeSpriteProperties(SpriteRef spriteRef, Sprite new_sprite, Sprite old_sprite);
	void setVision(int new_vision);
	void loadMouseSprite(const Sprite* sprite, int screen_num);
	void setMouseSprite(const Sprite* sprite);
	Sprite* getMouseSprite() { return mouseSprite; }
	int screenFocusIndex;
	bool isScreenFocusMode;

	void placeTiles(int first_x_tile, int first_y_tile, int width, int height, 
					 TILEDATA tile_set[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2]);

	void trimSprite(RECT trim);

	void screenshot(const TCHAR* filename);
    void screenshotV2(const CString& filename, SpriteRenderOptions options);

	void drawScreen(int my_screen);
	
	// IO functions
	void open_dmod();
	void parse_dink_ini();
	void check_dirty_dink_ini_quit();
	void parse_dink_dat();
	void parse_map_dat(int screen_order[NUM_SCREENS]);
	void parse_hard_dat();
	void read_tile_bmps();

	void backup_dmod_old();
	
	void save_dmod();
	void save_dmod_at(CString const& basePath);

	void save_dink_dat(CString const& filename);
	void save_map_dat(CString const& filename);
	void save_dink_ini(CString const& filename);
	void save_hard_dat(CString const& filename);

	int deallocate_sequences();
	int deallocate_map();
	int release_tile_bmps();
	// end IO functions

	Screen* screen[NUM_SCREENS+1];		//RW:+1 because will be used as 1->NUM_SCREENS.
	HardTileSelector hard_tile_selector;
	SpriteSelector sprite_selector;
	TileSelector tile_selector;
	SpriteLibrary sprite_library;
	UndoStack* undo_buffer;

	Sequence* sequence[MAX_SEQUENCES+1];	//RW:+1 because will be used as 1->MAX_SEQUENCES.

	int midi_num[NUM_SCREENS];
	int indoor[NUM_SCREENS];
	bool miniupdated[NUM_SCREENS];

	// coordinates of window in relation to position on map
	RECT window;
	int window_width;
	int window_height;
	int cur_vision;
	int cur_screen;

	CString dmod_path;	// pathname to dmod directory
	CString import_dmod_path;

	Minimap* minimap;

	int hoverMouseX, hoverMouseY;
	int hoverScreen, hoverRelX, hoverRelY;	// x and y coordinates of mouse in relation to current screen
	int hoverScreenOriginX, hoverScreenOriginY;
	int hoverTileX, hoverTileY; // x and y tile coordinates in the screen
	TILEDATA hoverTileData;
	int hoverScreenX, hoverScreenY;	// gives the row and column of the screen
	int screenOffsetX, screenOffsetY;	// how many pixels off the upper left screen is offset
	int hoverSpriteNum;
	int findSpriteNum;
	int findSpriteLastMouseX, findSpriteLastMouseY;
	RECT screens_displayed;

	bool screenmatch;
	EditorState editor_state;
	EditorSubmode screenMode;
	EditorState hardTileSelectorReturnState;
	Sprite* mouseSprite;

	ImportMap* import_map;

private:

    int getHoverSpriteNum();
	// calculate possible screens to draw

	int screenTempNum;
	int screenTempMidiNum;
	bool screenTempIsInside;

public:
    void generateSpriteReport();
    
	// Toggles the selected state of one sprite in multi-selection mode. Returns true if selection was performed
	bool tryDoMultispriteSelectToggle();

	// Deselects a selected sprite. Return value tells whether something was deselected
	bool tryDoMultispriteDeselect(SpriteRef spriteRef);

	// Tells whether a specific sprite on a specific screen is selected
	bool isSelectedMultisprite(SpriteRef spriteRef);

	std::vector<SpriteRef> selectedSprites;
	std::vector<MultiMouseSprite> mouseMultiSprites;

	bool rect_select_active;
	bool rect_select_exclusive;
	RECT selection_rect;

	//The lines newly added to dink.ini
	std::vector<std::string> dinkIniLines;
};
