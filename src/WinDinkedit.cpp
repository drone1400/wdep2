// WinDinkedit.cpp : Defines the class behaviors for the application.
#include "stdafx.h"
#include "WinDinkedit.h"
#include "MainFrm.h"
#include "WinDinkeditDoc.h"
#include "LeftView.h"
#include "Globals.h"
#include "Map.h"
#include "OpenDmod.h"
#include "Common.h"
#include "CommonFile.h"
#include "NewDmodDialog.h"
#include "CustomCCommandLineInfo.h"
#include "PathUtil.h"
#include "SimpleLog.h"
#include "StackWalker/StackHelper.h"

//Switch us from classic style to some new visual style (comctl32.dll v6)
#pragma comment(linker, "\"/manifestdependency:type='win32' \
					name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
					processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

CWinDinkeditApp* dinkedit_app;


BEGIN_MESSAGE_MAP(CWinDinkeditApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_NEW_DMOD, OnNewDmod)

	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinDinkeditApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinDinkeditApp::OnFileOpen)	
END_MESSAGE_MAP()


CWinDinkeditApp::CWinDinkeditApp()
{
}


//The one and only CWinDinkeditApp object
CWinDinkeditApp theApp;


BOOL CWinDinkeditApp::InitInstance()  //this is basically our main() function
{
	// ---------------------------------------------------------------------------------------------------------------
	// ---- initialize application path and logging ------------------------------------------------------------------
	// reading app path first since it is used in logging!
	TCHAR app_path_buff[MAX_PATH];
	// NOTE: drone1400 - using GetModuleFileName instead of GetCurrentDirectory to read WDE's directory
	// otherwise this can lead to the wrong directory if say for example launching WDE from another process
	// get the path to the WDE directory
	//if (GetCurrentDirectory(MAX_PATH, app_path) == 0)	
	if (GetModuleFileName(nullptr, app_path_buff, MAX_PATH) == 0)
		return false; // can not continue if we can't even read application base path...
	
	WDE_path.SetString(app_path_buff);
	Path_GetDirectoryOf(WDE_path,WDE_path);

	// initialize log in WDE path
	Log_Initialize(WDE_path);

	Log_Info("WDE Path initialized in...");
	Log_Info(WDE_path,LOG_FORMAT_CONTINUE);
	// ---------------------------------------------------------------------------------------------------------------

	
	// ---------------------------------------------------------------------------------------------------------------
	// ---- initialize crash logger ----------------------------------------------------------------------------------
	InitializeUnhandledExceptionFilter();
	// ---------------------------------------------------------------------------------------------------------------
	
	// ---------------------------------------------------------------------------------------------------------------
	// ---- initialize app stuff -------------------------------------------------------------------------------------
	setlocale(LC_ALL, "");
	INITCOMMONCONTROLSEX initControls;
	initControls.dwSize=sizeof(initControls);
	initControls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&initControls);
	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

// drone1400: Enable3dControls seems to be deprecated now...
// #ifdef _AFXDLL
// 	Enable3dControls();			// Call this when using MFC in a shared DLL
// #else
// 	Enable3dControlsStatic();	// Call this when linking to MFC statically
// #endif
	// ---------------------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------------------
	// ---- initialize SDL -------------------------------------------------------------------------------------------
	//This is here because we define SDL_MAIN_HANDLED. Note that on Windows this is actually optional (at least in SDL 2.24), but we do it anyway for good measure.
	SDL_SetMainReady();

#ifdef _DEBUG
	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);
#endif

	if (SDL_Init(SDL_INIT_VIDEO) != 0) //video implies events
	{
		CString fatalError;
		const char* sdl_err = SDL_GetError();
		if (!sdl_err || !*sdl_err)
			sdl_err = "<unknown error>";
		fatalError.Format(_T("Failed to initialize SDL graphics:\n%hs"), sdl_err);
		Log_Critical(fatalError);
		MB_ShowGeneric(fatalError,_T("WDEP2 - FATAL ERROR!"), MB_OK | MB_ICONERROR);
		return false;
	}
	
	//MFC is the one to process Windows events, SDL should take its dirty paws off of them (otherwise the two get into infinite mutual recursion)
	SDL_SetHint(SDL_HINT_WINDOWS_ENABLE_MESSAGELOOP, "0");

	//TODO: if we ever start using a version of SDL that doesn't unconditionally eat SC_KEYMENU, then we should set the hint for that here:
	//  SDL_SetHint(SDL_HINT_WINDOWS_DISABLE_MENU_MNEMONICS, "0");
	//Until then, we use custom-patched version of SDL.

	// use this to allow screensaver/dimming to work when the editor is minimized
	SDL_EnableScreenSaver();

	// ---------------------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------------------
	// ---- initialize configuration data-----------------------------------------------------------------------------

	//read the WDE+2 config file
	ReadWDEIni();

	// Change the registry key under which our settings are stored.
	SetRegistryKey(_T("WinDinkeditPlus2"));

	LoadStdProfileSettings(6);  // Load standard INI file options (including MRU (most recently used files))

	// ---------------------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------
	// Register the application's document templates. Document templates serve as the connection between documents, frame windows and views.
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CWinDinkeditDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CLeftView));
	AddDocTemplate(pDocTemplate);
	// ---------------------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------------------
	// ---- initialize command line data -----------------------------------------------------------------------------

	// Parse command line for standard shell commands, DDE, file open
	CustomCCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	bool open_file = false;
	CString open_file_path(_T(""));
	CString tempFilePath;
	
	// check if a normal file open parameter was passed...
	if (cmdInfo.GetNormalizedOpenFileName(tempFilePath))
	{
		open_file = true;
		open_file_path.SetString(tempFilePath);
	}

	// check if a dmod path was passed with dink's -game argument
	// this overrides a file open parameter
	if (cmdInfo.GetNormalizedGameArg(tempFilePath))
	{
		open_file = true;
		open_file_path.SetString(tempFilePath);
	}

	// check if dink_core_path_override is supplied via --refdir argument
	if (cmdInfo.GetNormalizedValidRefDirArg(tempFilePath))
	{
		is_dink_core_path_override_enabled = true;
		dink_core_path_override.SetString(tempFilePath);
		Log_Info(_T("DINK CORE - override will be used!"));
	}

	// ---------------------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------
	bool dinkInstallFound = false;
	if ((is_dink_core_path_override_enabled && IsDmodDir(dink_core_path_override)) ||
		IsDmodDir(dink_core_path))
	{
		Log_Info(_T("DINK CORE - installation was found!"));
		GuessMissingDinkInstallationPaths(dink_exe_path, dink_core_path, dink_skeleton_path, dink_dmods_path);
		dinkInstallFound = true;
	}
	else
	{
		Log_Warning(_T("DINK CORE - installation was not found! Attempting to auto-detect..."));
		
		CString detectedDinkExe;
		if (DetectDinkInstallation(detectedDinkExe))
		{
			Log_Info(_T("DINK CORE - installation was auto detected!"));
			Log_Info(detectedDinkExe);
			dink_exe_path = detectedDinkExe;
			GuessMissingDinkInstallationPaths(dink_exe_path, dink_core_path, dink_skeleton_path, dink_dmods_path);
			dinkInstallFound = true;
		}
	}

	if (!dinkInstallFound)
	{
		Log_Warning(_T("DINK CORE - No installation could be detected!!!"));
		MB_ShowWarning(_T("Could not detect where Dink is installed (tried looking for FreeDink, Dink HD, and classic Dink).\n\nGo to File -> Dink Installation to configure it manually."));
	}

	// ---------------------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------------------
	// ---- Wrapping things up... ------------------------------------------------------------------------------------

	// Dispatch commands specified on the command line
	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;
	if (!ProcessShellCommand(cmdInfo))
	{
		Log_Critical(_T("Fatal error initializing first time document!"));
		return FALSE;
	}

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	
	if (open_file)
	{
		Log_Info(_T("DMOD - Opening file automatically..."));
		Log_Info(open_file_path, LOG_FORMAT_CONTINUE);
		OpenDocumentFile(open_file_path);
	}

	
	
	return TRUE;
}


// CAboutDlg used for About dialog
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	enum { IDD = IDD_ABOUTBOX };

protected:
	void DoDataExchange(CDataExchange* pDX) override;    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileRund();
	afx_msg void OnFileRundinkedit();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// create the about box
void CWinDinkeditApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


void CWinDinkeditApp::OnFileOpen()
{
	TCHAR dmod_name[MAX_PATH];

	// create the open dmod dialog box
	OpenDmod newdlg(nullptr);

	// give the dialog a pointer to the dmod_name buffer
	newdlg.setDmodNamePtr(dmod_name);

	int retcode = newdlg.DoModal();

	if (retcode == IDOK)
	{
		OpenDocumentFile(dmod_name);
	}
}

CDocument* CWinDinkeditApp::OpenDocumentFile(LPCTSTR file_name) 
{
	CString tiles_path;
	tiles_path.SetString(dink_core_path);
	tiles_path.Append(_T("\\tiles"));
	if (!FolderExists(dink_core_path) || !FolderExists(tiles_path))
	{
		MB_ShowError(_T("The Dink core module appears to be incorrect or missing.\n\nPlease configure it under File -> Dink Installation."));
		return nullptr;
	}

	CString dmod_path;
	
	if (!_tcschr(file_name, _T('\\'))) {
		//RW: if the file name is not a full path
		//RW: OpenDocumentFile should get the whole path or else!!
		dmod_path.SetString(dink_dmods_path);
		dmod_path.AppendChar(_T('\\'));
		dmod_path.Append(file_name);
	} else {
		dmod_path.SetString(file_name);
	}

	if (!FolderExists(dmod_path))
	{
		MB_ShowError(_T("DMOD folder could not be found at path:\n"), dmod_path);
		return nullptr;
	}

	//RW: These clear away the crap that gets drawn over the toolbar and stuff
	mainWnd->RedrawWindow();
	mainWnd->m_wndToolBar.RedrawWindow();

	CDocument* doc = mainWnd->GetActiveDocument();
	if (doc == nullptr || doc->GetPathName() != dmod_path)
		return CWinApp::OpenDocumentFile(dmod_path); //open something new
	else if (current_map == nullptr)
	{
		doc->OnOpenDocument(dmod_path);	 //open what we just closed
		return doc;
	}
	return nullptr;
}


void CWinDinkeditApp::OnNewDmod() 
{
	TCHAR dmod_name[MAX_PATH];
	
	if (dink_skeleton_path.IsEmpty())
	{
		MB_ShowError(_T("Before being able to create a new dmod, you must configure the location of the skeleton dmod!\n\nYou can do that under File -> Dink Installation."));
		return;
	}

	NewDmodDialog newdlg(nullptr);

	// give the dialog a pointer to the dmod_name buffer
	newdlg.setDmodNamePtr(dmod_name);

	int retcode = newdlg.DoModal();

	if (retcode == IDOK)
	{
		OpenDocumentFile(dmod_name);
	}
}

int CWinDinkeditApp::ExitInstance()
{
	// save the WDE+2 config file
	WriteWDEIni();

	SDL_Quit();

	return CWinAppEx::ExitInstance();
}
