## WinDinkedit Plus 2

WinDinkedit Plus 2 is a level editor for the old Zelda-like game, Dink Smallwood.

### History

In the beginning, there was Dinkedit. Made together with Dink, it was a fullscreen program, just like the game itself. Tho a bit janky to use, it got the job done.

Then, in the name of ease of use, Gary Hertel (Nexis) and WC created WinDinkedit. They released it in 2001, and made fixes until 2003. They also credit a number of other people who helped resolve bugs.

Over the course of 2006 and 2007, ChickenFist (aka GameHampe) came up with new improvements, and published them as WinDinkedit Plus.

And then RW stumbled onto the scene. (Hey, that's me!)

I had been playing Dink for a few years, and was messing around in WDE+, trying to see if I can make a dmod of my own. Never had the creativity to do it, but I did notice certain annoyances in WDE+, and they got under my skin. I just couldn't let them go.

So in mid-2011, at the age of 16, armed with some C and C# knowledge, I downloaded the WDE+ source code, and ventured forth into the scary lands of C++. And I was defeated – the code was too complicated, the syntax too obtuse. Heck, the damned thing didn't even compile at first! I gave up after a month.

But the thought wouldn't let me rest. After a few months, I started again – and persevered. I fixed a bunch of problems, I made stuff look and work better, and I even added new features. I also inadvertently introduced new bugs (what a surprise). I released WinDinkedit Plus 2 in December 2011, and some updates in early 2012. But unlike everyone before me, I did not release the source code for WDE+2. The main reason was that I knew some of the code I wrote sucked, and I was too shy and embarrassed.

Over the years that followed, I remembered Dink and WDE a good number of times. I even tweaked the code a bit in 2016. I also had ideas for a new editor, but never had the motivation to work on it.

Then a pleasant surprise arrived in 2022: *evil V* (drone1400) sent me an email, asking if I still had the WDE+2 source code. Of course I had it. It was time to dust it off, and release it to the world. But I couldn't resist playing around with it some more, and so version 2.5 was born.

### License & Copyright

None of the original authors stated a license explicitly. For all intents and purposes, everything is still _all rights reserved_. That said, everyone released their version publicly, for the good of the Dink community. Therefore, in practice, you should also feel free to improve it.

It should go without saying that the code comes with no warranties.

Regarding the copyright, here is what I could piece together. Note that I have a somewhat-informed hunch that traces of original Dink code are also present. Therefore:

Copyright © 1997–1999 Robinson Technologies  
Copyright © 2001–2003 Gary Hertel and WC  
Copyright © 2006–2007 ChickenFist  
Copyright © 2011, 2012, 2016, 2022 Rudolf-Walter Kiss-Szakács (RW)

### The code

You want to make changes? Squash bugs? Or perhaps add even more? Your funeral.

First of all, you'll need to install a copy of Visual Studio, with the C++ components enabled, including MFC support. Yes, that's the big Visual Studio, not VSCode.

Everything is known to compile with VS 2022. If you have a much newer edition, it *might* work – or it might not. If you want to use an older VS, you will probably need to revert parts of the craziness I added in 2016; sorry about that.

The WDE user interface is made with MFC. Drawing all the Dink graphics is handled by SDL, version 2.

If you've never used MFC before, here are a couple of tips. All of the user interface is defined in the `WinDinkedit.rc` file, which you should open in the Resource View in VS. From there you'll be able to open all of WDE's dialogs and menus and whatnot in a nice graphical editor. The class wizard, opened with Ctrl+Shift+X, is your friend if you want to add new MFC-related functionality. Note that parts of MFC use a custom override system instead of virtual methods (MFC is *old*!), and the class wizard can help show you the way. Other than that: there are plenty of MFC resources on the internet.

The rest is "just C++". I'm sure you'll figure it out.

